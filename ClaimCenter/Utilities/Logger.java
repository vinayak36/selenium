package Utilities;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Time;
import java.util.Date;
import javax.imageio.ImageIO;

import resources.Utilities.LoggerHelper;

/**
 * Implements the Logger
 * 
 * @since 2012/03/08
 * @author Vinayak Hegde
 */
/**
 * @author Vinayak Hegde
 *
 */
public class Logger extends LoggerHelper {

	private String Summary_Path = "";
	private String logReportName = "";
	private String logFilePath = "";
	private String new_SummaryFile = "";
	public int screenshot_count = 0;
	public String screenshot_path = "";
	Date start = null;
	Date end = null;
	public String jpgFileName = "ScreenShot";
	private String logLocation;
	private String tcID;
	private String envURL;
	private String msgTypesToLog;
	private String srnshtLevel;
	private String nextLine = System.getProperty("line.separator");

	/**
	 * Instantiates the Logger with log file location and test environment URL
	 * @param logFolder
	 * 			- Log File Location
	 * @param env
	 * 			- ClaimCenter URL
	 * @param messageTypestoLog
	 * 			- Types of Message to log during current execution
	 * @param screenShotCaptureLevel
	 * 			- Events when the Screen shots should be captured
	 */
	public Logger(String logFolder, String env, String messageTypestoLog, String screenShotCaptureLevel) {
		logLocation = logFolder;
		envURL = env;
		msgTypesToLog = messageTypestoLog;
		srnshtLevel = screenShotCaptureLevel;
	}

	/**
	 * Sets the Test Case ID for logging purpose
	 * 
	 * @param testCaseID
	 *            - Test Case ID
	 * @since 2012/03/08
	 */
	public void setTCId(String testCaseID) {
		tcID = testCaseID;
		screenshot_count = 0;
	}
	
	/**
	 * Saves the Summary file with Header Information
	 * 
	 * @since 2012/03/08
	 */
	public void summaryHeader() {
		Summary_Path = logFilePath + "\\SummaryLog";
		String date = CCUtility
				.current_Date_SpecificFormat("yyyy.MMMMM.dd kk:mm");
		String newFileFormat = date.substring(5);
		newFileFormat = newFileFormat.substring(0, newFileFormat.indexOf('.'))
				+ newFileFormat.substring(newFileFormat.indexOf('.') + 1);
		newFileFormat = newFileFormat.replaceFirst(" ", "_");
		newFileFormat = newFileFormat.replaceFirst(":", "_");
		new_SummaryFile = Summary_Path + newFileFormat + ".txt";
		log_SummaryMessage("============================================================");
		log_SummaryMessage("RFT - Summary Statistics" + "\t");
		log_SummaryMessage("Run Date:      "
				+ CCUtility.current_Date_SpecificFormat("dd.MMM.yyyy"));
		start = new Date();
		String starttime[] = start.toString().split(" ");
		log_SummaryMessage("Start Time:    " + starttime[3]);
	}

	/**
	 * Logs the Summary footer in Summary log
	 * 
	 * @param tcPassed
	 *            - no of test cases passed
	 * @param tcFailed
	 *            - no of test cases failed
	 * @param tcExecuted
	 *            - no of test cases executed
	 * @since 2012/03/08
	 */
	public void summary_Footer(int tcPassed, int tcFailed, int tcExecuted) {
		end = new Date();
		String endtime[] = end.toString().split(" ");
		log_SummaryMessage("End Time:      " + endtime[3]);
		log_SummaryMessage("Elapsed time:  " + elapsedTime(start, end));
		log_SummaryMessage("Total Test Cases Executed: " + tcExecuted);
		log_SummaryMessage("                   Passed: " + tcPassed);
		log_SummaryMessage("                   Failed: " + tcFailed);
		log_SummaryMessage("============================================================");
	}

	/**
	 * Logs the specified Summary message in the Summary log
	 * 
	 * @param message
	 *            - Message to Log
	 * @since 2012/03/08
	 */
	public void log_SummaryMessage(String message) {
		File resultFile = new File(new_SummaryFile);
		BufferedWriter output = null;
		try {
			output = new BufferedWriter(new FileWriter(resultFile, true));
			output.write(message);
			output.write(System.getProperty("line.separator"));

		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (output != null)
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

	/**
	 * Creates the log file with name based on current TimeStamp. Also creates
	 * the folder structure for Logs and Screen shots
	 * 
	 * @since 2012/03/08
	 */
	public void createLogFile() {
		String timeStamp = CCUtility.current_Date_SpecificFormat(
				"yyyy.MM.dd kk:mm").trim();
		timeStamp = timeStamp.replaceAll("[.]", "-").replaceAll(":", "-")
				.replaceAll(" ", "-");
		logFilePath = new File(logLocation, timeStamp).toString();
		makeDir(logLocation);
		makeDir(logFilePath);
		screenshot_path = logFilePath + "\\ScreenShots";
		makeDir(screenshot_path);
		String title = "ClaimCenter Test Automation - Detail Log Report ";
		String date = CCUtility.current_Date_SpecificFormat("MMMMM.dd hh:mm");
		logReportName = date.replaceAll(" ", "-").replaceAll("[.]", "-").replaceAll(":", "-")+ ".txt";
		String msg = "\t\t" + title + "\t" + "("+ CCUtility.current_Date_SpecificFormat("yyyy.MMMMM.dd kk:mm")+ ")";
		
		log_Message(msg, MessageType.NONE);
		log_Message(nextLine, MessageType.NONE);
		log_Message("Environment : " + envURL, MessageType.NONE);
		log_Message(nextLine, MessageType.NONE);
		msg = "------------------------------------------------------------------------------------------------------------------------";
		log_Message(msg, MessageType.NONE);
		log_Message(nextLine, MessageType.NONE);
	}

	/**
	 * Logs the specified message with specified message type in the log file
	 * 
	 * @param message
	 *            - Message to log
	 * @param msgType
	 *            - Type of Message (like INFO, ERROR, PASS, FAIL etc)
	 * @since 2012/03/08
	 */
	public void log_Message(String message, MessageType msgType) {
		File resultFile = new File(logFilePath, logReportName);
		BufferedWriter output = null;

		if (message == null) { message = ""; }

		try {
			output = new BufferedWriter(new FileWriter(resultFile, true));
			if (msgType == MessageType.NONE) {
				output.write(message);
				output.write(nextLine);
			} else {
				if ( msgTypesToLog.trim().toUpperCase().contains(msgType.getMessageTypeText().toUpperCase()) ||
						msgTypesToLog.trim().compareTo("ALL") == 0 ) {
					output.write(
							CCUtility.current_Date_SpecificFormat("yyyy.MM.dd kk:mm")
							+ "-"
							+ msgType.getMessageTypeText() + " : " + message);
					// RFT Message
					LogRFTMessage(message, msgType);
					output.write(nextLine);
				}
			}
			output.flush();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Logs the claim numbers created for further user
	 * 
	 * @param message
	 *            - Test Case ID
	 * @param claimNumber
	 *            - Claim Number to log
	 * @since 2012/03/08
	 */
	public void log_ClaimNumber(String testCasID, String claimNumber) {
		
		File resultFile = new File(logFilePath, "Claims-" + logReportName.replace(".txt", ".csv") );
		BufferedWriter output = null;
		try {
			output = new BufferedWriter(new FileWriter(resultFile, true));
			output.write(testCasID + "," + claimNumber);
			output.write(nextLine);
			output.flush();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	/**
	 * Logs RFT Message with specified message Type
	 * 
	 * @param message
	 *            - Message to log
	 * @param msgType
	 *            - Message Type
	 * @since 2012/03/08
	 */
	private void LogRFTMessage(String message, MessageType msgType) {
		setCurrentLogFilter(ENABLE_LOGGING);
		switch (msgType) {
		case INFO:
			logInfo(message);
			break;
		case PASS:
			logTestResult(message, true);
			break;
		case ERROR:
			logError(message);
			break;
		case FAIL:
			logError(message);
			break;
		case WARNING:
			logWarning(message);
			break;
		}
		setCurrentLogFilter(DISABLE_LOGGING);
	}

	/**
	 * Calculates the timeStamp between specified start and end time
	 * 
	 * @param startTime
	 *            - Start Time
	 * @param endTime
	 *            - end Time
	 * @return TimeStamp String
	 * @since 2012/03/08
	 */
	private String elapsedTime(Date startTime, Date endTime) {
		int hrs = 0;
		int mins = 0;
		int secs = 0;

		float elapsed = (endTime.getTime() - startTime.getTime()) / 1000;
		String elapsedTime = String.valueOf(elapsed);
		String parts[] = null;
		parts = elapsedTime.split("\\.");
		secs = Integer.parseInt(parts[0]);
		if (secs > 3600) {
			hrs = secs / 3600;
			secs = secs % 3600;
		}
		if (secs > 60) {
			mins = secs / 60;
			secs = secs % 60;
		}
		elapsedTime = hrs + ":" + mins + ":" + secs;
		elapsedTime = Time.valueOf(elapsedTime).toString();

		return elapsedTime;
	}

	/**
	 * Captures the screen shot and saves it in folder at log location
	 * 
	 * @since 2012/03/08
	 */
	public void captureScreen( ScreenShotEvent screenShotEvent) {
		try {
			ScreenShotEvent sEventLevel = ScreenShotEvent.getEventByText(srnshtLevel);
			if (( sEventLevel == ScreenShotEvent.ScreenChange ) &&
					( screenShotEvent == ScreenShotEvent.UserAction)) {
				//Skip the UserAction level if ScreenChange is specified
				return;
			}
			if (( sEventLevel == ScreenShotEvent.ERROR ) && ( screenShotEvent != ScreenShotEvent.ERROR)) {
				//Skip the UserAction and ScreenChange level if ERROR is specified
				return;
			}
			screenshot_count = screenshot_count + 1;
			jpgFileName = screenshot_path + "\\" + tcID + "_" + screenshot_count + ".jpg";
			BufferedImage screencapture = new Robot().createScreenCapture(
					new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
			File file = new File(jpgFileName);
			ImageIO.write(screencapture, "jpg", file);

		} catch (Exception e) {
			//swallow the exception
		}
	}

	/**
	 * Creates the specified folders
	 * 
	 * @param path
	 *            - Folder path to create
	 * @return true if folder is created or already exists
	 * @since 2012/03/08
	 */
	public boolean makeDir(String path) {
		boolean suc = true;
		File dir = new File(path);
		if (!dir.exists()) {
			suc = dir.mkdir();
		}
		return suc;
	}

	/**
	 * List of the Message Types used for logging
	 * 
	 * @since 2012/03/08
	 */
	public enum MessageType {
		ERROR, PASS, FAIL, WARNING, INFO, NONE;

		/**
		 * Convert the current Message type to String
		 * 
		 * @return the String of current message Type
		 * @since 2012/03/08
		 */
		public String getMessageTypeText() {
			String messageType = "";
			switch (this) {
			case INFO:
				messageType = "INFO";
				break;
			case PASS:
				messageType = "PASS";
				break;
			case FAIL:
				messageType = "FAIL";
				break;
			case WARNING:
				messageType = "WARN";
				break;
			case ERROR:
				messageType = "ERROR";
				break;
			case NONE:
				messageType = "";
			}
			return messageType;
		}
	}

	/**
	 * Enumerator for Screen shots to capture
	 * The options are UserAction, ScreenChange, ERROR
	 * @author Vinayak Hegde
	 *
	 */
	public enum ScreenShotEvent {
		UserAction, ScreenChange, ERROR;
		
		private String getText() {
			String eventText = "";
			switch (this) {
			case UserAction:
				eventText = "UserAction";
				break;
			case ScreenChange:
				eventText = "ScreenChange";
				break;
			case ERROR:
				eventText = "ERROR";
				break;
			}
			return eventText;
		}

		private static ScreenShotEvent getEventByText(String eventText) {
			ScreenShotEvent event = UserAction;
			
			if ( eventText.contains("UserAction")) {
				event = UserAction;
			}
			else if (eventText.contains("ScreenChange") ) {
				event = ScreenChange;
			}
			else if (eventText.contains("ERROR")){
				event = ERROR;
			}
			return event;		
		}
		
	}
}
