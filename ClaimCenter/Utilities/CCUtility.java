package Utilities;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import resources.Utilities.CCUtilityHelper;

import com.rational.test.ft.ObjectNotFoundException;
import com.rational.test.ft.object.interfaces.BrowserTestObject;
import com.rational.test.ft.object.interfaces.TestObject;

/**
 * Implements the reusable methods
 * 
 * @since 2012/03/08
 * @author Vinayak Hegde
 */
public class CCUtility extends CCUtilityHelper {

	/**
	 * Close the Browser refereed by specified BrowserTestObject
	 * 
	 * @param browserObject
	 *            - Browser Test Object
	 * @since 2012/03/08
	 */
	public static void closeBrowser(BrowserTestObject browserObject) {
		browserObject.getProcess().kill();
		sleep(1);
	}// end of CloseBrowser

	/**
	 * Waits till the specified test object appears in the containerObject like,
	 * it waits till a row added in the table(container)
	 * 
	 * @param containerObject
	 *            - container test object
	 * @param textObjecttoWait
	 *            - Test object on which we need to wait
	 * @param timeoutSecs
	 *            - Maximum time in seconds to wait
	 * @throws ObjectNotFoundException
	 *             - Object not found during specified timeout
	 * @since 2012/03/08
	 */
	public static void waitForTestObject(TestObject containerObject, String textObjecttoWait, int timeoutSecs)
	throws ObjectNotFoundException {
		
		int timeelapsed = 0;
		while (true) {
			sleep(1);
			TestObject[] tObjs = containerObject.find(atDescendant(".text", textObjecttoWait));
			if (tObjs.length > 0) {
				// Object Found
				break;
			}
			timeoutSecs++;
			if (timeelapsed > timeoutSecs) {
				throw new ObjectNotFoundException("TimeOut: " + textObjecttoWait + " not Found");
			}
		}
	}

	/**
	 * Get the current machine name
	 * 
	 * @return Machine Name
	 * @since 2012/03/08
	 */
	public static String getMachineName() {
		String machineName = "";
		try {
			machineName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
		}
		return machineName;
	}

	/**
	 * Get the current Date and Time in specified format
	 * 
	 * @param format
	 *            - Formant String
	 * @return - Current Date and Time
	 * @since 2012/03/08
	 */
	public static String current_Date_SpecificFormat(String format) {
		java.util.Date now = new java.util.Date();
		// SimpleDateFormat f = new SimpleDateFormat("yyyy.MMMMM.dd kk:mm");
		SimpleDateFormat f = new SimpleDateFormat(format);
		String myDateTime = f.format(now);
		return myDateTime;
	}

	/**
	 * Convert the specified dateTime string to specified expected format
	 * 
	 * @param dateTime
	 *            - Date and Time
	 * @param currentFormat
	 *            - Current Format
	 * @param expectedFormat
	 *            - Expected Format
	 * @return - DateTime converted to expected format
	 * @since 2012/03/08
	 */
	public static String convertDateFormat(String dateTime,
			String currentFormat, String expectedFormat) {
		String dateTimeCC = "";
		try {
			// java.util.Date dateFormat = new
			// SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(dateTime);
			java.util.Date dateFormat = new SimpleDateFormat(currentFormat)
					.parse(dateTime);
			// String strCCDateFormat = "MM-dd-yyyy hh:mm a";
			dateTimeCC = new SimpleDateFormat(expectedFormat)
					.format(dateFormat);
		} catch (ParseException e) {
			// Swallow the Exception
			e.printStackTrace();
		}
		return dateTimeCC;
	}
}
