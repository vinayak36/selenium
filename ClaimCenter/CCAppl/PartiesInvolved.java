package CCAppl;
import java.util.Hashtable;

import resources.CCAppl.PartiesInvolvedHelper;
import TestData.ClaimCenterData;
import Utilities.Logger;
import Utilities.Logger.MessageType;

import com.rational.test.ft.object.interfaces.TestObject;
import com.rational.test.ft.object.interfaces.TextGuiTestObject;
 
 /**
	 * Script Name   : <b>PartiesInvolved</b>
	 * Generated     : <b>Sep 13, 2012 2:35:25 AM</b>
	 * Description   : Functional Test Script
	 * Original Host : WinNT Version 5.1  Build 2600 (S)
	 * 
	 * @since  2012/09/13
	 * @author Vinayak Hegde
	 */
	 
public class PartiesInvolved extends PartiesInvolvedHelper
{	
	private UIObjectsAPI ccRootObject;
	private Logger logger;
	
	
	/**
	 * Instantiates the Class and assigns root object and logger
	 * 
	 * @param rootObject
	 *            - root Test object
	 * @param log
	 *            - Logger instance to log the messages
	 * @since 2012/09/13
	 */
	public PartiesInvolved(UIObjectsAPI rootObjects,Logger log){
		ccRootObject=rootObjects;
		logger=log;
	}
	
	/**
	 * Creates New Contact 
	 * 
	 * @param contactIndex
	 *            - Index of the Contact data to use for searching
	 * @return return true if contact is Created
	 * @since 2012/09/13
	 * @author: Vinayak Hegde
	 */
	public void createNewContact(String contactIndex) throws Exception {

		logger.log_Message("Creating Contact in PartiesInvolved", MessageType.INFO);

		//get data
		Hashtable<String, String> data_Contact = ClaimCenterData.getData("Contacts",contactIndex);
		
		//New Contact
		//New Contact
		ccRootObject.button("Click", "NewContactButton");
		
	
		String[] contactTypeMenuList = new String[] {
				data_Contact.get("ContactType"),
				data_Contact.get("ContactSubType")};
		
		ccRootObject.menu("Click", "ContactTypeList", contactTypeMenuList);
		System.out.print(data_Contact.get("Mobile"));
		
		
		//Add Roles
		addRoleInfo(data_Contact.get("Roles"));		
		//
		AddressBook abObject = new AddressBook(ccRootObject,logger)	;
		
		abObject.fillInputValues(data_Contact);
		
		
		//Update
		ccRootObject.button("Click", "UpdateButton");
		//check if Contact is created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating new Contact '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}	
		
		logger.log_Message("New Contact created in PartiesInvolved", MessageType.INFO);
		
		
	}
	
	/**
	 * Method to  add an existng contact
	 * @param contactIndex
	 * 			-  - Index of the Contact data to use for searching
	 * @author Vinayak
	 * */
	public void addExistingContact(String contactIndex)throws Exception{
		logger.log_Message("Adding Existing Contact in PartiesInvolved", MessageType.INFO);

		//get data
		Hashtable<String, String> data_Contact = ClaimCenterData.getData("Contacts", contactIndex);
		ccRootObject.button("Click", "AddExistingContactBtn");
		
		//Search the contact
		new AddressBook(ccRootObject,logger).searchSelectContact(contactIndex);
		
		//add role info
		addRoleInfo(data_Contact.get("Roles"));
		
		//Update
		ccRootObject.button("Click", "UpdateButton");
		//check if Contact is created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while adding  an existing contact'" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}	
		
		logger.log_Message("Existing Contact added in PartiesInvolved", MessageType.INFO);
		
		
	}
	
	/**
	 * Verify a Contact
	 * @param contactIndex
	 * 			-  - Index of the Contact data to use for searching
	 * @author Vinayak
	 * */
	
	public Boolean verifyContactExists(String contactIndex)throws Exception{
		Boolean result= false;
		
		logger.log_Message("Check if the Party exists", MessageType.INFO);
		
		//get data
		Hashtable<String, String> data_contacts = ClaimCenterData.getData( "Contacts", contactIndex);
	
		//Find ContactInfo table
		TextGuiTestObject contactInfoTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				"ClaimContactInfoTable");
		String contactName = "";
		
		if( data_contacts.get("OrganizationName").isEmpty()){					
					contactName = data_contacts.get("FirstName") + " " + data_contacts.get("LastName");
		}//Otherwise
		else{
			contactName = data_contacts.get("OrganizationName");
		}
		
		TextGuiTestObject contactNameObject =  ccRootObject.findDescendantTestObject(
				contactInfoTable, ".class", "Html.SPAN", ".text", contactName);
		
		if ( contactNameObject == null) {
			throw new Exception ("Contact  with name" + contactName + " Not found"); 
		}
		
		logger.log_Message("Party with Name " + contactName + " found", Logger.MessageType.PASS);
		
		result = true;
		
		return result;
	}
	
	
	/**
	 * Delete a contact 
	 * 
	 * @param dataindex
	 *            - Index of the contact data to use
	 * @return return true if contact is Created
	 */
	public void deleteContact(String contactIndex)
		throws Exception {
		
		logger.log_Message("Delete Contact", MessageType.INFO);
		
		Hashtable<String, String> data_contacts = ClaimCenterData.getData(
				"Contacts", contactIndex);
		
		//Find ContactInfo table
		TextGuiTestObject contactInfoTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				"ClaimContactInfoTable");
		//Contact name;
		String contactName = null;
		
		if(data_contacts.get("OrganizationName").isEmpty()){
			contactName = data_contacts.get("FirstName")+" "+data_contacts.get("LastName");
		}
		else{
			contactName=data_contacts.get("OrganizationName");
		}
		
		//Find contact  based on Name
		TextGuiTestObject contactNameObject =  ccRootObject.findDescendantTestObject(
				contactInfoTable, ".class", "Html.SPAN", ".text", data_contacts.get("LastName"));
		if ( contactNameObject == null) {
			throw new Exception ("Contact with Name " +contactName + " Not found"); 
		}
		logger.log_Message("Contact with Name " +contactName+ " found", Logger.MessageType.PASS);
		
		//Get the row object with Last name
		String rowID = contactNameObject.getProperty(".id").toString();
		TextGuiTestObject contactRowOne =  ccRootObject.findDescendantTestObject(
				contactInfoTable, ".class", "Html.TR", ".id", rowID.replace("Name","0"));
		ccRootObject.checkBoxinTable("Check", contactRowOne, null, "CheckBox");
		
		//Delete
		ccRootObject.button("Click", "ClaimContactsDeleteButton");
		
		//check if Contact is deleted successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while deleting Contact '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		
		logger.log_Message("Contact with Name " +data_contacts.get("LastName") + " deleted", Logger.MessageType.PASS);
		
	}
	
	/**
	 * Edits an Existing contact
	 * @param dataIndex
	 *            - Index of the Contact data to use for searching
	 * @since 2012/03/08
	 */
	public void editContact(String contactIndex) 
		throws Exception {
		logger.log_Message("Edit Contact",MessageType.INFO);
		
		Hashtable<String, String> data_contacts = ClaimCenterData.getData(
				"Contacts", contactIndex);
		
		//Find ContactInfo table
		TextGuiTestObject contactInfoTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				"ClaimContactInfoTable");
		
		//Contact name;
		String contactName = null;
		
		if(data_contacts.get("OrganizationName").isEmpty()){
			contactName = data_contacts.get("FirstName")+" "+data_contacts.get("LastName");
		}
		else{
			contactName=data_contacts.get("OrganizationName");
		}
		//Find contact  based on Name
		TextGuiTestObject contactNameObject =  ccRootObject.findDescendantTestObject(
				contactInfoTable, ".class", "Html.SPAN", ".text", contactName);
		if ( contactNameObject == null) {
			throw new Exception ("Contact with Name " +contactName + " Not found"); 
		}
		//Get the row object with Last name
		String rowID = contactNameObject.getProperty(".id").toString();
		TextGuiTestObject contactRowOne =  ccRootObject.findDescendantTestObject(
				contactInfoTable, ".class", "Html.TR", ".id", rowID.replace("Name","0"));
		ccRootObject.linkinTable("Click", contactRowOne, "ClaimContactName");
		
		//Edit
		ccRootObject.link("Click", "EditButton");
		
		//Add Roles
		if(!data_contacts.get("Roles").isEmpty()){
			addRoleInfo(data_contacts.get("Roles"));
		}
		//
		AddressBook abObject = new AddressBook(ccRootObject,logger)	;
		
		abObject.fillInputValues(data_contacts);
		
		
		//Update
		ccRootObject.button("Click", "UpdateButton");
		sleep(0.5);
		//check if Contact is created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating Contact '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
	}
	
	/**
	 * Method to link a contact to the address book
	 * @author Vinayak*/
	public void linkContact(String contactIndex) throws Exception{
		
		
	logger.log_Message("Link  Contact to address book", MessageType.INFO);
		
		Hashtable<String, String> data_contacts = ClaimCenterData.getData(
				"Contacts", contactIndex);
		
	
		
		//Find ContactInfo table
		TextGuiTestObject contactInfoTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				"ClaimContactInfoTable");
		
		//Contact name;
		String contactName = null;
		
		if(data_contacts.get("OrganizationName").isEmpty()){
			contactName = data_contacts.get("FirstName")+" "+data_contacts.get("LastName");
		}
		else{
			contactName=data_contacts.get("OrganizationName");
		}
		
		
		//Find contact  based on Name
		TextGuiTestObject contactNameObject =  ccRootObject.findDescendantTestObject(
				contactInfoTable, ".class", "Html.SPAN", ".text",contactName);
		if ( contactNameObject == null) {
			throw new Exception ("Contact with Name " +contactName+ " Not found"); 
		}
		logger.log_Message("Contact with Name " +contactName + " found", Logger.MessageType.PASS);
		
		//Get the row object with Last name
		String rowID = contactNameObject.getProperty(".id").toString();
		TextGuiTestObject contactRowOne =  ccRootObject.findDescendantTestObject(
				contactInfoTable, ".class", "Html.TR", ".id", rowID.replace("Name","0"));
		ccRootObject.checkBoxinTable("Check", contactRowOne, null, "CheckBox");
		
		//Get the workspace area to create Note
		TestObject workspaceArea = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "PartiesInvolvedWorkspaceDiv");
		
		ccRootObject.buttonInTable("Click", workspaceArea, "linkABtn");
		

		//check if Contact is linked successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while linking Contact '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		
		
		logger.log_Message("Contact with Name " +data_contacts.get("LastName") + " linked to address book", Logger.MessageType.PASS);
		
	}
	
	
	
	/**
	 * Method to unlink  a contact from the address book*/
	public void unlinkContact(String contactIndex) throws Exception{
		
		
	logger.log_Message("Link  Contact to address book", MessageType.INFO);
		
		Hashtable<String, String> data_contacts = ClaimCenterData.getData(
				"Contacts", contactIndex);
		
	
		
		//Find ContactInfo table
		TextGuiTestObject contactInfoTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				"ClaimContactInfoTable");

		//Contact name;
		String contactName = null;
		
		if(data_contacts.get("OrganizationName").isEmpty()){
			contactName = data_contacts.get("FirstName")+" "+data_contacts.get("LastName");
		}
		else{
			contactName=data_contacts.get("OrganizationName");
		}
		
		
		
		
		//Find contact  based on Name
		TextGuiTestObject contactNameObject =  ccRootObject.findDescendantTestObject(
				contactInfoTable, ".class", "Html.SPAN", ".text", contactName);
		if ( contactNameObject == null) {
			throw new Exception ("Contact with Name " +contactName + " Not found"); 
		}
		logger.log_Message("Contact with Name " +contactName+ " found", Logger.MessageType.PASS);
		
		//Get the row object with Last name
		String rowID = contactNameObject.getProperty(".id").toString();
		TextGuiTestObject contactRowOne =  ccRootObject.findDescendantTestObject(
				contactInfoTable, ".class", "Html.TR", ".id", rowID.replace("Name","0"));
		ccRootObject.checkBoxinTable("Check", contactRowOne, null, "CheckBox");
		
		//Get the workspace area to create Note
		TestObject workspaceArea = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "PartiesInvolvedWorkspaceDiv");
		
		ccRootObject.buttonInTable("Click", workspaceArea, "unlinkABtn");
		
		//check if Contact is deleted successfully

		//check if Contact is deleted successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while unlinking Contact '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		
		
		
		logger.log_Message("Contact with Name " +data_contacts.get("LastName") + " unlinked to address book", Logger.MessageType.PASS);
		
	}
	
	/**
	 * Verify the Name and Role of an Existing contact 
	 * 
	 * @param dataindex
	 *            - Index of the contact data to use
	 * @return return true if contact is Created
	 */
	public void viewInvolvedPartyDetails(String dataIndex) throws Exception {
		
		logger.log_Message("View Parties Involved Details", MessageType.INFO);
		
		Hashtable<String, String> data_contacts = ClaimCenterData.getData( "Contacts", dataIndex);

		String nameToSearch = "";
		if ( data_contacts.get("OrganizationName").isEmpty()) {
			nameToSearch = data_contacts.get("FirstName") + " " + data_contacts.get("LastName");  
		}
		else {
			nameToSearch = data_contacts.get("OrganizationName");
		}

		TextGuiTestObject partiesInvolvedTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "ClaimContactInfoTable");
		
		//verify Role  from the table
		TextGuiTestObject contactRoleObject =  ccRootObject.findDescendantTestObject(
				partiesInvolvedTable, ".class", "Html.SPAN", ".text", nameToSearch.trim() );
		if ( contactRoleObject == null) {
				throw new Exception ("Contact with Name " + nameToSearch + " Not found");
		}
		logger.log_Message("Contact with Name " + nameToSearch + " found", Logger.MessageType.PASS);
	}
	
	/**
	 * Method to  role information.*/
	private void addRoleInfo(String rolesIndex) throws Exception {
		String[] roles = rolesIndex.split(",");
		int roleIndex = 0;
		for( String role : roles) {
			Hashtable<String, String> data_role = ClaimCenterData.getData("ContactRoles", role);
			ccRootObject.button("Click", "AddButton");
			
			//Select Role
			ccRootObject.comboBoxinTable("Select", ccRootObject.getBrowserDocObject(),  roleIndex + ":", "Role", data_role.get("RoleName"));
			//Owner
			ccRootObject.comboBoxinTable("Select",  ccRootObject.getBrowserDocObject(), roleIndex + ":", "Owner", data_role.get("Owner"));
			//
			if ( data_role.get("ActiveRole").equalsIgnoreCase("No") ) {
				ccRootObject.radioButtoninTable("Select",  ccRootObject.getBrowserDocObject(), roleIndex + ":", "isActiveNo");
			}
			//Comments
			ccRootObject.editBoxInTable("Input",  ccRootObject.getBrowserDocObject(),roleIndex+":", "ContactComments", data_role.get("Comments"));
			
			roleIndex ++;
		}
	}
}
