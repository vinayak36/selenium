package CCAppl;

import java.util.Hashtable;

import resources.CCAppl.AddressBookHelper;
import TestData.ClaimCenterData;
import Utilities.CCUtility;
import Utilities.Logger;
import Utilities.Logger.MessageType;
import Utilities.Logger.ScreenShotEvent;

import com.rational.test.ft.ObjectNotFoundException;
import com.rational.test.ft.object.interfaces.TextGuiTestObject;


/**
 * Description   : Automated the AddressBook feature, and provides methods to Create new contact,
 * Modify the contact details Search Address book, Delete contact, View contact details
 * @author Vinayak Hegde
 */
public class AddressBook extends AddressBookHelper
{	
	/**
	 * Script Name   : <b>AddressBook</b>
	 * Generated     : <b>Jun 6, 2012 2:54:56 AM</b>
	 * Description   : Functional Test Script
	 * Original Host : WinNT Version 5.1  Build 2600 (S)
	 * 
	 * @since  2012/06/06
	 * @author Vinayak Hegde
	 */

	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Contacts and initiates root Test object and Logger
	 * 
	 * @param rootObject
	 *            - Root test object
	 * @param log
	 *            - Logger instance
	 * @throws Exception 
	 */
	public AddressBook(UIObjectsAPI rootObject,Logger log) throws Exception{

		ccRootObject =rootObject;
		logger = log;
	}

	/**
	 * Create a new contact
	 * @param contactIndex
	 *   - Index of the Contact data from the "Contact" sheet
	 * @return 
	 * @throws Exception
	 */
	public void createContact(String contactIndex)throws Exception{
		logger.log_Message("Creating Address Book Contact", MessageType.INFO);

		//get data
		Hashtable<String, String> data_Contact = ClaimCenterData.getData("Contacts",contactIndex);

		clickNewContactMenu(data_Contact, "AddressMenuActionsBTN", "AddressMenuActionsButton");

		fillContactFormAndUpdate(data_Contact);

		logger.log_Message("New Contact create successully", MessageType.INFO);
	}

	public void fillContactFormAndUpdate(Hashtable<String, String> data_Contact) throws Exception {
		
		fillInputValues(data_Contact);
		//Click on Update Address Book
		ccRootObject.button("Click","UpdateAddressBookBtn");
		//check if Contact is created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating New Entity in Address Book'" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
	}

	public void clickNewContactMenu(Hashtable<String, String> data_Contact, String menuBtnUniqueId, String menuListUniqueId) throws Exception {
		//Actions-> Contact Type-> Contact Sub Type
		String[] addressBookMenuItemsList = null;
		if (data_Contact.get("ContactType").trim().equalsIgnoreCase("Company") ) {
			addressBookMenuItemsList = new String[] {
					"New " + data_Contact.get("ContactType").trim()};
		}
		else {
			addressBookMenuItemsList = new String[] {
					"New " + data_Contact.get("ContactType").trim(),
					data_Contact.get("ContactSubType").trim()};
		}

		ccRootObject.button("Click", menuBtnUniqueId);
		ccRootObject.menu("Click", menuListUniqueId,  addressBookMenuItemsList);
	}

	/**Method to populate the input values */
	public void fillInputValues(Hashtable<String, String> data_Contact) throws Exception{

		if(	data_Contact.get("ContactType").trim().equalsIgnoreCase("Person")||
				data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Lawyer")){
			//First  Name
			ccRootObject.editBox("Input", "FirstName",data_Contact.get("FirstName"));
			//Middle Name
			ccRootObject.editBox("Input", "MiddleNameText",data_Contact.get("MiddleName"));
			//Last Name
			ccRootObject.editBox("Input", "LastName",data_Contact.get("LastName"));
			//Prefix
			ccRootObject.comboBox("Select","PrefixSelect",data_Contact.get("Prefix"));
			//Sufix
			ccRootObject.comboBox("Select","SufixSelect",data_Contact.get("Suffix"));
		}
		else{ 
			//Add the organization name
			ccRootObject.editBox("Input","OrganizationName",data_Contact.get("OrganizationName"));
		}

		//Contact Info and Web & Email
		contactInfoWebandEmail(data_Contact);

		/*Primary Address*/
		if(!data_Contact.get("PrimaryAddress").trim().isEmpty()){
			addLocation(ccRootObject, data_Contact.get("PrimaryAddress"));
		}

		/*Additional Information*/   
		addAdditionalInfo(data_Contact);

		if ( data_Contact.get("ContactType").trim().equalsIgnoreCase("Person")) {
			//Driver's Info and Employement Information
			addEmployementandLicenceInfo(data_Contact);
		}

		if(data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Third Party")){
			//Risk Info
			addRiskInfo(data_Contact);
		}

		//Notes
		if(!data_Contact.get("Notes").trim().isEmpty()){
			ccRootObject.editBox("Input", "Notes", data_Contact.get("Notes"));
		}
		/*Alternate Addresses*/ 
		if(!data_Contact.get("OtherAddresses").trim().isEmpty()){

			String AlternateAddresses[] = data_Contact.get("OtherAddresses").trim().split(",");
			ccRootObject.link("Click","AddressesTab");

			for(String alternateAdress:AlternateAddresses){
				addLocation(ccRootObject, alternateAdress); 
				ccRootObject.button("Click","AddressesAddBtn");
			}
		}

		//External Attributes
		if(!data_Contact.get("ExternalAttributes").trim().isEmpty()){
			addExteranlAttributes(data_Contact.get("ExternalAttributes"));
		}	
	}

	/**
	 * @param data_Contact
	 * @throws Exception
	 */
	private void addRiskInfo(Hashtable<String, String> data_Contact)
	throws Exception {

		ccRootObject.editBox("Input", "OwnerOtherthanClaimant", data_Contact.get("OwnerOtherthanClaimant"));

		if ( data_Contact.get("InsuranceCompany").trim().isEmpty() ) {
			if ( ccRootObject.comboBox("Verify", "InsuranceCompany", data_Contact.get("InsuranceCompany")).startsWith("PASS")) {
				ccRootObject.comboBox("Select", "InsuranceCompany", data_Contact.get("InsuranceCompany"));
			}
			else 
			{
				ccRootObject.comboBox("Select", "InsuranceCompany", "Not listed or unknown");
				ccRootObject.editBox("Input", "OtherInsuranceCompany", data_Contact.get("InsuranceCompany"));
			}
		}
		ccRootObject.editBox("Input", "OtherPolicyNumber", data_Contact.get("PolicyNumber"));

		//TODO: Third Party Adjuster
	}

	/**
	 * Edit a Contact
	 * @param contactIndex
	 *            - Index of the Contact data from the "Contact" sheet.
	 * @return 
	 * @throws Exception
	 */
	public void editContact(String contactIndex) throws Exception{
		logger.log_Message("Editing Entity", MessageType.INFO);
		//get data
		Hashtable<String, String> data_Contact = ClaimCenterData.getData("Contacts", contactIndex);

		//Search and open the contact
		searchAddressBook(contactIndex);
		String contactName = "";
		if( data_Contact.get("ContactType").trim().equalsIgnoreCase("Person")||
				data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Lawyer")) {
			contactName = data_Contact.get("FirstName").trim()+ " " + data_Contact.get("LastName").trim();
		}
		else {
			contactName = data_Contact.get("OrganizationName").trim();
		}
		openContactFromSearchResult(contactName);

		///Click on edit
		ccRootObject.button("Click","AddressBookEditBtn");

		fillInputValues(data_Contact);

		//Click on Update Address Book
		ccRootObject.button("link","UpdateAddressBookBtn");
		//check if Contact is created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while editing Entity in Address Book'" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("Contact Edited successully", MessageType.INFO);
	}

	/**
	 * Searches the Contact on Search Address Book Screen 
	 * 
	 * @param contactIndex
	 *            - Index of the Contact data to use for searching
	 * @return return true if contact search find the contact
	 * @throws Exception 
	 * @since 2012/06/12
	 */
	public void searchAddressBook(String contactIndex) throws Exception {

		logger.log_Message("Searching AddressBook", MessageType.INFO);
		//get the data
		Hashtable<String, String> data_Contact = ClaimCenterData.getData( "Contacts", contactIndex);
		//Select Contact type
		String contactTypeToSearch = "";
		if ( data_Contact.get("ContactSubType").equalsIgnoreCase("Other")) {
			contactTypeToSearch = data_Contact.get("ContactType"); 
		}
		else {
			contactTypeToSearch = data_Contact.get("ContactSubType");
		}
		ccRootObject.comboBox("Select", "SearchContactType",contactTypeToSearch);

		//Name
		if(data_Contact.get("ContactType").trim().equalsIgnoreCase("Person")||
				data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Lawyer")){

			ccRootObject.editBox("Input", "FirstName", data_Contact.get("FirstName"));
			ccRootObject.editBox("Input", "ContactSearchLastName", data_Contact.get("LastName"));
		}
		else{
			ccRootObject.editBox("Input", "SearchContactName", data_Contact.get("OrganizationName"));
		}

		//Preferred Vendor
		if (data_Contact.get("ContactType").trim().equalsIgnoreCase("Vendor") ||
				data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Lawyer") ||
				data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Law Firm")) {

			if (data_Contact.get("PreferredVendor").trim().equals("Yes")) {
				ccRootObject.radioButton("Select", "PreferedVendorOnlyYes");
			}
			else if(data_Contact.get("PreferredVendor").trim().equals("No")) {
				ccRootObject.radioButton("Select", "PreferedVendorOnlyNo");
			}
		}

		//When contact type is either lawyer or law firm
		if(	data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Lawywer")||
				data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Law Firm")){
			ccRootObject.comboBox("Input", "LegalSpeciality", data_Contact.get("LegalSpeciality"));
		}

		//Select the vendor system
		ccRootObject.comboBox("Input", "VendorSystmComboBox", data_Contact.get("VendorSystem"));

		if ( !data_Contact.get("PrimaryAddress").trim().isEmpty()) {

			Hashtable<String, String> dataContacts_Location = ClaimCenterData.getData( "Locations", data_Contact.get("PrimaryAddress"));
			//Country
			ccRootObject.comboBox("Select", "LocationCountry", dataContacts_Location.get("Country"));
			//Province
			ccRootObject.comboBox("Select", "LocationProvince", dataContacts_Location.get("Province"));
			//City
			ccRootObject.editBox("Input", "LocationAddressCity", dataContacts_Location.get("City"));

			if ( dataContacts_Location.get("Country").equalsIgnoreCase("Canada")) {
				//Postal code
				ccRootObject.editBox("Input", "LocationPostalCode", dataContacts_Location.get("PostalCode"));
			}
			else {
				//ZIP Code
				ccRootObject.editBox("Input", "LocationZipCode", dataContacts_Location.get("PostalCode"));
				//County
				ccRootObject.editBox("Input", "LocationCounty", dataContacts_Location.get("County"));
			}
		}

		//Click Search Button
		ccRootObject.button("Click", "SearchBtn");

		//Check if search successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while searching the Address book  '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}

		logger.log_Message("Address book search was successul", MessageType.INFO);
	}


	/**
	 * Delete a address book contact
	 * 
	 * @param contactIndex
	 *            - Index of the Contact data to use for searching
	 * @return return true if contact search find the contact
	 * @throws Exception 
	 * @throws Exception 
	 * @since 2012/06/12
	 */
	public void deleteContact(String contactIndex) throws Exception{
		logger.log_Message("Delete a contact from AddressBook", MessageType.INFO);

		//get the data
		Hashtable<String, String> data_Contact = ClaimCenterData.getData( "Contacts", contactIndex);

		searchAddressBook(contactIndex);
		String contactName = "";
		if( data_Contact.get("ContactType").trim().equalsIgnoreCase("Person")||
				data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Lawyer")) {
			contactName = data_Contact.get("FirstName").trim()+ " " + data_Contact.get("LastName").trim();
		}
		else {
			contactName = data_Contact.get("OrganizationName").trim();
		}
		openContactFromSearchResult(contactName);
		//Click on Delete button
		ccRootObject.button("Click","AddressBookDeleteBtn");
	}


	/**
	 * view a address book contact
	 * 
	 * @param contactIndex
	 *            - Index of the Contact data to use for searching
	 * @return return true if contact search find the contact
	 * @throws Exception 
	 * @throws Exception 
	 * @since 2012/06/12
	 */
	public void viewContact(String contactIndex) throws Exception{
		logger.log_Message("view a contact from AddressBook", MessageType.INFO);
		//get the data
		Hashtable<String, String> data_Contact = ClaimCenterData.getData( "Contacts", contactIndex);
		searchAddressBook(contactIndex);
		String contactName = "";
		if( data_Contact.get("ContactType").trim().equalsIgnoreCase("Person")||
				data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Lawyer")) {
			contactName = data_Contact.get("FirstName").trim()+ " " + data_Contact.get("LastName").trim();
		}
		else {
			contactName = data_Contact.get("OrganizationName").trim();
		}
		openContactFromSearchResult(contactName);
	}	 



	/**Method to search and Open a contact
	 * @param -contactName
	 * @throws Exception
	 */
	private void openContactFromSearchResult(String contactName) throws Exception {

		// Search Table
		TextGuiTestObject resultTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "AddressSearchResultTable");

		//Find Contact based on Name
		TextGuiTestObject contactNameObject =  ccRootObject.findDescendantTestObject(
				resultTable, ".class", "Html.A", ".text", contactName.trim());
		if ( contactNameObject == null) {
			throw new Exception ("Contact  with name" + contactName.trim() + " Not found"); 
		}
		//Click the link
		contactNameObject.click();
		ccRootObject.waitForBrowserReady();
		logger.captureScreen(ScreenShotEvent.ScreenChange);
	}


	/**
	 * @param contactIndex
	 * @throws Exception
	 */
	public void searchSelectContact(String contactIndex) throws Exception {

		searchAddressBook(contactIndex);

		TextGuiTestObject resultTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "AddressSearchResultTable");

		Hashtable<String, String> data_Contact = ClaimCenterData.getData( "Contacts", contactIndex);

		String contactName = "";
		//when contact type is a person or lawyer or third party
		if(data_Contact.get("ContactType").trim().equalsIgnoreCase("Person")||
				data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Lawyer")){
			contactName = data_Contact.get("FirstName") + " " + data_Contact.get("LastName");
		}//Otherwise
		else{
			contactName = data_Contact.get("OrganizationName");
		}

		TextGuiTestObject contactNameObject =  ccRootObject.findDescendantTestObject(
				resultTable, ".class", "Html.A", ".text", contactName);

		if ( contactNameObject == null) {
			throw new Exception ("Contact  with name" + contactName + " Not found"); 
		}
		//Get the row object with activity
		String rowID = contactNameObject.getProperty(".id").toString();
		TextGuiTestObject contactRow =  ccRootObject.findDescendantTestObject(
				resultTable, ".class", "Html.TR", ".id", rowID.replace("DisplayName", "0"));

		ccRootObject.buttonInTable("Click", contactRow, "SearchResultAssignButton");
	}

	/**Method for external Attributes
	 * @throws Exception */
	private  void addExteranlAttributes(String EAIndex) throws Exception {
		Hashtable<String, String> dataContacts_ExternalAttributes = ClaimCenterData.getData("ExternalAttributes",EAIndex);

		if(ccRootObject.link("Verify", "ExternalAttributesTab").startsWith("PASS")){ 
			//go to External Index Screen.
			ccRootObject.link("Click", "ExternalAttributesTab");

			//Audatex Partner Key
			ccRootObject.editBox("Input","AudatexPartnerKey", dataContacts_ExternalAttributes.get("AudatexPartnerKey"));

			//XactNetAddress
			ccRootObject.editBox("Input","XactNetAddress", dataContacts_ExternalAttributes.get("XactNetAddress"));

			//VendorSystem
			if(!dataContacts_ExternalAttributes.get("VendorSystem").trim().isEmpty()){
				ccRootObject.button("Click","VendorSystemAddBtn");
				ccRootObject.comboBox("Select","VendorSystmComboBox", dataContacts_ExternalAttributes.get("VendorSystem"));
			}

			//Region
			if(!dataContacts_ExternalAttributes.get("Region").trim().isEmpty()){
				ccRootObject.button("Click","RegionAddBtn");
				ccRootObject.comboBox("Select","RegionComboBox", dataContacts_ExternalAttributes.get("Region"));
			}

			//DataSet
			if(!dataContacts_ExternalAttributes.get("DataSet").trim().isEmpty()){
				ccRootObject.button("Click","DataSetAddBtn");
				ccRootObject.comboBox("Select","DataSetComboBox", dataContacts_ExternalAttributes.get("DataSet"));
			}

			//ServiceAreaPostalCode
			if(!dataContacts_ExternalAttributes.get("ServiceAreaPostalCode").trim().isEmpty()){
				ccRootObject.button("Click","ServiceAreaPostalCodeAddBtn");
				ccRootObject.editBox("Input","ServiceAreaPostalCodeComboBox", dataContacts_ExternalAttributes.get("ServiceAreaPostalCode"));
			}
		}
	}

	/**Method to add Additional Information*/

	/**
	 * @param data_Contact
	 * @throws Exception
	 */
	private void addAdditionalInfo(Hashtable<String, String> data_Contact) throws Exception {

		if(data_Contact.get("ContactType").trim().equalsIgnoreCase("Person")){
			//Date of birth
			ccRootObject.editBox("Input", "DOB",
					CCUtility.convertDateFormat( data_Contact.get("DateofBirth"),
							"yyyy-MM-dd hh:mm:ss","MMddyyyy"));
			//Gender
			ccRootObject.comboBox("Select", "Gender", data_Contact.get("Gender"));
			//Primary Language
			ccRootObject.comboBox("Select", "PrimaryLanguage",data_Contact.get("PrimaryLanguage"));
			//PreferredVerbalLanguage 
			ccRootObject.comboBox("Select", "PreferredVerbalLanguage",data_Contact.get("PreferredVerbalLanguage"));
			//TODO: Guardian
			//TODO: Representative
		}
		if(data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Lawyer")){

			//JurisdictionLicencedtoPractice
			ccRootObject.editBox("Input","JurisdictionLicencedtoPractice",data_Contact.get("JurisdictionLicencedtoPractice"));
			//LegalSpecialty
			ccRootObject.comboBox("Select","Specialty",data_Contact.get("LegalSpecialty"));
			//Is he a preferred vendor?
			if(data_Contact.get("PreferredVendor").trim().equalsIgnoreCase("Yes")){
				ccRootObject.radioButton("Select","PreferedVendorOnlyYes");
			}
		}

		if(data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Law Firm")){
			//Is he a preferred vendor?
			if(data_Contact.get("PreferredVendor").trim().equalsIgnoreCase("Yes")){
				ccRootObject.checkBox("Check","PreferedVendorOnlyYes");		 
			}
			//Legal Specialty
			ccRootObject.comboBox("Select","Specialty",data_Contact.get("LegalSpecialty"));
		}

		if( data_Contact.get("ContactType").trim().equalsIgnoreCase("Vendor")){
			//Is he a preferred vendor?
			if(data_Contact.get("PreferredVendor").trim().equalsIgnoreCase("Yes")){
				ccRootObject.radioButton("Select","PreferedVendorOnlyYes");		 
			}

			if(data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Body Shop")){
				//Business license
				ccRootObject.editBox("Input","BusinessLicense",data_Contact.get("LicenceNumber"));
			}
		}

		if(data_Contact.get("ContactType").trim().equalsIgnoreCase("Company")){
			//Primary Language
			ccRootObject.comboBox("Select", "PrimaryLanguage",data_Contact.get("PrimaryLanguage"));
			//TODO: Legal Representative
		}
	}


	/**
	 * @param data_Contact
	 * @throws Exception
	 */
	private void addEmployementandLicenceInfo(
			Hashtable<String, String> data_Contact) throws Exception {
		//Occupation
		ccRootObject.editBox("Input", "Occupation",data_Contact.get("Occupation"));



		//Driver's license number 
		ccRootObject.editBox("Input", "DriversLicenceNumber",data_Contact.get("LicenceNumber"));
		//Driver's license class
		ccRootObject.editBox("Input", "DriversLicenceClass",data_Contact.get("LicenceClass"));
		//Province
		ccRootObject.comboBox("Select", "LicenseProvince", data_Contact.get("LicenceProvince"));
		//DateLicenced
		ccRootObject.editBox("Input", "DateLicenced", CCUtility.convertDateFormat(
				data_Contact.get("DateLicenced"), "yyyy-MM-dd hh:mm:ss","MMddyyyy"));

		//License status
		if(ccRootObject.comboBox("Verify", "DriversLicenceStatus", data_Contact.get("LicenceStatus")).startsWith("PASS")){
			ccRootObject.comboBox("Select", "DriversLicenceStatus", data_Contact.get("LicenceStatus"));
		}
	}


	/**
	 * Fills the location fields. Since this is used in other place its made public and static   
	 * @param ccRootObject
	 * @param location
	 * @throws Exception
	 */
	public static void addLocation(UIObjectsAPI ccRootObject, String location) throws Exception {

		Hashtable<String, String> dataContacts_Location = ClaimCenterData.getData("Locations", location);
		//Address 1
		ccRootObject.editBox("Input", "LocationAddressLine1", dataContacts_Location.get("Address1"));
		//Address 2
		ccRootObject.editBox("Input", "LocationAddressLine2", dataContacts_Location.get("Address2"));
		//Country
		ccRootObject.comboBox("Select", "LocationCountry", dataContacts_Location.get("Country"));
		//Province
		ccRootObject.comboBox("Select", "LocationProvince", dataContacts_Location.get("Province"));
		//City
		ccRootObject.editBox("Input", "LocationAddressCity", dataContacts_Location.get("City"));
		if ( dataContacts_Location.get("Country").equalsIgnoreCase("Canada") ||
				dataContacts_Location.get("Country").trim().isEmpty() ){
			//PostalCode	
			ccRootObject.editBox("Input", "LocationPostalCode", dataContacts_Location.get("PostalCode"));
		}
		else {
			//ZIP Code
			ccRootObject.editBox("Input", "LocationZipCode", dataContacts_Location.get("PostalCode"));
			//County
			ccRootObject.editBox("Input", "LocationCounty", dataContacts_Location.get("County"));
		}
		//Address Type
		try {
			ccRootObject.comboBox("Select", "AddressType", dataContacts_Location.get("AddressType"));
		} catch (ObjectNotFoundException e) {
		}
		//Location Description
		try {
			ccRootObject.editBox("Input", "LocationDescription", dataContacts_Location.get("LocationDesc"));
		} catch (ObjectNotFoundException  e) {
		}
		//Valid until
		try {
			if ( !dataContacts_Location.get("ValidUntil").trim().isEmpty()) {

				ccRootObject.editBox("Input", "ValidUntil", CCUtility.convertDateFormat(
						dataContacts_Location.get("ValidUntil"),
						"yyyy-MM-dd hh:mm:ss","MMddyyyy"));
			}
		} catch (ObjectNotFoundException  e) {
		}
	}

	/**Method to add Contact Information*/

	/**
	 * @param data_Contact
	 * @throws Exception
	 */
	private void contactInfoWebandEmail(Hashtable<String, String> data_Contact) throws Exception {

		if(	data_Contact.get("ContactType").trim().equalsIgnoreCase("Person")){
			//Primary Phone
			ccRootObject.comboBox("Select","PrimaryPhone",data_Contact.get("PrimaryPhone"));  
			//Business Phone
			ccRootObject.editBox("Input", "ContactBusinessPhone",data_Contact.get("BusinessPhone"));
			//Home Phone
			ccRootObject.editBox("Input", "HomeNumber",data_Contact.get("HomePhone"));
			//Mobile Phone
			ccRootObject.editBox("Input", "MobileNumber",data_Contact.get("Mobile"));
		}
		else if( data_Contact.get("ContactSubType").trim().equalsIgnoreCase("Lawyer") ){
			ccRootObject.editBox("Select","AttorneyPhone",data_Contact.get("BusinessPhone"));
			//TODO: Assistant
		}
		else {
			//TODO: Primary Contact
			//Phone Number
			ccRootObject.editBox("Input","PhoneNumber",data_Contact.get("BusinessPhone"));  					
		}
		//FAX
		ccRootObject.editBox("Input", "FAX",data_Contact.get("Fax"));
		//Toll Free
		ccRootObject.editBox("Input", "TollFree",data_Contact.get("TollFree"));
		if(	data_Contact.get("ContactType").trim().equalsIgnoreCase("Person")){
			//Main Email
			ccRootObject.editBox("Input", "PrimaryEmail", data_Contact.get("MainEmail"));
			//Alternate Email
			ccRootObject.editBox("Input", "SecondaryEmail",data_Contact.get("AlternateEmail"));
		}
		else {
			//Main Email
			ccRootObject.editBox("Input", "Email", data_Contact.get("MainEmail"));
			//Alternate Email
			ccRootObject.editBox("Input", "AlternateEmail",data_Contact.get("AlternateEmail"));
		}
		//WebSite
		ccRootObject.editBox("Input", "WebSite",data_Contact.get("WebSite"));

		//Preferred Method
		ccRootObject.comboBox("Select", "PreferredMethod", data_Contact.get("PreferredMethod"));
	}
}
