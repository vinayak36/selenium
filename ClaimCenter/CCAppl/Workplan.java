package CCAppl;
import java.util.Hashtable;

import resources.CCAppl.WorkplanHelper;
import TestData.ClaimCenterData;
import Utilities.CCUtility;
import Utilities.Logger;
import Utilities.Logger.MessageType;

import com.rational.test.ft.NotYetAbleToPerformActionException;
import com.rational.test.ft.UnsupportedActionException;
import com.rational.test.ft.object.interfaces.TestObject;
import com.rational.test.ft.object.interfaces.TextGuiTestObject;

/**
 * Implements the WorkPlan Module
 * 
 * @authorVinayak Hegde
 */
public class Workplan extends WorkplanHelper
{
	private UIObjectsAPI ccRootObject;
	private Logger logger;
	
	/**
	 * Instantiates the Contacts and initiates root Test object and Logger
	 * 
	 * @param rootObject
	 *            - Root test object
	 * @param log
	 *            - Logger instance
	 * @since 2012/03/08
	 */
	public Workplan(UIObjectsAPI rootObject, Logger log) {
		ccRootObject = rootObject;
		logger = log;
	}

	/**
	 * Creates new Activity in Opened Claim
	 * @param activityIndex
	 *            - Data Index for Activity in "Activities" sheet
	 * @throws Exception 
	 */
	public void createNewActivity(String activityIndex) throws Exception {
		logger.log_Message("Creating New Activity", MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_Activity = ClaimCenterData.getData("Activities", activityIndex);
		//Actions-> Activity Type-> Subject
		//String[] activityMenuItemsList = new String[] {"Correspondence", "Send reservation of rights letter"};
		String[] activityMenuItemsList = new String[] {
				data_Activity.get("ActivityType"),
				data_Activity.get("Subject")};
		
		ccRootObject.button("Click", "ActionsButton");
		ccRootObject.menu("Click", "MenuActionsList", activityMenuItemsList);
		
		//Fill Activity Data
		fillActivityDetails(data_Activity, "MainContent" );
		
		//Document Template
		ccRootObject.editBox("Input", "ActivityDocumentTemplate", data_Activity.get("DocumentTemplate"));
		//Email Template
		ccRootObject.editBox("Input", "ActivityEmailTemplate", data_Activity.get("EmailTemplate"));
		if ( data_Activity.get("AssignWhileCreating").equalsIgnoreCase("Yes") ) {
			//Assign To
			Hashtable<String, String> data_Assignment = ClaimCenterData.getData("Assignments", data_Activity.get("AssignTo"));
			//Get user name
			ccRootObject.comboBox("Select", "ActivityAssignTo", data_Assignment.get("AssigneeToSelectFromList"));
		}
		//Save
		ccRootObject.button("Click", "ActivityUpdateButton");
		
		//check if any error occurred
		
		String validationSpanText = ccRootObject.htmlText("Get", "ValidationMessages");
		if (validationSpanText != null) {
			String screenName = validationSpanText.substring(0, validationSpanText.indexOf(":"));
			String validationMessages = validationSpanText.replace( screenName, "");
			String error = "Errors while creating new Activity '" + validationMessages + "'";
			throw new Exception(error);
		}

		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating new Activity '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("New activity " + data_Activity.get("Subject") + " created successully", MessageType.INFO);
	}

	/**
	 * Fills the Activity Details
	 * @param data_Activity
	 *   		- Activity Data collection
	 * @throws Exception 
	 */
	private void fillActivityDetails(Hashtable<String, String> data_Activity, String containerUniqueID  ) throws Exception {

		//Get the container Object
		TestObject containerObj = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				containerUniqueID);
		
		//Subject
		//Description
		ccRootObject.editBoxInTable("Input", containerObj, null, "ActivityDescription", data_Activity.get("Description"));
		//Related To
		ccRootObject.comboBoxinTable("Select", containerObj, null, "RelatedTo", data_Activity.get("RelatedTo"));

		//Refresh container Object
		containerObj = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				containerUniqueID);		

		//Due Date
		ccRootObject.editBoxInTable("Input", containerObj, null, "ActivityDueDate",
				CCUtility.convertDateFormat( data_Activity.get("DueDate"), 
						"yyyy-MM-dd hh:mm:ss",
						"MM-dd-yyyy").replace("-", ""));
		//Escalation Date
		ccRootObject.editBoxInTable("Input", containerObj, null, "ActivityEscalationDate", 
				CCUtility.convertDateFormat( data_Activity.get("EscalationDate"),
						"yyyy-MM-dd hh:mm:ss",
						"MM-dd-yyyy").replace("-", ""));
		//Priority
		ccRootObject.comboBoxinTable("Select", containerObj, null, "ActivityPriority", data_Activity.get("Priority"));
		//Mandatory
		if ( data_Activity.get("Mandatory").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButtoninTable("Select", containerObj, null, "ActivityMandatoryYes");
		}

		//Calendar Importance
		ccRootObject.comboBoxinTable("Select", containerObj, null, "ActivityCalendarImportance", data_Activity.get("CalendarImportance"));
		//Recurring
		//Externally Owned
		if ( data_Activity.get("ExternallyOwned").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButtoninTable("Select", containerObj, null, "ActivityExternallyOwnedYes");
		}
		else if (data_Activity.get("ExternallyOwned").equalsIgnoreCase("No")) {
			ccRootObject.radioButtoninTable("Select", containerObj, null, "ActivityExternallyOwnedNo");
		}
		//External Owner
		ccRootObject.comboBoxinTable("Select", containerObj, null, "ActivityExternalOwner", data_Activity.get("ExternalOwner"));
	}

	//Assign Activity
	/**
	 * Assigns the activity to specified users 
	 * @param activityIndex
	 * 				- Activity Index from "Activities" sheet
	 * @throws Exception
	 */
	public void assignActivity(String activityIndex) throws Exception {
		
		//Get Activity data
		Hashtable<String, String> data_Activity = ClaimCenterData.getData("Activities", activityIndex);
		//Select Activity
		selectActivity(data_Activity.get("Subject"));
		//Click Assign
		ccRootObject.button("Click", "WorkPlanAssignButton");
		Assignment assigment = new Assignment(ccRootObject, logger);
		assigment.doAssignment(data_Activity.get("AssignTo"));
	}

	/**
	 * Select the Activity in WorkPlan table
	 * @param activitySubject
	 * 				- Activity Subject
	 * @throws Exception
	 * 				- In case activity not found
	 */
	private void selectActivity(String activitySubject)
			throws Exception {
		//Find workItem table
		TextGuiTestObject workplanTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				"ClaimWorkplanTable");
		
		//Find activity based on Subject
		TextGuiTestObject activitySubjectObject =  ccRootObject.findDescendantTestObject(
				workplanTable, ".class", "Html.A", ".text", activitySubject);
		if ( activitySubjectObject == null) {
			throw new Exception ("Activity with Subject " + activitySubject + " Not found"); 
		}
		//Get the row object with activity
		String rowID = activitySubjectObject.getProperty(".id").toString();
		TextGuiTestObject activityRow =  ccRootObject.findDescendantTestObject(
				workplanTable, ".class", "Html.TR", ".id", rowID.replace("Subject", "0"));
		
		ccRootObject.checkBoxinTable("Check",activityRow, "", "CheckBox");
	}
	
	/**
	 * Skip Activity
	 * @param activityIndex
	 * 			- Activity to select and Skip
	 * @throws Exception
	 */
	public void skipActivity ( String activityIndex) throws Exception {
		//Get Activity data
		Hashtable<String, String> data_Activity = ClaimCenterData.getData("Activities", activityIndex);
		//Select Activity
		selectActivity(data_Activity.get("Subject"));
		ccRootObject.button("Click", "WorkPlanSkipButton");
	}

	/**
	 * Complete Activity
	 * @param activityIndex
	 * 			- Activity to select and Complete
	 * @throws Exception
	 */
	public void completeActivity ( String activityIndex) throws Exception {
		//Get Activity data
		Hashtable<String, String> data_Activity = ClaimCenterData.getData("Activities", activityIndex);
		//Select Activity
		selectActivity(data_Activity.get("Subject"));
		ccRootObject.button("Click", "WorkPlanCompleteButton");
	}

	/**
	 * Approve Activity
	 * @param activityIndex
	 * 			- Activity to select and approve
	 * @throws Exception
	 */
	public void approveActivity ( String activityIndex) throws Exception {
		//Get Activity data
		Hashtable<String, String> data_Activity = ClaimCenterData.getData("Activities", activityIndex);
		//Select Activity
		selectActivity(data_Activity.get("Subject"));
		ccRootObject.button("Click", "WorkPlanApproveButton");
	}

	/**
	 *Reject Activity 
	 * @param activityIndex
	 * 				- Activity to select and Reject
	 * @throws Exception
	 */
	public void rejectActivity ( String activityIndex) throws Exception {
		//Get Activity data
		Hashtable<String, String> data_Activity = ClaimCenterData.getData("Activities", activityIndex);
		//Select Activity
		selectActivity(data_Activity.get("Subject"));
		ccRootObject.button("Click", "WorkPlanRejectButton");
	}

	/**
	 * Update Activity
	 * @param activityIndex
	 * 			- Activity to update
	 * @param addNotesOnly
	 * 			- Flag if just notes to be added
	 * @throws Exception
	 */
	public void updateActivity ( String activityIndex, Boolean addNote) throws Exception {
		//Get Activity data
		Hashtable<String, String> data_Activity = ClaimCenterData.getData("Activities", activityIndex);
		//Open Activity
		openActivity(data_Activity);

		// fill Activity Details
		if ( !addNote) {
			//Get Workspace Container
			fillActivityDetails(data_Activity, "ActivityDetailsinWorkspaceTable" );
		}
		else {
			//Add New Note
			addNewNote(data_Activity.get("Notes"), "ActivityNotesinWorkspaceTable");
		}		
		//Update
		ccRootObject.button("Click", "ActivityUpdateButton");
	}

	/**
	 * Adds notes
	 * @param noteIndex
	 * 			- Notes to add
	 * @throws Exception 
	 */
	private void addNewNote(String noteIndex, String containerUniqueID) throws Exception {
		
		//Get container Object
		TestObject activityNewNotesArea = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				containerUniqueID);
		
		Hashtable<String, String> data_Notes = ClaimCenterData.getData("Notes", noteIndex);
		
		//Topic
		ccRootObject.comboBoxinTable("Select", activityNewNotesArea, null, "NoteTopic", data_Notes.get("Topic"));
		//Subject
		ccRootObject.editBoxInTable("Input", activityNewNotesArea, null, "NoteSubject", data_Notes.get("Subject"));

		// Refresh container Object
		activityNewNotesArea = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				containerUniqueID);

		//Related To
		ccRootObject.comboBoxinTable("Select", activityNewNotesArea, null, "RelatedTo", data_Notes.get("RelatedTo"));
		//Confidential
		if ( data_Notes.get("Confidential").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButtoninTable("Select", activityNewNotesArea, null, "ActivityNoteConfidentialYes");
		} else if ( data_Notes.get("Confidential").equalsIgnoreCase("No")) {
			ccRootObject.radioButtoninTable("Select", activityNewNotesArea, null, "ActivityNoteConfidentialNo");
		}
		//Text
		ccRootObject.editBoxInTable("Input", activityNewNotesArea, null, "ActivityNoteText", data_Notes.get("Text"));
	}

	/**
	 * Opens the specified activity
	 * @param data_Activity
	 * 			- Activity to open
	 * @throws Exception
	 */
	private void openActivity(Hashtable<String, String> data_Activity) throws Exception {
		//Find workItem table
		TextGuiTestObject workplanTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				"ClaimWorkplanTable");
		//Find activity based on Subject
		TextGuiTestObject activitySubjectObject =  ccRootObject.findDescendantTestObject(
				workplanTable, ".class", "Html.A", ".text", data_Activity.get("Subject"));
		if ( activitySubjectObject == null) {
			throw new Exception ("Activity with Subject " + data_Activity.get("Subject") + " Not found"); 
		}
		//Click on subject to open it
		try {
			activitySubjectObject.click();
		} catch (NotYetAbleToPerformActionException  e) {
			sleep(2);
			activitySubjectObject.click();
		} catch (UnsupportedActionException e) {
			sleep(2);
			activitySubjectObject.click();
		}
		ccRootObject.waitForBrowserReady();
	}

	/**
	 * Method to select multiple or all activities based on activity filter
	 * @param assignmentIndex
	 * 			- Assignment data index to do assignment
	 * @throws Exception
	 */
	public void assignAllActivities(String assignmentIndex) throws Exception {
		
		//Filter to All Open Activities
		ccRootObject.comboBox("Select","WorkplanFilter","All open activities");
		//Select All activities displayed 
		ccRootObject.checkBox("Check","CheckBox");
		//Click Assign
		ccRootObject.button("Click", "WorkPlanAssignButton");
		
		//Do Assignment
		Assignment assignment = new Assignment(ccRootObject,logger);
		assignment.doAssignment(assignmentIndex);
	}
	
	/**
	 * Method to Skip all activities
	 * @throws Exception
	 */
	public void skipAllActivities () throws Exception {
		//Filter to All Open Activities
		ccRootObject.comboBox("Select","WorkplanFilter","All open activities");
		//Select All activities displayed 
		ccRootObject.checkBox("Check","CheckBox");
		ccRootObject.button("Click", "WorkPlanSkipButton");
	}

	/**
	 * Method to Complete all activities
	 * @throws Exception
	 */
	public void completeAllActivities() throws Exception {
		//Filter to All Open Activities
		ccRootObject.comboBox("Select","WorkplanFilter","All open activities");
		//Select All activities displayed 
		ccRootObject.checkBox("Check","CheckBox");
		ccRootObject.button("Click", "WorkPlanCompleteButton");
	}
	
	/**
	 * Method to Approve all activities
	 * @throws Exception
	 */
	public void approveAllActivities() throws Exception {
		//Filter to All Open Activities
		ccRootObject.comboBox("Select","WorkplanFilter","All open activities");
		//Select All activities displayed 
		ccRootObject.checkBox("Check","CheckBox");
		ccRootObject.button("Click", "WorkPlanApproveButton");
	}

	/**
	 * Method to Reject all activities
	 * @throws Exception
	 */
	public void rejectAllActivities() throws Exception {
		//Filter to All Open Activities
		ccRootObject.comboBox("Select","WorkplanFilter","All open activities");
		//Select All activities displayed 
		ccRootObject.checkBox("Check","CheckBox");
		ccRootObject.button("Click", "WorkPlanRejectButton");
	}
}
