package CCAppl;

import java.util.Hashtable;

import resources.CCAppl.AssignmentHelper;
import TestData.ClaimCenterData;
import Utilities.Logger;
import Utilities.Logger.MessageType;

import com.rational.test.ft.NotYetAbleToPerformActionException;
import com.rational.test.ft.UnsupportedActionException;
import com.rational.test.ft.object.interfaces.TextGuiTestObject;

/**
 * Implements the Assignments Module, This class and methods are used to assign claims,
 * exposures, activities and matters etc. 
 * @author Vinayak Hegde
 * @since 2012/06/25
 */
public class Assignment extends AssignmentHelper {
	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Contacts and initiates root Test object and Logger
	 * 
	 * @param rootObject
	 *            - Root test object
	 * @param log
	 *            - Logger instance
	 * @since 2012/03/08
	 */
	public Assignment(UIObjectsAPI rootObject, Logger log) {
		ccRootObject = rootObject;
		logger = log;
	}

	/**
	 * Does assignment. This is used in various screens/features for assignments
	 * 
	 * @param assignmentIndex
	 *            - Index of the Assignment sheet
	 * @throws Exception
	 *             - If the user or group not found
	 */
	public void doAssignment(String assignmentIndex) throws Exception {
		
		logger.log_Message("Doing Assignment", MessageType.INFO);
		//Do Assignment
		Hashtable<String, String> data_Assignment = ClaimCenterData.getData( "Assignments", assignmentIndex);

		// Select how you would like to do the assignment
		if (!data_Assignment.get("AssigneeToSelectFromList").trim().isEmpty()) {
			//Select from list:
			ccRootObject.radioButton("Select", "SelectFromList");
			
			ccRootObject.comboBox("Select", "AssignmentUserList",
					data_Assignment.get("AssigneeToSelectFromList"));
			
			ccRootObject.button("Click", "AssignButton");
			
		} else {
			//Find a user or group
			ccRootObject.radioButton("Select", "FindUserGroupOrQueue");

			//Search For
			ccRootObject.comboBox("Select", "SearchFor", data_Assignment.get("SearchFor"));
			
			if (data_Assignment.get("SearchFor").equalsIgnoreCase("User")) {
				// SearchUser
				searchAssignmentUser(data_Assignment.get("User"));
			}
			if (data_Assignment.get("SearchFor").equalsIgnoreCase("Group")) {
				
				Hashtable<String, String> data_Group = ClaimCenterData.getData(
						"Groups", data_Assignment.get("Groups"));

				// Select first contact from result
				if (!searchAndSelectGroup(data_Group.get("GroupName"), "")) {
					throw new Exception("Assignment Group not found");
				}
			}
			//TODO: Check if any validation rules hit
		}
		String validationSpanText = ccRootObject.htmlText("Get", "ValidationMessages");
		if (validationSpanText != null) {
			throw new Exception ( validationSpanText );					
		}
		else {
			logger.log_Message("Assignment Done", MessageType.INFO);
		}
	}

	/**
	 * Searches the User for assignment
	 * 
	 * @param userIndex
	 *            - Index of the Users
	 * @throws Exception
	 */
	public void searchAssignmentUser(String userIndex) throws Exception {
		
		Hashtable<String, String> data_User = ClaimCenterData.getData("Users", userIndex);

		//If user name is specified, we need not other fields
		if ( data_User.get("UserName").trim().isEmpty() ) {
			
			ccRootObject.editBox("Input", "FirstName", data_User.get("FirstName"));
			ccRootObject.editBox("Input", "LastName", data_User.get("LastName"));
			
			if (!data_User.get("Groups").trim().isEmpty()) {
				Hashtable<String, String> data_Group = ClaimCenterData.getData("Groups",
						data_User.get("Groups"));
				
				// Search group if not found in the list
				if (ccRootObject.comboBox("Verify", "UserGroup",data_Group.get("GroupName")).startsWith("PASS")) {
					ccRootObject.comboBox("Select", "UserGroup", data_Group.get("GroupName"));
					}
				else {
					// Click Menu
					ccRootObject.clickButtonToOpenContextMenu("GroupMenuIconBtn","GroupMenuIconMenuList");
					ccRootObject.menu("Click", "GroupMenuIconMenuList", new String[] { "Search for Group..." });
					if (!searchAndSelectGroup(data_Group.get("GroupName"),data_Group.get("Type"))) {
						throw new Exception("Group " + data_Group.get("GroupName")
							+ " not found for the user "
							+ data_User.get("UserName"));
					}
				}
			}
		}
		else {
			//Enter user name. We do not need other fields
			ccRootObject.editBox("Input", "UserName", data_User.get("UserName"));
		}

		// Select first contact from result
		ccRootObject.button("Click", "SearchBtn");
		// Search Table
		TextGuiTestObject resultTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "UserSearchTable");
		
		// Find the first record
		TextGuiTestObject firstItemSelectBtn = ccRootObject.findExcelMappedTestObject(
				resultTable,
				"SearchResultAssignButton");
		
		if (firstItemSelectBtn != null) {
			firstItemSelectBtn.click();
			ccRootObject.waitForBrowserReady();
		} else {
			throw new Exception("Assignment User not found");
		}
	}

	/**
	 * Search the group and select first row from the result
	 * 
	 * @param groupIndex
	 *            Group Name from Groups Table
	 * @return true if a group found else false
	 * @throws Exception
	 */
	private Boolean searchAndSelectGroup(String groupName, String groupType) throws Exception {

		Boolean groupSelected = false;
		// Group Name
		ccRootObject.editBox("Input", "GroupName", groupName);
		// Group Type
		ccRootObject.comboBox("Select", "GroupType", groupType);
		// Click Search Button
		ccRootObject.button("Click", "SearchBtn");
		// Search Table
		TextGuiTestObject resultTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "GroupSearchResultTable");
		
		TextGuiTestObject firstItemSelectBtn = ccRootObject.findExcelMappedTestObject(
				resultTable, "SearchResultAssignButton");
		
		if (firstItemSelectBtn != null) {
			firstItemSelectBtn.click();
			ccRootObject.waitForBrowserReady();
			groupSelected = true;
		} else {
			// No item found
			groupSelected = false;
		}
		return groupSelected;
	}
}
