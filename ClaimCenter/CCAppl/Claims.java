package CCAppl;
import resources.CCAppl.ClaimsHelper;
import Utilities.Logger;
import Utilities.Logger.MessageType;

import com.rational.test.ft.object.interfaces.TextGuiTestObject;

/**
 * Implements the Claim Level operations 
 * @author Vinayak Hegde	
 */
public class Claims extends ClaimsHelper
{
	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Class and assigns root object and logger
	 * 
	 * @param rootObject
	 *            - root Test object
	 * @param log
	 *            - Logger instance to log the messages
	 * @since 2012/06/11
	 */
	public Claims(UIObjectsAPI rootObject, Logger log) {
		ccRootObject = rootObject;
		logger = log;
	}
	
	/**
	 * Open the Claim number specified
	 * @param claimNo
	 * @throws Exception 
	 */
	public Boolean openClaim(String claimNo) throws Exception {
		Boolean result = false;		
		
		logger.log_Message("Opening Claim " + claimNo, MessageType.INFO);
		ccRootObject.link("Click", "ClaimMenuBtn");
		//ccRootObject.editBox("Input", "ClaimMenuFindClaim", dataIndex.replace("-", "").replace(" ", ""));
		ccRootObject.editBox("Input", "ClaimMenuFindClaim", claimNo);
		ccRootObject.button("Click", "ClaimMenuFindClaimButton");
		//Check if the claim is opened
		TextGuiTestObject claimsTab = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
		"ClaimsTab");
		String claimTabLabel = claimsTab.getProperty(".text").toString();
		if ( claimTabLabel.replace("-", "").replace(" ", "").contains(
				claimNo.replace("-", "").replace(" ", "")) ){
			result = true;
		}
		else {
			//Get the validation error
			String validationSpanText = ccRootObject.htmlText("Get", "ValidationMessages");
			if (validationSpanText != null) {
				logger.log_Message( validationSpanText , Logger.MessageType.ERROR);					
			}
		}
		
		return result;
	}

	/**
	 * TODO: Assign the opened claim to specified user/group
	 * @param assignmentIndex
	 * @throws Exception
	 */
	public Boolean assignClaim(String assignmentIndex) throws Exception {
		Assignment assignment = new Assignment(ccRootObject, logger);
		assignment.doAssignment(assignmentIndex);
		return true;
	}

	/**
	 * TODO: Close the specified claim
	 * @param closingNotes
	 * @throws Exception
	 */
	public Boolean closeClaim(String closingNotes) throws Exception {
		return true;
	}
	
	/**
	 * Verify the Claim Status
	 * @param expectedStatus - Status to verify - Open, Close
	 * @throws Exception
	 */
	public Boolean verifyClaimStatus(String expectedStatus) throws Exception {
		//Click Summary
		ccRootObject.link("Click", "ClaimSummary");
		//Click on Claim Status
		ccRootObject.button("Click", "ClaimSummaryClaimStatus");
		
		Boolean claimStatusMatched = ccRootObject.VerifyTextMessage(expectedStatus);
		if ( claimStatusMatched )
		{
			logger.log_Message("Expected Claim Status '" + expectedStatus + "' Matched", Logger.MessageType.INFO);
		}
		else {
			logger.log_Message("Expected Claim Status '" + expectedStatus + "' did not Matched", Logger.MessageType.ERROR);
		}
		
		return claimStatusMatched;
	}
}

