package CCAppl;

import java.util.Hashtable;


import TestData.ClaimCenterData;
import Utilities.CCUtility;
import Utilities.Logger;
import Utilities.Logger.MessageType;

import resources.CCAppl.DocumentsHelper;
import com.rational.test.ft.*;
import com.rational.test.ft.object.interfaces.*;
import com.rational.test.ft.object.interfaces.SAP.*;
import com.rational.test.ft.object.interfaces.WPF.*;
import com.rational.test.ft.object.interfaces.dojo.*;
import com.rational.test.ft.object.interfaces.siebel.*;
import com.rational.test.ft.object.interfaces.flex.*;
import com.rational.test.ft.object.interfaces.generichtmlsubdomain.*;
import com.rational.test.ft.script.*;
import com.rational.test.ft.value.*;
import com.rational.test.ft.vp.*;
import com.ibm.rational.test.ft.object.interfaces.sapwebportal.*;

/**
 * Description   : Functional Test Script
 * @author Vinayak Hegde
 */
public class Documents extends DocumentsHelper
{
	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Contacts and initiates root Test object and Logger
	 * 
	 * @param rootObject
	 *            - Root test object
	 * @param log
	 *            - Logger instance
	 */
	public Documents(UIObjectsAPI rootObject,Logger log){
		ccRootObject = rootObject;
		logger =log;
	}

	/**
	 * Attach existing document(AED) in Opened Claim
	 * @param docIndex
	 *            - Data Index for Documents in "Documents" sheet
	 * @throws Exception 
	 */
	public void attactExistingDocument(String docIndex) throws Exception{
		logger.log_Message("Attaching a existing document", MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_doc = ClaimCenterData.getData("Documents",docIndex);

		//Actions->Attach an existing document
		String[] documentsMenuItemsList = new String[] {"Attach an existing document to this claim"     };

		ccRootObject.button("Click", "ActionsButton");
		ccRootObject.menu("Click", "MenuActionsList", documentsMenuItemsList);

		if(!data_doc.get("DocumentPath").trim().isEmpty()){

			//Click on Browse button
			ccRootObject.button("Click","DocumentBrowse");
			TestObject chooseFileWindow = RootTestObject.getRootTestObject().find(
					atChild(".class", "#32770", ".processName", "iexplore.exe"))[0];
			ccRootObject.editBoxInTable("Input", chooseFileWindow, null, "FileNameEditBox", data_doc.get("DocumentPath"));
			ccRootObject.buttonInTable("Click", chooseFileWindow, "DialogOpenBtn");
		}

		//Enter the description
		ccRootObject.editBox("Input", "DocumentDescription", data_doc.get("Description"));

		//Select the Document Type
		ccRootObject.comboBox("Select","DocumentMIMEType",data_doc.get("DocumentType"));

		//Select Related to
		ccRootObject.comboBox("Select","RelatedTo",data_doc.get("RelatedTo"));

		//Enter the Author
		ccRootObject.editBox("Input","DocumentAuthor",data_doc.get("Author"));

		//Enter the Recipient
		ccRootObject.editBox("Input","DocumentRecipient",data_doc.get("Recipient"));

		//Whether the document is inbound or not?
		if(!data_doc.get("Inbound").trim().isEmpty()){
			if(data_doc.get("Inbound").trim().equalsIgnoreCase("yes")){	
				ccRootObject.radioButton("Check","DocumentInboundYes");
			}
			else if(data_doc.get("Inbound").trim().equalsIgnoreCase("no")){
				ccRootObject.radioButton("Check","DocumentInboundNo");
			}
			else{
				throw new Exception("Invalid input for 'Inbound'");
			}
		}
		//Select Status
		ccRootObject.comboBox("Select","DocumentStatus",data_doc.get("Status"));

		//Select Security Type;
		ccRootObject.comboBox("Select","DocumentSecurityType",data_doc.get("SecurityType"));

		//Select Type
		ccRootObject.comboBox("Select","DocumentType",data_doc.get("Type"));

		//Click on update button
		ccRootObject.button("Click","UpdateButton");

		//check if document is created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while attaching  '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("Document " + data_doc.get("Name") + " attached successully", MessageType.INFO);
	}

	/**
	 * Search for a document in Opened Claim
	 * @param docIndex
	 *            - Data Index for Documents in "Documents" sheet
	 * @throws Exception 
	 */
	public void searchDocument(String docIndex) throws Exception{
		logger.log_Message("Searching an existing document", MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_doc = ClaimCenterData.getData("Documents",docIndex);

		//Select the related to
		ccRootObject.comboBox("Select","RelatedTo",data_doc.get("RelatedTo"));

		//Select Status
		ccRootObject.comboBox("Select","DocumentStatus",data_doc.get("Status"));

		//Select DocumentType
		ccRootObject.comboBox("Select","SearchDocumentDoctype",data_doc.get("DocumentType"));

		//Select Section
		ccRootObject.comboBox("Select","DocumentSection",data_doc.get("Section"));

		//Enter the name 
		ccRootObject.editBox("Input", "SearchDocumentNameOrId", data_doc.get("Name"));

		//Enter the Author
		ccRootObject.editBox("Input","DocumentAuthor",data_doc.get("Author"));

		//Click on Search button
		ccRootObject.button("Click","DocumentSearchBtn");

		//check if document  search successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while searching the document  '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("Document search was successul", MessageType.INFO);
	}

	/**
	 * Edit a document in Opened Claim
	 * @param docIndex
	 *            - Data Index for Documents in "Documents" sheet
	 * @throws Exception 
	 */
	public void editDocument(String docIndex) throws Exception{
		logger.log_Message("Editing an existing document", MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_doc = ClaimCenterData.getData("Documents",docIndex);

		//Open a document from table
		openDocumentFromTable(data_doc.get("Name"));

		//Click on edit button
		ccRootObject.button("Click","EditButton");

		//Enter the name 
		ccRootObject.editBox("Input", "DocumentName", data_doc.get("Name"));

		//Enter the description
		ccRootObject.editBox("Input","DocumentDescription",data_doc.get("Description"));

		//Select Section
		ccRootObject.comboBox("Select","DocumentSection",data_doc.get("Section"));

		//Select the Document Type
		ccRootObject.comboBox("Select","DocumentMIMEType",data_doc.get("DocumentType"));

		//Select Related to
		ccRootObject.comboBox("Select","RelatedTo",data_doc.get("RelatedTo"));

		//Enter the Author
		ccRootObject.editBox("Input","DocumentAuthor",data_doc.get("Author"));

		//Enter the Recipient
		ccRootObject.editBox("Input","DocumentRecipient",data_doc.get("Recipient"));

		//Whether the document is inbound or not?
		if(data_doc.get("Inbound").trim().equalsIgnoreCase("yes")){	
			ccRootObject.radioButton("Check","DocumentInboundYes");
		}
		else if(data_doc.get("Inbound").trim().equalsIgnoreCase("No")){
			ccRootObject.radioButton("Check","DocumentInboundYes");
		}

		//Select Status
		ccRootObject.comboBox("Select","DocumentStatus",data_doc.get("Status"));

		//Select Security Type;
		ccRootObject.comboBox("Select","DocumentSecurityType",data_doc.get("SecurityType"));

		//Select Type
		ccRootObject.comboBox("Select","DocumentType",data_doc.get("Type"));

		//Check whether  to hide or not?
		if(data_doc.get("Hidden").trim().equalsIgnoreCase("Yes")){	
			ccRootObject.radioButton("Check","DocumentHiddenYes");
		}

		//Check whether to upload changes
		if(data_doc.get("UploadChanges").trim().equalsIgnoreCase("Yes")){	
			ccRootObject.radioButton("Check","UploadChangesYes");

			if(!data_doc.get("DocumentPath").trim().isEmpty()){

				//Click on Browse button
				ccRootObject.button("Click","DocumentBrowse");
				TestObject chooseFileWindow = RootTestObject.getRootTestObject().find(
						atChild(".class", "#32770", ".processName", "iexplore.exe"))[0];
				ccRootObject.editBoxInTable("Input", chooseFileWindow, null, "FileNameEditBox", data_doc.get("DocumentPath"));
				ccRootObject.buttonInTable("Click", chooseFileWindow, "DialogOpenBtn");
			}
		}

		//Click on update button
		ccRootObject.button("Click","Document:UpdateBtn");
		//check if document  edit was successful
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while editing  '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("Document " + data_doc.get("Name") + " edited successully", MessageType.INFO);
	}

	/**
	 * verify a document in Opened Claim
	 * @param docIndex
	 *            - Data Index for Documents in "Documents" sheet
	 * @throws Exception 
	 */
	public void verifyDocument(String docIndex) throws Exception{
		logger.log_Message("verifying an existing document", MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_doc = ClaimCenterData.getData("Documents",docIndex);

		//Open a document from table
		openDocumentFromTable(data_doc.get("Name"));

		//Verify the name 
		ccRootObject.editBox("Verify", "DocumentName", data_doc.get("Name"));

		//Verify the description
		ccRootObject.editBox("Verify","DocumentDescription",data_doc.get("Description"));

		//Verify Section
		ccRootObject.comboBox("Verify","DocumentSection",data_doc.get("Section"));

		//Verify the Document Type
		ccRootObject.comboBox("Verify","DocumentDocumentType",data_doc.get("DocumentType"));

		//Verify Related to
		ccRootObject.comboBox("Verify","RelatedTo",data_doc.get("RelatedTo"));

		//Verify the Author
		ccRootObject.editBox("Verify","DocumentAuthor",data_doc.get("Author"));

		//Verify the Recipient
		ccRootObject.editBox("Verify","DocumentRecipient",data_doc.get("Recipient"));

		//Verify Status
		ccRootObject.comboBox("Verify","DocumentStatus",data_doc.get("Status"));

		//Verify Security Type;
		ccRootObject.comboBox("Verify","DocumentSecurityType",data_doc.get("SecurityType"));

		//Verify Type
		ccRootObject.comboBox("Verify","DocumentType",data_doc.get("Type"));
	}

	/**
	 * Opens a  document from the table.
	 * @param document name
	 * @throws Exceptions*/
	private void openDocumentFromTable(String docName) throws Exception{
		//Find  document table
		TextGuiTestObject documentTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
		"DocumentTable");

		//Find document based on Name
		TextGuiTestObject documentNametObject =  ccRootObject.findDescendantTestObject(
				documentTable , ".class", "Html.A", ".text", docName);

		if ( documentNametObject == null) {
			throw new Exception ("Document with NAme " + docName+ " Not found"); 
		}
		//Get the row object with Document
		String rowID = documentNametObject.getProperty(".id").toString();
		TextGuiTestObject documentRow =  ccRootObject.findDescendantTestObject(
				documentTable, ".class", "Html.TR", ".id", rowID.replace("Name", "2"));

		//click link in  table
		ccRootObject.linkinTable("Click",documentRow,"DocumentNameLink");
	}


	/**
	 * deletes a  document from the table.
	 * @param doc Index
	 * @throws Exception 
	 * @throws Exceptions
	 * */

	public void deleteDocument(String docIndex) throws Exception{
		logger.log_Message("verifying an existing document", MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_doc = ClaimCenterData.getData("Documents",docIndex);

		//Find  document table
		TextGuiTestObject documentTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
		"DocumentTable");

		//Find document based on Name
		TextGuiTestObject documentNametObject =  ccRootObject.findDescendantTestObject(
				documentTable , ".class", "Html.A", ".text", data_doc.get("Name"));
		if ( documentNametObject == null) {
			throw new Exception ("Document with NAme " + data_doc.get("Name")+ " Not found"); 
		}

		//Get the row object with Document
		String rowID = documentNametObject.getProperty(".id").toString();
		TextGuiTestObject documentRow =  ccRootObject.findDescendantTestObject(
				documentTable, ".class", "Html.TR", ".id", rowID.replace("Name", "9"));

		//click link in  table
		ccRootObject.linkinTable("Click",documentRow,"DeleteNote");
	}	

	/**
	 * Link Document: Indicates the existanse of  a document
	 * @param docIndex
	 * throws Exception
	 * @author Vinayak Hegde
	 * @date 06/05/2012
	 */
	public void indicateExistanceofDocument(String docIndex) throws Exception{
		logger.log_Message("Indicate the Existance of Document", MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_doc = ClaimCenterData.getData("Documents",docIndex);

		//Actions->Attach an existing document
		String[] documentsMenuItemsList = new String[] {"Indicate existance of a perticular document"     };

		ccRootObject.button("Click", "ActionsButton");
		ccRootObject.menu("Click", "MenuActionsList", documentsMenuItemsList);

		//Enter the name 
		ccRootObject.editBox("Input", "DocumentName", data_doc.get("Name"));

		//Enter the description
		ccRootObject.editBox("Input", "DocumentDescription", data_doc.get("Description"));

		//Enter the description
		ccRootObject.editBox("Input", "DocumentDescription", data_doc.get("Description"));

		//Select the Document Type
		ccRootObject.comboBox("Select","DocumentMIMEType",data_doc.get("DocumentType"));

		//Select Related to
		ccRootObject.comboBox("Select","RelatedTo",data_doc.get("RelatedTo"));

		//Enter the Author
		ccRootObject.editBox("Input","DocumentAuthor",data_doc.get("Author"));

		//Enter the Recipient
		ccRootObject.editBox("Input","DocumentRecipient",data_doc.get("Recipient"));

		//Whether the document is inbound or not?
		if(!data_doc.get("Inbound").trim().isEmpty()){
			if(data_doc.get("Inbound").trim().equalsIgnoreCase("yes")){	
				ccRootObject.radioButton("Check","DocumentInboundYes");
			}
			else if(data_doc.get("Inbound").trim().equalsIgnoreCase("no")){
				ccRootObject.radioButton("Check","DocumentInboundNo");
			}
		}
		//Select Status
		ccRootObject.comboBox("Select","DocumentStatus",data_doc.get("Status"));

		//Select Security Type;
		ccRootObject.comboBox("Select","DocumentSecurityType",data_doc.get("SecurityType"));

		//Select Type
		ccRootObject.comboBox("Select","DocumentType",data_doc.get("Type"));

		//Click on update button
		ccRootObject.button("Click","UpdateButton");

		//check if document is created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors creating the new link '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
	}
}

