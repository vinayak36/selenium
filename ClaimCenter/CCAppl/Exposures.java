package CCAppl;
import java.util.Hashtable;

import resources.CCAppl.ExposuresHelper;
import TestData.ClaimCenterData;
import Utilities.Logger;
import Utilities.Logger.MessageType;

import com.rational.test.ft.UnsupportedActionException;
import com.rational.test.ft.object.interfaces.*;
import com.rational.test.ft.script.Anchor;
import com.rational.test.ft.script.ITestObjectMethodState;
import com.rational.test.ft.script.MouseModifiers;

/**
 * Implements the ClaimCenter Exposures Module
 * @author Vinayak Hegde
 */
public class Exposures extends ExposuresHelper
{
	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Class and assigns root object and logger
	 * 
	 * @param rootObject
	 *            - root Test object
	 * @param log
	 *            - Logger instance to log the messages
	 * @since 2012/04/30
	 */
	public Exposures(UIObjectsAPI rootObject, Logger log) {
		ccRootObject = rootObject;
		logger = log;
	}	

	/**
	 * Create New Exposure
	 * @param chooseBy
	 * 			- Option to Select the Menu item to create new exposure
	 * @param exposureIndex
	 * 			- Data Index for the exposure to create
	 * @throws Exception 
	 */
	public void createNewExposure(String exposureIndex) throws Exception {

		Hashtable<String, String> data_Exposure = ClaimCenterData.getData("Exposures", exposureIndex);
		logger.log_Message("Creating New Exposure for Coverage " + data_Exposure.get("Coverage"), MessageType.INFO);
		String[] newExposureMenuItemsList = null;
		if ( data_Exposure.get("CoverageSubtype").equalsIgnoreCase( data_Exposure.get("PrimaryCoverage")) ||
				data_Exposure.get("CoverageSubtype").trim().isEmpty() ) {
			//No CoverageSubtype
			newExposureMenuItemsList = new String[] {data_Exposure.get("Coverage"), data_Exposure.get("PrimaryCoverage")};
		} else {
			//Selects CoverageSubtype
			newExposureMenuItemsList = new String[] {
					data_Exposure.get("Coverage"),
					data_Exposure.get("PrimaryCoverage"),
					data_Exposure.get("CoverageSubtype")};
		}
		ccRootObject.button("Click", "ActionsButton");
		ccRootObject.menu("Click", "MenuActionsList", newExposureMenuItemsList);

		//Get the MainContent Window.
		TestObject mainContent = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "MainContent");

		if(data_Exposure.get("ExposureType").contains("Vehicle")){
			vehicleExposure(data_Exposure,mainContent);
		}
		else if(data_Exposure.get("ExposureType").contains("Property")){
			propertyExposure(data_Exposure);
		}
		else{
			throw new Exception("Check Exposure Type");
		}		

		//Click Update to save the Exposure
		ccRootObject.button("Click", "UpdateButton");
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating Exposure '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		//Click if Warnings are displayed, if yes close them and update again
		if ( clearWarnings()) {
			logger.log_Message("Warnings displayed while saving Exposure and Closed", Logger.MessageType.WARNING);
			ccRootObject.button("Click", "UpdateButton");
		}
		logger.log_Message("Exposure Coverage " + data_Exposure.get("Coverage") + 
				"with Primary Coverage " + data_Exposure.get("PrimaryCoverage") + 
				" and Sub Coverage Type " + data_Exposure.get("CoverageSubtype") +
				" created successfully", MessageType.INFO);
	}

	/**
	 * @param data_Exposure
	 * @param mainContent
	 * @throws Exception 
	 */
	private void propertyExposure(Hashtable<String, String> data_Exposure) throws Exception {
		//Get the MainContent Window
		TestObject mainContent = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "MainContent");


		logger.log_Message("Add Claimant information", MessageType.INFO);

		/*claimant*/
		//Loss Party
		if ( ccRootObject.comboBox("Verify", "LossParty	", data_Exposure.get("LossParty")).startsWith("PASS") ) {
			ccRootObject.comboBox("Select", "LossParty", data_Exposure.get("LossParty"));
		}

		//Claimant Name
		if(ccRootObject.comboBox("Verify", "ClaimantName", data_Exposure.get("ClaimantName")).startsWith("PASS")){
			ccRootObject.comboBox("Select", "ClaimantName", data_Exposure.get("ClaimantName"));
		}

		//Claimant Type
		if(ccRootObject.comboBox("Verify", "ClaimantType", data_Exposure.get("ClaimantType")).startsWith("PASS")){
			ccRootObject.comboBox("Select", "ClaimantType", data_Exposure.get("ClaimantType"));
		}


		//Other Financial Info

		if(ccRootObject.comboBox("Verify", "otherFinancialInfo", data_Exposure.get("OtherFinancialInfo")).startsWith("PASS")){
			ccRootObject.comboBox("Select", "otherFinancialInfo", data_Exposure.get("OtherFinancialInfo"));
		}


		//Verify : ClaimnatAddress
		ccRootObject.VerifyTextField(mainContent, "ClaimnantAddressText", data_Exposure.get("ClaimantAddress"));

		//Add alternate address
		ccRootObject.comboBox("Select", "AlternateContact", data_Exposure.get("AlternateContact"));

		/*Exposure*/
		//Verify Primary Coverage
		ccRootObject.VerifyTextField(mainContent, "PrimaryCoverageText", data_Exposure.get("PrimaryCoverage"));


		//Verify SubCoverage
		ccRootObject.VerifyTextField(mainContent, "CoverageSubtypeText", data_Exposure.get("CoverageSubtype"));



		//Kind of Loss
		if ( ccRootObject.comboBox("Verify", "kindOfLoss", data_Exposure.get("KindofLoss")).startsWith("PASS") ) {
			ccRootObject.comboBox("Select", "kindOfLoss", data_Exposure.get("KindofLoss"));
		}

		//insured vehicle info
		if ( ccRootObject.comboBox("Verify", "ExposureCoverage", data_Exposure.get("Coverage")).startsWith("PASS") ) {
			ccRootObject.comboBox("Select", "ExposureCoverage", data_Exposure.get("Coverage"));
		}

		//Letter generation
		if ( data_Exposure.get("GenerateAutomaticLetters").equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "GenerateAutomaticLettersYes");
		}
		else if( data_Exposure.get("GenerateAutomaticLetters").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "LimitedWaiverDepreciationNo");
		}

		//Injury Incident
		if ( !data_Exposure.get("InjuryIncident").trim().isEmpty() ) {
			//TODO: Injury Incident
			Hashtable<String, String> data_InjuryIncident = ClaimCenterData.getData("InjuryIncidents",
					data_Exposure.get("InjuryIncident"));

			String injuryIncident = data_InjuryIncident.get("InjuredPerson") + " " +
			data_InjuryIncident.get("Severity") + " " +
			data_InjuryIncident.get("DetailedInjuryType");

			if ( ccRootObject.comboBox("Verify", "InjuryIncident", injuryIncident).startsWith("PASS") ) {
				ccRootObject.comboBox("Select", "InjuryIncident", injuryIncident);
			}
			else {
				//Create New Injury Incident
				ccRootObject.clickButtonToOpenContextMenu("InjuryIncidentPickerBtn", "InjuryIncidentMenuList");
				ccRootObject.menu("Click", "InjuryIncidentMenuList", new String[] {"New Incident..."});
				Incidents iIncident = new Incidents( ccRootObject,logger);
				iIncident.injuryLiability(data_Exposure.get("InjuryIncident"));
			}
		}

		if ( !data_Exposure.get("VehicleIncident").trim().isEmpty() ) {

			if ( ccRootObject.comboBox("Verify", "vehicleIncident", data_Exposure.get("Coverage")).startsWith("PASS") ) {
				ccRootObject.comboBox("Select", "vehicleIncidentIncident", data_Exposure.get("Coverage"));
			}
			else {
				//Create New vehicle Incident
				ccRootObject.clickButtonToOpenContextMenu("vehicleIncidentPickerBtn", "vehicleIncidentMenuList");
				ccRootObject.menu("Click", "vehicleIncidentMenuList", new String[] {"New Incident..."});
				Incidents vIncident = new Incidents( ccRootObject,logger);
				vIncident.addVehicleIncidentAUTO(data_Exposure.get("VehicleIncident"));
			}
		}

		//Property Incident
		if ( !data_Exposure.get("PropertyIncident").trim().isEmpty() ) {
			//TODO: Injury Incident
			//Hashtable<String, String> data_propertyIncident = ClaimCenterData.getData("PropertyIncident ",
			//	data_Exposure.get("PropertyIncident "));

			/*String propertyIncident = data_propertyIncident.get("PropertyIncident") + " " +
			data_propertyIncident.get("Severity") + " " +
			data_propertyIncident.get("DetailedInjuryType");
			 */
			//if ( ccRootObject.comboBox("Verify", "InjuryIncident", propertyIncident).startsWith("PASS") ) {
			//	ccRootObject.comboBox("Select", "InjuryIncident", propertyIncident);
			//}
			//	else {
			//Create New Property Incident
			ccRootObject.clickButtonToOpenContextMenu("PropertyIncidentPickerBtn", "PropertyIncidentMenuList");
			ccRootObject.menu("Click", "PropertyIncidentMenuList", new String[] {"New Incident..."});
			Incidents pIncident = new Incidents( ccRootObject,logger);
			pIncident.propertyLiability(data_Exposure.get("propertyIncident"));
			//}
		}
		//Note
		

		// Loss Jurisdiction
		if(ccRootObject.comboBox("Verify", "LossJurisdiction", data_Exposure.get("LossJurisdiction")).startsWith("Pass")){
			ccRootObject.comboBox("Input", "LossJurisdiction", data_Exposure.get("LossJurisdiction"));
		}
		//ClaimBasis
		if(ccRootObject.comboBox("Verify", "ClaimBasis", data_Exposure.get("ClaimBasis")).startsWith("Pass")){
			ccRootObject.comboBox("Input", "ClaimBasis", data_Exposure.get("ClaimBasis"));
		}
		//LossControlCode
		if(ccRootObject.comboBox("Verify", "LossControlCode", data_Exposure.get("LossControlCode")).startsWith("Pass")){
			ccRootObject.comboBox("Input", "LossControlCode", data_Exposure.get("LossControlCode"));
		}
		//Other Insurers
		if(data_Exposure.get("OtherCoverage").equalsIgnoreCase("Yes")){
			ccRootObject.radioButton("Select", "OtherCoverageInfoNo");

			//Other Insurers involvement
			if (!data_Exposure.get("OtherCoverage").trim().isEmpty() ) {
				for( String otherCarrier : data_Exposure.get("OtherCarriers").split(",")) {
					if ( !otherCarrier.trim().isEmpty()) {
						Hashtable<String, String> data_OtherCarrier = ClaimCenterData.getData("OtherCarriers", otherCarrier);
						//Add
						ccRootObject.button("Click", "OtherCoverageAddButton");
						//Insurer
						ccRootObject.editBox("Input", "OtherCoverage", data_OtherCarrier.get("Insurer"));
						ccRootObject.editBox("Input", "OtherCoveragePolicyNumber", data_OtherCarrier.get("PolicyNumber"));
						ccRootObject.editBox("Input", "OtherCoverageContactName", data_OtherCarrier.get("Contact"));
						ccRootObject.editBox("Input", "OtherCoverageContactPhone", data_OtherCarrier.get("Phone"));
						ccRootObject.editBox("Input", "OtherCoverageContactNotes", data_OtherCarrier.get("Notes"));
					}
				}
			}
		}
		else if(data_Exposure.get("OtherCoverage").equalsIgnoreCase("No")){
			ccRootObject.radioButton("Select", "OtherCoverageInfoYes");
		}		


		//Disability
		//Disability benefit
		if(ccRootObject.editBox("Verify", "disabilityBenefit", data_Exposure.get("DisabilityBenefit")).startsWith("Pass")){
			ccRootObject.editBox("Input", "disabilityBenefit", data_Exposure.get("DisabilityBenefit"));
		}

		//Recovery Status
		//Transfer of Status
		if(ccRootObject.editBox("Verify", "transferOfStatus", data_Exposure.get("LossTransferStatus")).startsWith("Pass")){
			ccRootObject.editBox("Input", "transferOfStatus", data_Exposure.get("LossTransferStatus"));
		}

		//Priority Status
		if(ccRootObject.editBox("Verify", "priorityStatus", data_Exposure.get("PriorityStatus")).startsWith("Pass")){
			ccRootObject.editBox("Input", "priorityStatus", data_Exposure.get("PriorityStatus"));
		}

		//Repaired or  Replaced?
		if(ccRootObject.comboBox("Verify", "repairedOrReplaced", data_Exposure.get("RepairedOrReplaced")).startsWith("Pass")){
			ccRootObject.comboBox("Select", "repairedOrReplaced", data_Exposure.get("RepairedOrReplaced"));
		}

		//Salvage status
		if(ccRootObject.comboBox("Verify", "salvageStatus", data_Exposure.get("SalvageStatus")).startsWith("Pass")){
			ccRootObject.comboBox("Select", "salvageStatus", data_Exposure.get("SalvageStatus"));
		}

		//Sub-Rogation status
		if ( ccRootObject.comboBox("Verify", "subrogationStatus", data_Exposure.get("SubrogationStatus")).startsWith("PASS") ) {
			ccRootObject.comboBox("Select", "subrogationStatus", data_Exposure.get("SubrogationStatus"));
		}
	}

	/**
	 * @param data_Exposure
	 * @throws Exception
	 */
	private void vehicleExposure(Hashtable<String, String> data_Exposure, TestObject mainContent)
	throws Exception {
		//Exposure
		//Loss Party
		ccRootObject.comboBox("Select", "LossParty", data_Exposure.get("LossParty"));
		//Kind of Loss
		ccRootObject.comboBox("Select", "KindofLoss", data_Exposure.get("KindofLoss"));
		//Insured Vehicle
		ccRootObject.comboBox("Select", "ExposureCoverage", data_Exposure.get("Coverage"));
		//Generate Automatic Letters
		if ( data_Exposure.get("GenerateAutomaticLetters").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButton("Select", "GenerateAutomaticLettersYes");
		}
		else if (data_Exposure.get("GenerateAutomaticLetters").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButton("Select", "GenerateAutomaticLettersNo");
		}

		//Owner
		//Claimant : It populates when Insured is selected
		if ( !data_Exposure.get("LossParty").equalsIgnoreCase("Insured") ) {
			ccRootObject.comboBox("Select", "ClaimantName", data_Exposure.get("ClaimantName"));
			ccRootObject.comboBox("Select", "ClaimantType", data_Exposure.get("ClaimantType"));
		}
		//Other Financial Interests
		ccRootObject.comboBox("Select", "OtherFinancialInterests", data_Exposure.get("OtherFinancialInterests"));

		//GST/HST Registered? 
		//Other Tax Exemptions? 
		//Contact Prohibited?
		//Alternate Contact
		ccRootObject.comboBox("Select", "AlternateContact", data_Exposure.get("AlternateContact"));

		/*Exposure*/
		//Verify Primary Coverage
		ccRootObject.VerifyTextField(mainContent, "primaryCoverage", data_Exposure.get("PrimaryCoverage"));

		//Verify SubCoverage
		ccRootObject.VerifyTextField(mainContent, "CoverageSubtype", data_Exposure.get("CoverageSubtype"));

		//Verify SubCoverage
		ccRootObject.VerifyTextField(mainContent, "CoverageSubtype", data_Exposure.get("CoverageSubtype"));

		//Incident Overview
		if ( !data_Exposure.get("InjuryIncident").trim().isEmpty() ) {
			//Injury Incident
			Hashtable<String, String> data_InjuryIncident = ClaimCenterData.getData("InjuryIncidents",
					data_Exposure.get("InjuryIncident"));

			String injuryIncident = data_InjuryIncident.get("InjuredPerson") + " " +
			data_InjuryIncident.get("Severity") + " " +
			data_InjuryIncident.get("DetailedInjuryType");

			if ( ccRootObject.comboBox("Verify", "InjuryIncident", injuryIncident).startsWith("PASS") ) {
				ccRootObject.comboBox("Select", "InjuryIncident", injuryIncident);
			}
			else {
				//Create New Injury Incident
				ccRootObject.clickButtonToOpenContextMenu("InjuryIncidentPickerBtn", "InjuryIncidentMenuList");
				ccRootObject.menu("Click", "InjuryIncidentMenuList", new String[] {"New Incident..."});
				Incidents incident = new Incidents( ccRootObject,logger);
				incident.addInjuryIncident(data_Exposure.get("InjuryIncident"));
			}
		}

		if ( !data_Exposure.get("VehicleIncident").trim().isEmpty() ) {
			Hashtable<String, String> data_VehicleIncident = ClaimCenterData.getData("VehicleIncidents",
					data_Exposure.get("VehicleIncident"));

			String vehcile = "";
			if ( data_VehicleIncident.get("LossParty").equalsIgnoreCase("Insured")) {
				vehcile = data_VehicleIncident.get("SelectVehicle");
			}
			else {
				vehcile = data_VehicleIncident.get("Year") + " " +
				data_VehicleIncident.get("Make") + " " +
				data_VehicleIncident.get("Model");
			}
			if ( ccRootObject.comboBox("Verify", "VehicleIncident", vehcile).startsWith("PASS"))
			{
				ccRootObject.comboBox("Select", "VehicleIncident", vehcile);
			}
			else {
				ccRootObject.clickButtonToOpenContextMenu("VehicleIncidentPickerBtn", "VehicleIncidentMenuList");
				ccRootObject.menu("Click", "VehicleIncidentMenuList", new String[] {"New Incident..."});
				Incidents incident = new Incidents( ccRootObject, logger);
				incident.addVehicleIncidentAUTO(data_Exposure.get("VehicleIncident"));
			}
		}

		if ( !data_Exposure.get("PropertyIncident").trim().isEmpty() ) {
			//TODO
		}

		//Notes
		Notes note = new Notes(ccRootObject, logger);
		note.fillNoteData(data_Exposure.get("Notes"), ccRootObject.getBrowserDocObject());

		//Recovery Status
		//Salvage Status
		if ( ccRootObject.comboBox("Verify", "SalvageStatus", data_Exposure.get("SalvageStatus") ).startsWith("PASS")) {
			ccRootObject.comboBox("Select", "SalvageStatus", data_Exposure.get("SalvageStatus") );
		}
		//Subrogation Status
		if ( ccRootObject.comboBox("Verify", "SubrogationStatus", data_Exposure.get("SubrogationStatus")).startsWith("PASS")) {
			ccRootObject.comboBox("Select", "SubrogationStatus", data_Exposure.get("SubrogationStatus"));
		}
	}

	/**
	 * Assigns the specified Exposure to adjuster 
	 * @param exposureIndex
	 * 			- Index of the exposure to assign
	 * @throws Exception
	 */
	public void assignExposure(String exposureIndex) throws Exception {

		//Get Exposure data
		Hashtable<String, String> data_Exposure = ClaimCenterData.getData("Exposures", exposureIndex);
		//Select the Exposure
		selectExposure(
				data_Exposure.get("ExposureType"), data_Exposure.get("PrimaryCoverage"),
				data_Exposure.get("ClaimantName"), data_Exposure.get("KindofLoss"));
		//Click Assign
		ccRootObject.button("Click", "AssignExposures");
		//Assign Exposure
		Assignment assigment = new Assignment(ccRootObject, logger);
		assigment.doAssignment(data_Exposure.get("AssignTo"));
	}

	/**
	 * Assigns all the Exposures to specified adjuster
	 * @param assignmentIndex
	 * @throws Exception 
	 */
	public void assignAllExposures(String assignmentIndex) throws Exception {
		//Select All Exposures
		ccRootObject.checkBox("Check", "SelectAllExposuresCheckBox");
		//Click Assign
		ccRootObject.button("Click", "AssignExposures");
		//Assign Exposure
		Assignment assigment = new Assignment(ccRootObject, logger);
		assigment.doAssignment(assignmentIndex);
	}

	/**
	 * Clicks the Refresh button to refresh exposures
	 * @throws Exception 
	 * 
	 */
	public void refreshExposure() throws Exception {
		//click Refresh button
		ccRootObject.button("Click", "RefreshExposures");
	}

	/**
	 * Closes the Exposure
	 * @param exposureIndex
	 * 			- Index of the Exposure to Close
	 * @throws Exception 
	 */
	public void closeExposure(String exposureIndex) throws Exception {
		logger.log_Message("Closing Exposure " + exposureIndex, MessageType.INFO);
		//Get Exposure data
		Hashtable<String, String> data_Exposure = ClaimCenterData.getData("Exposures", exposureIndex);
		//Select the Exposure
		selectExposure(data_Exposure.get("ExposureType"), data_Exposure.get("PrimaryCoverage"),
				data_Exposure.get("ClaimantName"), data_Exposure.get("KindofLoss"));
		//Click Close Exposure
		ccRootObject.button("Click", "CloseExposures");
		//Enter Note and Outcome and Update
		Hashtable<String, String> data_ClsingNotes = ClaimCenterData.getData("ClosingNotes", data_Exposure.get("ClosingNote"));
		ccRootObject.editBox("Input", "Note", data_ClsingNotes.get("Note"));
		//Outcome
		ccRootObject.comboBox("Select", "Outcome", data_ClsingNotes.get("Outcome"));
		ccRootObject.button("Click", "UpdateButton");

		//Check if the error found
		String validationSpanText = ccRootObject.htmlText("Get", "ValidationMessages");
		if (validationSpanText != null) {
			String screenName = validationSpanText.substring(0, validationSpanText.indexOf(":"));
			logger.log_Message("Found errors while closing Exposure " + screenName, Logger.MessageType.INFO);
			String validationMessages = validationSpanText.replace( screenName, "");
			logger.log_Message("Errors Message(s) '" + validationMessages + "'", Logger.MessageType.ERROR);
		}
		else {
			logger.log_Message("Exposure " + exposureIndex + " closed.", MessageType.INFO);
		}
	}

	/**
	 * TODO: Adds a New Reserve for the exposure
	 * @param exposureIndex
	 * 			- Index of the Exposure to Close 
	 * @throws Exception
	 */
	public void createReserve(String exposureIndex) throws Exception {
		logger.log_Message("Creating Reserve(s) for Exposure " + exposureIndex, MessageType.INFO);
		//Get Exposure data
		Hashtable<String, String> data_Exposure = ClaimCenterData.getData("Exposures", exposureIndex);
		//Select the Exposure
		selectExposure(data_Exposure.get("ExposureType"), data_Exposure.get("PrimaryCoverage"),
				data_Exposure.get("ClaimantName"),data_Exposure.get("KindofLoss"));
		//Reserves Data
		String[] reserves = data_Exposure.get("Reserves").trim().split(",");
		//TODO: Set Reserve
	}

	/**
	 * Prints or Export the Exposure
	 * @param exposureIndex
	 * 			- Index of the Exposure to Print 
	 */
	public void printExport(String exposureIndex) {
		//TODO
	}

	/**
	 * Verifies the exposure details for the exposure specified
	 * @param exposureIndex
	 * 			- Index of the Exposure to Verify 
	 * @throws Exception 
	 */
	public void verifyExposureDetails(String exposureIndex) throws Exception {

		Hashtable<String, String> data_Exposure = ClaimCenterData.getData("Exposures", exposureIndex);
		logger.log_Message("Verifying the Expsoure Details - " + exposureIndex, MessageType.INFO);
		//Select the Exposure
		openExposure(data_Exposure.get("ExposureType"), data_Exposure.get("PrimaryCoverage"),
				data_Exposure.get("ClaimantName"), data_Exposure.get("KindofLoss"));

		logger.log_Message("Found and Opened the Exposure - " +
				data_Exposure.get("ExposureType") + " " +
				data_Exposure.get("PrimaryCoverage") + " " + 
				data_Exposure.get("ClaimantName")  + " " +
				data_Exposure.get("KindofLoss"), MessageType.INFO);

		//Get the MainContent Window.
		TestObject mainContent = ccRootObject.findExcelMappedTestObject(ccRootObject.getBrowserDocObject(), "MainContent");

		//Exposure Details
		int failedFields = 0;
		//Loss Party
		if ( !ccRootObject.VerifyTextField( mainContent,
				"LossPartyText", data_Exposure.get("LossParty"))){
			failedFields++;
		}
		//Primary Coverage
		if (!ccRootObject.VerifyTextField(mainContent, "PrimaryCoverageText", data_Exposure.get("PrimaryCoverage"))){
			failedFields++;
		}
		//Coverage Sub type
		String covSubType = (data_Exposure.get("CoverageSubtype").isEmpty())?
				data_Exposure.get("PrimaryCoverage") : data_Exposure.get("CoverageSubtype");
				if (!ccRootObject.VerifyTextField(mainContent, "CoverageSubtypeText", covSubType)){
					failedFields++;
				}

				//Legislation
				if (!ccRootObject.VerifyTextField(mainContent, "LegislationText", data_Exposure.get("Legislation"))){
					failedFields++;
				}
				//KindofLoss 
				if (!ccRootObject.VerifyTextField(mainContent, "KindofLossText", data_Exposure.get("KindofLoss"))){
					failedFields++;
				}
				//GenerateAutomaticLetters
				if (!ccRootObject.VerifyTextField(mainContent, "GenerateAutomaticLettersText", 
						data_Exposure.get("GenerateAutomaticLetters"))){
					failedFields++;
				}
				//AdjusterLink
				Hashtable<String, String> data_Assignment = ClaimCenterData.getData("Assignments", data_Exposure.get("AssignTo"));
				Hashtable<String, String> data_User = ClaimCenterData.getData("Users", data_Assignment.get("User"));
				String userFullName = data_User.get("FirstName") + " " + data_User.get("LastName");
				if (!ccRootObject.VerifyTextField(mainContent,"AdjusterLink", userFullName)){
					failedFields++;
				}
				//TODO: UnitText
				//Status
				if (!ccRootObject.VerifyTextField(mainContent,"StatusText", data_Exposure.get("Status"))){
					failedFields++;
				}
				//ClaimantText
				if (!ccRootObject.VerifyTextField(mainContent,"ClaimantNameLink", data_Exposure.get("ClaimantName"))){
					failedFields++;
				}
				//ClaimantTypeText
				if (!ccRootObject.VerifyTextField(mainContent, "ClaimantTypeText", data_Exposure.get("ClaimantType"))){
					failedFields++;
				}
				//TODO: VehicleIncidentText
				if (!ccRootObject.VerifyTextField(mainContent,"VehicleIncidentText", data_Exposure.get("Coverage"))){
					failedFields++;
				}
				//TODO DriverText

				if ( failedFields != 0 ) {
					throw new Exception("Verification of Exposure Details Failed");
				}
	}

	/**
	 * Selects the exposure in the Exposures table based on parameters
	 * @param exposureType
	 * 				- Exposure Type
	 * @param primaryCoverage
	 *  			- Primary Coverage
	 * @param claimantName
	 *   			- Claimant Name
	 * @param adjuster
	 *   			- Adjuster Name 
	 * @throws Exception
	 */
	private void selectExposure(String exposureType, String primaryCoverage,
			String claimantName, String kindOfLoss) throws Exception {

		//Filter by Claimant
		ccRootObject.comboBox("Select", "ExposuresFilter", claimantName);

		TestObject exposuretoSelect = findExposure(exposureType, primaryCoverage, kindOfLoss);

		if( exposuretoSelect == null ) {
			throw new Exception("No Exposure found matching with " + exposureType + " " + primaryCoverage +
					" " + claimantName);
		}

		//Click the CheckBox
		ccRootObject.checkBoxinTable("Check", exposuretoSelect, "", "CheckBox");
	}

	/**
	 * Opens the exposure in the Exposures table based on parameters
	 * @param exposureType
	 * 				- Exposure Type
	 * @param primaryCoverage
	 *  			- Primary Coverage
	 * @param claimantName
	 *   			- Claimant Name
	 * @param adjuster
	 *   			- Adjuster Name 
	 * @throws Exception
	 */
	private void openExposure(String exposureType, String primaryCoverage,
			String claimantName, String kindOfLoss) throws Exception {

		//Filter by Claimant
		ccRootObject.comboBox("Select", "ExposuresFilter", claimantName);

		TestObject exposuretoSelect = findExposure(exposureType, primaryCoverage, kindOfLoss);

		if( exposuretoSelect == null ) {
			throw new Exception("No Exposure found matching with " + exposureType + " " + primaryCoverage +
					" " + claimantName);
		}
		ccRootObject.linkinTable("Click", exposuretoSelect, "ExposureOrder");
	}

	/**
	 * Finds the exposure conforming the specified values
	 * @param exposureType
	 * 			- Exposure Type
	 * @param primaryCoverage
	 * 			- Primary Coverage
	 * @param kindOfLoss
	 * 			- Kind of Loss
	 * @param adjuster
	 * 			- Adjuster 
	 * @param Status
	 * 			- Exposure Status 
	 * @return Exposure object, Else Null
	 */
	private TestObject findExposure(String exposureType, String primaryCoverage,
			String kindOfLoss) {

		TestObject exposuresTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
		"ExposuresTable");
		//Get All Rows
		Anchor rowAnchor = atDescendant(".class", "Html.TR");
		TestObject[] allExposuers = exposuresTable.find(rowAnchor, false);
		//Find the exposures 
		TestObject exposuretoSelect = null;
		for( TestObject exposure : allExposuers ) {
			TestObject temp = null;
			exposure.waitForExistence();
			//by specified Types
			temp = ccRootObject.findDescendantTestObject(
					exposure, ".class", "Html.TextNode", ".text", exposureType);
			if ( temp != null) {
				//Filter by Coverage
				temp = ccRootObject.findDescendantTestObject(
						exposure, ".class", "Html.TextNode", ".text", primaryCoverage);
			}
			if ( temp != null) {
				//Filter by Coverage
				temp = ccRootObject.findDescendantTestObject(
						exposure, ".class", "Html.TextNode", ".text", kindOfLoss);
			}

			//Exit if we got the exposure
			if ( temp!= null ) {
				exposuretoSelect = exposure;
				break;
			}
		}
		return exposuretoSelect;
	}

	/**
	 * Closes the Warning message in Workspace area if they are shown
	 * @return - true if the warning messages found
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	private boolean clearWarnings() throws Exception {

		boolean closed = false;
		String clearWarningBtnExists = ccRootObject.button("Verify", "ClearBtn");
		if (clearWarningBtnExists.contains("PASS")) {
			closed = true;
			ccRootObject.button("Click", "ClearBtn");
		}
		return closed;
	}	

	/**
	 * Creates External Assignment ( Rental, Audatex or xactAnalysys)
	 * @param externalAssignmentIndex
	 */
	public void createExternalAssignment(String externalAssignmentIndex) {
		//TODO
	}

	/**
	 * Submits a new External Assignment ( Rental, Audatex or xactAnalysys)
	 * @param externalAssignmentIndex
	 */
	public void createAdnSubmitExternalAssignment(String externalAssignmentIndex) {
		//TODO
	}
}
