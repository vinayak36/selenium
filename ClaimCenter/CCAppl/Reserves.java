package CCAppl;

import java.util.Hashtable;

import resources.CCAppl.ReservesHelper;
import TestData.ClaimCenterData;
import Utilities.Logger;
import Utilities.Logger.MessageType;

import com.rational.test.ft.object.interfaces.TextGuiTestObject;

/**
 * Description   : Functional Test Script
 * @author Vinayak Hegde
 */
public class Reserves extends ReservesHelper
{	
	/**
	 * Script Name   : <b>Reserves</b>
	 * Generated     : <b>Jun 18, 2012 9:34:19 AM</b>
	 * Description   : Functional Test Script
	 * Original Host : WinNT Version 5.1  Build 2600 (S)
	 * 
	 * @since  2012/06/18
	 * @author Vinayak Hegde
	 */

	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Contacts and initiates root Test object and Logger
	 * 
	 * @param rootObject
	 *            - Root test object
	 * @param log
	 *            - Logger instance
	 * @throws Exception 
	 */

	public Reserves(UIObjectsAPI rootObject,Logger log){

		ccRootObject=rootObject;
		logger =log;
	}

	/**
	 * Create New Reserve
	 * @param reIndex
	 * 				-Index of data to be used
	 * @throws Exception */
	public void createReserve(String reIndex) throws Exception{
		logger.log_Message("Creating New Reserve", MessageType.INFO);

		//get data
		Hashtable<String, String> data_Reserve = ClaimCenterData.getData(
				"Reserves", reIndex);

		//Actions->Reserves
		String[] reserveMenuItemsList = new String[] {"Reserve"};

		ccRootObject.button("Click", "ActionsButton");
		ccRootObject.menu("Click", "MenuActionsList",  reserveMenuItemsList);

		//Click on Add button in Set Reserve screen.
		ccRootObject.button("Click","AddButton");



		//Select the Exposure	
		ccRootObject.comboBox("Select","NewReserveExposureType", data_Reserve.get("Exposure").trim());

		//Select the CostType	
		ccRootObject.comboBox("Select","SelectCostType", data_Reserve.get("CostType"));


		//Select the CostCategory	
		ccRootObject.comboBox("Select","NewReserveCostCategory", data_Reserve.get("CostCategory"));

		//Select the ReserveReason	
		ccRootObject.comboBox("Select","NewReserveReason", data_Reserve.get("ReserveReason"));

		//Select the ReserveReason	
		ccRootObject.editBox("Input","NewAvailableReserve", data_Reserve.get("NewAvailableReserve"));

		//Click on "OK " button
		ccRootObject.button("Click","UpdateButton");

		//check if there  are no error messages
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating New Reserve '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}

		//Click on Save
		ccRootObject.button("Click","UpdateButton");

		//check if there  are no error messages
		errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating New Reserve '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}

		logger.log_Message("New Reserve created successully", MessageType.INFO);


	}

	/**
	 * Create New Reserve
	 * @param reIndex
	 * 				-Index of data to be used
	 * @throws Exception */
	public void  verifySummary(String reIndex) throws Exception{
		logger.log_Message("Creating New Reserve", MessageType.INFO);

		//get data
		Hashtable<String, String> data_Reserve = ClaimCenterData.getData(
				"Reserves", reIndex);
		//Click on  the financials in claim
		ccRootObject.link("Click","Financials");

		//Find Summary table
		TextGuiTestObject financialsSummaryTable  = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),"ClaimFinancialsSummryTable");

		//Find Exposure 
		TextGuiTestObject ExposureObject =  ccRootObject.findDescendantTestObject(
				financialsSummaryTable, ".class", "Html.SPAN", ".text", data_Reserve.get("Exposure"));
		if ( ExposureObject == null) {
			throw new Exception ("Activity with Subject " + ExposureObject + " Not found"); 
		}
		//Get the row object with activity
		String rowID = ExposureObject.getProperty(".id").toString();
		TextGuiTestObject ExposureRow =  ccRootObject.findDescendantTestObject(
				financialsSummaryTable, ".class", "Html.TR", ".id", rowID.replace("Exposure", "1"));
		ccRootObject.editBoxInTable("Verify", ExposureRow, null, "Amountlink", data_Reserve.get("RemainingReserve"));
	}
}
