package CCAppl;
import java.util.Hashtable;

import resources.CCAppl.LitigationHelper;
import TestData.ClaimCenterData;
import Utilities.Logger;
import Utilities.Logger.MessageType;

import com.rational.test.ft.*;
import com.rational.test.ft.object.interfaces.*;
import com.rational.test.ft.object.interfaces.SAP.*;
import com.rational.test.ft.object.interfaces.WPF.*;
import com.rational.test.ft.object.interfaces.dojo.*;
import com.rational.test.ft.object.interfaces.siebel.*;
import com.rational.test.ft.object.interfaces.flex.*;
import com.rational.test.ft.object.interfaces.generichtmlsubdomain.*;
import com.rational.test.ft.script.*;
import com.rational.test.ft.value.*;
import com.rational.test.ft.vp.*;
import com.ibm.rational.test.ft.object.interfaces.sapwebportal.*;

/**
 * Description   : Functional Test Script
 * @author Vinayak Hegde
 */
public class Litigation extends LitigationHelper
{
	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Class and assigns root object and logger
	 * 
	 * @param rootObject
	 *            - root Test object
	 * @param log
	 *            - Logger instance to log the messages
	 * @since 2012/26/09
	 */
	public Litigation(UIObjectsAPI rootObject, Logger log) {
		ccRootObject = rootObject;
		logger = log;
	}

	public void ViewMatters(String dataIndex) {
		
	}
	
	public void AssignMatter ( String dataIndex, String assignmentIdx) throws Exception {
		
		//Go to Litigation
		//check the Litigation
		//click Assign
		Assignment asm = new Assignment(ccRootObject, logger);
		asm.doAssignment(assignmentIdx);
	}

	/**
	 * 
	 * @param dataIndex
	 * @throws Exception
	 */
	public void NewMatter(String dataIndex) throws Exception {
		
		Hashtable<String, String> data_Litigation = ClaimCenterData.getData("Litigation", dataIndex);
		
		logger.log_Message("Creating a new Matter", MessageType.INFO);
		
		//Click New Matter
		ccRootObject.button("Click", "NewMatterButton");
		
		//Matter Type
		ccRootObject.comboBox("Select", "MatterType", data_Litigation.get("MatterType"));
		ccRootObject.editBox("Input", "MatterName", data_Litigation.get("Name"));
		ccRootObject.comboBox("Select", "MatterClaimType", data_Litigation.get("ClaimType"));
		//TODO: Fill other info
		
		//Update
		//Click on add button for property Incidents.
		ccRootObject.button("Click", "UpdateButton");

		//check if note is created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating property Incident " + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("Matter - " + data_Litigation.get("Name") + " Created Successfully", MessageType.INFO);
	}
}
