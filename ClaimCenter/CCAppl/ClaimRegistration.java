package CCAppl;

import java.util.Hashtable;

import resources.CCAppl.ClaimRegistrationHelper;
import TestData.ClaimCenterData;
import Utilities.CCUtility;
import Utilities.Logger;
import Utilities.Logger.MessageType;
import Utilities.Logger.ScreenShotEvent;

import com.rational.test.ft.object.interfaces.TextGuiTestObject;

/**
 * Implements the ClaimCenter Claims Registration module(FNOL)
 * 
 * @since 2012/03/08
 * @author Vinayak Hegde
 */
public class ClaimRegistration extends ClaimRegistrationHelper {
	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Class and assigns root object and logger
	 * 
	 * @param rootObject
	 *            - root Test object
	 * @param log
	 *            - Logger instance to log the messages
	 * @since 2012/03/08
	 */
	public ClaimRegistration(UIObjectsAPI rootObject, Logger log) {
		ccRootObject = rootObject;
		logger = log;
	}

	public void searchPolicy(String testStepType, String dataIndex) throws Exception {
		// Get CreateClaim Data
		Hashtable<String, String> dataFNOL_CreateClaim = ClaimCenterData.getData("Create_Claim", dataIndex);
		//Open New Claim Wizard From Tab bar
		openTabMenuItem("ClaimMenuBtn", "ClaimTabMenuList", new String[] { "New Claim" });

		if (searchCreatePolicy(
				dataFNOL_CreateClaim.get("PolicyNumber"), dataFNOL_CreateClaim.get("Product"),
				dataFNOL_CreateClaim.get("DateofLoss"), testStepType)) {
			logger.log_Message("Policy Retrieved Successfully", Logger.MessageType.INFO);
		}
		else {
			throw new Exception ("Policy Not Found or Search Adapter Failed");
		}
	}

	/**
	 * Creates new Claim through FNOL wizard
	 * 
	 * @param testStepType
	 *            - Claim Type to create
	 * @param dataIndex
	 *            - Index of the data to use
	 * @since 2012/06/25
	 */
	public void createNewClaim(String testStepType, String dataIndex) throws Exception {
		// Get CreateClaim Data
		Hashtable<String, String> dataFNOL_CreateClaim = ClaimCenterData.getData("Create_Claim", dataIndex);

		//Open New Claim Wizard From Tab bar
		openTabMenuItem("ClaimMenuBtn", "ClaimTabMenuList", new String[] { "New Claim" });

		// Step 1: Search and Select Policy
		if (searchCreatePolicy(
				dataFNOL_CreateClaim.get("PolicyNumber"), dataFNOL_CreateClaim.get("Product"),
				dataFNOL_CreateClaim.get("DateofLoss"), testStepType)) {

			// Next
			ccRootObject.button("Click", "NextBtn");

			if ( !dataFNOL_CreateClaim.get("PartiesInvolved").trim().isEmpty()) {
				//Parties Involved
				ccRootObject.link("Click", "PartiesInvolvedFW");
				
				PartiesInvolved pv = new PartiesInvolved(ccRootObject, logger);
				//Get the list of Contacts to create
				String[] contacts = dataFNOL_CreateClaim.get("PartiesInvolved").split(",");
				for ( String contact : contacts ) {
					//Create - Contact
					pv.createNewContact(contact);
				}
				//Back
				ccRootObject.link("Click", "Back");
			}

			//TODO - Verify Special Account during FNOL
			if(!dataFNOL_CreateClaim.get("PolicyDetails").trim().isEmpty()){
				//Policy				
				ccRootObject.link("Click", "PolicyWizardStepGroup");
				// Get CreateClaim Data
				Hashtable<String, String> dataFNOL_PolicyDetails = ClaimCenterData.getData("PolicyDetails", dataIndex);
				if(!dataFNOL_PolicyDetails.get("PartiesInvolved").trim().isEmpty()){
					PolicyDetails pds = new PolicyDetails(ccRootObject, logger);
					pds.verifySpecialAccount(dataFNOL_PolicyDetails.get("PartiesInvolved"),testStepType);
				}
				//Back
				ccRootObject.link("Click", "Back");			
			}

			fullFNOLWizard(testStepType, dataFNOL_CreateClaim);

		} else {
			logger.log_Message("Policy Not Found", Logger.MessageType.ERROR);
		}
	}

	/**
	 * Verifies the expected message on Last Wizard page
	 * 
	 * @param expectedMessage
	 *            - Message to Verify
	 * @return True if the expected message found on the page
	 * @since 2012/03/08
	 */
	public String verifyAndGetClaimNo(String expectedMessage) {
		String claimNo = "";

		String expMessage = ClaimCenterData.getData("ExpectedMessages", expectedMessage).get("Message");
		Boolean claimCreated = ccRootObject.VerifyTextMessage(expMessage);

		if (claimCreated) {
			// If Claim Saved, Get the Claim Number
			String completeMsg = ccRootObject.htmlText("Get", "ClaimSavedHeader");
			if (completeMsg != null) {
				claimNo = extractClaimNumber(completeMsg, "");
			}
		} else {
			logger.log_Message("Expected Message '" + expMessage + "' NOT Found", Logger.MessageType.ERROR);
			// Validation Error Message from workspace region
			String validationSpanText = ccRootObject.htmlText("Get", "ValidationMessages");
			if (validationSpanText != null) {
				String screenName = validationSpanText.substring(0, validationSpanText.indexOf(":"));
				logger.log_Message("Found errors " + screenName, Logger.MessageType.ERROR);
				String validationMessages = validationSpanText.replace( screenName, "");
				logger.log_Message("Errors Message '" + validationMessages + "'", Logger.MessageType.ERROR);
			}
			// Errors on Contents region
			String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
			if (errorMessagesContents != null) {
				logger.log_Message("Errors Message '" + errorMessagesContents + "'", Logger.MessageType.ERROR);
			}
			logger.captureScreen(ScreenShotEvent.ERROR);
			claimCreated = false;
		}
		return claimNo;
	}

	/**
	 * Extracts the Claim number from screen message and saves it in Excel file
	 * 
	 * @param msg
	 *            - screen message
	 * @param dataIndex
	 *            - excel row index where the Claim number to save
	 * @since 2012/03/08
	 */
	private String extractClaimNumber(String msg, String dataIndex) {
		String claimNo = "";
		String claimSavedMessage = msg.substring(
				0,
				msg.indexOf("successfully saved.")
				+ "successfully saved.".length());
		// Save the Claim Number is DataSheet
		claimNo = claimSavedMessage.replace("Claim ", "").replace(
				" has been successfully saved.", "");
		logger.log_Message("Claim Number " + claimNo + " created successfully", Logger.MessageType.INFO);
		return claimNo;
	}

	/**
	 * Runs the Full FNOL Wizard to create claim
	 * 
	 * @param dataFNOL_CreateClaim
	 *            - data to create the claim
	 * @throws Exception 
	 * @since 2012/06/25
	 */
	private void fullFNOLWizard(String claimType, Hashtable<String, String> dataFNOL_CreateClaim) throws Exception {

		// Get LossDetails Data
		Hashtable<String, String> dataFNOL_LossDetails = ClaimCenterData.getData(
				"LossDetails",
				dataFNOL_CreateClaim.get("LossDetails"));

		//Step 2:Basic Information
		basicInformation(dataFNOL_LossDetails.get("HowReported"),
				dataFNOL_LossDetails.get("ReportedBy"),
				dataFNOL_LossDetails.get("RelationshipToPolicyholderReportedBy"),
				dataFNOL_LossDetails.get("DateReported"),
				dataFNOL_LossDetails.get("MainContactType"),
				dataFNOL_LossDetails.get("MainContact"),
				dataFNOL_LossDetails.get("RelationshiptoPolicyholderMainContact"),
				dataFNOL_LossDetails.get("InvolvedPolicyVehicles"),
				dataFNOL_LossDetails.get("PreferredMethod"));

		ccRootObject.button("Click", "NextBtn");

		//Close Duplicate Warnings if appeared
		if (closeClaimsWarnings()) {
			logger.log_Message("Duplicate Claims Found and Closed", Logger.MessageType.WARNING);
		}

		// Step 3 of 4: Add Claim Information
		addClaimInformation( claimType, dataFNOL_LossDetails);

		ccRootObject.button("Click", "NextBtn");

		// Step 5 of 5: Save and Assign Claim
		assignment( dataFNOL_CreateClaim.get("AssignTo"), dataFNOL_CreateClaim.get("Note"));

		// Finish
		ccRootObject.button("Click", "FinishBtn");
		logger.log_Message("Finished Full FNOL Wizard", Logger.MessageType.INFO);
	}

	/**
	 * Clicks and Opens the tab menu items specified in array
	 * 
	 * @param tabMenuBtn
	 *            - Tab bar item name
	 * @param menuListID
	 *            - unique ID of the menu item to click
	 * @param tabMenuItems
	 *            - list of hierarchical menu item to click
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	private void openTabMenuItem(String tabMenuBtn, String menuListID,
			String[] tabMenuItems) throws Exception {
		ccRootObject.link("Click", tabMenuBtn);
		ccRootObject.menu("Click", menuListID, tabMenuItems);
	}

	/**
	 * Enters the policy number and claim type and Loss Date and searches it on the screen
	 * 
	 * @param policyNo
	 *            - policy number to search and Policy type
	 * @param product
	 * 			  - Product Dominion or Chieftain
	 * @return - returns true if policy search find the policy
	 * @throws Exception 
	 * @since 2012/06/26
	 */
	private Boolean searchCreatePolicy(String policyNo, String product,
			String dateofLoss, String policyType) throws Exception {

		Boolean policyFound = false;
		//Policy Type
		ccRootObject.comboBox("Select","PolicyType", policyType);

		//Product
		ccRootObject.comboBox("Select","Product", product);
		
		//Get current Date
		String lossDate = CCUtility.convertDateFormat( dateofLoss, "yyyy-MM-dd hh:mm:ss", "MMddyyyy");
		
		//Loss Date
		ccRootObject.editBox("Input", "LossDate", lossDate);
		
		//Policy Number
		ccRootObject.editBox("Input", "SearchPolicyNumber", policyNo);

		ccRootObject.button("Click", "SearchBtn");

		logger.log_Message("Policy Search Screen Opened", Logger.MessageType.INFO);

		// Verify Policy is returned .by checking loss date edit box
		if (ccRootObject.editBox("Verify", "NewClaimLossDate", "").startsWith("PASS")) {
			logger.log_Message("Policy Found and Selected", Logger.MessageType.INFO);
			policyFound = true;
		}
		return policyFound;
	}

	/**
	 * Enters the basic information on the FNOL wizard Basic Information page
	 * 
	 * @param howReported
	 *            - how reported fields data
	 * @param name
	 *            - Reporter Name
	 * @param reporterRelation
	 *            - Relation of policy holder with reporter
	 * @param vehicles
	 *            - policy vehicles to select
	 * @param mainContact
	 *            - main contact type
	 * @param mainContactName
	 *            - main contact name
	 * @param mainContactRelation
	 *            - relation of policy holder with main contact
	 * @throws Exception 
	 * @since 2012/06/25
	 */
	private void basicInformation( String howReported, String name, String reporterRelation,
			String dateReported, String mainContactType, String mainContactName, 
			String mainContactRelation, String involvedPolicyVehicles, String preferredMethod) throws Exception {
		
		// HowReported
		ccRootObject.comboBox("Select", "HowReported", howReported);
		// Reporter Name
		ccRootObject.comboBox("Select", "ReporterName", name);
		
		//Reporter Relation
		ccRootObject.comboBox("Select", "ReporterRelation", reporterRelation);

		//Date Reported
		if ( !dateReported.trim().isEmpty()) {
			ccRootObject.editBox("Input", "DateReported", CCUtility.convertDateFormat(dateReported,
					"yyyy-MM-dd hh:mm:ss", "MMddyyyyhhmma"));
		}
	
		if (mainContactType.equalsIgnoreCase("Same as reporter")) {
			// Check Main Contact
			ccRootObject.radioButton("Select", "MainContactSameasReporter");
		}

		if (mainContactType.equalsIgnoreCase("Different person")) {
			// Check Main Contact
			ccRootObject.radioButton("Select", "MainContactDifferentPerson");
			// Main Contact Name
			ccRootObject.comboBox("Select", "MainContactName", mainContactName);
			// Relation to Insured
			ccRootObject.comboBox("Select", "RelationtoPolicyholder", mainContactRelation);
		}
		
		//Preferred Method
		if ( preferredMethod.equalsIgnoreCase("Phone") ){
			ccRootObject.radioButton("Select", "PreferredMethodPhone");
		}
		else if ( preferredMethod.equalsIgnoreCase("Email") ){
			ccRootObject.radioButton("Select", "PreferredMethodEmail");
		}
		else if ( preferredMethod.equalsIgnoreCase("Text") ){
			ccRootObject.radioButton("Select", "PreferredMethodText");
		}
		
		//Check the involved policy vehicles
		//For property this should be blank
		if ( !involvedPolicyVehicles.trim().isEmpty()) {
			basicInformationInvolvedVehicles(involvedPolicyVehicles);
		}
		
		logger.log_Message("Basic Information Filled", Logger.MessageType.INFO);
	}

	/**
	 * Enters the basic information on the FNOL wizard Basic Information page
	 * 
	 * @param vehicles
	 *            - policy vehicles to select
	 * @throws Exception 
	 * @since 2012/06/25
	 */
	private void basicInformationInvolvedVehicles(String involvedPolicyVehicles)throws Exception {

		String[] policyVehicles = involvedPolicyVehicles.split(",");

		for (String vehicle : policyVehicles) {
			// Get Vehicles Data
			Hashtable<String, String> vehicleIncidents = ClaimCenterData.getData(
					"VehicleIncidents", vehicle);
			String vehicletoSelect = vehicleIncidents.get("SelectVehicle").trim();

			TextGuiTestObject vehicleSecTable = ccRootObject.findDescendantTestObject(
					ccRootObject.getBrowserDocObject(), 
					".class", "Html.TABLE", ".text", vehicletoSelect);
			if(vehicleSecTable!=null){
				ccRootObject.checkBoxinTable("Check", vehicleSecTable, "", "InvolvedVehicleCheckBox");
			}
		}
	}

	/**
	 * Closes the Warning message in Workspace area if they are shown
	 * @return - true if the warning messages found
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	private boolean closeClaimsWarnings() throws Exception {
		
		boolean closed = false;
		String closeWarningExists = ccRootObject.button("Verify", "DuplicateClaimCloseBtn");
		if (closeWarningExists.contains("PASS")) {
			closed = true;
			ccRootObject.button("Click", "DuplicateClaimCloseBtn");
		}
		return closed;
	}

	/**
	 * Enters the Claim Information in FNOL Wizard
	 * 
	 * @param lossCause
	 *            - Loss Cause
	 * @param location
	 *            - Location of incident
	 * @throws Exception 
	 * @since 2012/06/25
	 */
	private void addClaimInformation( String claimType,
			Hashtable<String, String> dataFNOL_LossDetails) throws Exception {
		
		if ( claimType.equalsIgnoreCase("Personal auto")) {
			addClaimInformationAuto(dataFNOL_LossDetails);
		}
		
		if ( claimType.equalsIgnoreCase("Personal Property")) {
			addClaimInformationProperty(dataFNOL_LossDetails);
		}
		logger.log_Message("Add Claim Information - Done", Logger.MessageType.INFO);
	}

	/**
	 * Add Claim Information for Property Claims
	 * @param dataFNOL_LossDetails
	 * 			- Data to enter
	 * @throws Exception
	 */
	private void addClaimInformationProperty( Hashtable<String, String> dataFNOL_LossDetails) throws Exception {

		//Loss Details
		//Cause of Loss
		ccRootObject.comboBox("Select", "CauseOfLoss", dataFNOL_LossDetails.get("CauseofLoss"));
		//Sub Cause of Loss
		ccRootObject.comboBox("Select", "SubCauseOfLoss", dataFNOL_LossDetails.get("SubCauseofLoss"));
		//Description of Loss
		ccRootObject.editBox("Input", "Description", dataFNOL_LossDetails.get("LossDescription"));
		
		//General
		//Catastrophe Code
		ccRootObject.comboBox("Select", "CatastropheCode", dataFNOL_LossDetails.get("CatastropheCode"));

		//Report Only
		if ( dataFNOL_LossDetails.get("ReportOnly").equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "StatusReportOnlyYes");
		}
		//Coverage in Question
		if ( dataFNOL_LossDetails.get("ReportOnly").equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "CoverageinQuestionYes");
		}

		//Loss Location
		addLocation(dataFNOL_LossDetails.get("LossLocation"));
		
		//Damage Type - Fire
		if ( dataFNOL_LossDetails.get("DamageTypeFire").equalsIgnoreCase("Yes") )
		{
			//Check the Fire
			ccRootObject.checkBox("Check", "DamageTypeFire");
		}
		//Fire questions
		if ( dataFNOL_LossDetails.get("DamageTypeFire").equalsIgnoreCase("Yes") ||
				dataFNOL_LossDetails.get("CauseofLoss").equalsIgnoreCase("Fire"))
		{
			answerFireQuestions(dataFNOL_LossDetails.get("FireQuestions"));
		}
		
		//Building
		if ( dataFNOL_LossDetails.get("Building").equalsIgnoreCase("Yes")) {
			ccRootObject.checkBox("Check", "BuildingCheckbox");
			ccRootObject.comboBox("Select", "PolicyLocation", dataFNOL_LossDetails.get("PropertyName") );
			ccRootObject.editBox("Input", "BuildingDamageDescription", dataFNOL_LossDetails.get("PropertyDamageDesc"));
		}
		
		//Contents/Non-Building
		if ( dataFNOL_LossDetails.get("ContentsOrNonBuilding").equalsIgnoreCase("Yes")) {
			ccRootObject.checkBox("Check", "ContentsCheckbox");
			ccRootObject.editBox("Input", "ContentsDamageDescription", dataFNOL_LossDetails.get("ContentsOrNonBuildingDamageDesc") );
		}

		//ALE/Loss of Rents
		if ( dataFNOL_LossDetails.get("ALELossofRents").equalsIgnoreCase("Yes")) {
			ccRootObject.checkBox("Check", "ALECheckbox");
			ccRootObject.editBox("Input", "ALEDamageDescription", dataFNOL_LossDetails.get("ALELossofRentsDamageDesc") );
		}
		
		//Injured person
		if ( !dataFNOL_LossDetails.get("Injuries").trim().isEmpty()) { 
			addInjuredPerson(dataFNOL_LossDetails.get("Injuries"));
		}
		
		//Add Property Damages
		addPropertyDamages(dataFNOL_LossDetails.get("PropertyDamages"));
		
		//Witnesses
		addWitnesses(dataFNOL_LossDetails.get("Witnesses"));

		//Officials
		addOfficials(dataFNOL_LossDetails.get("Officials"));

		//Charges
		addCharges(dataFNOL_LossDetails.get("Charges"));
	}

	/**
	 * @param injuries
	 * @throws Exception
	 */
	private void addInjuredPerson(String injuries) throws Exception {
		
		for ( String injury : injuries.split(",")) {
			if ( !injury.trim().isEmpty()) {
				
				logger.log_Message("Adding Injury " + injury + " ", MessageType.INFO);
				Hashtable<String, String> data_ContactDetails = ClaimCenterData.getData("Contacts", injury );
				ccRootObject.button("Click", "AddInjuryButton");
				
				String contactDisplayName = data_ContactDetails.get("FirstName") + " " + data_ContactDetails.get("LastName");
				
				if ( ccRootObject.comboBox("Verify", "ContactPerson", contactDisplayName).startsWith("PASS")) {
					ccRootObject.comboBox("Select", "ContactPerson", data_ContactDetails.get("ContactName"));
				}
				
				//First Name
				ccRootObject.editBox("Input", "FirstName", data_ContactDetails.get("FirstName"));
				//Last Name
				ccRootObject.editBox("Input", "LastName", data_ContactDetails.get("LastName"));

				//DOB- Not Required
				ccRootObject.editBox("Input", "DOB2", CCUtility.convertDateFormat(data_ContactDetails.get("DateofBirth"),
						"yyyy-MM-dd hh:mm:ss", "MMddyyyy"));
				
				//Minor at time of Loss
				if ( data_ContactDetails.get("MinorAtTimeofLoss").equalsIgnoreCase("Yes") ) {
					ccRootObject.radioButton("Select", "MinorattimeofLossYes");
				}
				
				//Gender
				ccRootObject.comboBox("Select", "Gender", data_ContactDetails.get("Gender"));
				
				//Add Address
				AddressBook.addLocation(ccRootObject, data_ContactDetails.get("PrimaryAddress"));
	
				//Contact Prohibited?
				if ( data_ContactDetails.get("ContactProhibited").equalsIgnoreCase("Yes") ) {
					ccRootObject.radioButton("Select", "ContactProhibitedYes");
				}
				
				//Primary Phone 
				ccRootObject.comboBox("Select", "PrimaryPhoneType", data_ContactDetails.get("PrimaryPhone"));
				//Business Phone
				ccRootObject.editBox("Input", "BusinessPhone", data_ContactDetails.get("BusinessPhone"));
				//Home Phone   
				ccRootObject.editBox("Input", "HomePhoneNumber", data_ContactDetails.get("HomePhone"));
				//Mobile   
				ccRootObject.editBox("Input", "Mobile", data_ContactDetails.get("Mobile"));
				//Email   
				ccRootObject.editBox("Input", "EmailAddress", data_ContactDetails.get("MainEmail"));
				//Alternate Email   
				ccRootObject.editBox("Input", "AlternateEmail", data_ContactDetails.get("AlternateEmail"));
				
				//Preferred Method
				ccRootObject.comboBox("Select", "PreferredMethod2", data_ContactDetails.get("PreferredMethod"));

				//Describe Injuries
				ccRootObject.editBox("Input", "InjuryDescription", data_ContactDetails.get("InjuryDesc"));
				
				//Risk Info
				if ( data_ContactDetails.get("InsuranceCompany").trim().isEmpty()) {
					if ( ccRootObject.comboBox("Verify", "InsuranceCompany2",
							data_ContactDetails.get("InsuranceCompany")).equalsIgnoreCase("PASS")) {
						ccRootObject.comboBox("Select", "InsuranceCompany2", data_ContactDetails.get("InsuranceCompany"));
					}
					else {
						ccRootObject.comboBox("Select", "InsuranceCompany2", "Not listed or unknown");
						//Other 
						ccRootObject.editBox("Input", "OtherInsuranceCompany", data_ContactDetails.get("InsuranceCompany"));
					}
					//Policy Number
					ccRootObject.editBox("Input", "PolicyNumber2", data_ContactDetails.get("PolicyNumber"));
	
					//Third Party Adjuster
					ccRootObject.comboBox("Select", "ThirdPartyAdjuster", data_ContactDetails.get("ThirdPartyAdjuster"));
				}
				//Notes
				ccRootObject.editBox("Input", "ContactNotes", data_ContactDetails.get("Notes"));
				
				//Click on finish
				ccRootObject.button("Click", "UpdateButton");

				//check  created successfully
				String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
				if ( errorMessagesContents != null) {
					if (!errorMessagesContents.trim().isEmpty()) {
						String error = "Errors while creating property  laibility'" + errorMessagesContents + "'";
						throw new Exception(error);
					}
				}
				logger.log_Message("Injured " + injury + " Person is added", MessageType.INFO);
			}
		}
	}
	

	/**
	 * @param fireQuestions
	 * @throws Exception
	 */
	private void answerFireQuestions( String fireQuestions) throws Exception {
		
		Hashtable<String, String> data_FireQuestions = ClaimCenterData.getData("FireQuestions", 
				fireQuestions);
		
		//Is Anyone Injured?
		if ( data_FireQuestions.get("IsAnyoneInjured").equalsIgnoreCase("Yes") )
		{
			ccRootObject.radioButton("Select", "IsAnyoneInjuredYes");
		}
		else if ( data_FireQuestions.get("IsAnyoneInjured").equalsIgnoreCase("No") )
		{
			ccRootObject.radioButton("Select", "IsAnyoneInjuredeNo");
		}
		//What was the source of the fire?
		ccRootObject.editBox("Input", "FireSource", data_FireQuestions.get("Whatwasthesourceofthefire"));
		//How was the Fire Discovered?
		ccRootObject.editBox("Input", "HowWasFireDiscovered", data_FireQuestions.get("HowwastheFireDiscovered"));
		//Did the Fire Department respond?
		if ( data_FireQuestions.get("DidtheFireDepartmentrespond").equalsIgnoreCase("Yes") )
		{
			ccRootObject.radioButton("Select", "FireDeptRespondedYes");
		}
		else if ( data_FireQuestions.get("DidtheFireDepartmentrespond").equalsIgnoreCase("No") )
		{
			ccRootObject.radioButton("Select", "FireDeptRespondedNo");
		}
		//Is the Fire Marshal Involved?
		if ( data_FireQuestions.get("IstheFireMarshalInvolved").equalsIgnoreCase("Yes") )
		{
			ccRootObject.radioButton("Select", "FireMarshalInvolvedYes");
		}
		else if ( data_FireQuestions.get("IstheFireMarshalInvolved").equalsIgnoreCase("No") )
		{
			ccRootObject.radioButton("Select", "FireMarshalInvolvedNo");
		}
		//Smoke Alarm/Detector Activated?
		if ( data_FireQuestions.get("SmokeAlarmDetectorActivated").equalsIgnoreCase("Yes") )
		{
			ccRootObject.radioButton("Select", "SmokeAlarmActivatedYes");
		}
		else if ( data_FireQuestions.get("SmokeAlarmDetectorActivated").equalsIgnoreCase("No") )
		{
			ccRootObject.radioButton("Select", "SmokeAlarmActivatedNo");
		}
		//Sprinkler System Activated?
		if ( data_FireQuestions.get("SprinklerSystemActivated").equalsIgnoreCase("Yes") )
		{
			ccRootObject.radioButton("Select", "SprinkSystemActivatedYes");
		}
		else if ( data_FireQuestions.get("SprinklerSystemActivated").equalsIgnoreCase("No") )
		{
			ccRootObject.radioButton("Select", "SprinkSystemActivatedNo");
		}
		//Is there a Monitored Alarm System?
		if ( data_FireQuestions.get("IsthereaMonitoredAlarmSystem").equalsIgnoreCase("Yes") )
		{
			ccRootObject.radioButton("Select", "MonitoredAlarmSystemYes");
		}
		else if ( data_FireQuestions.get("IsthereaMonitoredAlarmSystem").equalsIgnoreCase("No") )
		{
			ccRootObject.radioButton("Select", "MonitoredAlarmSystemNo");
		}

		//Arson Involved?
		if ( data_FireQuestions.get("ArsonInvolved").equalsIgnoreCase("Yes") )
		{
			ccRootObject.radioButton("Select", "ArsonInvolvedYes");
		}
		else if ( data_FireQuestions.get("ArsonInvolved").equalsIgnoreCase("No") )
		{
			ccRootObject.radioButton("Select", "ArsonInvolvedNo");
		}

		//Smoke Damage Only?
		if ( data_FireQuestions.get("SmokeDamageOnly").equalsIgnoreCase("Yes") )
		{
			ccRootObject.radioButton("Select", "SmokeDamageOnlyYes");
		}
		else if ( data_FireQuestions.get("SmokeDamageOnly").equalsIgnoreCase("No") )
		{
			ccRootObject.radioButton("Select", "SmokeDamageOnlyNo");
		}

		//Is the Home/Building Habitable?
		if ( data_FireQuestions.get("IstheHomeBuildingHabitable").equalsIgnoreCase("Yes") )
		{
			ccRootObject.radioButton("Select", "IsHomeHabitableYes");
		}
		else if ( data_FireQuestions.get("IstheHomeBuildingHabitable").equalsIgnoreCase("No") )
		{
			ccRootObject.radioButton("Select", "IsHomeHabitableNo");
		}
		//Is the Home/Building Secure?
		if ( data_FireQuestions.get("IstheHomeBuildingSecure").equalsIgnoreCase("Yes") )
		{
			ccRootObject.radioButton("Select", "IsHomeSecureYes");
		}
		else if ( data_FireQuestions.get("IstheHomeBuildingSecure").equalsIgnoreCase("No") )
		{
			ccRootObject.radioButton("Select", "IsHomeSecureNo");
		}
	}
	
	
	/**
	 * Add Claim Information for Auto Claims
	 * @param dataFNOL_LossDetails
	 * 			- Data
	 * @throws Exception
	 */
	private void addClaimInformationAuto(
			Hashtable<String, String> dataFNOL_LossDetails) throws Exception {
		//General
		//Catastrophe Code
		ccRootObject.comboBox("Select", "CatastropheCode", dataFNOL_LossDetails.get("CatastropheCode"));
		//Special Claim Permission
		ccRootObject.comboBox("Select", "SpecialClaimPermission", dataFNOL_LossDetails.get("SpecialClaimPermission"));

		//Loss Details
		//Cause of Loss
		ccRootObject.comboBox("Select", "CauseOfLoss", dataFNOL_LossDetails.get("CauseofLoss"));
		//Sub Cause of Loss
		ccRootObject.comboBox("Select", "SubCauseOfLoss", dataFNOL_LossDetails.get("SubCauseofLoss"));
		//Fault Rating
		ccRootObject.comboBox("Select", "ClaimFaultRating", dataFNOL_LossDetails.get("FaultRating"));
		//Weather
		ccRootObject.comboBox("Select", "Weather", dataFNOL_LossDetails.get("Weather"));
		//Description of Loss
		ccRootObject.editBox("Input", "Description", dataFNOL_LossDetails.get("LossDescription"));
		
		//Drivers
		//Is there a First Party driver?
		ccRootObject.comboBox("Select", "IsThereAFirstPartyDriver", dataFNOL_LossDetails.get("FirstPartyDriver"));

		//Is there one or more TP drivers?
		ccRootObject.comboBox("Select", "IsThereOneOrMoreTPDrivers", dataFNOL_LossDetails.get("TPDrivers"));

		//Loss Location
		addLocation(dataFNOL_LossDetails.get("LossLocation"));
		
		//Assignments
		//Insured Vehicle Injuries
		ccRootObject.comboBox("Select", "InsuredVehicleInjuries", dataFNOL_LossDetails.get("InsuredVehicleInjuries"));
		//Other than Insured Driver Injuries 
		ccRootObject.comboBox("Select", "OtherthanInsuredDriverInjuries", dataFNOL_LossDetails.get("OtherthanInsuredDriverInjuries"));
		//Core 
		ccRootObject.comboBox("Select", "Core", dataFNOL_LossDetails.get("Core"));

		//Notification and Contact
		//Report Only
		if ( dataFNOL_LossDetails.get("ReportOnly").equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "StatusReportOnlyYes");
		}
		//Coverage in Question
		if ( dataFNOL_LossDetails.get("ReportOnly").equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "CoverageinQuestionYes");
		}
		
		//Add the Vehicle Incidents Details
		addVehicleIncidents(
				dataFNOL_LossDetails.get("InvolvedPolicyVehicles"),
				dataFNOL_LossDetails.get("TPVehicles"));
		
		//Add Pedestrians
		addPedestrainDetails( dataFNOL_LossDetails.get("Pedestrians"));

		//Add Property Damages
		addPropertyDamages(dataFNOL_LossDetails.get("PropertyLiabilityIncidents"));
		
		//At the Scene
		//Witnesses
		addWitnesses(dataFNOL_LossDetails.get("Witnesses"));

		//Officials
		addOfficials(dataFNOL_LossDetails.get("Officials"));

		//Charges
		addCharges(dataFNOL_LossDetails.get("Charges"));
	}

	/**
	 * Add Charges to the Claim
	 * @param charges
	 * 			- Reference to the Charges 
	 * @throws Exception 
	 */
	private void addCharges(String charges) throws Exception {
		
		int chargesCount = 0;
		for ( String charge : charges.split(",")) {
			if ( !charge.trim().isEmpty()) {
				Hashtable<String, String> data_Charges = ClaimCenterData.getData("Charges", charge);
				ccRootObject.button("Click", "AddCharges");
				//Get the Charge Table object
				TextGuiTestObject chargesTable = ccRootObject.findExcelMappedTestObject(
						ccRootObject.getBrowserDocObject(), "ChargesTable");
				//*Party
				ccRootObject.comboBoxinTable("Select", chargesTable,
						chargesCount+":", "ChargePartyType", data_Charges.get("Party"));
				//*Name
				ccRootObject.editBoxInTable("Input", chargesTable,
						chargesCount+":", "ChargePartyName", data_Charges.get("Name"));
				//Comments 
				ccRootObject.editBoxInTable("Input", chargesTable,
						chargesCount+":", "ChargeComments", data_Charges.get("Charge"));
			}
		}
	}

	/**
	 * @param officials
	 * @throws Exception 
	 */
	private void addOfficials(String officials) throws Exception {
		
		int officialsCount = 0;
		for ( String official : officials.split(",")) {
			if ( !official.trim().isEmpty()) {
				Hashtable<String, String> data_Officials = ClaimCenterData.getData("Officials", official);
				ccRootObject.button("Click", "AddOfficial");
				//Get the officials Table object
				TextGuiTestObject officialsTable = ccRootObject.findExcelMappedTestObject(
						ccRootObject.getBrowserDocObject(), "OfficialsTable");
				//*Type
				ccRootObject.comboBoxinTable("Select", officialsTable,
						officialsCount+":", "OfficialType", data_Officials.get("Type"));
				//Name/Badge#
				ccRootObject.editBoxInTable("Input", officialsTable,
						officialsCount+":", "OfficialNameOrBadge", data_Officials.get("Name"));
				//Report
				ccRootObject.editBoxInTable("Input", officialsTable,
						officialsCount+":", "OfficialReportNumber", data_Officials.get("Report"));
				//#Department
				ccRootObject.editBoxInTable("Input", officialsTable,
						officialsCount+":", "OfficialDepartment", data_Officials.get("Department"));
			}
		}
	}

	/**
	 * @param dataFNOL_LossDetails
	 * @throws Exception 
	 */
	private void addWitnesses(String witnesses) throws Exception {

		int witnessCount = 0;
		for ( String witness : witnesses.split(",")) {
			if ( !witness.trim().isEmpty()) {
				Hashtable<String, String> data_Witness = ClaimCenterData.getData("Witnesses", witness);
				ccRootObject.button("Click", "AddWitness");
				//Get Witness Table
				TextGuiTestObject witnessTable = ccRootObject.findExcelMappedTestObject(
						ccRootObject.getBrowserDocObject(), "WitnessesTable");
				//FirstName
				ccRootObject.editBoxInTable("Input", witnessTable, witnessCount +":",
						"WitnessFirstName", data_Witness.get("FirstName"));
				//LastName
				ccRootObject.editBoxInTable("Input", witnessTable, witnessCount +":",
						"WitnessLastName", data_Witness.get("LastName"));
				//HomePhone
				ccRootObject.editBoxInTable("Input", witnessTable, witnessCount +":",
						"WitnessHomePhone", data_Witness.get("HomePhone"));
				//Perspective
				ccRootObject.editBoxInTable("Input", witnessTable, witnessCount +":",
						"WitnessPerspective", data_Witness.get("Perspective"));
				witnessCount++;
			}
		}
	}

	/**
	 * Adds Property Damages to Vehicle Claim
	 * @param propertyDamages
	 * @throws Exception 
	 */
	private void addPropertyDamages(String propertyDamages) throws Exception {
		
		for ( String propertyDamage : propertyDamages.split(",")) {
			if ( !propertyDamage.trim().isEmpty()) {
				//TODO: Not Yet Implemented
				ccRootObject.button("Click", "AddPropertyDamageBtn");
				Incidents inc = new Incidents(ccRootObject, logger);
				inc.propertyLiability(propertyDamage);
				//Damage
				//Property Description
				//Damage Description
				//Damage to Building, Non Building
				//Voluntary PD paid
				//Property Location
				//Inspection
				//Repairs
				//Salvage
				//Owner Info
				//New Note
			}
		}
	}
	
	/**
	 * Adds Pedestrain Information
	 * @param data_ContactDetails
	 * @throws Exception
	 */
	private void addPedestrainDetails( String pedestrians) throws Exception {
		
		for ( String pedestrian : pedestrians.split(",")) {
			if ( !pedestrian.trim().isEmpty()) {
				
				Hashtable<String, String> data_ContactDetails = ClaimCenterData.getData("Contacts", pedestrian );
				ccRootObject.button("Click", "AddPedestrainBtn");
				String contactDisplayName = data_ContactDetails.get("FirstName") + " " + data_ContactDetails.get("LastName");
				if ( ccRootObject.comboBox("Verify", "ContactPerson", contactDisplayName).startsWith("PASS")) {
					ccRootObject.comboBox("Select", "ContactPerson", data_ContactDetails.get("ContactName"));
				}
				else {
					//First Name
					ccRootObject.editBox("Input", "FirstName", data_ContactDetails.get("FirstName"));
					//Last Name
					ccRootObject.editBox("Input", "LastName", data_ContactDetails.get("LastName"));
					
					//Add Address
					AddressBook.addLocation(ccRootObject, data_ContactDetails.get("PrimaryAddress"));
					
					//Business Phone
					ccRootObject.editBox("Input", "BusinessPhone", data_ContactDetails.get("BusinessPhone"));
					//Home Phone   
					ccRootObject.editBox("Input", "HomePhoneNumber", data_ContactDetails.get("HomePhone"));
					//Mobile   
					ccRootObject.editBox("Input", "Mobile", data_ContactDetails.get("Mobile"));
					//Primary Phone 
					ccRootObject.comboBox("Select", "PrimaryPhoneType", data_ContactDetails.get("PrimaryPhone"));
					//Email   
					ccRootObject.editBox("Input", "EmailAddress", data_ContactDetails.get("MainEmail"));
					//Alternate Email   
					ccRootObject.editBox("Input", "AlternateEmail", data_ContactDetails.get("AlternateEmail"));
				}
				//Notes
				ccRootObject.editBox("Input", "ContactNotes", data_ContactDetails.get("Notes"));
				
				//Injured
				if ( data_ContactDetails.get("Injured").equalsIgnoreCase("Yes") ) {
					ccRootObject.radioButton("Select", "InjuredYes");
				}
				else {
					ccRootObject.radioButton("Select", "InjuredNo");
				}
			}
		}	
	}	
	
	/**
	 * @param dataFNOL_LossDetails
	 * @throws Exception
	 */
	private void addVehicleIncidents( String insuredVehicles, String tpVehicles) throws Exception {
		
		//Edit Policy Vehicle Incident
		for( String policyVehicle : insuredVehicles.split(",")) {
			if ( !policyVehicle.trim().isEmpty() ) {
				//Get the Vehicle Data
				Hashtable<String, String> data_VehicleIncident = ClaimCenterData.getData("VehicleIncidents", policyVehicle);
				String vehicleName = data_VehicleIncident.get("SelectVehicle").substring(
						0, data_VehicleIncident.get("SelectVehicle").indexOf("(")-1).trim();
				//Check the Vehicle link and click to Open
				TextGuiTestObject vehicleLink = ccRootObject.findDescendantTestObject(
						ccRootObject.getBrowserDocObject(), ".class", "Html.A", ".text", vehicleName);
				vehicleLink.click();
				ccRootObject.waitForBrowserReady();
				Incidents vehIncident = new Incidents(ccRootObject, logger);
				vehIncident.addVehicleIncidentAUTO(policyVehicle);
			}
		}
		
		//Add Third Party Vehicles
		for( String tpVehicle : tpVehicles.split(",")) {
			if ( !tpVehicle.trim().isEmpty() ) {
				//AddVehicleBtn
				ccRootObject.button("Click", "AddVehicleBtn");
				Incidents vehIncident = new Incidents(ccRootObject, logger);
				vehIncident.addVehicleIncidentAUTO(tpVehicle);
			}
		}
	}

	/**
	 * Adds location
	 * @param location - Location Index
	 * @throws Exception
	 */
	private void addLocation(String location) throws Exception {

		Hashtable<String, String> dataFNOL_Location = ClaimCenterData.getData( "Locations", location);
		if (dataFNOL_Location.get("SelectAddress").trim().isEmpty()) {
			ccRootObject.comboBox("Select", "LossLocation", "New...");
			AddressBook.addLocation(ccRootObject, location);
		}
		else {
			ccRootObject.comboBox("Select", "LossLocation", dataFNOL_Location.get("SelectAddress"));
		}
	}

	/**
	 * Assigns the Claim and exposures on FNOL Wizard
	 * 
	 * @param assignClaimAndAllExposures
	 * @param assignTo
	 * @param note
	 * @throws Exception 
	 * @since 2012/06/25
	 */
	private void assignment(String assignTo, String note) throws Exception {

		//Get the User Details
		Boolean assigneeExists = ccRootObject.comboBox("Verify", "AssignTo", assignTo).startsWith("PASS"); 
		if (assigneeExists) {
			ccRootObject.comboBox("Select", "AssignTo", assignTo);
		}
		else if (assignTo.startsWith("Search")) {
			String tokens[] = assignTo.split(" ");
			//Click on Assign helper
			ccRootObject.link("Click", "SearchAssignmentBtn");
			//Search and Select the user
			Assignment asm = new Assignment(ccRootObject, logger);
			asm.searchAssignmentUser(tokens[1].trim());
		}
		ccRootObject.editBox("Input", "Note", note);
	}

	/**
	 * Creates new Claim through FNOL wizard for Unverified Policy
	 * 
	 * @param testStepType
	 *            - Claim Type to create
	 * @param dataIndex
	 *            - Index of the data to use
	 * @since 2012/08/14
	 */
	public void unverifiedPolicy_CreateClaim(String testStepType, String dataIndex) throws Exception {

		// Get CreateClaim Data
		Hashtable<String, String> dataFNOL_CreateClaim = ClaimCenterData.getData("Create_Claim", dataIndex);

		//Open New Claim Wizard From Tab bar
		openTabMenuItem("ClaimMenuBtn", "ClaimTabMenuList", new String[] { "New Claim" });

		//Create Unverified Policy
		ccRootObject.radioButton("Select", "CreateUnverifiedPolicy");
		
		// Step 1: Search and Select Policy
		if (createUnverifiedPolicy(dataFNOL_CreateClaim.get("PolicyNumber"), testStepType, dataFNOL_CreateClaim)) {

			// Next
			ccRootObject.button("Click", "NextBtn");

			//Create, Edit and Verify parties Involved
			if(!dataFNOL_CreateClaim.get("PartiesInvolved").trim().isEmpty()){
				//Parties Involved
				ccRootObject.link("Click", "PartiesInvolvedFW");
				
				PartiesInvolved pv = new PartiesInvolved(ccRootObject, logger);
				pv.createNewContact(dataFNOL_CreateClaim.get("PartiesInvolved"));
				//Back
				ccRootObject.link("Click", "Back");
			}
			
			if (testStepType.trim().equalsIgnoreCase("Personal auto")) {
				fullFNOLWizard(testStepType, dataFNOL_CreateClaim);
			}
			if (testStepType.trim().equalsIgnoreCase("Personal Property")) {
				fullFNOLWizard(testStepType, dataFNOL_CreateClaim);
			} 
		} else {
			logger.log_Message("Policy Not Found", Logger.MessageType.ERROR);
		}
	}

	/**
	 * Enters the policy number and claim type and Loss Date and searches it on the screen for an unverified policy
	 * 
	 * @param policyNo
	 *            - policy number to search and Policy type
	 * @return - returns true if policy search find the policy
	 * @throws Exception 
	 * @since 2012/06/26
	 */
	private Boolean createUnverifiedPolicy(String policyNo, String claimType, Hashtable<String, String> dataFNOL_CreateClaim) throws Exception {
		Boolean policyFound = false;

		// Get PoolicyDetails Data
		Hashtable<String, String> dataFNOL_PolicyDetails = ClaimCenterData.getData(
				"PolicyDetails", dataFNOL_CreateClaim.get("PolicyDetails"));		
		
		//Policy Number
		ccRootObject.editBox("Input", "SearchPolicyNumber2", policyNo);
		
		//Claim Type
		ccRootObject.comboBox("Select","PolicyType",claimType);
		
		//Claim Type
		ccRootObject.comboBox("Select","PolicyType",claimType);
	
		//Get current Date
		String currentDateTime = CCUtility.current_Date_SpecificFormat("MM-dd-yyyy hh:mm a");
		String[] dateTokens = currentDateTime.split(" ");

		//Claim Loss Date and Time
		ccRootObject.editBox("Input", "NewClaimLossDate",
				dateTokens[0].replace("-", "") + dateTokens[1].replace(":", "") + dateTokens[2]);
		
		//Product
		ccRootObject.comboBox("Select", "Product", dataFNOL_PolicyDetails.get("Product") );
				
		//Effective Date		
		ccRootObject.editBox("Input", "EffectiveDate", dataFNOL_PolicyDetails.get("EffectiveDate") );
				
		//Expiration Date
		ccRootObject.editBox("Input", "ExpirationDate", dataFNOL_PolicyDetails.get("ExpirationDate") );
		
		//check if Contact is created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating coverage '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		Hashtable<String, String> data_Contact =ClaimCenterData.getData(
				"Contacts", dataFNOL_PolicyDetails.get("Contacts"));

		String[] policyDetailsMenuItemsList = new String[] {
				data_Contact.get("ContactType").trim(),data_Contact.get("ContactSubType").trim()
				};

		//Name Helper Click
		while(!(boolean)ccRootObject.button("Verify", "UpdateButton").startsWith("PASS")){
				if(ccRootObject.link("Verify","InsuredNamehelper").startsWith("PASS")){
					ccRootObject.link("Click","InsuredNamehelper");
					if(ccRootObject.menu("VerifyMenuOpened", "InsuredNameMenuList",  policyDetailsMenuItemsList).startsWith("PASS")){
						ccRootObject.menu("Click", "InsuredNameMenuList",  policyDetailsMenuItemsList);
					}
				}
		}			
		
		//Add Address Book
		AddressBook ucad = new AddressBook(ccRootObject, logger);
		sleep(1);
		ucad.fillInputValues(data_Contact);
		while(!((boolean)ccRootObject.button("Verify", "AddPolicyDriverButton").startsWith("PASS")||(boolean)ccRootObject.button("Verify", "AddPolicyLocationButton").startsWith("PASS"))){
			ccRootObject.button("Click", "UpdateButton");
		}
		
		logger.log_Message("Policy Create Screen Opened",
						Logger.MessageType.INFO);
		policyFound = true;
		//}

		
		PolicyDetails pds = new PolicyDetails(ccRootObject, logger);
		if (claimType.trim().equalsIgnoreCase("Personal auto")) {
			
			//Click on Add Driver button
			ccRootObject.button("Click", "AddPolicyDriverButton");
			
			pds.addDrivers(dataFNOL_PolicyDetails.get("DriverDetails"));
			
			String[] Coverages = dataFNOL_PolicyDetails.get("Coverage").split(",");
			pds.addCoverages(Coverages, claimType);
		}
		if (claimType.trim().equalsIgnoreCase("Personal Property")) {
			
			//Click on Add Location button
			ccRootObject.button("Click", "AddPolicyLocationButton");
			pds.addLocation(dataFNOL_PolicyDetails.get("LocationDetails"));
			
			//Click on Add Location Risk button
			ccRootObject.button("Click", "AddLocationRisk");
			pds.addLocationRisk(dataFNOL_PolicyDetails.get("LocationDetails"),claimType);
			String[] Coverages = dataFNOL_PolicyDetails.get("Coverage").split(",");
			pds.addCoverages(Coverages, claimType);
						
		}
		
		return policyFound;
	}
}
