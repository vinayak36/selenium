package CCAppl;

import java.util.Hashtable;

import resources.CCAppl.SearchHelper;
import TestData.ClaimCenterData;
import Utilities.Logger;
import Utilities.Logger.MessageType;

import com.rational.test.ft.object.interfaces.TextGuiTestObject;



public class Search extends SearchHelper
{	
	/**
	 * Script Name   : <b>Search</b>
	 * Generated     : <b>Jul 17, 2012 10:00:34 AM</b>
	 * Description   : Functional Test Script
	 * Original Host : WinNT Version 5.1  Build 2600 (S)
	 * 
	 * @since  2012/07/17
	 * @author Vinayak Hegde
	 */
	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Contacts and initiates root Test object and Logger
	 * 
	 * @param rootObject
	 *            - Root test object
	 * @param log
	 *            - Logger instance
	 * @throws Exception 
	 */	
	public Search(UIObjectsAPI rootObject,Logger log){

		ccRootObject =rootObject;
		logger = log;
	}

	/*Method to go to Search screen.**/
	public void openTabMenuItem() throws Exception{

		ccRootObject.link("Click", "SearchMenuBtn");
	}


	/**
	 * Search for Claim.
	 * @param serIndex
	 *            - Index of the CSearch data from the "SearchDetails" sheet.
	 * @return 
	 * @throws Exception
	 */
	public void searchClaim(String serIndex) throws Exception{
		logger.log_Message("Claim Search", MessageType.INFO);

		//Get the data
		Hashtable<String, String> data_Search = ClaimCenterData.getData("ClaimsSearch", serIndex);
		Hashtable<String, String> dataSearch_ClaimDetails = ClaimCenterData.getData("Create_Claim", data_Search.get("ClaimDetails"));
		Hashtable<String, String> dataSearch_LossDetails=ClaimCenterData.getData("LossDetails", data_Search.get("LossDetails"));
		Hashtable<String, String> dataSearch_Vehicle=ClaimCenterData.getData("VehicleIncidents", data_Search.get("VehicleIncidents"));
		Hashtable<String, String> dataSearch_Jurisdiction=ClaimCenterData.getData("ProvinceCodes", data_Search.get("Jurisdiction"));
		Hashtable<String, String> dataSearch_PolicyDetails=ClaimCenterData.getData("PolicyDetails", data_Search.get("PolicyDetails"));

		openTabMenuItem();

		//Select Search For
		ccRootObject.comboBox("Select", "SearchFor", dataSearch_ClaimDetails.get("ClaimantType"));

		//Enter the first name
		ccRootObject.editBox("Input", "FirstName", dataSearch_ClaimDetails.get("FirstName"));

		//Enter Last Name
		ccRootObject.editBox("Input", "LastName", dataSearch_ClaimDetails.get("LastName"));

		//Enter the organisation name
		ccRootObject.editBox("Input", "OrganisationName", dataSearch_ClaimDetails.get("OrganizationName"));

		//Enter Claim Number
		ccRootObject.editBox("Input", "ClaimNo", dataSearch_ClaimDetails.get("ClaimNo"));

		//Enter Policy Number
		ccRootObject.editBox("Input", "PolicyNumber", dataSearch_ClaimDetails.get("PolicyNumber"));

		//Select Catastrophy code
		ccRootObject.comboBox("Select", "CatCode", dataSearch_LossDetails.get("CatastropheCode"));

		//Enter the VIn
		ccRootObject.editBox("Input", "VIN", dataSearch_LossDetails.get("VIN"));

		//Enter the LicnsePlate
		ccRootObject.editBox("Input", "LicensePlate", dataSearch_Vehicle.get("LicensePlate"));

		//Select the  Product 
		ccRootObject.comboBox("Select", "Product", dataSearch_PolicyDetails.get("Product"));

		//Select the LOB 
		ccRootObject.comboBox("Select", "LOB", dataSearch_ClaimDetails.get("LineofBusiness"));

		//Select the LossType
		ccRootObject.comboBox("Select", "LossType", dataSearch_ClaimDetails.get("LossType"));

		//Select Loss Jurisdiction
		ccRootObject.comboBox("Select", "Jurisdiction", dataSearch_Jurisdiction.get("Province"));


		//Click on Search Button
		ccRootObject.button("Click","SearchBtn");


		//check if there  are no error messages
		String  errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while searching '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}

		logger.log_Message("Claim search successful", MessageType.INFO);

	}

	/**
	 *Create Associated Claims.
	 * @param serIndex
	 *            - Index of the Search data from the "SearchDetails" sheet.
	 * @return 
	 * @throws Exception
	 */
	public void createAssociatedClaims(String associatedClaimIndex) throws Exception{

		logger.log_Message("Create Associated Claims", MessageType.INFO);

		//Get the data
		Hashtable<String, String> data_AssociatedClaim = ClaimCenterData.getData("ClaimsAssociations", associatedClaimIndex);

		//Navigate to Associated Claim screen.
		ccRootObject.link("Click", "LossDetails");
		ccRootObject.link("Click", "Associations");
		//Click on New Association button
		ccRootObject.button("Click", "NewAssociationBtn");

		//Enter the title
		ccRootObject.editBox("Input", "NewAssociationTitle", data_AssociatedClaim.get("Title"));

		//Select the type
		ccRootObject.comboBox("Select", "NewAssociationType", data_AssociatedClaim.get("Type"));

		//Enter the Description
		ccRootObject.editBox("Input", "Description", data_AssociatedClaim.get("Description"));

		//Find Claim table
		TextGuiTestObject associatedClaimInfoTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
		"associatedClaimInfoTable");
		//Find the Header name	
		TextGuiTestObject associatedClaimNameObject =  ccRootObject.findDescendantTestObject(
				associatedClaimInfoTable, ".class", "Html.SPAN", ".text", "Claim");		

		String[] nameObjectsplit=associatedClaimNameObject.getProperty(".id").toString().split(":");
		String commonId=nameObjectsplit[0]+":"+nameObjectsplit[1]+":"+nameObjectsplit[2]+":"+nameObjectsplit[3];



		//Claim association
		String[] associatedClaims = data_AssociatedClaim.get("PrimaryClaim").split(",");
		int claimCount=associatedClaims.length;
		int claimNumber=1;
		while(claimNumber<=claimCount){
			ccRootObject.button("Click", "AddBtn");
			for(String associatedClaim:associatedClaims){
				String claimId=commonId+":"+claimNumber+":Claim";
				TextGuiTestObject associatedClaimrow = ccRootObject.findDescendantTestObject(
						associatedClaimInfoTable, ".class", "Html.INPUT.text", ".id",claimId);
				//associated the claims.
				ccRootObject.editBoxes("Input", associatedClaimrow, associatedClaim);
			}
			claimNumber++;
		}


		//Click on the update button
		ccRootObject.button("Click", "UpdateButton");

		//check if there  are no error messages
		String  errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while associating a claim'" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}

		logger.log_Message("laim Associated successully", MessageType.INFO);

	}

	public void searchAssociatedClaims(String associatedClaimIndex) throws Exception{

		logger.log_Message("Search Associated Claims", MessageType.INFO);

		//Get the data
		Hashtable<String, String> data_AssociatedClaim = ClaimCenterData.getData("ClaimsAssociations", associatedClaimIndex);

		//Navigate to Associated Claim screen.
		ccRootObject.link("Click", "LossDetails");
		ccRootObject.link("Click", "Associations");
		//Click on New Association button
		ccRootObject.button("Click", "FindAssociationBtn");

		//Enter the title
		ccRootObject.editBox("Input", "AssociationTitle", data_AssociatedClaim.get("Title"));

		//Select the type
		ccRootObject.editBox("Input", "AssociationClaim", data_AssociatedClaim.get("PrimaryClaim"));


		//Click on the search button
		ccRootObject.button("Click", "SearchBtn");

		//Click on the select button
		ccRootObject.button("Click", "SearchResultAssignButton");

		//Click on Update button
		ccRootObject.button("Click", "UpdateButton");


		//check if there  are no error messages
		String  errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while searching '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}

		logger.log_Message("Assoicated Claim successully", MessageType.INFO);
	}
}
