package CCAppl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import com.rational.test.ft.object.interfaces.TestObject;
import com.rational.test.ft.script.Anchor;

import resources.CCAppl.NotesHelper;
import TestData.ClaimCenterData;
import Utilities.Logger;
import Utilities.Logger.MessageType;

/**
 * Implements the Notes Module.
 * @author Vinayak Hegde
 */
public class Notes extends NotesHelper {	
	private UIObjectsAPI ccRootObject;
	private Logger logger;
	
	/**
	 * Instantiates the Contacts and initiates root Test object and Logger
	 * 
	 * @param rootObject
	 *            - Root test object
	 * @param log
	 *            - Logger instance
	 * @since 2012/03/08
	 */
	public Notes(UIObjectsAPI rootObject, Logger log) {
		ccRootObject = rootObject;
		logger = log;
	}
	 
	/**
	 * Creates new Notes in Opened Claim : Method to create a note without using a a template
	 * @param NotesIndex
	 *            - Data Index for Note in "Notes" sheet
	 * @throws Exception 
	 */
	public void createNewNote(String noteIndex) throws Exception {
		
		logger.log_Message("Creating New Note", MessageType.INFO);
		//Actions->New Notes
		String[] noteMenuItemsList = new String[] {"Note"};
		ccRootObject.button("Click", "ActionsButton");
		ccRootObject.menu("Click", "MenuActionsList",noteMenuItemsList);
		
		//Get the workspace area to create Note
		TestObject workspaceArea = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "WorkspaceDiv");
		
		fillNoteData(noteIndex, workspaceArea);
		
		//Update
		ccRootObject.linkinTable("Click", workspaceArea, "UpdateButton");
		
		//check if note is created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating new Note '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("New Note created successully", MessageType.INFO);
		
	}

	/**
	 * Fills the Notes Data in the form. This is used in multiple places where note needs to be entered
	 * @param data_Notes
	 * @param containerArea
	 * @throws Exception
	 */
	public void fillNoteData(String noteIndex, TestObject containerArea) throws Exception {

		//Get the data
		Hashtable<String, String> data_Notes = ClaimCenterData.getData( "Notes", noteIndex);
		//Topic
		ccRootObject.comboBoxinTable("Select", containerArea, null, "NoteTopic", data_Notes.get("Topic"));
		//Subject
		ccRootObject.editBoxInTable("Input", containerArea, null, "NoteSubject", data_Notes.get("Subject"));
		//Related To
		if ( ccRootObject.findExcelMappedTestObject(containerArea, "RelatedTo") != null ) {
			ccRootObject.comboBoxinTable("Select", containerArea, null, "RelatedTo", data_Notes.get("RelatedTo"));
		}
		//Confidential
		ccRootObject.comboBoxinTable("Select", containerArea, null, "NoteConfidential", data_Notes.get("Confidential"));
		//Text
		ccRootObject.editBoxInTable("Input", containerArea, null, "NotesText", data_Notes.get("Text"));
	}

	 /**
	 * Creates new Notes in Opened Claim : Method to create document using a template;
	 * @param NotesIndex
	 *            - Data Index for Note in "Notes" sheet
	 * @throws Exception 
	 */
	public void createNoteFromTemplate(String noteIndex) throws Exception{
		
		logger.log_Message("Creating New Note from Template", MessageType.INFO);
		
		//Get the data
		Hashtable<String, String> data_Notes = ClaimCenterData.getData("Notes", noteIndex);
		
		//Actions->New Notes
		String[] noteMenuItemsList = new String[] {"Note"};
		ccRootObject.button("Click", "ActionsButton");
		ccRootObject.menu("Click", "MenuActionsList",noteMenuItemsList);
		
		//Click on the Use Template button.
		ccRootObject.button("Click", "NotesUseTemplateButton");
		//Search the template:Topic
		ccRootObject.comboBox("Select", "NoteTopic", data_Notes.get("Topic"));
		
		//Search the template:Type
		ccRootObject.comboBox("Select", "NoteTemplateType", data_Notes.get("Type"));
		
		//Search the template:LoB
		ccRootObject.comboBox("Select", "LossType", data_Notes.get("LineofBusiness"));
		
		//Search the template:keyword
		ccRootObject.editBox("Input", "NoteTemplateKeyword",  data_Notes.get("Keywords"));
		
		//Click on Search button
		ccRootObject.button("Click", "SearchBtn");
		
		//Find the first record with matching Template, Topic, Type and Loss Type and
		
		TestObject templateToSelect = findTemplateFromResult(data_Notes.get("Template"));
		if( templateToSelect == null ) {
			throw new Exception("No Template found matching with " + data_Notes.get("Template"));
		}
		ccRootObject.linkinTable("Click", templateToSelect, "SearchResultAssignButton");
		
		//Related To
		ccRootObject.comboBox("Select", "RelatedTo", data_Notes.get("RelatedTo"));
		//Confidential
		ccRootObject.comboBox("Select", "NoteConfidential", data_Notes.get("Confidential"));
		
		//No need to fill the Notes form again as we selected the template

		//Update
		ccRootObject.button("Click", "NoteUpdateButton");
		
		//check if note is created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating new Note '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("New Note from Template " + data_Notes.get("Template") + " created successully", MessageType.INFO);
	}

	/**
	 * Finds the Note template from the search result
	 * @param template
	 * 			- Template Name
	 * @return Exposure object, Else Null
	 */
	private TestObject findTemplateFromResult(String template) {
		
		TestObject templateResultTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				"NoteTemplateSearchResultTable");
		//Get All Rows
		Anchor rowAnchor = atDescendant(".class", "Html.TR");
		TestObject[] allTemplates = templateResultTable.find(rowAnchor, false);  
		//Find the template 
		TestObject templateToSelect = null;
		for( TestObject templateObj : allTemplates ) {
			TestObject temp = null;
			templateObj.waitForExistence();
			//by specified Types
			temp = ccRootObject.findDescendantTestObject(
					templateObj, ".class", "Html.TextNode", ".text", template);
			//Exit if we got the exposure
			if ( temp!= null ) {
				templateToSelect = templateObj;
				break;
			}
		}
		return templateToSelect;
	}	
	
	/**
	 * Search Notes : Method to search for a Note a verify details
	 * @param notesIndex
	 *            - Data Index for Note in "Notes" sheet
	 * @throws Exception 
	 */
	public void searchAndVerifyNote(String noteIndex) throws Exception{
		
		logger.log_Message("SearchForNote", MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_Notes = ClaimCenterData.getData("Notes", noteIndex);
		
		searchNote(data_Notes);
		logger.log_Message("Verifying Expected Notes Details", MessageType.INFO);
		TestObject noteFound = findNoteFromResult(
				data_Notes.get("Author"),
				data_Notes.get("Topic"),
				data_Notes.get("RelatedTo"),
				data_Notes.get("Subject"),
				data_Notes.get("Text"));
		
		if ( noteFound != null ) {
			logger.log_Message("Note Search and Verification is successful", MessageType.INFO);
		}
		else{
			logger.log_Message("Could not find Note with " +
					"Author :" + data_Notes.get("Author") +
					"Topic :" + data_Notes.get("Topic") +
					"RelatedTo :" + data_Notes.get("RelatedTo") +
					"Subject :" + data_Notes.get("Subject") +
					"Body :" + data_Notes.get("Text"),
					MessageType.INFO);
		}
	}

	/**
	 * Find the Row with expected note from the result
	 * @param data_Notes
	 * 			- Data to find
	 * @return - Note Row TestObject if matched else null
	 */
	private TestObject findNoteFromResult(
			String author, String topic, String relatedTo,
			String subject, String body) {
		
		TestObject notesResultTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "NotesResultsTable");

		//Get All Rows
		Anchor rowAnchor = atChild(".class", "Html.TR");
		TestObject[] allNotes = notesResultTable.getChildren()[0].find(rowAnchor, false);
		TestObject noteFound = null;
		for( TestObject noteRow : allNotes ) {
			TestObject temp = null;
			//Author
			temp = ccRootObject.findDescendantTestObject(
					noteRow, ".class", "Html.TextNode", ".text", author);
			if ( temp != null) {
				//Topic
				temp = ccRootObject.findDescendantTestObject(
						noteRow, ".class", "Html.TextNode", ".text", topic);
			}
			if ( temp != null) {
				//Related To
				temp = ccRootObject.findDescendantTestObject(
						noteRow, ".class", "Html.TextNode", ".text", relatedTo);
			}
			if ( temp != null) {
				//Subject
				temp = ccRootObject.findDescendantTestObject(
						noteRow, ".class", "Html.TextNode", ".text", subject);
			}
			if ( temp != null) {
				//Body
				temp = ccRootObject.findDescendantTestObject(
						noteRow, ".class", "Html.TextNode", ".text", body);
			}
			//Exit if we got the Note
			if ( temp!= null ) {
				noteFound = noteRow;
				break;
			}
		}
		return noteFound;
	}

	/**
	 * Fills the Note Search Criteria and does search
	 * @param data_Notes
	 * 			- Data to use for Search
	 * @throws Exception - If the search returns zero result
	 */
	private void searchNote(Hashtable<String, String> data_Notes)
			throws Exception {
		//Find the 
		ccRootObject.editBox("Input","NoteSearchFindText",data_Notes.get("Text"));
		
		//Find the note:Author
		ccRootObject.comboBox("Select","NoteSearchAuthor",data_Notes.get("Author"));
		
		//Find the Note:RelatedTo
		ccRootObject.comboBox("Select","NoteSearchRelatedTo",data_Notes.get("RelatedTo"));
		
		//Find the Note: Topic
		ccRootObject.comboBox("Select","NoteTopic",data_Notes.get("Topic"));
		
		//click on the search button
		ccRootObject.button("Click","SearchBtn");
		//check if search returned results
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Error while searching '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("Note Search results found", MessageType.INFO);
	}
	
	/**
	 * Edit the existing note with new values
	 * @param currentNoteIndex
	 * 			- Existing Note to find
	 * @param updatedNoteIndex
	 * 			- To values to edit with
	 * @throws Exception 
	 */
	public void editNoteTextAppendDate(String noteToUpdate) throws Exception {
		
		logger.log_Message("Editing the Note " + noteToUpdate + " with " + noteToUpdate, MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_Notes = ClaimCenterData.getData("Notes", noteToUpdate);
		
		//Search Note
		searchNote(data_Notes);
		logger.log_Message("Find the note from results", MessageType.INFO);
		TestObject noteFound = findNoteFromResult(
				data_Notes.get("Author"),
				data_Notes.get("Topic"),
				data_Notes.get("RelatedTo"),
				data_Notes.get("Subject"),
				data_Notes.get("Text"));
		
		//Click Edit to open the Note
		ccRootObject.linkinTable("Click", noteFound, "EditNoteLink");
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		ccRootObject.editBox("Input", "NotesText", "Appended Dt:" + dateFormat.format(date));
		ccRootObject.button("Click", "UpdateButton");
		//Verify that Note is updated
		TestObject updatedNoteFound = findNoteFromResult(
				data_Notes.get("Author"),
				data_Notes.get("Topic"),
				data_Notes.get("RelatedTo"),
				data_Notes.get("Subject"),
				"Appended Dt:" + dateFormat.format(date));
		if ( updatedNoteFound != null ) {
			logger.log_Message("Note Body Updated Successfully", MessageType.INFO);
		}
		else{
			logger.log_Message("Could not find the updated Note with values " +
					"Author :" + data_Notes.get("Author") +
					"Topic :" + data_Notes.get("Topic") +
					"RelatedTo :" + data_Notes.get("RelatedTo") +
					"Subject :" + data_Notes.get("Subject") +
					"Body :" + "Appended Dt:" + dateFormat.format(date),
					MessageType.INFO);
		}
	}
	
	/**
	 * Delete the existing note
	 * @param noteToDelete
	 * 			- Existing Note to find and delete
	 * @throws Exception 
	 */
	public void deleteNote(String noteToDelete) throws Exception {
		
		logger.log_Message("Deleting the Note " + noteToDelete, MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_Notes = ClaimCenterData.getData("Notes", noteToDelete);
		
		//Search Note
		searchNote(data_Notes);
		logger.log_Message("Find the note from results", MessageType.INFO);
		TestObject noteFound = findNoteFromResult(
				data_Notes.get("Author"),
				data_Notes.get("Topic"),
				data_Notes.get("RelatedTo"),
				data_Notes.get("Subject"),
				data_Notes.get("Text"));
		
		//Click Edit to open the Note
		ccRootObject.linkinTable("Click", noteFound, "DeleteNote");
		logger.log_Message("Note is Deleted", MessageType.INFO);
	}
}
