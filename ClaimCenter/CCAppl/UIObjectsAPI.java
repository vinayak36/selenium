package CCAppl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

import resources.CCAppl.UIObjectsAPIHelper;
import Utilities.Logger;
import Utilities.Logger.ScreenShotEvent;

import com.rational.test.ft.ObjectNotFoundException;
import com.rational.test.ft.UnsupportedActionException;
import com.rational.test.ft.object.interfaces.BrowserTestObject;
import com.rational.test.ft.object.interfaces.SelectGuiSubitemTestObject;
import com.rational.test.ft.object.interfaces.TestObject;
import com.rational.test.ft.object.interfaces.TextGuiTestObject;
import com.rational.test.ft.object.interfaces.ToggleGUITestObject;
import com.rational.test.ft.object.interfaces.TopLevelTestObject;
import com.rational.test.ft.script.Anchor;
import com.rational.test.ft.value.RegularExpression;

/**
 * Core Module which warps the RFT Test Objects The test objects are identified
 * by descriptive method using Objects Properties stored in excel
 * 
 * @since 2012/03/08
 * @author Vinayak Hegde
 */
public class UIObjectsAPI extends UIObjectsAPIHelper {
	private TestObject containerObject;
	private BrowserTestObject bObject;
	private Connection conn = null;
	private Statement stmnt = null;
	private String projectName = "";
	private Logger logger;

	/**
	 * Instantiates the Class using browser object
	 * 
	 * @param browserObject
	 *            - Browser Object
	 * @param log
	 *            - Logger
	 * @since 2012/03/08
	 */
	public UIObjectsAPI(BrowserTestObject browserObject, Logger log) {
		bObject = browserObject;
		TestObject[] docs = bObject.find(atDescendant(".class",
				"Html.HtmlDocument"));
		containerObject = docs[0];
		containerObject.waitForExistence();
		// Open the Excel Map
		openExcelMap("R:\\Test Automation Framework\\CC\\CC7.0\\TestCases\\MAP_ClaimCenter7.xls",
				"ClaimCenter7");
		logger = log;
	}

	/**
	 * Gets the root TestObject of this class
	 * 
	 * @return the root TestObject of this class
	 * @since 2012/03/08
	 */
	public TestObject getBrowserDocObject() {
		return containerObject;
	}

	/**
	 * Get the BrowserTestObject used in this class
	 * 
	 * @return the BrowserTestObject
	 * @since 2012/03/08
	 */
	public BrowserTestObject getCCBrowser() {
		return bObject;
	}

	/**
	 * Find the Edit box Control with specified Id and performs the specified
	 * function on it
	 * 
	 * @param func
	 *            - Action to perform on the editbox
	 * @param uniqueObjectID
	 *            - Id of the edit box
	 * @param input_data
	 *            - Data to enter
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public String editBox(String func, String uniqueObjectID, String input_data) throws Exception {
		String result = "FAIL";
		if (func.equalsIgnoreCase("Input")) {
			if ( !input_data.trim().isEmpty() ) {
				TextGuiTestObject editBox = findExcelMappedTestObject(containerObject, uniqueObjectID);
				if (editBox == null) {
					throw new ObjectNotFoundException("TextBox " + uniqueObjectID + " is not found");
				}
				if( isDisabled(editBox)) {
					throw new Exception("TextBox " + uniqueObjectID + " is disabled");
				}
				String exitingValue = (String) editBox.getProperty(".value");
				if ( !exitingValue.equalsIgnoreCase(input_data)) {
					//Html.TEXTAREA has issue when autoscroll. so setting the property value
					if( editBox.getProperty(".class").toString().equalsIgnoreCase("Html.TEXTAREA") ) {
						editBox.setProperty(".value",input_data);
					}
					else { 
						editBox.setText(input_data);
					}
					waitForBrowserReady();
				}
			}
			result = "PASS";
			logger.captureScreen(ScreenShotEvent.UserAction);
		}
		if (func.equalsIgnoreCase("Verify")) {

			TextGuiTestObject editBox = findExcelMappedTestObject(containerObject, uniqueObjectID);
			setCurrentLogFilter(ENABLE_LOGGING);
			if (editBox != null) {
				waitForBrowserReady();
				logger.log_Message("Verified Object " + uniqueObjectID, Logger.MessageType.INFO);
				result = "PASS";
			}
			else {
				logger.log_Message("Object " + uniqueObjectID + " not Found", Logger.MessageType.ERROR);
			}
			setCurrentLogFilter(DISABLE_LOGGING);
		}
		return result;
	}

	/**
	 * Find the EditBox Control with dynamic id and performs the specified action on it
	 * for e.g. EditBox in a table which has index
	 * @param func
	 *            - Action to perform on the editBox
	 * @param container
	 *            - Table name where the editBox is located
	 * @param htmlIDPrefix
	 *            - Prefix to append before htmlID
	 * @param uniqueObjectID
	 *            - Id of the edit box
	 * @param input_data
	 *            - Data to enter
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 */
	public String editBoxInTable(String func, TestObject container, String htmlIDPrefix, String uniqueObjectID, String input_data ) throws Exception {
		String result = "FAIL";
		// Find the Id in Excel
		Hashtable<String, String> editBoxProps = getObjectPropetiesFromExcelMap(uniqueObjectID);
		TextGuiTestObject editBox = null;
		if (!editBoxProps.get("ObjectHTMLID").isEmpty()) {
			RegularExpression htmlIdRE = null;
			if ( htmlIDPrefix == null || htmlIDPrefix.isEmpty()) {
				htmlIdRE = new RegularExpression(editBoxProps.get("ObjectHTMLID") + "$", true) ;
			}
			else {
				htmlIdRE = new RegularExpression( htmlIDPrefix + editBoxProps.get("ObjectHTMLID") + "$", true) ;
			}
			editBox = findDescendantTestObjectRE(container, ".id", htmlIdRE, null, null);
		}
		else{
			editBox = findExcelMappedTestObject(container, uniqueObjectID);
		}

		if (func.equalsIgnoreCase("Input")) {
			if (editBox == null) {
				throw new ObjectNotFoundException("TextBox " + uniqueObjectID + " is not found");
			}
			if ( !input_data.trim().isEmpty() ) {
				if( isDisabled(editBox) ) {
					throw new Exception("TextBox " + uniqueObjectID + " is disabled");
				}
				String exitingValue = editBox.getProperty(".value") == null ? "" : (String)editBox.getProperty(".value");
				if ( !exitingValue.equalsIgnoreCase(input_data)) {
					if( editBox.getProperty(".class").toString().equalsIgnoreCase("Html.TEXTAREA") ) {
						editBox.setProperty(".value",input_data);
					}
					else { 
						editBox.setText(input_data);
					}
					editBox.waitForExistence();
				}
			}
			result = "PASS";
			logger.captureScreen(ScreenShotEvent.UserAction);
		}
		if (func.equalsIgnoreCase("Verify")) {
			if (editBox != null) {
				result = "PASS";
			}
		}
		return result;
	}
	
	/**
	 * Find the Button Control with specified Id and performs the specified
	 * action on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param uniqueObjectID
	 *            - Id of the button
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public String button(String func, String uniqueObjectID) throws Exception {
		String result = "FAIL";
		TextGuiTestObject button = findExcelMappedTestObject(containerObject, uniqueObjectID);
		if (func.equalsIgnoreCase("Click")) {
			if (button == null) {
				throw new ObjectNotFoundException("Button "
						+ uniqueObjectID + " is not found");
			}
			button.waitForExistence();
			logger.captureScreen(ScreenShotEvent.ScreenChange);
			if( isDisabled(button)) {
				throw new Exception("Button " + uniqueObjectID + " is disabled");
			}
			button.click();
			waitForBrowserReady();
			result = "PASS";
		}

		if (func.equalsIgnoreCase("Verify")) {
			if (button != null) {
				result = "PASS";
			}
		}
		return result;
	}

	/**
	 * Find the Button Control with specified Id and performs the specified
	 * action on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param uniqueObjectID
	 *            - Id of the Button
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public String buttonInTable(String func, TestObject container, String uniqueObjectID) throws Exception {
		String result = "FAIL";

		TextGuiTestObject button = findExcelMappedTestObject(container, uniqueObjectID);
		if (func.equalsIgnoreCase("Click")) {
			if (button == null) {
				throw new ObjectNotFoundException("Button " + uniqueObjectID + " is not found");
			}
			button.waitForExistence();
			logger.captureScreen(ScreenShotEvent.ScreenChange);
			if( isDisabled(button) ) {
				throw new Exception("Button " + uniqueObjectID + " is disabled");
			}
			button.click();
			waitForBrowserReady();
			result = "PASS";
			logger.captureScreen(ScreenShotEvent.ScreenChange);
		}

		if (func.equalsIgnoreCase("Verify")) {
			if (button != null) {
				result = "PASS";
			}
		}
		return result;
	}	
	
	/**
	 * Find the Link Control with specified Id and performs the specified action
	 * on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param uniqueObjectID
	 *            - Id of the Link
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public String link(String func, String uniqueObjectID) throws Exception {
		String result = "FAIL";
		TextGuiTestObject link = findExcelMappedTestObject(containerObject, uniqueObjectID);
		if (func.equalsIgnoreCase("Click")) {
			if (link == null) {
				throw new ObjectNotFoundException("Link " + uniqueObjectID + " is not found");
			}
			link.waitForExistence();
			logger.captureScreen(ScreenShotEvent.ScreenChange);
			if( isDisabled(link) ) {
				throw new Exception("Link " + uniqueObjectID + " is disabled");
			}
			link.click();
			waitForBrowserReady();
			logger.captureScreen(ScreenShotEvent.ScreenChange);
			result = "PASS";
		}

		if (func.equalsIgnoreCase("Verify")) {
			if (link != null) {
				result = "PASS";
			}
		}
		return result;
	}

	/**
	 * Find the Link Control with specified Id and performs the specified action
	 * on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param uniqueObjectID
	 *            - Id of the Link
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public String linkinTable(String func, TestObject container, String uniqueObjectID) throws Exception {
		String result = "FAIL";

		TextGuiTestObject link = findExcelMappedTestObject(container, uniqueObjectID);
		if (func.equalsIgnoreCase("Click")) {
			if (link == null) {
				throw new ObjectNotFoundException("Link " + uniqueObjectID + " is not found");
			}
			link.waitForExistence();
			logger.captureScreen(ScreenShotEvent.ScreenChange);
			if( isDisabled(link) ) {
				throw new Exception("Link " + uniqueObjectID + " is disabled");
			}
			link.click();
			waitForBrowserReady();
			logger.captureScreen(ScreenShotEvent.ScreenChange);
			result = "PASS";
		}

		if (func.equalsIgnoreCase("Verify")) {
			if (link != null) {
				result = "PASS";
			}
		}
		return result;
	}
	
	/**
	 * Find the Link Control with specified Id and performs the specified action
	 * on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param uniqueObjectID
	 *            - Id of the Link
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/08/27
	 */
	public String linkinTable2(String func, TestObject container, String htmlIDPrefix,
			String uniqueObjectID) throws Exception {
			String result = "FAIL";
			// Find the Id in Excel
			Hashtable<String, String> linkInProps = getObjectPropetiesFromExcelMap(uniqueObjectID);
			if (linkInProps.get("ObjectHTMLID").isEmpty()
					&& linkInProps.get("ObjectLabel").isEmpty()) {
				throw new Exception(
						"Atleast ObjectID or ObjectLabel is required for identifying the UI Object");
			}
			TestObject linkInObject = null;
			if (!linkInProps.get("ObjectHTMLID").isEmpty() ) {
				RegularExpression htmlIdRE = null;
				if ( htmlIDPrefix == null || htmlIDPrefix.isEmpty()) {
					htmlIdRE = new RegularExpression(linkInProps.get("ObjectHTMLID") + "$", true) ;
				}
				else {
					htmlIdRE = new RegularExpression(htmlIDPrefix + linkInProps.get("ObjectHTMLID") + "$", true) ;
				}
				
				linkInObject = findDescendantTestObjectRE( container, ".id", htmlIdRE, null, null);
			}
			else {
				linkInObject = findExcelMappedTestObject(container, uniqueObjectID);
			}
			linkInObject.waitForExistence();
			if (func.equalsIgnoreCase("Click")) {
				if (linkInObject == null) {
					throw new ObjectNotFoundException("Link " + uniqueObjectID + " is not found");
				}

				ToggleGUITestObject link = new ToggleGUITestObject( linkInObject);
				
				if( isDisabled(link)) {
					throw new Exception("Link " + uniqueObjectID + " is disabled");
				}
					link.click();
					waitForBrowserReady();
				result = "PASS";
				logger.captureScreen(ScreenShotEvent.UserAction);
			}
			if (func.equalsIgnoreCase("Verify")) {
				if (linkInObject != null) {
					result = "PASS";
				}
			}
			return result;
	}	
	
	/**
	 * Find the Menu Control with specified Id and performs the specified action
	 * on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param menuBodyUniqueId
	 *            - Main Menu item
	 * @param subMenuItems
	 *            - List of sub menu items to perform the action
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public String menu(String func, String menuBodyUniqueId, String[] subMenuItems) throws Exception {

		String result = "FAIL";
		TextGuiTestObject menuList = findExcelMappedTestObject(containerObject, menuBodyUniqueId);
		if (func.equalsIgnoreCase("Click")) {
			for (String subItem : subMenuItems) {
				if ( (Integer)menuList.getProperty(".offsetHeight") < 0) {
					throw new Exception("Menu is not opened");
				}
				sleep(0.5);
				TextGuiTestObject subItemObj = findDescendantTestObject( menuList, ".text", subItem, "", "");
				if (subMenuItems[subMenuItems.length - 1].equalsIgnoreCase(subItem)) {
					logger.captureScreen(ScreenShotEvent.ScreenChange);
					if( isDisabled(subItemObj)) {
						throw new Exception("Menuitem " + subItem + " is disabled");
					}
					subItemObj.click();
					waitForBrowserReady();
					logger.captureScreen(ScreenShotEvent.ScreenChange);					
					result = "PASS";
					break;
				} else {
					subItemObj.click();
					waitForBrowserReady();
				}
			}
		}
		if (func.equalsIgnoreCase("VerifyMenuOpened")) {
			if ( (Integer)menuList.getProperty(".offsetHeight") > 0) {
				result = "PASS";
			}
		}
		return result;
	}

	/**
	 * Find the ComboBox Control with specified Id and performs the specified
	 * action on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param uniqueObjectID
	 *            - Id of the comboBox
	 * @param itemName
	 *            - Item name to select based on action to perform
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public String comboBox(String func, String uniqueObjectID, String itemName) throws Exception {
		String result = "FAIL";
		if (func.equalsIgnoreCase("Select")) {
			TestObject comboObj = findExcelMappedTestObject(containerObject,uniqueObjectID);
			if (!itemName.isEmpty()) {
				if (comboObj == null) {
					throw new ObjectNotFoundException("ComboBox " + uniqueObjectID + " is not found");
				}
				comboObj.waitForExistence();
				SelectGuiSubitemTestObject comboBox = new SelectGuiSubitemTestObject( comboObj);
				if( isDisabled(comboBox)) {
					throw new Exception("ComboBox " + uniqueObjectID + " is disabled");
				}
				if ( !comboBox.getText().contains(itemName) ) {
					throw new Exception("Item " + itemName + " Not Found in the combobox");
				}
				String exitingValue = (String) comboBox.getProperty(".value");
				if ( !exitingValue.equalsIgnoreCase(itemName)) {
					
					try {
						comboBox.select(itemName);
					}
					catch( UnsupportedActionException e)
					{
						bObject.inputKeys(itemName);
						bObject.inputKeys("{TAB}");
						//comboBox.setState(SINGLE_SELECT, atText(itemName));
						sleep(1);
					}
					waitForBrowserReady();
				}
				result = "PASS";
				logger.captureScreen(ScreenShotEvent.UserAction);
			}
		}
		if (func.equalsIgnoreCase("Verify")) {
			TestObject comboObj = findExcelMappedTestObject(containerObject,uniqueObjectID);
			if (comboObj != null) { 
				String text = (String) comboObj.getProperty(".text");
				if (text.contains(itemName)) {
					result = "PASS";
				}
			}
		}

		if (func.equalsIgnoreCase("VerifySelection")) {
			TestObject comboObj = findExcelMappedTestObject(containerObject,uniqueObjectID);
			if (comboObj != null) { 
				String text = (String) comboObj.getProperty(".value");
				if (text.contains(itemName)) {
					result = "PASS";
				}
			}
		}
		return result;
	}

	/**
	 * Find the ComboBox Control with specified Id and performs the specified
	 * action on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param table
	 *            - Table where the ComboBox is present 
	 * @param uniqueObjectID
	 *            - Id of the comboBox
	 * @param itemName
	 *            - Item name to select based on action to perform
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public String comboBoxinTable(String func, TestObject container, String htmlIDPrefix, String uniqueObjectID, String itemName) throws Exception {
		String result = "FAIL";
		
		// Find the Id in Excel
		Hashtable<String, String> comboBoxProps = getObjectPropetiesFromExcelMap(uniqueObjectID);
		if (comboBoxProps.get("ObjectHTMLID").isEmpty()
				&& comboBoxProps.get("ObjectLabel").isEmpty()) {
			throw new Exception(
					"Atleast ObjectID or ObjectLabel is required for identifying the UI Object");
		}
		SelectGuiSubitemTestObject comboBox = null;
		if (!comboBoxProps.get("ObjectHTMLID").isEmpty()) {
			RegularExpression htmlIdRE = null;
			if ( htmlIDPrefix == null || htmlIDPrefix.isEmpty()) {
				htmlIdRE = new RegularExpression(comboBoxProps.get("ObjectHTMLID") + "$", true) ;
			}
			else {
				htmlIdRE = new RegularExpression(htmlIDPrefix+comboBoxProps.get("ObjectHTMLID") + "$", true) ;
			}
			comboBox = new SelectGuiSubitemTestObject(
					findDescendantTestObjectRE(container, ".id", htmlIdRE, null, null));
		}
		else {
			comboBox = new SelectGuiSubitemTestObject(
					findExcelMappedTestObject(container, uniqueObjectID));		
		}
		if (func.equalsIgnoreCase("Select")) {
			if (!itemName.isEmpty()) {
				if (comboBox == null) {
					throw new ObjectNotFoundException("ComboBox "
							+ uniqueObjectID + " is not found");
				}
				comboBox.waitForExistence();

				if( isDisabled(comboBox)) {
					throw new Exception("ComboBox " + uniqueObjectID + " is disabled");
				}
				
				if ( !comboBox.getText().contains(itemName) ) {
					throw new Exception("Item " + itemName + " Not Found in the combobox");
				}

				String exitingValue = (String) comboBox.getProperty(".value");
				if ( !exitingValue.equalsIgnoreCase(itemName)) {
					
					try {
						comboBox.select(itemName);
					}
					catch( UnsupportedActionException e)
					{
						bObject.inputKeys(itemName);
						bObject.inputKeys("{TAB}");
						//comboBox.setState(SINGLE_SELECT, atText(itemName));
						sleep(1);
					}
					waitForBrowserReady();
				}
				result = "PASS";
				logger.captureScreen(ScreenShotEvent.UserAction);
			}
		}
		if (func.equalsIgnoreCase("Verify") && comboBox != null) {
			String text = (String) comboBox.getProperty(".text");
			if (text.contains(itemName)) {
				result = "PASS";
			}
		}

		if (func.equalsIgnoreCase("VerifySelection") && comboBox != null) {
			String text = (String) comboBox.getProperty(".value");
			if (text.contains(itemName)) {
				result = "PASS";
			}
		}
		return result;
	}

	/**
	 * Find the CheckBox Control with specified Id and performs the specified
	 * action on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param uniqueObjectID
	 *            - Id of the CheckBox
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public String checkBox(String func, String uniqueObjectID) throws Exception {
		String result = "FAIL";
		TestObject chkBoxObj = findExcelMappedTestObject(containerObject, uniqueObjectID);
		if (func.equalsIgnoreCase("Check")) {
			if (chkBoxObj == null) {
				throw new ObjectNotFoundException("CheckBox " + uniqueObjectID + " is not found");
			}
			chkBoxObj.waitForExistence();
			ToggleGUITestObject checkBox = new ToggleGUITestObject(chkBoxObj);
			if ((Boolean)checkBox.getProperty(".checked") == false) {
				
				if( isDisabled(checkBox)) {
					throw new Exception("CheckBox " + uniqueObjectID + " is disabled");
				}
				checkBox.click();
				waitForBrowserReady();
			}
			result = "PASS";
			logger.captureScreen(ScreenShotEvent.UserAction);
		}

		if (func.equalsIgnoreCase("UnCheck")) {
			if (chkBoxObj == null) {
				throw new ObjectNotFoundException("CheckBox "
						+ uniqueObjectID + " is not found");
			}
			chkBoxObj.waitForExistence();
			ToggleGUITestObject checkBox = new ToggleGUITestObject(chkBoxObj);
			if ((Boolean)checkBox.getProperty(".checked") == true) {

				if( isDisabled(checkBox)) {
					throw new Exception("CheckBox " + uniqueObjectID + " is disabled");
				}
				checkBox.click();
				waitForBrowserReady();
			}
			result = "PASS";
			logger.captureScreen(ScreenShotEvent.UserAction);
		}
		
		if (func.equalsIgnoreCase("Verify")) {
			if (chkBoxObj != null) {
				result = "PASS";
			}
		}
		return result;
	}

	/**
	 * Find the CheckBox Control with specified Id and performs the specified
	 * action on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param table
	 *            - Table name where the check box is located
	 * @param uniqueObjectID
	 *            - Id of the CheckBox
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public String checkBoxinTable(String func,TestObject container, String htmlIDPrefix, String uniqueObjectID) throws Exception {
		String result = "FAIL";
		
		// Find the Id in Excel
		Hashtable<String, String> checkBoxProps = getObjectPropetiesFromExcelMap(uniqueObjectID);
		if (checkBoxProps.get("ObjectHTMLID").isEmpty()
				&& checkBoxProps.get("ObjectLabel").isEmpty()) {
			throw new Exception(
					"Atleast ObjectID or ObjectLabel is required for identifying the UI Object");
		}
		//find checkBox in the table
		TestObject chkbocObj = null;
		if (!checkBoxProps.get("ObjectHTMLID").isEmpty()) {
			RegularExpression htmlIdRE = null;
			if ( htmlIDPrefix == null || htmlIDPrefix.isEmpty()) {
				htmlIdRE = new RegularExpression(checkBoxProps.get("ObjectHTMLID") + "$", true) ;
			}
			else {
				htmlIdRE = new RegularExpression(htmlIDPrefix + checkBoxProps.get("ObjectHTMLID") + "$", true) ;
			}
			chkbocObj = findDescendantTestObjectRE(container, ".id", htmlIdRE, null, null);
		}
		else {
			chkbocObj = findExcelMappedTestObject(container, uniqueObjectID);
		}
		if (func.equalsIgnoreCase("Check")) {
			if (chkbocObj == null) {
				throw new ObjectNotFoundException("CheckBox "
						+ uniqueObjectID + " is not found");
			}

			ToggleGUITestObject checkBox = new ToggleGUITestObject(chkbocObj);
			checkBox.waitForExistence();
			if ((Boolean)checkBox.getProperty(".checked") == false) {

				if( isDisabled(checkBox)) {
					throw new Exception("CheckBox " + uniqueObjectID + " is disabled");
				}
				checkBox.click();
				waitForBrowserReady();
			}
			result = "PASS";
			logger.captureScreen(ScreenShotEvent.UserAction);
		}

		if (func.equalsIgnoreCase("UnCheck")) {
			if (chkbocObj == null) {
				throw new ObjectNotFoundException("CheckBox "
						+ uniqueObjectID + " is not found");
			}

			ToggleGUITestObject checkBox = new ToggleGUITestObject(
					chkbocObj);
			checkBox.waitForExistence();
			if ((Boolean)checkBox.getProperty(".checked") == true) {

				if( isDisabled(checkBox)) {
					throw new Exception("CheckBox " + uniqueObjectID + " is disabled");
				}
				checkBox.click();
				waitForBrowserReady();
			}
			result = "PASS";
			logger.captureScreen(ScreenShotEvent.UserAction);
		}
		
		if (func.equalsIgnoreCase("Verify")) {
			if (chkbocObj != null) {
				result = "PASS";
			}
		}
		return result;
	}

	/**
	 * Find the RadioButton Control with specified Id and performs the specified
	 * action on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param uniqueObjectID
	 *            - Id of the CheckBox
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public String radioButton(String func, String uniqueObjectID) throws Exception {
		String result = "FAIL";
		TestObject radioObject = findExcelMappedTestObject(containerObject,uniqueObjectID);
		if (func.equalsIgnoreCase("Select")) {
			if (radioObject == null) {
				throw new ObjectNotFoundException("RadioButton "
						+ uniqueObjectID + " is not found");
			}
			radioObject.waitForExistence();
			ToggleGUITestObject radioButton = new ToggleGUITestObject(radioObject);
			if( isDisabled(radioButton)) {
				throw new Exception("RadioButton " + uniqueObjectID + " is disabled");
			}
			Boolean selected = (Boolean) radioButton.getProperty(".checked");
			if ( !selected ) {
				radioButton.select();
				waitForBrowserReady();
				waitForBrowserReady();
			}
			result = "PASS";
			logger.captureScreen(ScreenShotEvent.UserAction);
		}
		if (func.equalsIgnoreCase("Verify")) {
			if (radioObject != null) {
				result = "PASS";
			}
		}
		return result;
	}

	/**
	 * Find the RadioButton Control with specified Id and performs the specified
	 * action on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param table
	 *            - Dynamic part of the RadioButton ID which need to prefix with
	 *            htnl ID
	 * @param uniqueObjectID
	 *            - Id of the RadioButton
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public String radioButtoninTable(String func, TestObject container, String htmlIDPrefix,
			String uniqueObjectID) throws Exception {
		String result = "FAIL";
		
		// Find the Id in Excel
		Hashtable<String, String> radioBoxProps = getObjectPropetiesFromExcelMap(uniqueObjectID);
		if (radioBoxProps.get("ObjectHTMLID").isEmpty()
				&& radioBoxProps.get("ObjectLabel").isEmpty()) {
			throw new Exception(
					"Atleast ObjectID or ObjectLabel is required for identifying the UI Object");
		}
		TestObject radioObject = null;
		if (!radioBoxProps.get("ObjectHTMLID").isEmpty() ) {
			RegularExpression htmlIdRE = null;
			if ( htmlIDPrefix == null || htmlIDPrefix.isEmpty()) {
				htmlIdRE = new RegularExpression(radioBoxProps.get("ObjectHTMLID") + "$", true) ;
			}
			else {
				htmlIdRE = new RegularExpression(htmlIDPrefix + radioBoxProps.get("ObjectHTMLID") + "$", true) ;
			}
			
			radioObject = findDescendantTestObjectRE( container, ".id", htmlIdRE, null, null);
		}
		else {
			radioObject = findExcelMappedTestObject(container, uniqueObjectID);
		}
		radioObject.waitForExistence();
		if (func.equalsIgnoreCase("Select")) {
			if (radioObject == null) {
				throw new ObjectNotFoundException("RadioButton " + uniqueObjectID + " is not found");
			}

			ToggleGUITestObject radioButton = new ToggleGUITestObject( radioObject);
			
			if( isDisabled(radioButton)) {
				throw new Exception("RadioButton " + uniqueObjectID + " is disabled");
			}
			Boolean selected = (Boolean) radioButton.getProperty(".checked");
			if( !selected ) {
				radioButton.select();
				waitForBrowserReady();
			}
			result = "PASS";
			logger.captureScreen(ScreenShotEvent.UserAction);
		}
		if (func.equalsIgnoreCase("Verify")) {
			if (radioObject != null) {
				result = "PASS";
			}
		}
		return result;
	}

	/**
	 * Find the Html Text Control with specified Id and performs the specified
	 * action on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param uniqueObjectID
	 *            - Id of the htmlText
	 * @return - the text of the control
	 * @since 2012/03/08
	 */
	public String htmlText(String func, String uniqueObjectID) {
		String htmltext = null;
		try {
			TextGuiTestObject text = findExcelMappedTestObject(containerObject,uniqueObjectID);
			if (func.equalsIgnoreCase("Get")) {
				if (text == null) {
					throw new ObjectNotFoundException("Text " + uniqueObjectID + " is not found");
				}
				text.waitForExistence();
				htmltext = text.getProperty(".text").toString();
			}
		} catch (ObjectNotFoundException e) {
		}
		return htmltext;
	}

	/**
	 * Verifies that the text specified is shown on web page. Does manual
	 * verification
	 * 
	 * @param textMessage
	 *            - Action to perform
	 * @return - true if the text is shown on the page
	 * @since 2012/03/08
	 */
	public Boolean VerifyTextMessage(String textMessage) {
		Boolean messageFound = false;
		try {
			TextGuiTestObject text = findDescendantTestObject(containerObject, ".class", "Html.TextNode", ".text", textMessage);
			setCurrentLogFilter(ENABLE_LOGGING);
			if (text != null) {
				waitForBrowserReady();
				logger.log_Message("Verified Message - " + textMessage, Logger.MessageType.INFO);
				messageFound = true;
			}
			else {
				messageFound = false;
				logger.log_Message("Text Message - " + textMessage + " not Found", Logger.MessageType.ERROR);
			}
			setCurrentLogFilter(DISABLE_LOGGING);
		} catch (ObjectNotFoundException e) {
			// Object Not Found
			messageFound = false;
			logger.log_Message("Text Message - " + textMessage + " not Found", Logger.MessageType.ERROR);
			setCurrentLogFilter(DISABLE_LOGGING);
		}
		return messageFound;
	}
	
	/**
	 * Verify the Text Field with expected value
	 * @param section
	 * 			- Section of the screen page
	 * @param uniqueObjectID
	 * 			- IF of the object to verify
	 * @param expectedValue
	 * 			- Expected Value to compare
	 * @return
	 * 			- true if the exepctedValue matched
	 */
	public Boolean VerifyTextField(TestObject container, String uniqueObjectID, String expectedValue) {
		Boolean result = false;
		try {
			TextGuiTestObject textObject = findExcelMappedTestObject(container, uniqueObjectID);
			
			if( expectedValue.trim().isEmpty() && textObject == null) {
				//In this case we return true to skip the verification
				return true;
			}
			
			setCurrentLogFilter(ENABLE_LOGGING);
			if (textObject.getProperty(".text").toString().equalsIgnoreCase(expectedValue)) {
				logger.log_Message("Field - " + uniqueObjectID +
						" matched. Expected - \"" + expectedValue +
						"\" Actual - \"" + textObject.getProperty(".text") + "\"",
						Logger.MessageType.INFO );
				result = true;
			}
			else
			{
				logger.log_Message("Field - " + uniqueObjectID +
						" NOT matched. Expected - \"" + expectedValue +
						"\" Actual - \"" + textObject.getProperty(".text") + "\"",
						Logger.MessageType.ERROR );
				result = false;
			}
			setCurrentLogFilter(DISABLE_LOGGING);
		} catch (ObjectNotFoundException e) {
			// Object Not Found
			logger.log_Message("Field - " + uniqueObjectID + " not found", Logger.MessageType.ERROR);
			result = false;
		}
		return result;
	}
	
	/**
	 * Find the TextGuiTestObject on the page when containerObject is external
	 * 
	 * @param containerObject
	 *            - Container Object
	 * @param uniqueObjectID
	 *            - ID to find
	 * @return - returns the instance of TextGuiTestObject found
	 * @throws ObjectNotFoundException
	 *             - if TextGuiTestObject is not found with specified id
	 * @since 2012/03/08
	 */
	public TextGuiTestObject findExcelMappedTestObject( TestObject containerObject, String uniqueObjectID)
			throws ObjectNotFoundException {
		// Find the Id in Excel
		Hashtable<String, String> tObjProps = getObjectPropetiesFromExcelMap(uniqueObjectID);
		
		if (tObjProps.get("ObjectHTMLID").isEmpty() && tObjProps.get("ObjectLabel").isEmpty()) {
			throw new ObjectNotFoundException(
					"Atleast ObjectID or ObjectLabel is required for identifying the UI Object");
		}
		TextGuiTestObject testObject = null;
		//Set Regular Expression
		RegularExpression classRE = ( tObjProps.get("ObjectClass").isEmpty())?
				null :
				new RegularExpression(tObjProps.get("ObjectClass") + "$", true) ;

		RegularExpression labelRE = (tObjProps.get("ObjectLabel").isEmpty()) ?
				null:
				new RegularExpression(tObjProps.get("ObjectLabel") + "$", true) ;
		
		RegularExpression htmlIDRE = (tObjProps.get("ObjectHTMLID").isEmpty()) ?
				null:
				new RegularExpression(tObjProps.get("ObjectHTMLID") + "$", true) ;
		
		if (htmlIDRE != null) {
			testObject = findDescendantTestObjectRE(containerObject,
					".class", classRE, ".id", htmlIDRE);
		} else {
			if (classRE == null) {
				throw new ObjectNotFoundException(
					"Object Class is required for identifying the UI Object along with ObjectLabel");
			}
			// Try with .text
			testObject = findDescendantTestObjectRE(containerObject, ".class", classRE, ".text", labelRE);
			if (testObject == null) {
				// Try with .label
				testObject = findDescendantTestObjectRE(containerObject, ".class", classRE, "label", labelRE);					
			}
			if (testObject == null) {
				// Try with .title
				testObject = findDescendantTestObjectRE(containerObject, ".class", classRE, ".title", labelRE);					
			}
		}
		return testObject;
	}

	/**
	 * Find the TextGuiTestObject on the page when containerObject is external
	 * 
	 * @param containerObject
	 *            - Container Object
	 * @param property1
	 *            - Property 1
	 * @param value1
	 *            - Value 1
	 * @param property2
	 *            - Property 2
	 * @param value2
	 *            - Value 2
	 * @return - returns the instance of TextGuiTestObject found
	 * @since 2012/03/08
	 */
	public TextGuiTestObject findDescendantTestObject( TestObject containerObject,
			String property1, String value1,
			String property2, String value2) {
		
		Anchor anchor = null;
		if (property2 == null) {
			anchor = atDescendant(property1, value1);
		} else {
			anchor = atDescendant(property1, value1, property2, value2);
		}
		TextGuiTestObject desc = null;
		TestObject[] uiObjects = containerObject.find(anchor, false);
		if (uiObjects.length > 0) {
			desc = new TextGuiTestObject(uiObjects[0]);
			desc.waitForExistence();
		}

		return desc;
	}

	/**
	 * Find the TextGuiTestObject on the page when containerObject is external
	 * 
	 * @param containerObject
	 *            - Container Object
	 * @param property1
	 *            - Property 1
	 * @param value1
	 *            - Value 1
	 * @param property2
	 *            - Property 2
	 * @param value2
	 *            - Value 2
	 * @return - returns the instance of TextGuiTestObject found
	 * @since 2012/03/08
	 */
	public TextGuiTestObject findDescendantTestObjectRE( TestObject containerObject,
			String property1, RegularExpression value1,
			String property2, RegularExpression value2) {
		
		Anchor anchor = null;
		if (property2 == null) {
			anchor = atDescendant(property1, value1);
		} else {
			anchor = atDescendant(property1, value1, property2, value2);
		}
		TextGuiTestObject desc = null;
		
		TestObject[] uiObjects = containerObject.find(anchor, false);
		if (uiObjects.length > 0) {
			desc = new TextGuiTestObject(uiObjects[0]);
			desc.waitForExistence();
		}

		return desc;
	}
	
	/**
	 * Opens the Excel Object map file to read the objects properties and values
	 * 
	 * @param mapFile
	 *            - map file name
	 * @param project
	 *            - sheet name in the excel file
	 * @since 2012/03/08
	 */
	void openExcelMap(String mapFile, String project) {
		try {
			String queryString = "Driver={Microsoft Excel Driver (*.xls)};DBQ="
					+ mapFile + ";ReadOnly=1";
			conn = DriverManager.getConnection("jdbc:odbc:" + queryString, "",
					"");
			stmnt = conn.createStatement();
			projectName = project;
		} catch (Exception e) {
			//Swallow the error message
		}
	}
	
	/**
	 * Checks id the specified object is disabled
	 * Note: RFT "isEnabled()" method internally uses property ".disabled" which returns
	 * true always. So this method is required 
	 * @param object
	 * @return
	 * 		- Returns true if the object is disabled
	 */
	private Boolean isDisabled(TestObject object) {
		Boolean disabled;
		try {
			disabled = (Boolean)object.getProperty("disabled");
		} catch (Exception e) {
			disabled = false;
		} 
		return disabled;
	}
	
	/**
	 * Reads the Objects properties from the excel map file
	 * 
	 * @param uniqueObjectID
	 *            - Index of the record to read from the file
	 * @return - Collection of the properties
	 * @since 2012/03/08
	 */
	public Hashtable<String, String> getObjectPropetiesFromExcelMap(
			String uniqueObjectID) {
		Hashtable<String, String> objectProps = new Hashtable<String, String>();
		try {
			if (!uniqueObjectID.isEmpty()) {
				String query = "select ObjectClass, ObjectLabel, ObjectHTMLID from ["
						+ projectName
						+ "$] where UniqueObjectID='"
						+ uniqueObjectID + "';";
				ResultSet rs = stmnt.executeQuery(query);
				while (rs.next()) {
					String objClass = rs.getString("ObjectClass");
					String objLabel = rs.getString("ObjectLabel");
					String objID = rs.getString("ObjectHTMLID");
					if (objClass != null) {
						objectProps.put("ObjectClass", objClass);
					} else {
						objectProps.put("ObjectClass", "");
					}
					if (objLabel != null) {
						objectProps.put("ObjectLabel", objLabel);
					} else {
						objectProps.put("ObjectLabel", "");
					}
					if (objID != null) {
						objectProps.put("ObjectHTMLID", objID);
					} else {
						objectProps.put("ObjectHTMLID", "");
					}
				}
				rs.close();
			}
		} catch (Exception e) {
			//Swallow the exception message
		}
		return objectProps;
	}

	/**
	 * Close the Excel Object map if already opened
	 * 
	 * @since 2012/03/08
	 */
	void closeExcelMap() {
		try {
			if (stmnt != null)
				stmnt.close();
			if (conn != null)
				conn.close();
		} catch (SQLException e) {
			//Swallow the exception
		}
	}
	
	/**
	 * Waits till the browser is idle and ready for next actions. Used for sync
	 * 
	 * @since 2012/03/08
	 */
	public void waitForBrowserReady() {
		//Wait for the Browser to get ready state
		int bstatus = (Integer)bObject.getProperty(".readyState");
		while ( bstatus != 4) {
			bstatus = (Integer)bObject.getProperty(".readyState");
			sleep(0.5);
		}
		//For more stability wait 0.5 sec
		sleep(0.5);
		//Get the new browser root object
		//TestObject[] docs = bObject.find(atDescendant(".class", "Html.HtmlDocument"));
		//containerObject = docs[0];
	}

	/**
	 * Logs the message when exception is caught
	 * @param result
	 * 			- Result to return
	 * @param e
	 * 			- Exception to log 
	 * @return
	 * 			- returns the result with exception message attached to it 
	 */
	@SuppressWarnings("unused")
	private String logExceptionMessage(String result, Exception e) {
		result += " " + e.getMessage();
		StackTraceElement[] st = e.getStackTrace();
		String stack = "";
		for (StackTraceElement ste : st) {
			stack += " at Method " + ste.getMethodName() + " on line# "
					+ ste.getLineNumber() + " in file# " + ste.getFileName();

			if (ste.getClassName().equalsIgnoreCase("CCAppl")) {
				break;
			}
		}
		logger.log_Message(e.getMessage(), Logger.MessageType.ERROR);
		logger.log_Message(stack, Logger.MessageType.ERROR);
		logger.captureScreen(ScreenShotEvent.ERROR);
		logger.log_Message("Screenshot Path - " + logger.jpgFileName, Logger.MessageType.INFO);
		return result;
	}

	/**
	 * Clicks the object by finding the screen points.
	 * This is to overcome the UnsupportedActionException 
	 * @param uniqueObjectID
	 * @throws Exception 
	 */
	public void clickButtonToOpenContextMenu(String uniqueObjectIDButton, String uniqueObjectIDMenu) throws Exception {
		
		int noOfAttempts = 5;
		for( int attempt = 0; attempt < noOfAttempts; attempt++ ){
			button("Click", uniqueObjectIDButton);
			if ( menu("VerifyMenuOpened", uniqueObjectIDMenu, null).startsWith("PASS") )
			{
				//Context Menu Opened.
				break;
			}
		}
	}

	/**
	 * Find the ComboBoxes Control with specified TestObject and performs the specified
	 * action on it
	 * 
	 * @param func
	 *            - Action to perform
	 * @param containerObject
	 *            - TestObject of the comboBox
	 * @param itemName
	 *            - Item name to select based on action to perform
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/07/08
	 */
	public String comboBoxes(String func, TestObject containerObject, String itemName) throws Exception {
		String result = "FAIL";
		
		if (func.equalsIgnoreCase("Select")) {
			if (!itemName.isEmpty()) {
				if (containerObject == null) {
					throw new ObjectNotFoundException("ComboBox "
							+ containerObject + " is not found");
				}
				containerObject.waitForExistence();
				SelectGuiSubitemTestObject comboBox = new SelectGuiSubitemTestObject( containerObject);
				if( isDisabled(comboBox)) {
					throw new Exception("ComboBox " + containerObject + " is disabled");
				}
				if ( !comboBox.getText().contains(itemName) ) {
					throw new Exception("Item " + itemName + " Not Found in the combobox");
				}
				String exitingValue = (String) comboBox.getProperty(".value");
				if ( !exitingValue.equalsIgnoreCase(itemName)) {
					comboBox.select(itemName);
					//comboBox.setProperty(".value", itemName);
					waitForBrowserReady();
				}
				/*SelectGuiSubitemTestObject comboBox2 = new SelectGuiSubitemTestObject( comboObj);
				String exitingValue2 = (String) comboBox2.getProperty(".value");
				if ( !exitingValue2.equalsIgnoreCase(itemName)) {
					comboBox2.select(itemName);
					//comboBox.setProperty(".value", itemName);
					waitForBrowserReady();
				}*/
				result = "PASS";
				logger.captureScreen(ScreenShotEvent.UserAction);
			}
		}
		if (func.equalsIgnoreCase("Verify") && containerObject != null) {
			String text = (String) containerObject.getProperty(".text");
			if (text.contains(itemName)) {
				result = "PASS";
			}
		}

		if (func.equalsIgnoreCase("VerifySelection") && containerObject != null) {
			String text = (String) containerObject.getProperty(".value");
			if (text.contains(itemName)) {
				result = "PASS";
			}
		}
		return result;
	}

/**
	 * Find the Edit boxes Control with specified TestObject and performs the specified
	 * function on it
	 * 
	 * @param func
	 *            - Action to perform on the editbox
	 * @param uniqueObjectID
	 *            - Id of the edit box
	 * @param input_data
	 *            - Data to enter
	 * @return - PASS or FAIL based on action
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public String editBoxes(String func, TestObject containerObject, String input_data) throws Exception {
		String result = "FAIL";
		
		if (func.equalsIgnoreCase("Input")) {
			if (containerObject == null) {
				throw new ObjectNotFoundException("TextBox " + containerObject + " is not found");
			}
			if ( !input_data.trim().isEmpty() ) {
				if( isDisabled(containerObject)) {
					throw new Exception("TextBox " + containerObject + " is disabled");
				}
				String exitingValue = (String) containerObject.getProperty(".value");
				if ( !exitingValue.equalsIgnoreCase(input_data)) {
					//Html.TEXTAREA has issue when autoscroll. so setting the property value
					if( containerObject.getProperty(".class").toString().equalsIgnoreCase("Html.TEXTAREA") ) {
						containerObject.setProperty(".value", input_data);
					}
					else { 
						((TextGuiTestObject) containerObject).setText(input_data);
					}
					waitForBrowserReady();
				}
			}
			result = "PASS";
			logger.captureScreen(ScreenShotEvent.UserAction);
		}
		/*if (func.equalsIgnoreCase("Verify")) {
			IFtVerificationPoint verifyeditBox = vpManual(containerObject, null, containerObject);
			setCurrentLogFilter(ENABLE_LOGGING);
			if (verifyeditBox.compareAndLog(false)) {
				waitForBrowserReady();
				logger.log_Message("Verified Object " + uniqueObjectID, Logger.MessageType.INFO);
				result = "PASS";
			}
			setCurrentLogFilter(DISABLE_LOGGING);
		}*/
		return result;
	}
}
