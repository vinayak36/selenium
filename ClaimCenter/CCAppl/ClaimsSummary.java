package CCAppl;
import java.util.Hashtable;

import resources.CCAppl.ClaimsSummaryHelper;
import TestData.ClaimCenterData;
import Utilities.Logger;


/**
 * Implements the ClaimSummary Module
 * 
 * @author Kaushik
 */
public class ClaimsSummary extends ClaimsSummaryHelper {	
	private UIObjectsAPI ccRootObject;
	private Logger logger;
	
	/**
	 * Instantiates the Contacts and initiates root Test object and Logger
	 * 
	 * @param rootObject
	 *            - Root test object
	 * @param log
	 *            - Logger instance
	 * @since 2012/05/11
	 */
	public ClaimsSummary(UIObjectsAPI rootObject, Logger log) {
		ccRootObject = rootObject;
		logger = log;
	}

	/**
	 * Verifies the Claim status on Claim Status page after FNOL
	 * 
	 * @param dataIndex
	 *            - Index of the data to use
	 * @return True if the expected message found on the page
	 * @since 2012/05/08
	 */
	public void verifyFnolClaimStatus(String dataIndex) throws Exception {
		
		Hashtable<String, String> dataFNOL_CreateClaim = ClaimCenterData.getData("Create_Claim", dataIndex);
		
		//Verify if claim status is Open
		String expMesStatus = dataFNOL_CreateClaim.get("Status");
		
		Boolean claimStatus = ccRootObject.VerifyTextMessage(expMesStatus);

		if (claimStatus) {
			// If claim Status is correct
			logger.log_Message("Expected Message '" + expMesStatus + "' Found", Logger.MessageType.PASS);
		
		} else {
			logger.log_Message("Expected Message '" + expMesStatus + "' NOT Found", Logger.MessageType.ERROR);
			claimStatus = false;
		}

		//Verify if Line of Business as Given in FNOL step
		String expMesLOB = dataFNOL_CreateClaim.get("LineofBusiness");
		Boolean bolLoB = ccRootObject.VerifyTextMessage(expMesLOB);
		if (bolLoB) {
			// If claim Status is correct
			logger.log_Message("Expected Message '" + expMesLOB + "' Found", Logger.MessageType.PASS);
		
		} else {
			logger.log_Message("Expected Message '" + expMesLOB + "' NOT Found", Logger.MessageType.ERROR);
			bolLoB = false;
		}
	}

	/**
	 * Verify existing Claim Status: Verification of claim for an existing claim 
	 * 
	 * @param dataIndex
	 *            - Index of the data to use
	 * @return True if the expected message found on the page
	 * @since 2012/05/08
	 */
	public void verifyExistingClaimstatus(String dataIndex) {
		// TODO Auto-generated method stub
	}
}
