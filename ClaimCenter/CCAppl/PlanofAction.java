package CCAppl;

import java.util.Hashtable;

import resources.CCAppl.PlanofActionHelper;
import TestData.ClaimCenterData;
import Utilities.Logger;
import Utilities.Logger.MessageType;

import com.rational.test.ft.object.interfaces.TextGuiTestObject;

/**
 * Description   : Functional Test Script
 * @author  Vinayak Hegde
 */
public class PlanofAction extends PlanofActionHelper
{
	/**
	 * Script Name   : <b>PlanofAction</b>
	 * Generated     : <b>May 17, 2012 11:54:02 AM</b>
	 * Description   : Functional Test Script
	 * Original Host : WinNT Version 5.1  Build 2600 (S)
	 * 
	 * @since  2012/05/17
	 * @author Vinayak Hegde
	 */
	private UIObjectsAPI ccRootObject;
	private Logger logger;
	
	/**
	 * Instantiates the PlanofAction and initiates root Test object and Logger
	 * 
	 * @param rootObject
	 *            - Root test object
	 * @param log
	 *            - Logger instance
	 * @since 2012/03/17
	 */
	public PlanofAction(UIObjectsAPI rootObjects,Logger log){
		ccRootObject = rootObjects;
		logger = log;
	}

	/**
	 * Creates new Evaluation  in Opened Claim
	 * @param evalIndex
	 *            - Data Index for Evaluation in "Evaluation" sheet
	 * @throws Exception 
	 * 
	 */
	public void createNewEvalution(String evalIndex) throws Exception{

		logger.log_Message("Creating New Evaluation", MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_eval = ClaimCenterData.getData("Evaluations",evalIndex);

		//Click on  New Evaluations button
		ccRootObject.button("click","NewEvalutionBtn");

		//Enter the name for Evaluation
		ccRootObject.editBox("Input","EvaluationName",data_eval.get("Name"));

		//Select the related to value from drop-down:Mandatory
		ccRootObject.comboBox("Select","EvaluationExposure",data_eval.get("Exposure"));

		//Enter the Insured Liability percentage
		ccRootObject.editBox("Input","EvalInsuredLiability",data_eval.get("InsuredLiability%"));

		//Enter the Claimant Liability percentage
		ccRootObject.editBox("Input","EvalClaimantLiability",data_eval.get("ClaimantLiability%"));

		//Enter the other Liability percentage
		ccRootObject.editBox("Input","EvalOtherLiability",data_eval.get("OtherLiability%"));


		//Enter  the Bodily Damage:General Damages amount
		ccRootObject.editBox("Input","EvalBDGeneralDamage",data_eval.get("GeneralDamage$"));

		//Enter the Bodily Damage :Deductible amount
		ccRootObject.editBox("Input","EvalBDDeductible",data_eval.get("Deductible$"));

		//Enter the Bodily Damage :FLA amount 
		ccRootObject.editBox("Input","EvalBDFLA",data_eval.get("FLA$"));

		////Enter the  Bodily Damage :FLA Deductible amount
		ccRootObject.editBox("Input","EvalBDFLADeductible",data_eval.get("FLADeductible$"));

		//Enter the Bodily Damage :PJIAmountNonPecuniary amount
		ccRootObject.editBox("Input","EvalBDPJIAmountNonPecuniary",data_eval.get("PJIAmountNonPecuniaryt$"));

		//Enter the Bodily Damage: Past Wage Loss 
		ccRootObject.editBox("Input","EvalBDPastWageLoss",data_eval.get("PastWageLoss$"));

		//Enter the Bodily Damage:Past Housekeeping
		ccRootObject.editBox("Input","EvalBDPastHousekeeping",data_eval.get("PastHousekeeping$"));

		//Enter the Bodily Damage:other expenses
		ccRootObject.editBox("Input","EvalBDOtherExpenses",data_eval.get("OtherExpenses$"));

		//Enter the Bodily Damage: Past Healthcare Costs
		ccRootObject.editBox("Input","EvalBDPastHealthcareCosts",data_eval.get("PastHealthcareCosts$"));

		//Enter the Bodily Damage:Credits
		ccRootObject.editBox("Input","EvalBDCredits",data_eval.get("Credits$"));

		//Enter the likely Bodily Damage:PJI Amount Pecuniary amount
		ccRootObject.editBox("Input","EvalBDPJIAmountPecuniary",data_eval.get("PJIAmountPecuniary$"));

		//Enter the Bodily Damage:FutureEconomicLosses amount
		ccRootObject.editBox("Input","EvalBDFutureEconomicLosses",data_eval.get("FutureEconomicLosses$"));


		//Enter the Bodily Damage:Future Healthcare Cost amount
		ccRootObject.editBox("Input","EvalBDFutureHealthcareCosts",data_eval.get("FutureHealthcareCosts$"));

		//Enter the Bodily Damage: Credits for Future Healthcare Cost amount
		ccRootObject.editBox("Input","EvalBDFutureHealthcareCredit",data_eval.get("FutureHealthcareCredit$"));

		//Enter the Bodily Damage: Cost amount
		ccRootObject.editBox("Input","EvalBDCost",data_eval.get("BDCosts$"));

		//Enter the Bodily Damage: Disbursment amount
		ccRootObject.editBox("Input","EvalBDDisbursment",data_eval.get("BDDisbursment$"));


		//Enter the Accident Benefit:IRB amount
		ccRootObject.editBox("Input","EvalABIRB",data_eval.get("IRB$"));

		//Enter the Accident Benefit:Care giver amount
		ccRootObject.editBox("Input","EvalABHomeMaker",data_eval.get("HomeMaker$"));

		//Enter the Accident Benefit:Non Earner
		ccRootObject.editBox("Input","EvalABNonEarner ",data_eval.get("NonEarner$"));

		//Enter the Accident Benefit:Medical amount
		ccRootObject.editBox("Input","EvalABMedical",data_eval.get("Medical$"));

		//Enter the Accident Benefit:Rehabilitation amount
		ccRootObject.editBox("Input","EvalABRehabilitation",data_eval.get("Rehabilitation$"));

		//Enter the Accident Benefit:Attendant Care amount
		ccRootObject.editBox("Input","EvalABAttendantCare",data_eval.get("AttendantCare$"));

		//Enter the Accident Benefit:house keeping amount
		ccRootObject.editBox("Input","EvalABHousekeeping",data_eval.get("Housekeeping$"));

		//Enter the Accident Benefit:other amount
		ccRootObject.editBox("Input","EvalABOther",data_eval.get("ABOther$"));

		//Enter the Accident Benefit:Cost amount
		ccRootObject.editBox("Input","EvalABCost",data_eval.get("ABCost$"));

		//Enter the Accident Benefit: Disbursements  amount
		ccRootObject.editBox("Input","EvalABDisbursements",data_eval.get("ABDisbursements$"));

		//Enter the Accident Benefit: Comments
		ccRootObject.editBox("Input","EvalComments",data_eval.get("Comments"));

		//Click on the update button
		ccRootObject.button("Click","UpdateButton");

		//check if Evaluation is created  successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating'" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("Evaluation " + data_eval.get("Name") + " edited successully", MessageType.INFO);
	}

	/**Method to Edit a Evaluation
	 * @param newEvalIndex
	 *            - Data Index for Evaluation in "Evaluation" sheet
	 *  @throws Exception
	 *            **/
	public void editEvaluation(String newEvalIndex) throws Exception{

		logger.log_Message("Editing Evaluation", MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_eval = ClaimCenterData.getData("Evaluations",newEvalIndex);

		//select the Evaluation based on the Name
		selectEvaluation(data_eval.get("Name"));

		//Click on Edit button
		ccRootObject.button("Click","EditButton");

		//Enter the name for Evaluation:Mandatory
		ccRootObject.editBox("Input","EditEvalName",data_eval.get("Name"));

		//Select the related to value from drop-down:Mandatory
		ccRootObject.comboBox("Select","EvaluationExposure",data_eval.get("Exposure"));

		//Enter the Insured Liability percentage
		ccRootObject.editBox("Input","EvalInsuredLiability",data_eval.get("InsuredLiability%"));

		//Enter the Claimant Liability percentage
		ccRootObject.editBox("Input","EvalClaimantLiability",data_eval.get("ClaimantLiability%"));

		//Enter the other Liability percentage
		ccRootObject.editBox("Input","EvalOtherLiability",data_eval.get("OtherLiability%"));

		//Enter  the Bodily Damage:General Damages amount
		ccRootObject.editBox("Input","EvalBDGeneralDamage",data_eval.get("GeneralDamage$"));

		//Enter the Bodily Damage :Deductible amount
		ccRootObject.editBox("Input","EvalBDDeductible",data_eval.get("Deductible$"));

		////Enter the  Bodily Damage :FLA Deductible amount
		ccRootObject.editBox("Input","EvalBDFLADeductible",data_eval.get("FLADeductible$"));

		//Enter the Bodily Damage :PJIAmountNonPecuniary amount
		ccRootObject.editBox("Input","EvalBDPJIAmountNonPecuniary",data_eval.get("PJIAmountNonPecuniaryt$"));

		//Enter the Bodily Damage: Past Wage Loss 
		ccRootObject.editBox("Input","EvalBDPastWageLoss",data_eval.get("PastWageLoss$"));

		//Enter the Bodily Damage:Past Housekeeping
		ccRootObject.editBox("Input","EvalBDPastHousekeeping",data_eval.get("PastHousekeeping$"));

		//Enter the Bodily Damage:other expenses
		ccRootObject.editBox("Input","EvalBDOtherExpenses",data_eval.get("OtherExpenses$"));

		//Enter the Bodily Damage: Past Healthcare Costs
		ccRootObject.editBox("Input","EvalBDPastHealthcareCosts",data_eval.get("PastHealthcareCosts$"));

		//Enter the Bodily Damage:Credits
		ccRootObject.editBox("Input","EvalBDCredits",data_eval.get("Credits$"));

		//Enter the likely Bodily Damage:PJI Amount Pecuniary amount
		ccRootObject.editBox("Input","EvalBDPJIAmountPecuniary",data_eval.get("PJIAmountPecuniary$"));

		//Enter the Bodily Damage:FutureEconomicLosses amount
		ccRootObject.editBox("Input","EvalBDFutureEconomicLosses",data_eval.get("FutureEconomicLosses$"));

		//Enter the Bodily Damage:Future Healthcare Cost amount
		ccRootObject.editBox("Input","EvalBDFutureHealthcareCosts",data_eval.get("FutureHealthcareCosts$"));

		//Enter the Bodily Damage: Credits for Future Healthcare Cost amount
		ccRootObject.editBox("Input","EvalBDFutureHealthcareCredit",data_eval.get("FutureHealthcareCredit$"));

		//Enter the Bodily Damage: Cost amount
		ccRootObject.editBox("Input","EvalBDCost",data_eval.get("BDCosts$"));

		//Enter the Bodily Damage: Disbursment amount
		ccRootObject.editBox("Input","EvalBDDisbursment",data_eval.get("BDDisbursment$"));

		//Enter the Accident Benefit:IRB amount
		ccRootObject.editBox("Input","EvalABIRB",data_eval.get("IRB$"));

		//Enter the Accident Benefit:Care giver amount
		ccRootObject.editBox("Input","EvalABHomeMaker",data_eval.get("HomeMaker$"));

		//Enter the Accident Benefit:Non Earner
		ccRootObject.editBox("Input","EvalABNonEarner ",data_eval.get("NonEarner$"));

		//Enter the Accident Benefit:Medical amount
		ccRootObject.editBox("Input","EvalABMedical",data_eval.get("Medical$"));

		//Enter the Accident Benefit:Rehabilitation amount
		ccRootObject.editBox("Input","EvalABRehabilitation",data_eval.get("Rehabilitation$"));

		//Enter the Accident Benefit:Attendant Care amount
		ccRootObject.editBox("Input","EvalABAttendantCare",data_eval.get("AttendantCare$"));

		//Enter the Accident Benefit:house keeping amount
		ccRootObject.editBox("Input","EvalABHousekeeping",data_eval.get("Housekeeping$"));

		//Enter the Accident Benefit:other amount
		ccRootObject.editBox("Input","EvalABOther",data_eval.get("ABOther$"));

		//Enter the Accident Benefit:Cost amount
		ccRootObject.editBox("Input","EvalABCost",data_eval.get("ABCost$"));

		//Enter the Accident Benefit: Disbursements  amount
		ccRootObject.editBox("Input","EvalABDisbursements",data_eval.get("ABDisbursements$"));

		//Enter the Accident Benefit: Disbursements  amount
		ccRootObject.editBox("Input","EvalComments",data_eval.get("Comments"));

		//Click on the update button
		ccRootObject.button("Click","UpdateButton");

		//check if Evaluation is edited successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while editing '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("Evaluation " + data_eval.get("Name") + " edited successully", MessageType.INFO);

	}

	/**Method to select a evaluation based on the name
	 * @param String evalName
	 * 					-Name of the evaluation from the Evaluation Sheet.
	 * @throws Exception
	 * */
	private void selectEvaluation(String evalName) throws Exception {

		//Find Negotiation table
		TextGuiTestObject evaluationTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				"SettlementPlanEvaluationTable");

		//Find Evaluation based on Name
		TextGuiTestObject evaluationTableObject =  ccRootObject.findDescendantTestObject(
				evaluationTable , ".class", "Html.SPAN", ".text",evalName);
		if ( evaluationTableObject == null) {
			throw new Exception ("Evaluation with name " + evaluationTableObject  + " Not found"); 
		}
		//Get the row object with Negotiation
		String rowID =  evaluationTableObject.getProperty(".id").toString();
		TextGuiTestObject evalRow =  ccRootObject.findDescendantTestObject(
				evaluationTable , ".class", "Html.TR", ".id", rowID.replace("Name", "0"));
		ccRootObject.checkBoxinTable("Check", evalRow, null, "CheckBox"); 
	}

	/** Delere the Evaluation
	 * @param evalIndex
	 * @throws Exception
	 */
	public void deleteEvaluation(String evalIndex) throws Exception{

		logger.log_Message("Deleting  Evaluation", MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_eval = ClaimCenterData.getData("Evaluations",evalIndex);

		//select the Evaluation based on the Name
		selectEvaluation(data_eval.get("Name"));

		//click on delete button
		ccRootObject.button("Click","EvalDeleteBtn");

		//check if Evaluation is deleted successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while deleteing'" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("Evaluation " + data_eval.get("Name") + " deleted successully", MessageType.INFO);

	}

	/**Method to verify a Evaluation
	 * @param evalIndex
	 *            - Data Index for Evaluation in "Evaluation" sheet
	 *  @throws Exception
	 *
	 **/
	public void verifyEvaluation(String evalIndex) throws Exception{
		logger.log_Message("Verifying  Evaluation", MessageType.INFO);
		//Get the data
		Hashtable<String, String> data_eval = ClaimCenterData.getData("Evaluations",evalIndex);

		//select the Evaluation based on the Name
		selectEvaluation(data_eval.get("Name"));

		//Verify the name for Evaluation:Mandatory
		ccRootObject.editBox("Verify","EvaluationName",data_eval.get("Name"));

		//Verify the related to value from drop-down:Mandatory
		ccRootObject.comboBox("Verify","EvaluationExposure",data_eval.get("Exposure"));

		//Verify the Insured Liability percentage
		ccRootObject.editBox("Verify","EvalInsuredLiability",data_eval.get("InsuredLiability%"));

		//Verify the Claimant Liability percentage
		ccRootObject.editBox("Verify","EvalClaimantLiability",data_eval.get("ClaimantLiability%"));

		//Verify the other Liability percentage
		ccRootObject.editBox("Verify","EvalOtherLiability",data_eval.get("OtherLiability%"));


		//Verify  the Bodily Damage:General Damages amount
		ccRootObject.editBox("Verify","EvalBDGeneralDamage",data_eval.get("GeneralDamage$"));

		//Verify the Bodily Damage :Deductible amount
		ccRootObject.editBox("Verify","EvalBDDeductible",data_eval.get("Deductible$"));

		////Verify the  Bodily Damage :FLA Deductible amount
		ccRootObject.editBox("Verify","EvalBDFLADeductible",data_eval.get("FLADeductible$"));

		//Verify the Bodily Damage :PJIAmountNonPecuniary amount
		ccRootObject.editBox("Verify","EvalBDPJIAmountNonPecuniary",data_eval.get("PJIAmountNonPecuniaryt$"));

		//Verify the Bodily Damage: Past Wage Loss 
		ccRootObject.editBox("Verify","EvalBDPastWageLoss",data_eval.get("PastWageLoss$"));

		//Verify the Bodily Damage:Past Housekeeping
		ccRootObject.editBox("Verify","EvalBDPastHousekeeping",data_eval.get("PastHousekeeping$"));

		//Verify the Bodily Damage:other expenses
		ccRootObject.editBox("Verify","EvalBDOtherExpenses",data_eval.get("OtherExpenses$"));

		//Verify the Bodily Damage: Past Healthcare Costs
		ccRootObject.editBox("Verify","EvalBDPastHealthcareCosts",data_eval.get("PastHealthcareCosts$"));

		//Verify the Bodily Damage:Credits
		ccRootObject.editBox("Verify","EvalBDCredits",data_eval.get("Credits$"));

		//Verify the likely Bodily Damage:PJI Amount Pecuniary amount
		ccRootObject.editBox("Verify","EvalBDPJIAmountPecuniary",data_eval.get("PJIAmountPecuniary$"));

		//Verify the Bodily Damage:FutureEconomicLosses amount
		ccRootObject.editBox("Verify","EvalBDFutureEconomicLosses",data_eval.get("FutureEconomicLosses$"));

		//Verify the Bodily Damage:Future Healthcare Cost amount
		ccRootObject.editBox("Verify","EvalBDFutureHealthcareCosts",data_eval.get("FutureHealthcareCosts$"));

		//Verify the Bodily Damage: Credits for Future Healthcare Cost amount
		ccRootObject.editBox("Verify","EvalBDFutureHealthcareCredit",data_eval.get("FutureHealthcareCredit$"));

		//Verify the Bodily Damage: Cost amount
		ccRootObject.editBox("Verify","EvalBDCost",data_eval.get("BDCosts$"));

		//Verify the Bodily Damage: Disbursment amount
		ccRootObject.editBox("Verify","EvalBDDisbursment",data_eval.get("BDDisbursment$"));

		//Verify the Accident Benefit:IRB amount
		ccRootObject.editBox("Verify","EvalABIRB",data_eval.get("IRB$"));

		//Verify the Accident Benefit:Care giver amount
		ccRootObject.editBox("Verify","EvalABHomeMaker",data_eval.get("HomeMaker$"));

		//Verify the Accident Benefit:Non Earner
		ccRootObject.editBox("Verify","EvalABNonEarner ",data_eval.get("NonEarner$"));

		//Verify the Accident Benefit:Medical amount
		ccRootObject.editBox("Verify","EvalABMedical",data_eval.get("Medical$"));

		//Verify the Accident Benefit:Rehabilitation amount
		ccRootObject.editBox("Verify","EvalABRehabilitation",data_eval.get("Rehabilitation$"));

		//Verify the Accident Benefit:Attendant Care amount
		ccRootObject.editBox("Verify","EvalABAttendantCare",data_eval.get("AttendantCare$"));

		//Verify the Accident Benefit:house keeping amount
		ccRootObject.editBox("Verify","EvalABHousekeeping ",data_eval.get("Housekeeping$"));

		//Verify the Accident Benefit:other amount
		ccRootObject.editBox("Verify","EvalABOther",data_eval.get("ABOther$"));

		//Verify the Accident Benefit:Cost amount
		ccRootObject.editBox("Verify","EvalABCost",data_eval.get("ABCost$"));

		//Verify the Accident Benefit: Disbursements  amount
		ccRootObject.editBox("Verify","EvalABDisbursements",data_eval.get("ABDisbursements$"));

		//Verify the Accident Benefit: Disbursements  amount
		ccRootObject.editBox("Verify","EvalComments",data_eval.get("Comments"));
	}	
}
