package CCAppl;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Hashtable;

import resources.CCAppl.CCApplHelper;
import TestData.ClaimCenterData;
import TestData.ClaimCenterTestCase;
import Utilities.CCUtility;
import Utilities.Logger;
import Utilities.Logger.MessageType;
import Utilities.Logger.ScreenShotEvent;

import com.rational.test.ft.NotYetAbleToPerformActionException;
import com.rational.test.ft.UnsupportedActionException;
import com.rational.test.ft.object.interfaces.BrowserTestObject;
import com.rational.test.ft.object.interfaces.GuiTestObject;
import com.rational.test.ft.object.interfaces.ProcessTestObject;
import com.rational.test.ft.object.interfaces.RootTestObject;
import com.rational.test.ft.object.interfaces.TestObject;
import com.rational.test.ft.object.interfaces.TextGuiTestObject;
import com.rational.test.ft.script.ITestObjectMethodState;
import com.rational.test.ft.script.MouseModifiers;

/**
 * This is Master Test suite which defines all the automation Keywords in
 * CliamCenter application
 * 
 * @since 2012/03/08
 * @author Vinayak Hegde
 */
public class CCAppl extends CCApplHelper {
	private BrowserTestObject bObject;
	private UIObjectsAPI ccRootObject;
	private Logger logger;
	private ClaimCenterTestCase testCase;

	/**
	 * Opens the browser and navigates to the specified URL. It instantiates and
	 * returns the CCAppl object
	 * 
	 * @param applUrl
	 *            - ClaimCenter URL
	 * @param log
	 *            - Logger instance to log the messages
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	public static CCAppl openClaimCenter(String applUrl, Logger log) throws Exception {
		CCAppl appInstance = null;
		ProcessTestObject browserProcess = startBrowser(applUrl);
		browserProcess.waitForExistence(20, 2);
		TestObject browserObj = getOpenedBrowser(browserProcess, 20);
		if (browserObj == null) {
			throw new Exception("Unable to Open ClaimCenter");
		}
		appInstance = new CCAppl(new BrowserTestObject(browserObj), log);
		if (appInstance != null) {
			appInstance.bObject.maximize();
			appInstance.bObject.activate();
			appInstance.ccRootObject = new UIObjectsAPI(
					appInstance.bObject, log);
			//Wait till Login Screen is coming
			CCUtility.waitForTestObject(
					appInstance.ccRootObject.getBrowserDocObject(),
					"Log In", 20);
		}

		return appInstance;
	}// end of StartAppl

	/**
	 * Returns RFT TestObject of the browser documents based on the Process
	 * object. This makes sure that we have correct browser instance to execute
	 * the test cases
	 * 
	 * @param browserProcess
	 *            - Browser Windows Process
	 * @param timeoutSecs
	 *            - Maximum time to wait for browser to opened
	 * @since 2012/03/08
	 */
	private static TestObject getOpenedBrowser(ProcessTestObject browserProcess, int timeoutSecs) {
		int secsElapsed = 0;
		Boolean browserFound = false;
		TestObject browserExpected = null;
		while (secsElapsed < timeoutSecs && !browserFound) {
			//RootTestObject root = RootTestObject.getRootTestObject();
			TestObject[] openedBrowsers = find(atChild(".class", "Html.HtmlBrowser"));
			for (TestObject browser : openedBrowsers) {
//				if (browser.getProcess().getProcessId() == browserProcess.getProcessId()) {
//					browserExpected = browser;
//					browserFound = true;
//					break;
//				}
				//Temp to bypass the debug issue
				browserExpected = browser;
				browserFound = true;
				break;
			}
			sleep(1);
			secsElapsed++;
		}
		return browserExpected;
	}

	/**
	 * Close the ClaimCenter Application and the browser
	 * 
	 * @param appl
	 *            - ClaimCenter Application
	 * @since 2012/03/08
	 */
	public static void closeClaimCenter(CCAppl appl) {
		appl.ccRootObject.closeExcelMap();
		appl.bObject.getProcess().kill();
	}

	/**
	 * Instantiate the CCAppl and assign the browser and logger
	 * 
	 * @param browserObject
	 *            - Browser Object
	 * @param log
	 *            - Logger
	 * @since 2012/03/08
	 */
	private CCAppl(BrowserTestObject browserObject, Logger log) {
		bObject = browserObject;
		logger = log;
	}

	/**
	 * Sets the current test case for further use 
	 * @param tc
	 * 			- Test Case
	 */
	public void setTestCase(ClaimCenterTestCase tc) {
		testCase = tc;
	}

	/**
	 * Login Keyword: Reads the user name and password from Login sheet and
	 * Enters them to Login to ClaimCenter
	 * @param testStepType
	 *            - For Login, it is GlobalSettings Index
	 * @param dataIndex
	 *            - User Role, like DefaultUser, 
	 * @param expectedMessage
	 *            - Expected message in case we need to verify (not used in this
	 *            keyword)
	 * @since 2012/03/08
	 */
	@TCKeyword(Name = "Login")
	public Boolean login(String testStepType, String dataIndex, String expectedMessage) {
		Boolean passed = false;
		try {
			// Get Login Data
			Hashtable<String, String> dataLogin = ClaimCenterData.getData("Users", dataIndex);

			ccRootObject.editBox("Input", "LoginUserName", dataLogin.get("UserName"));
			ccRootObject.editBox("Input", "Password", dataLogin.get("Password"));
			ccRootObject.button("Click", "LogInBtn");

			// Verify the logout button
			if (ccRootObject.link("Verify", "LogOut").startsWith("PASS")) {
				logger.log_Message("Login sucessful", Logger.MessageType.INFO);
				passed = true;
			}
		} catch (Exception exception) {
			logExceptionMessage("Login", testStepType, dataIndex, exception);
		}
		return passed;
	}

	/**
	 * Logout Keyword: Clicks on logout button and closes the warning message if
	 * appears
	 * 
	 * @param testStepType
	 *            - Not used for Logout Keyword
	 * @param dataIndex
	 *            - not used in this keyword
	 * @param expectedMessage
	 *            - not used in this keyword
	 * @since 2012/03/08
	 */
	@TCKeyword(Name = "Logout")
	public Boolean logout(String testStepType, String dataIndex, String expectedMessage) {

		Boolean passed = false;
		try {
			ccRootObject.link("Click", "LogOut");
			Boolean unSavedTasksWarningDisplayed = false;
			// Find Logout Warning and close it
			TestObject diagObjs[] = RootTestObject.getRootTestObject().find(
					atChild(".class", "Html.Dialog"));
			if (diagObjs.length > 0) {
				TextGuiTestObject okBtn;
				unSavedTasksWarningDisplayed = true;
				diagObjs[0].waitForExistence();
				okBtn = ccRootObject.findExcelMappedTestObject(diagObjs[0],
				"DialogBoxOKBtn");
				logger.captureScreen(ScreenShotEvent.ERROR);
				try {
					okBtn.click();
				} catch (NotYetAbleToPerformActionException  e) {
					sleep(2);
					okBtn.click();
				} catch (UnsupportedActionException e) {
					sleep(2);
					okBtn.click();
				}
			}
			// Wait for complete Logout
			ccRootObject.waitForBrowserReady();

			// Verify the logout by verifying login button
			if (ccRootObject.button("Verify", "LogInBtn").startsWith("PASS")) {
				passed = true;
				if (unSavedTasksWarningDisplayed) {
					logger.log_Message(
							"Logout sucessful but 'Unsaved Work' Warning displayed and is Closed ",
							Logger.MessageType.WARNING);
				} else {
					logger.log_Message("Logout sucessful", Logger.MessageType.INFO);
				}
			}
		} catch (Exception exception) {
			logExceptionMessage("Logout", testStepType, dataIndex, exception);
		}
		return passed;
	}

	/**
	 * Create_Claim Keyword: Creates claim based on the data provided and
	 * testStepType
	 * 
	 * @param testStepType
	 *            - claim type, Auto, Quick or Auto first and final
	 * @param dataIndex
	 *            - Index of the data to use for creating the claim
	 * @param expectedMessage
	 *            - Expected message in case we need to verify
	 * @since 2012/03/08
	 */
	@TCKeyword(Name = "Create_Claim")
	public Boolean createNewClaim(String testStepType, String dataIndex, String expectedMessage) {
		Boolean result = false;
		ClaimRegistration creg = new ClaimRegistration(ccRootObject, logger);
		try {
			creg.createNewClaim(testStepType, dataIndex);
			String newClaimCreated = creg.verifyAndGetClaimNo(expectedMessage);
			if ( !newClaimCreated.isEmpty()) {
				//Save New Claim No for the test case
				logger.log_ClaimNumber(testCase.testCaseID(), newClaimCreated);
				testCase.SaveNewClaimNo(newClaimCreated);
				result = true;
				//Open Claim
				ccRootObject.link("Click", "GoToClaim");
			}
		} catch (Exception exception) {
			logExceptionMessage("Create_Claim", testStepType, dataIndex, exception);
		}
		return result;
	}

	/**
	 * SearchPolicy Keyword: Opens FNOL and Search Policy
	 * testStepType
	 * 
	 * @param testStepType
	 *            - 
	 * @param dataIndex
	 *            - Index of the data to use for creating the claim
	 * @param expectedMessage
	 *            - Expected message in case we need to verify
	 * @since 2012/03/08
	 */
	@TCKeyword(Name = "SearchPolicy")
	public Boolean searchPolicy(String testStepType, String dataIndex, String expectedMessage) {
		Boolean result = false;
		ClaimRegistration creg = new ClaimRegistration(ccRootObject, logger);
		try {
			creg.searchPolicy(testStepType, dataIndex);
			String newClaimCreated = creg.verifyAndGetClaimNo(expectedMessage);
			if ( !newClaimCreated.isEmpty()) {
				//Save New Claim No for the test case
				logger.log_ClaimNumber(testCase.testCaseID(), newClaimCreated);
				testCase.SaveNewClaimNo(newClaimCreated);
				result = true;
				//Open Claim
				ccRootObject.link("Click", "GoToClaim");
			}
		} catch (Exception exception) {
			logExceptionMessage("Create_Claim", testStepType, dataIndex, exception);
		}
		return result;
	}
	
	/**
	 *Claims Keyword: Performs Action on Claims
	 * 
	 * @param testStepType
	 *            - Sub Action to perform on Claims. Currently, the available actions are,
	 *            OpenClaim, AssignClaim, CloseClaim, VerifyClaimStatus
	 * @param dataIndex
	 *            - Data used to perform the sub action on Claims
	 * @param expectedMessage
	 *            - Expected message in case we need to verify
	 * @since 2012/03/08
	 */
	@TCKeyword(Name = "Claims")
	public Boolean claims(String testStepType, String dataIndex, String expectedMessage) {
		Boolean result = false;
		try {
			Claims cl = new Claims(ccRootObject, logger);
			if ( testStepType.trim().equalsIgnoreCase("OpenClaim") ) {
				result = cl.openClaim(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("AssignClaim") ) {
				result = cl.assignClaim(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("CloseClaim") ) {
				result = cl.closeClaim(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("VerifyClaimStatus") ) {
				result = cl.verifyClaimStatus(dataIndex);
			}
		} catch (Exception exception) {
			logExceptionMessage("Claims", testStepType, dataIndex, exception);
		}
		return result;
	}	

	/**
	 * Exposure Keyword: Keyword to performs the sub actions on Exposure.
	 * 
	 * @param testStepType
	 *            - SubAction to perform. The implemented sub actions are, CreateNewExposure, 
	 * AssignExposure, AssignAllExposures, RefreshExposure, CloseExposure, CreateReserveonExposure,
	 * PrintExportExposure and VerifyExposureDetails
	 * @param dataIndex
	 *            - Index of the data to use for creating the exposure
	 * @param expectedMessage
	 *            - Expected message in case we need to verify
	 * @since 2012/05/02
	 */
	@TCKeyword(Name = "Exposures")
	public Boolean exposures(String testStepType, String dataIndex, String expectedMessage) {
		Boolean result = false;
		try {
			//Click Exposures link
			ccRootObject.link("Click", "Exposures");
			Exposures exposure = new Exposures(ccRootObject, logger);
			if ( testStepType.trim().equalsIgnoreCase("CreateNewExposure") ) {
				//exposure.createNewExposure("Choose by Coverage", dataIndex);
				exposure.createNewExposure(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("AssignExposure") ) {
				exposure.assignExposure(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("AssignAllExposures") ) {
				exposure.assignAllExposures(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("RefreshExposure") ) {
				exposure.refreshExposure();
			}
			if ( testStepType.trim().equalsIgnoreCase("CloseExposure") ) {
				exposure.closeExposure(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("CreateReserveonExposure") ) {
				exposure.createReserve(dataIndex);
			}

			if ( testStepType.trim().equalsIgnoreCase("VerifyExposureDetails") ) {
				exposure.verifyExposureDetails(dataIndex);
			}
			result = true;
		} catch (Exception exception) {
			logExceptionMessage("Exposures", testStepType, dataIndex, exception);
		}
		return result;
	}

	/**
	 * AddIncident Keyword: Creates new Incident
	 * 
	 * @param testStepType
	 *            - Incident Type
	 * @param dataIndex
	 *            - Index of the data to use for creating the Incident
	 * @param expectedMessage
	 *            - Expected message in case we need to verify
	 * @since 2012/03/08
	 */
	@TCKeyword(Name = "CreateIncident")
	public Boolean addIncident(String testStepType, String dataIndex, String expectedMessage) {
		Boolean result = false;
		try {
			Incidents cinc = new Incidents(ccRootObject, logger);
			if ( testStepType.trim().equalsIgnoreCase("Auto") ) {
				cinc.addVehicleIncidentAUTO(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("Building") ||
				 testStepType.trim().equalsIgnoreCase("Contents/Non Building")  ||
				 testStepType.trim().equalsIgnoreCase("ALE/Loss of Rents") ||
				 testStepType.trim().equalsIgnoreCase("Property Liability") ||
				 testStepType.trim().equalsIgnoreCase("Watercraft") ) {
				
				cinc.addPropertyIncident(testStepType, dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("Injury") ) {
				cinc.addInjuryIncident(dataIndex);
			}
			result = true;
		} catch (Exception exception) {
			logExceptionMessage("CreateIncident", testStepType, dataIndex, exception);
		}
		return result;
	}

	/**
	 * WorkPlan Keyword: WorkPlan - Performs SubAction on WorkPlan
	 * 
	 * @param testStepType
	 *            - Sub Action to perform on WorkPlan. The available actions are,
	 *            CreateActivity, AssignActivity, UpdateActivity, AddNotestoActivity
	 *            SkipActivity, CompleteActivity, ApproveActivity and RejectActivity
	 * @param dataIndex
	 *            - Activity Index from Activities sheet
	 * @param expectedMessage
	 *            - Expected message in case we need to verify
	 * @since 2012/03/08
	 */
	@TCKeyword(Name = "WorkPlan")
	public Boolean workPlan(String testStepType, String dataIndex, String expectedMessage) {
		Boolean result = false;
		try {
			Workplan cwp = new Workplan(ccRootObject, logger);
			//Click WorkPlan
			ccRootObject.link("Click", "WorkPlan");
			if ( testStepType.trim().equalsIgnoreCase("CreateActivity") ) {
				cwp.createNewActivity(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("AssignActivity") ) {
				cwp.assignActivity(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("UpdateActivity") ) {
				cwp.updateActivity(dataIndex, false);
			}
			if ( testStepType.trim().equalsIgnoreCase("AddNotestoActivity") ) {
				cwp.updateActivity(dataIndex, true);
			}
			if ( testStepType.trim().equalsIgnoreCase("SkipActivity") ) {
				cwp.skipActivity(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("CompleteActivity") ) {
				cwp.completeActivity(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("ApproveActivity") ) {
				cwp.approveActivity(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("RejectActivity") ) {
				cwp.rejectActivity(dataIndex);
			}

			if(testStepType.trim().equalsIgnoreCase("AssignAllActivities")){
				cwp.assignAllActivities(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("SkipAllActivities")){
				cwp.skipAllActivities();
			}
			if(testStepType.trim().equalsIgnoreCase("CompleteAllActivities")){
				cwp.completeAllActivities();
			}
			if(testStepType.trim().equalsIgnoreCase("ApproveAllActivities")){
				cwp.approveAllActivities();
			}
			if(testStepType.trim().equalsIgnoreCase("RejectAllActivities")){
				cwp.rejectAllActivities();
			}

			result = true;
		} catch (Exception exception) {
			logExceptionMessage("WorkPlan", testStepType, dataIndex, exception);
		}
		return result;
	}

	/**
	 * Loss Details Keywords
	 * @param testStepType
	 * 			- Action to perform on Loss Details
	 * @param dataIndex
	 * 			- Data to Use from Loss Details Sheet
	 * @param expectedMessage
	 * 			- Expected Error Message, in case of negative test cases
	 * @return
	 */
	@TCKeyword(Name = "LossDetails")
	public Boolean lossDetails(String testStepType, String dataIndex, String expectedMessage) {
		Boolean result = false;
		try {
			LossDetails lossDetails = new LossDetails(ccRootObject, logger);
			//Click Loss Details
			ccRootObject.link("Click", "LossDetails");
			if ( testStepType.trim().equalsIgnoreCase("VerifyLossDetails") ) {
				lossDetails.verifyLossDetails(dataIndex);
			}
			if ( testStepType.trim().equalsIgnoreCase("UpdateLossDetails") ) {
				ccRootObject.button("Click", "EditButton");
				lossDetails.updateLossDetails(dataIndex);
			}
			result = true;
		} catch (Exception exception) {
			logExceptionMessage("LossDetails", testStepType, dataIndex, exception);
		}
		return result;
	}

	/**
	 * Notes Keyword: Notes - Performs Action on Notes
	 * 
	 * @param testStepType
	 *            - Sub Action to perform on Notes. The available actions are,
	 *            Create Notes,Create Note using template,Edit Notes,Search Notes and  Delete Notes
	 *            
	 * @param dataIndex
	 *            - Notes Index from Notes sheet
	 * @param expectedMessage
	 *            - Expected message in case we need to verify
	 * @since 2012/03/08
	 */
	@TCKeyword(Name = "Notes")
	public Boolean notes(String testStepType,  String dataIndex, String expectedMessage) {
		Boolean result = false;
		try{
			Notes cn = new Notes(ccRootObject, logger);
			//Click Notes
			ccRootObject.link("Click", "NotesLink");
			//TO create note without using any template;
			if ( testStepType.trim().equalsIgnoreCase("NewNote") ) {
				cn.createNewNote(dataIndex);
			}
			//To create note using a Template;
			if(testStepType.trim().equalsIgnoreCase("CreateNoteFromTemplate")){
				cn.createNoteFromTemplate(dataIndex);
			}
			//TO search for Note
			if(testStepType.trim().equalsIgnoreCase("SearchAndVerifyNote")){
				cn.searchAndVerifyNote(dataIndex);
			}

			//TO edit the Note Text
			if(testStepType.trim().equalsIgnoreCase("EditNoteText")){
				cn.editNoteTextAppendDate(dataIndex);
			}

			//TO Delete the Note
			if(testStepType.trim().equalsIgnoreCase("DeleteNote")){
				cn.deleteNote(dataIndex);
			}

			result = true;

		}catch(Exception exception){
			result = false;
			logExceptionMessage("Notes", testStepType, dataIndex, exception);	
		}
		return result;
	}

	/**
	 *PartiesInvolved_Contacts Keyword: Parties_Involved - Performs SubAction on PartiesInvolved_Contats
	 * 
	 * @param testStepType
	 *            - Sub Action to perform on PartiesInvolved_Contacts. Currently, the available actions are,
	 *            	newContact, addExistingContact, editContact, verifyContactDetails
	 * @param dataIndex
	 *            - Activity Index from Claim_Status sheet
	 * @param expectedMessage
	 *            - Expected message in case we need to verify
	 * @since 2012/03/08
	 */
	@TCKeyword(Name = "PartiesInvolved")
	public Boolean partiesInvolvedContacts(String testStepType, String dataIndex, String expectedMessage) {
		Boolean result = false;
		try {
			PartiesInvolved piContacts = new PartiesInvolved(ccRootObject,logger);

			//Parties Involved
			ccRootObject.link("Click", "PartiesInvolved");
			
			if ( testStepType.trim().equalsIgnoreCase("NewContact") ) {
				piContacts.createNewContact(dataIndex);
			}

			if ( testStepType.trim().equalsIgnoreCase("EditContact") ) {
				piContacts.editContact(dataIndex);
			}
			
			if ( testStepType.trim().equalsIgnoreCase("AddExistingContact") ) {
				piContacts.addExistingContact(dataIndex);
			}

			if ( testStepType.trim().equalsIgnoreCase("VerifyContactDetails") ) {
				result=piContacts.verifyContactExists(dataIndex);
			}

			if ( testStepType.trim().equalsIgnoreCase("DeleteContact") ) {
				
				piContacts.deleteContact(dataIndex);
			}
			
			if(testStepType.trim().equalsIgnoreCase("LinkContactsToAddressBook")){
				piContacts.linkContact(dataIndex);
			}
			
			if(testStepType.trim().equalsIgnoreCase("UnlinkContactsToAddressBook")){
				piContacts.unlinkContact(dataIndex);
			}
			result=true;
		} catch (Exception exception) {
			logExceptionMessage("PartiesInvolved_Contacts", testStepType, dataIndex, exception);
		}
		return result;
	}

	/**
	 * PlanofAction Keyword - Performs the actions on plnofAction tab
	 * @param testStepType
	 * 			- Sub actions which can be performed, CreateEvaluation, EditEvaluations, DeleteEvaluation
	 * 				VerifyEvaluation
	 * @param dataIndex
	 * 			- Evaluation data sheet index
	 * @param expectedMessage
	 * 			- Expected message if verification is required
	 * @return
	 */
	@TCKeyword(Name = "PlanofAction")
	public Boolean planofAction(String testStepType, String dataIndex, String expectedMessage){

		Boolean result=false;

		try{
			PlanofAction poa = new PlanofAction(ccRootObject, logger);

			//Click Settlement Plan
			ccRootObject.link("Click", "PlanofAction");

			//Create new Evaluation
			if(testStepType.trim().equalsIgnoreCase("CreateEvaluation")){
				poa.createNewEvalution(dataIndex);
			}

			//Edit an Evaluations
			if(testStepType.trim().equalsIgnoreCase("EditEvaluations")){
				poa.editEvaluation(dataIndex);
			}

			//Delete
			if(testStepType.trim().equalsIgnoreCase("DeleteEvaluation")){
				poa.deleteEvaluation(dataIndex);
			}

			//Verify
			if(testStepType.trim().equalsIgnoreCase("VerifyEvaluation")){
				poa.verifyEvaluation(dataIndex);
			}
			result=true;		

		}catch(Exception exception){

			logExceptionMessage("SettlementPlan_Evaluation", testStepType, dataIndex,exception);
		}		
		return result;
	}	

	/**
	 * Documents Keyword: Keyword to attach a document,edit, verify and delete document
	 * 
	 * @param testStepType
	 *            -  to attach a document,edit, verify and delete document to verify
	 * @param dataIndex
	 *            - dataIndex from Documents sheet
	 * @param expectedMessage
	 *            - not used in this keyword
	 * @since 2012/05/30
	 */
	@TCKeyword(Name = "Documents")
	public Boolean documents(String testStepType, String dataIndex, String expectedMessage){
		Boolean result = false;

		try{
			Documents cd = new Documents(ccRootObject,logger);

			ccRootObject.link("Click", "Documents");
		
			//Click Documents
			if(testStepType.trim().equalsIgnoreCase("AttachExistingDocument")){
				cd.attactExistingDocument(dataIndex);
			}

			if(testStepType.trim().equalsIgnoreCase("SearchDocument")){
				cd.searchDocument(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("EditDocument")){
				cd.editDocument(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("VerifyDocument")){
				cd.verifyDocument(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("DeleteDocuments")){
				cd.deleteDocument(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("IndicateExistingDocument")){
				cd.indicateExistanceofDocument(dataIndex);
			}
			result=true;

		}catch(Exception exception){
			logExceptionMessage("Documents", testStepType, dataIndex,exception);
		}
		return result;
	}

	/**
	 * Administration Keyword: Administration tab operations
	 * 
	 * @param testStepType
	 *            -  to attach a document,edit, verify and delete document
	 *              to verify
	 * @param dataIndex
	 *            - dataIndex from Documents sheet.
	 * @param expectedMessage
	 *            - not used in this keyword
	 * @since 2012/05/30
	 */
	@TCKeyword(Name = "Administration")
	public Boolean administration(String testStepType, String dataIndex, String expectedMessage){
		Boolean result = false;
		try{
			Administration admin = new Administration(ccRootObject,logger);

			if(testStepType.trim().equalsIgnoreCase("ViewUserDetails")){
				admin.viewUserDetails(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("CreateUser")){
				admin.createUser(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("EditUser")){
				admin.editUser(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("CreateGroup")){
				admin.createNewGroup(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("ViewGroupDetails")){
				admin.verifyGroupDetails(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("EditGroup")){
				admin.editGroup(dataIndex);
			}
			result=true;

		}catch(Exception exception){
			logExceptionMessage("Administration", testStepType, dataIndex,exception);
		}
		return result;
	}

	/**
	 * Address Keyword: Keyword to create,edit,search and delete new Address book contact.
	 * 
	 * @param testStepType
	 *            -  to  to create,edit,search and delete new Address book contact.
	 *             
	 * @param dataIndex
	 *            - dataIndex from Contacts sheet.
	 * @param expectedMessage
	 *            - not used in this keyword
	 * @since 2012/06/09
	 */
	@TCKeyword(Name="AddressBook")
	public Boolean addressBook(String testStepType,String dataIndex,String expectedMessage){
		Boolean result=false;
		try{
			//Open Address Book
			ccRootObject.link("Click", "AddressBookMenuBtn");
			AddressBook cad = new AddressBook(ccRootObject,logger);

			if(testStepType.trim().equalsIgnoreCase("CreateContact")){
				cad.createContact(dataIndex);
			}

			if(testStepType.trim().equalsIgnoreCase("EditContact")){
				cad.createContact(dataIndex);
			}

			if(testStepType.trim().equalsIgnoreCase("SearchAddressBook")){
				cad.searchAddressBook(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("DeleteContact")){
				cad.deleteContact(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("ViewContact")){
				cad.viewContact(dataIndex);
			}
			result=true;
		}catch(Exception exception){
			logExceptionMessage("AddressBook", testStepType, dataIndex,exception);
		}

		return result;
	}	

	/**
	 * Reserve: Performs SubAction on Reserves
	 * 
	 * @param testStepType
	 *            - Create Reserve 
	 * @param dataIndex
	 *            - Reserve Index from Reserves sheet
	 * @param expectedMessage
	 *            - Expected message in case we need to verify
	 * @since 2012/06/25
	 */
	@TCKeyword(Name="Reserves")
	public Boolean reserve(String testStepType,String dataIndex,String expectedMessage){
		Boolean result=false;
		try{
			Reserves cr =new Reserves(ccRootObject,logger);

			if(testStepType.trim().equalsIgnoreCase("CreateNewReserve")){
				cr.createReserve(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("VerifySummary")){
				cr.verifySummary(dataIndex);
			}
			result=true;

		}catch(Exception exception){
			logExceptionMessage("Reserves", testStepType, dataIndex,exception);
		}
		return result;
	}

	/**
	 * Financials:  Performs SubAction on Financials
	 * 
	 * @param testStepType
	 *            - Create manual Draft,cheque,quick cheque
	 * @param dataIndex
	 *            - Cheque Index from Reserves sheet
	 * @param expectedMessage
	 *            - Expected message in case we need to verify
	 * @since 2012/06/25
	 */
	@TCKeyword(Name="Financials")
	public Boolean cheques(String testStepType,String dataIndex,String expectedMessage){
		Boolean result=false;
		try{
			Financials ccq = new Financials(ccRootObject,logger);
			if(testStepType.trim().equalsIgnoreCase("CreateManualDraft")){
				ccq.createNewDraft(dataIndex);
			}

			if(testStepType.trim().equalsIgnoreCase("CreateCheque")){
				ccq.createNewCheque(dataIndex);
			}
			if(testStepType.trim().equalsIgnoreCase("QuickCheque")){
				ccq.createNewQuickCheque(dataIndex);
			}
			
			result=true;	
			
		}catch(Exception exception){
			logExceptionMessage("Cheques", testStepType, dataIndex,exception);
		}

		return result;
	}	
	
	/**
	 * UnverifiedPolicy_Claim Keyword: Creates claim on an unverified policy and based on the data provided and
	 * testStepType
	 * 
	 * @param testStepType
	 *            - claim type Personal Auto, Personal Property
	 * @param dataIndex
	 *            - Index of the data to use for creating the claim
	 * @param expectedMessage
	 *            - Expected message in case we need to verify
	 * @since 2012/07/02
	 */
	@TCKeyword(Name = "UnverifiedPolicy_Claim")
	public Boolean unverifiedPolicyNewClaim(String testStepType, String dataIndex, String expectedMessage) {
		Boolean result = false;
		ClaimRegistration creg = new ClaimRegistration(ccRootObject, logger);
		try {
			creg.unverifiedPolicy_CreateClaim(testStepType, dataIndex);
			String newClaimCreated = creg.verifyAndGetClaimNo(expectedMessage);
			if ( !newClaimCreated.isEmpty()) {
				//Save New Claim No for the test case
				testCase.SaveNewClaimNo(newClaimCreated);
				result = true;
			}
		} catch (Exception exception) {
			logExceptionMessage("UnverifiedPolicy_Claim", testStepType, dataIndex, exception);
		}
		return result;
	}	

	/**
	 * @param testStepType
	 * @param dataIndex
	 * @param expectedMessage
	 * @return
	 */
	@TCKeyword(Name="Search")
	public Boolean search(String testStepType,String dataIndex,String expectedMessage){
		Boolean result=false;

		try{
			Search css = new Search(ccRootObject,logger);
			if(testStepType.trim().equalsIgnoreCase("SearchForClaim")){
				css.searchClaim(dataIndex);			
			}

			if(testStepType.trim().equalsIgnoreCase("CreateAssociatedClaim")){
				css.createAssociatedClaims(dataIndex);		
			}

			if(testStepType.trim().equalsIgnoreCase("SearchAssociatedClaim")){
				css.searchAssociatedClaims(dataIndex);			
			}
			result=true;
		}
		catch(Exception exception){
			logExceptionMessage("Search", testStepType, dataIndex,exception);	
		}

		return result;
	}	

	/**
	 * @param testStepType
	 * @param dataIndex
	 * @param expectedMessage
	 * @return
	 */
	@TCKeyword(Name="ExternalAssignments")
	public Boolean externalAssignments(String testStepType, String dataIndex, String expectedMessage){
		Boolean result=false;
		try{
			ExternalAssignments exAsm = new ExternalAssignments(ccRootObject,logger);
			
			if(testStepType.trim().equalsIgnoreCase("CreateRentalAssignment")){
				exAsm.createRentalAssignment(dataIndex, false);
			}

			if(testStepType.trim().equalsIgnoreCase("SubmitNewRentalAssignment")){
				exAsm.submitNewRentalAssignment(dataIndex, false);
			}

			if(testStepType.trim().equalsIgnoreCase("CreateAudatexAssignment")){
				exAsm.createAudatexAssignment(dataIndex, false);
			}

			if(testStepType.trim().equalsIgnoreCase("SubmitNewAudatexAssignment")){
				exAsm.submitNewAudatexAssignment(dataIndex, false);
			}
			result=true;
		}
		catch(Exception exception){
			logExceptionMessage("Search", testStepType, dataIndex,exception);	
		}

		return result;
	}	
	
	/**
	 * @param testStepType
	 * @param dataIndex
	 * @param expectedMessage
	 * @return
	 */
	@TCKeyword(Name="Litigation")
	public Boolean litigation(String testStepType, String dataIndex, String expectedMessage){
		Boolean result=false;
		try{
			ccRootObject.link("Click", "LigitationLink");
			
			Litigation lgt = new Litigation(ccRootObject,logger);
			
			if ( testStepType.equalsIgnoreCase("NewMatter")) {
				lgt.NewMatter(dataIndex);
			}
			result = true;
		}
		catch(Exception exception){
			logExceptionMessage("Litigation", testStepType, dataIndex,exception);	
		}

		return result;
	}	
	
	/**
	 * Keyword annotation: Used to identify the methods which are ClaimCenter
	 * business Keywords
	 * 
	 * @param Name
	 *            - Name of the keyword
	 * @since 2012/03/08
	 */
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface TCKeyword {
		String Name() default "";
	}

	/**
	 * Logs the exception message in summary report for analysis and debugging
	 * 
	 * @param testStep
	 *            - Test Step name
	 * @param testStepType
	 *            - Type of Test Step
	 * @param dataIndex
	 *            - Test data index used for execution of the step
	 * @param exception
	 *            - Exception caught
	 * @since 2012/03/08
	 */
	private void logExceptionMessage(String testStep, String testStepType,
			String dataIndex, Throwable exception) {

		StackTraceElement[] st = exception.getStackTrace();
		String stack = "";
		for (StackTraceElement ste : st) {
			stack += " at Method " + ste.getMethodName() + " on line# "
			+ ste.getLineNumber() + " in file# " + ste.getFileName();
			if (ste.getClassName().equalsIgnoreCase(this.getClass().getName())) {
				// Do not get the stack beyond this class
				break;
			}
		}
		logger.log_Message("\"" + exception.getMessage() + "\"    " + stack, Logger.MessageType.ERROR);
		logger.captureScreen(ScreenShotEvent.ERROR);
		logger.log_Message("Screenshot Path - " + logger.jpgFileName, Logger.MessageType.INFO);
		logout("", "", "");
	}
}
