package CCAppl;
import java.util.Hashtable;

import com.rational.test.ft.object.interfaces.TestObject;
import com.rational.test.ft.script.LowLevelMouseEvent;

import resources.CCAppl.LossDetailsHelper;
import TestData.ClaimCenterData;
import Utilities.CCUtility;
import Utilities.Logger;
import Utilities.Logger.MessageType;

/**
 * Implements the ClaimCenter Incidents Module
 * @since 2012/05/11
 * @author Vinayak Hegde
 */
public class LossDetails extends LossDetailsHelper
{
	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Class and assigns root object and logger
	 * 
	 * @param rootObject
	 *            - root Test object
	 * @param log
	 *            - Logger instance to log the messages
	 * @since 2012/05/11
	 */
	public LossDetails(UIObjectsAPI rootObject, Logger log) { 
		ccRootObject = rootObject; logger = log;
	}
	
	/**
	 * Verifies the fields on Loss Details Screen with specified expected Loss Detail
	 * @param lossDetailsIndex
	 * 			- Loss Details Row to verify
	 * @throws Exception
	 */
	public void verifyLossDetails(String lossDetailsIndex) throws Exception {

		//Get the Loss Details Data
		Hashtable<String, String> data_LossDetails = ClaimCenterData.getData("LossDetails", lossDetailsIndex);
		logger.log_Message("Verifying the Loss Details - " + lossDetailsIndex, MessageType.INFO);
		
		//Get the MainContent Window.
		TestObject mainContent = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "MainContent");
	
		int failedFieldsCount = 0;
		//Loss Details
		//Cause of Loss
		if ( !ccRootObject.VerifyTextField(mainContent,
				"LossCauseText", data_LossDetails.get("CauseofLoss"))){
			failedFieldsCount++;
		}
		//TODO: Sub Cause of Loss
		//Fault Rating
		String percentage = "";
		if ( data_LossDetails.get("FaultRating").contains("%") ) {
			percentage = data_LossDetails.get("FaultRating"); 
		}
		else { 
			Integer perc = Integer.parseInt(data_LossDetails.get("FaultRating")) * 100;
			percentage = perc.toString() + "%";
		}
		if ( !ccRootObject.VerifyTextField(mainContent,
				"FaultRatingText", percentage )){
			failedFieldsCount++;
		}
		//Date of Loss
		if ( !ccRootObject.VerifyTextField(mainContent,
				"DateofLossText",
				CCUtility.convertDateFormat(data_LossDetails.get(
						"DateofLoss"),"yyyy-MM-dd hh:mm:ss", "MMM d, yyyy hh:mm a"))){
			failedFieldsCount++;
		}
		//TODO: Weather
		//Loss Location
		Hashtable<String, String> data_LossLocation = ClaimCenterData.getData("Locations",data_LossDetails.get("LossLocation"));
		String lossLocation = data_LossLocation.get("Address1");

		lossLocation = (data_LossLocation.get("Address2").isEmpty())?
				lossLocation : lossLocation + ", " + data_LossLocation.get("Address2");
		
		lossLocation = data_LossLocation.get("City").isEmpty()?lossLocation:
			lossLocation + ", " + data_LossLocation.get("City");

		String provicneCode = ClaimCenterData.getData(
				"ProvinceCodes", "Province", data_LossLocation.get("Province")).get("Code");
		
		if( provicneCode != null ) {
			lossLocation = provicneCode.isEmpty()? lossLocation :
				lossLocation + ", " + provicneCode;
		}
		
		lossLocation = data_LossLocation.get("PostalCode").isEmpty()?lossLocation:
			lossLocation + " " + data_LossLocation.get("PostalCode");
	
		if ( !ccRootObject.VerifyTextField(mainContent,
				"LocationText", lossLocation)){
			failedFieldsCount++;
		}
		/*//Is Address in Canada or US?
		if ( !ccRootObject.VerifyTextField(mainContent,
				"IsAddressInCanadaorUSText", data_LossLocation.get("IsAddressInCanadaOrUS"))){
			failedFieldsCount++;
		}*/
		//TODO: Location Description
		//Loss Description
		if ( !ccRootObject.VerifyTextField(mainContent,
				"Description", data_LossDetails.get("LossDescription"))){
			failedFieldsCount++;
		}
		//General
		//TODO: Catastrophe Code
		//Report Only?
		if ( !ccRootObject.VerifyTextField(mainContent,
				"ReportOnlyText", data_LossDetails.get("ReportOnly"))){
			failedFieldsCount++;
		}
		//TODO: Coverage in Question?
		//TODO: Litigation Status
		//Uninsured Loss?
		if ( !ccRootObject.VerifyTextField(mainContent,
				"UninsuredLossText", data_LossDetails.get("UninsuredLoss"))){
			failedFieldsCount++;
		}
		
		//TODO: Uninsured Amount
		//TODO: Reinsurance Applied?
		//TODO: Security risk
		//Notification and Contact
		//Date Reported
		if ( !ccRootObject.VerifyTextField(mainContent,
				"DateReportedText",
				CCUtility.convertDateFormat(data_LossDetails.get("DateReported"),
						"yyyy-MM-dd hh:mm:ss", "MMM d, yyyy hh:mm a")
						)){
			failedFieldsCount++;
		}
		//Date Input
		if ( !ccRootObject.VerifyTextField(mainContent,
				"DateInputText",
				CCUtility.convertDateFormat(data_LossDetails.get("DateInput"),
						"yyyy-MM-dd hh:mm:ss", "MMM d, yyyy hh:mm a")
						)){
			failedFieldsCount++;
		}
		//TODO: How Reported?
		//Reported By
		if ( !ccRootObject.VerifyTextField(mainContent,
				"ReportedByLink", data_LossDetails.get("ReportedBy"))){
			failedFieldsCount++;
		}
		//Relationship to Insured
		if ( !ccRootObject.VerifyTextField(mainContent,
				"ReportedByRelationshiptoInsuredText",
				data_LossDetails.get("RelationshipToPolicyholderReportedBy"))){
			failedFieldsCount++;
		}
		//Main Contact
		if ( !ccRootObject.VerifyTextField(mainContent,
				"MainContacLink", 
				data_LossDetails.get("MainContact"))){
			failedFieldsCount++;
		}

		if ( !ccRootObject.VerifyTextField(mainContent,
				"MainContactRelationshiptoInsuredText", 
				data_LossDetails.get("RelationshiptoPolicyholderMainContact"))){
			failedFieldsCount++;
		}
		
		//Relationship to Insured
		//Drivers
		//Is there a First Party driver?
		if ( !ccRootObject.VerifyTextField(mainContent,
				"IsFirstPartyDriverText", data_LossDetails.get("FirstPartyDriver"))){
			failedFieldsCount++;
		}
		//Is there one or more TP drivers?
		if ( !ccRootObject.VerifyTextField(mainContent,
				"IsTPDriverText ", data_LossDetails.get("TPDrivers"))){
			failedFieldsCount++;
		}
		//TODO: Vehicles
		//TODO: Properties
		//TODO: Injuries
		//TODO: Officials
		//TODO: Charges
		if ( failedFieldsCount != 0 ) {
			throw new Exception("Verification of Loss Details Failed");
		}
		
	}
	
	/**
	 * Modifies the Loss Details of the claims
	 * @param lossDetailsIndex
	 * 			- Data to use from Loss Details screen 
	 * @throws Exception
	 */
	public void updateLossDetails(String lossDetailsIndex) throws Exception {
		//Get the Loss Details Data
		Hashtable<String, String> data_LossDetails = ClaimCenterData.getData("LossDetails", lossDetailsIndex);
		logger.log_Message("Updating the Loss Details - " + lossDetailsIndex, MessageType.INFO);
		//Loss Details
		//Cause of Loss
		ccRootObject.comboBox("Select", "CauseOfLoss", data_LossDetails.get("CauseofLoss"));
		//Sub Cause of Loss
		ccRootObject.comboBox("Select", "SubCauseOfLoss", data_LossDetails.get("SubCauseofLoss"));
		//Fault Rating
		String faultRatingPerc = "";
		if ( data_LossDetails.get("FaultRating").contains("%") ) {
			faultRatingPerc = data_LossDetails.get("FaultRating"); 
		}
		else { 
			Integer perc = Integer.parseInt(data_LossDetails.get("FaultRating")) * 100;
			faultRatingPerc = perc.toString() + "%";
		}
		ccRootObject.comboBox("Select", "FaultRating", faultRatingPerc);

		//Date of Loss
		ccRootObject.editBox("Input", "DateofLoss", 
				CCUtility.convertDateFormat(
						data_LossDetails.get("DateofLoss"),"yyyy-MM-dd hh:mm:ss",
						"MM/dd/yyyy hh:mm a").replace("/", "").replace(" ", "").replace(":", ""));
		//Weather
		ccRootObject.comboBox("Select", "Weather", data_LossDetails.get("Weather"));
		logger.log_Message("Updating the Loss Location", MessageType.INFO);
		//Loss Location
		Hashtable<String, String> data_LossLocation = ClaimCenterData.getData("Locations",data_LossDetails.get("LossLocation"));
		
		String lossLocation = data_LossLocation.get("Address1");
		
		lossLocation = (data_LossLocation.get("Address2").isEmpty())?
				lossLocation : lossLocation + ", " + data_LossLocation.get("Address2");
		
		lossLocation = data_LossLocation.get("City").isEmpty()?lossLocation:
			lossLocation + ", " + data_LossLocation.get("City");
		
		String provicneCode = ClaimCenterData.getData(
				"ProvinceCodes", "Province", data_LossLocation.get("Province")).get("Code");
		if( provicneCode != null ) {
			lossLocation = provicneCode.isEmpty()? lossLocation :
				lossLocation + ", " + provicneCode;
		}	
		
		lossLocation = data_LossLocation.get("PostalCode").isEmpty()?lossLocation:
			lossLocation + " " + data_LossLocation.get("PostalCode");
		
		if ( ccRootObject.comboBox("Verify", "LossLocation", lossLocation).startsWith("PASS")){
			ccRootObject.comboBox("Select", "LossLocation", lossLocation);
		}
		else {
			ccRootObject.comboBox("Select", "LossLocation", "New...");
			AddressBook.addLocation(ccRootObject, data_LossDetails.get("LossLocation"));
		}
		if ( data_LossLocation.get("IsAddressInCanadaOrUS").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButton("Select", "IsAddressInCanadaorUSYes");
		}
		else if ( data_LossLocation.get("IsAddressInCanadaOrUS").equalsIgnoreCase("No")) {
			ccRootObject.radioButton("Select", "IsAddressInCanadaorUSNo");
		}
		
		//Loss Description
		ccRootObject.editBox("Input", "Description", data_LossDetails.get("LossDescription"));
		//General
		logger.log_Message("Updating the General Fields", MessageType.INFO);
		//Catastrophe Code
		ccRootObject.comboBox("Select", "CatastropheCode", data_LossDetails.get("CatastropheCode"));
		//Report Only?
		if ( data_LossDetails.get("ReportOnly").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButton("Select", "ReportOnlyYes");
		}
		else if ( data_LossDetails.get("ReportOnly").equalsIgnoreCase("No")) {
			ccRootObject.radioButton("Select", "ReportOnlyNo");
		}
		
		//Coverage in Question?
		if ( data_LossDetails.get("CoverageinQuestion").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButton("Select", "CoverageinQuestionYes");
		}
		else if ( data_LossDetails.get("CoverageinQuestion").equalsIgnoreCase("No")) {
			ccRootObject.radioButton("Select", "CoverageinQuestionNo");
		}
		//Litigation Status
		ccRootObject.comboBox("Select", "LitigationStatus", data_LossDetails.get("LitigationStatus"));
		//Uninsured Loss?
		if ( data_LossDetails.get("UninsuredLoss").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButton("Select", "UninsuredLossYes");
			//Uninsured Amount
			ccRootObject.editBox("Input", "UninsuredAmount", data_LossDetails.get("UninsuredAmount"));
		}
		else if ( data_LossDetails.get("UninsuredLoss").equalsIgnoreCase("No")) {
			ccRootObject.radioButton("Select", "UninsuredLossNo");
		}
		//Reinsurance Applied?
		//TODO: Security risk
		//Notification and Contact
		logger.log_Message("Updating Notification and Contact", MessageType.INFO);
		//Date Reported
		ccRootObject.editBox("Input", "DateReported", CCUtility.convertDateFormat(data_LossDetails.get("DateReported"),
				"yyyy-MM-dd hh:mm:ss", "MM/dd/yyyy hh:mm a").replace("/", "").replace(" ", "").replace(":", ""));
		//Date Input
		//How Reported?
		ccRootObject.comboBox("Select", "HowReported", data_LossDetails.get("HowReported"));
		//Reported By
		ccRootObject.comboBox("Select", "ReportedBy", data_LossDetails.get("ReportedBy"));
		//Relationship to Insured
		ccRootObject.comboBox("Select", "ReportedByRelationshipToInsured", 
				data_LossDetails.get("RelationshipToPolicyholderReportedBy"));
		//Main Contact
		ccRootObject.comboBox("Select", "MainContact", data_LossDetails.get("MainContact"));
		//Relationship to Insured
		ccRootObject.comboBox("Select", "MainContactRelationshipToInsured",
				data_LossDetails.get("RelationshiptoPolicyholderMainContact"));
		//Drivers
		logger.log_Message("Updating Drivers", MessageType.INFO);
		//Is there a First Party driver?
		ccRootObject.comboBox("Select", "IsThereAFirstPartyDriver", data_LossDetails.get("FirstPartyDriver"));
		//Is there one or more TP drivers?
		ccRootObject.comboBox("Select", "IsThereOneOrMoreTPDrivers", data_LossDetails.get("TPDrivers"));
		//TODO:Vehicles
		//TODO:Properties
		//TODO:Injuries
		//TODO:Officials
		//TODO:Charges
		//TODO:Witnesses
		ccRootObject.button("Click", "UpdateButton");
		//Check if the error found
		String validationSpanText = ccRootObject.htmlText("Get", "ValidationMessages");
		if (validationSpanText != null) {
			String screenName = validationSpanText.substring(0, validationSpanText.indexOf(":"));
			logger.log_Message("Found errors while closing Exposure " + screenName, Logger.MessageType.INFO);
			String validationMessages = validationSpanText.replace( screenName, "");
			logger.log_Message("Errors Message(s) '" + validationMessages + "'", Logger.MessageType.ERROR);
		}
		else {
			logger.log_Message("Loss Details updated with " + lossDetailsIndex + " modified.", MessageType.INFO);
		}
	}
}