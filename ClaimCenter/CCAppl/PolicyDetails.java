package CCAppl;

import java.util.Hashtable;

import resources.CCAppl.PolicyDetailsHelper;
import TestData.ClaimCenterData;
import Utilities.CCUtility;
import Utilities.Logger;
import Utilities.Logger.MessageType;
import Utilities.Logger.ScreenShotEvent;

import com.rational.test.ft.NotYetAbleToPerformActionException;
import com.rational.test.ft.UnsupportedActionException;
import com.rational.test.ft.object.interfaces.GuiTestObject;
import com.rational.test.ft.object.interfaces.TestObject;
import com.rational.test.ft.object.interfaces.TextGuiTestObject;
import com.rational.test.ft.script.ITestObjectMethodState;
import com.rational.test.ft.script.MouseModifiers;

/**
 * Implements the ClaimCenter PolicyDetails Methods. This is used to create Claims for UnVerified Policies
 * 
 * @author Kaushik
 */
public class PolicyDetails extends PolicyDetailsHelper {
	private UIObjectsAPI ccRootObject;
	private Logger logger;
	
	/**
	 * Instantiates the PolicyDetails and initiates root Test object and Logger
	 * 
	 * @param rootObject
	 *            - Root test object
	 * @param log
	 *            - Logger instance
	 * @since 2012/07/06
	 */
	public PolicyDetails(UIObjectsAPI rootObject, Logger log) {
		ccRootObject = rootObject;
		logger = log;
	}
	
	/**
	 * Adds New location 
	 * 
	 * @param locationIndex
	 *            - Index of the location
	 * @since 2012/07/06
	 * @author: Kaushik
	 */
	public void addLocation(String locationIndex) throws Exception {

		//Number
		ccRootObject.editBox("Input", "LocationNumber", locationIndex);
		AddressBook.addLocation(ccRootObject, locationIndex);
		//Update button
		ccRootObject.button("Click", "UpdateButton");
	}
	
	/**
	 * Adds Location Risk Details
	 * 
	 * @param locationIndex
	 *            - Index of the Location data to use for searching
	 * @param claimType
	 *            - Indicates the type of claim 
	 * @since 2012/07/06
	 * @author: Kaushik
	 */
	public void addLocationRisk(String locationIndex, String claimType) 
		throws Exception {

		Hashtable<String, String> data_location = ClaimCenterData.getData(
				"Locations", locationIndex);
		
		//Risk Number
		ccRootObject.editBox("Input", "RUNumber", data_location.get("RUNumber"));
		
		//Location Risk Province
		ccRootObject.comboBox("Select", "LocationRiskProvince",data_location.get("Province"));
		
		//Location History Form Type
		ccRootObject.editBox("Input", "LocationHistoryFormType", data_location.get("LocationHistoryFormType"));
		
		//Location Deductible Amount
		ccRootObject.editBox("Input", "LocationDeductibleAmt", data_location.get("LocationDeductibleAmt"));
		
		//Location Construction Year
		ccRootObject.editBox("Input", "LocationConstructionYear", data_location.get("LocationConstructionYear"));
		
		//Location Updated Heating
		ccRootObject.comboBox("Select", "LocationUpdatedHeating",data_location.get("LocationUpdatedHeating"));
		
		//Location Updated Plumbing
		ccRootObject.comboBox("Select", "LocationUpdatedPlumbing",data_location.get("LocationUpdatedPlumbing"));
		
		//Location Updated Wiring
		ccRootObject.comboBox("Select", "LocationUpdatedWiring",data_location.get("LocationUpdatedWiring"));
		
		//Location Updated Roofing
		ccRootObject.comboBox("Select", "LocationUpdatedRoofing",data_location.get("LocationUpdatedRoofing"));
		
	}
	
	/**
	 * Adds New Driver 
	 * 
	 * @param contactIndex
	 *            - Index of the Contact data to use for searching
	 * @since 2012/07/06
	 * @author: Kaushik
	 */
	public void addDrivers(String contactIndex) 
		throws Exception {

		Hashtable<String, String> data_contacts = ClaimCenterData.getData("Contacts", contactIndex);
		
		//Select a Driver
		ccRootObject.comboBox("Select", "PolicyDriversContact", data_contacts.get("ContactName"));
		
		//Input Driver number
		ccRootObject.editBox("Input", "PolicyDriversNumber", data_contacts.get("DriversLicenseNumber"));
		
		logger.log_Message("Succesfully able to Select contact  " +data_contacts.get("ContactName") , Logger.MessageType.PASS);
	}

	/**
	 * Verify Special Account 
	 * 
	 * @param contactIndex
	 *            - Index of the Contact data to use for searching
	 * @return return true if contact is Created
	 * @since 2012/08/10
	 * @author: Kaushik
	 */
	public void verifySpecialAccount(String contactIndex, String claimType) 
		throws Exception {

		Hashtable<String, String> data_contacts = ClaimCenterData.getData("Contacts", contactIndex);		
		
		if(data_contacts.get("Account No").equalsIgnoreCase("Yes")){
			if(ccRootObject.htmlText("Get", "AccountNumber").equalsIgnoreCase(data_contacts.get("Account No"))){
				logger.log_Message("Account No - " + data_contacts.get("Account No") + "Exixts",
					Logger.MessageType.INFO );
			}
			if(ccRootObject.htmlText("Get", "AccountName").equalsIgnoreCase(data_contacts.get("Account Name"))){
				logger.log_Message("Account Name - " + data_contacts.get("Account Name") + "Exixts",
						Logger.MessageType.INFO);
			}
		}
	}
	
	/**
	 * Add Coverages 
	 * 
	 * @param Coverages
	 *            - Index of the Contact data to use for searching
	 * @param claimType
	 *            - Indicates the type of claim 
	 * @since 2012/07/06
	 * @author: Kaushik
	 */
	public void addCoverages(String[] Coverages, String claimType) 
		throws Exception {
		
		int count = 0;
		
		//One Policy Coverage at a time
		for(String Coverage : Coverages){		
			
			count++;
			int append = count - 1; 
			
			//Get coverage date
			Hashtable<String, String> data_coverages = ClaimCenterData.getData(
					"Coverages", Coverage);
			
			if (claimType.equalsIgnoreCase("Personal auto")){
				//Click on Add Auto Policy Coverage button
				ccRootObject.button("Click", "AddEditableAutoPolicyCoverages");
				
				
				//Find autoCoverage table
				TextGuiTestObject autoCoveragesInfoTable = ccRootObject.findExcelMappedTestObject(
						ccRootObject.getBrowserDocObject(),
						"AutoPolicyCoveragesTable");
					
				//Find the Header name	
				TextGuiTestObject coveragesNameObject =  ccRootObject.findDescendantTestObject(
						autoCoveragesInfoTable, ".class", "Html.SPAN", ".text", "Name");			
				
				String[]coveragesRowSplit= coveragesNameObject.getProperty(".id").toString().split(":");			
				
				String Common = coveragesRowSplit[0]+":"+coveragesRowSplit[1]+":"+coveragesRowSplit[2]+":"+coveragesRowSplit[3]+":"+
				coveragesRowSplit[4]+":"+coveragesRowSplit[5];			
				
				String coverageType = Common+":"+append+":"+"CoverageType";	
				String coverageDesc = Common+":"+append+":"+"ex_CoverageDesc";
				String coverageDeduct = Common+":"+append+":"+"Deductible";
				String coverageIncidentLimit = Common+":"+append+":"+"IncidentLimit";
					
				TextGuiTestObject coverageTypeObject =  ccRootObject.findDescendantTestObject(
						autoCoveragesInfoTable, ".class", "Html.SELECT", ".id",coverageType);
				//Select coverage type			
				ccRootObject.comboBoxes("Select", coverageTypeObject, data_coverages.get("CoverageType"));
				
				//Find autoCoverage table
				TextGuiTestObject autoCoveragesInfoTable2 = ccRootObject.findExcelMappedTestObject(
						ccRootObject.getBrowserDocObject(),
						"AutoPolicyCoveragesTable");
				sleep(2);
				TextGuiTestObject coverageDescObject =  ccRootObject.findDescendantTestObject(
						autoCoveragesInfoTable2, ".class", "Html.INPUT.text", ".id",coverageDesc);
				sleep(2);
				//Edit coverage Description			
				ccRootObject.editBoxes("Input", coverageDescObject, data_coverages.get("CoverageDesc"));
				
				//Find autoCoverage table
				TextGuiTestObject autoCoveragesInfoTable3 = ccRootObject.findExcelMappedTestObject(
						ccRootObject.getBrowserDocObject(),
						"AutoPolicyCoveragesTable");
				sleep(2);
				TextGuiTestObject coverageDeductObject =  ccRootObject.findDescendantTestObject(
						autoCoveragesInfoTable3, ".class", "Html.INPUT.text", ".id",coverageDeduct);
				sleep(2);
				//Edit coverage Deductable	
				ccRootObject.editBoxes("Input", coverageDeductObject, data_coverages.get("CoverageDeduct"));
				
				//Find autoCoverage table
				TextGuiTestObject autoCoveragesInfoTable4 = ccRootObject.findExcelMappedTestObject(
						ccRootObject.getBrowserDocObject(),
						"AutoPolicyCoveragesTable");
				TextGuiTestObject coverageInciLimitObject =  ccRootObject.findDescendantTestObject(
						autoCoveragesInfoTable4, ".class", "Html.INPUT.text", ".id",coverageIncidentLimit);
				//Edit coverage Incident Limit	
				ccRootObject.editBoxes("Input", coverageInciLimitObject, data_coverages.get("CoverageIncidentLimit"));
			}
			if (claimType.equalsIgnoreCase("Personal Property")){
				
				//Add Editable Property Coverages
				ccRootObject.button("Click", "AddEditablePropertyCoverages");
				
				//Find autoCoverage table
				TextGuiTestObject propertycoverageInfoTable = ccRootObject.findExcelMappedTestObject(
						ccRootObject.getBrowserDocObject(),
						"PropertyCoveragesTable");
				//Find the Header name	
				TextGuiTestObject coveragesNameObject =  ccRootObject.findDescendantTestObject(
						propertycoverageInfoTable, ".class", "Html.SPAN", ".text", "Name");			
				
				String[]coveragesRowSplit= coveragesNameObject.getProperty(".id").toString().split(":");			
				
				String Common2 = coveragesRowSplit[0]+":"+coveragesRowSplit[1]+":"+coveragesRowSplit[2]+":"+coveragesRowSplit[3]+":"+
				coveragesRowSplit[4]+":"+coveragesRowSplit[5]+":"+coveragesRowSplit[6];			
				
				String coverageType = Common2+":"+append+":"+"CoverageType";	
				String coverageDeduct = Common2+":"+append+":"+"Deductible";
				String coverageIncidentLimit = Common2+":"+append+":"+"IncidentLimit";
					
				TextGuiTestObject coverageTypeObject =  ccRootObject.findDescendantTestObject(
						propertycoverageInfoTable, ".class", "Html.SELECT", ".id",coverageType);
				//Select coverage type			
				ccRootObject.comboBoxes("Select", coverageTypeObject, data_coverages.get("CoverageType"));
				
				//Find autoCoverage table
				TextGuiTestObject propertycoverageInfoTable2 = ccRootObject.findExcelMappedTestObject(
						ccRootObject.getBrowserDocObject(),
						"PropertyCoveragesTable");
				sleep(2);
				TextGuiTestObject coverageDeductObject =  ccRootObject.findDescendantTestObject(
						propertycoverageInfoTable2, ".class", "Html.INPUT.text", ".id",coverageDeduct);
				sleep(2);
				//Edit coverage Deductable	
				ccRootObject.editBoxes("Input", coverageDeductObject, data_coverages.get("CoverageDeduct"));
				
				//Find autoCoverage table
				TextGuiTestObject propertycoverageInfoTable3 = ccRootObject.findExcelMappedTestObject(
						ccRootObject.getBrowserDocObject(),
						"PropertyCoveragesTable");
				sleep(2);
				TextGuiTestObject coverageInciLimitObject =  ccRootObject.findDescendantTestObject(
						propertycoverageInfoTable3, ".class", "Html.INPUT.text", ".id",coverageIncidentLimit);
				sleep(2);
				//Edit coverage Incident Limit	
				ccRootObject.editBoxes("Input", coverageInciLimitObject, data_coverages.get("CoverageIncidentLimit"));
				
			}
			//check if Contact is created successfully
			String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
			if ( errorMessagesContents != null) {
				if (!errorMessagesContents.trim().isEmpty()) {
					String error = "Errors while creating coverage '" + errorMessagesContents + "'";
					throw new Exception(error);
				}
			}
			logger.log_Message("Succesfully able to create coverage  " + Coverage , Logger.MessageType.PASS);
		}
	}
}
