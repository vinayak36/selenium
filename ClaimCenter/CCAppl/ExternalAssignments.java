package CCAppl;
import java.util.Hashtable;

import resources.CCAppl.ExternalAssignmentsHelper;
import TestData.ClaimCenterData;
import Utilities.Logger;

import com.rational.test.ft.*;
import com.rational.test.ft.object.interfaces.*;
import com.rational.test.ft.object.interfaces.SAP.*;
import com.rational.test.ft.object.interfaces.WPF.*;
import com.rational.test.ft.object.interfaces.dojo.*;
import com.rational.test.ft.object.interfaces.siebel.*;
import com.rational.test.ft.object.interfaces.flex.*;
import com.rational.test.ft.object.interfaces.generichtmlsubdomain.*;
import com.rational.test.ft.script.*;
import com.rational.test.ft.value.*;
import com.rational.test.ft.vp.*;
import com.ibm.rational.test.ft.object.interfaces.sapwebportal.*;

/**
 * Implements the External Assignment. It provides methods to create, submit, edit and view the 
 * external assignments like Rental, Audatex and XactAnalysis
 * @author Vinayak Hegde
 */
public class ExternalAssignments extends ExternalAssignmentsHelper
{
	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Class and assigns root object and logger
	 * 
	 * @param rootObject
	 *            - root Test object
	 * @param log
	 *            - Logger instance to log the messages
	 * @since 2012/08/16
	 */
	public ExternalAssignments(UIObjectsAPI rootObject, Logger log) {
		ccRootObject = rootObject;
		logger = log;
	}
	
	/**
	 * Create Rental Assignment
	 * @param externalAssignmentIndex
	 * @param fromExposure
	 * @throws Exception
	 */
	public void createRentalAssignment(String externalAssignmentIndex, Boolean fromExposure) throws Exception {
		
		Hashtable<String, String> data_RentalAssignment = ClaimCenterData.getData("RentalAssignments", externalAssignmentIndex);
		openNewAssignment(fromExposure, "Rental", data_RentalAssignment);

		//Rental Assignment
		if( data_RentalAssignment.get("Discount").equalsIgnoreCase("Yes")) {
			fillDataNonDiscountRentalAssignment(data_RentalAssignment);
		}
		else {
			fillDataDiscountRentalAssignment(data_RentalAssignment);
		}
			
		ccRootObject.button("Click", "UpdateButton");
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating Assignment '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		//Click if Warnings are displayed, if yes close them and update again
		if ( clearWarnings()) {
			logger.log_Message("Warnings displayed while saving Assignment and Closed", Logger.MessageType.WARNING);
			ccRootObject.button("Click", "UpdateButton");
		}
	}

	/**
	 * @param fromExposure
	 * @param data_ExternalAssignment
	 * @throws Exception
	 */
	private void openNewAssignment(Boolean fromExposure, String assignmentType, Hashtable<String, String> data_ExternalAssignment) throws Exception {
		
		String[] assignmentMenuItems = null; 
		if( !fromExposure ) {
			//Click External Assignment
			ccRootObject.link("Click", "ExternalAssignmentLink");
			Hashtable<String, String> data_Incident = null;
			String incidentName = "";
			if ( data_ExternalAssignment.get("IncidentType").trim().isEmpty() )
			{
				throw new Exception("Incident Type should not be Empty");
			}
			if( data_ExternalAssignment.get("IncidentType").equalsIgnoreCase("Vehicle") || 
				data_ExternalAssignment.get("IncidentType").equalsIgnoreCase("Auto")) {
				data_Incident = ClaimCenterData.getData("VehicleIncidents", data_ExternalAssignment.get("Incident"));
				
				if( data_Incident.get("LossParty").equalsIgnoreCase("Insured") ) {
					incidentName = "I - " + data_Incident.get("SelectVehicle"); 
				}
				else if (data_Incident.get("LossParty").equalsIgnoreCase("Third Party")) {
					incidentName = "TP - " + data_Incident.get("Year") + " " + data_Incident.get("Make") + " " + data_Incident.get("Model"); 
				}
			}
			else if( data_ExternalAssignment.get("IncidentType").equalsIgnoreCase("Property")){
				//TODO: Not Implemented
			}
			assignmentMenuItems = new String[] { assignmentType, incidentName };
		}
		else {
			assignmentMenuItems = new String[] { assignmentType };
		}
		
		//Click assignment
		ccRootObject.button("Click", "NewAssignmentButton");
		ccRootObject.menu("Click", "NewAssignmentMenuList", assignmentMenuItems);
	}

	/**
	 * @param data_RentalAssignment
	 * @throws Exception
	 */
	private void fillDataDiscountRentalAssignment( Hashtable<String, String> data_RentalAssignment) throws Exception {
		
		//Discount
		ccRootObject.radioButton("Select", "RentalAssignmentDiscount");
		//Reservation Information
		//Regional Office
		ccRootObject.comboBox("Select", "RegionalOffice", data_RentalAssignment.get("RegionalOffice"));
		//Adjuster
		ccRootObject.comboBox("Select", "Adjuster", data_RentalAssignment.get("Adjuster"));
		//Rental Controlled By
		if(data_RentalAssignment.get("RentalControlledBy").equalsIgnoreCase("Adjuster")){
			ccRootObject.radioButton("Select", "RentalControlledByAdjuster");
		}
		else if(data_RentalAssignment.get("RentalControlledBy").equalsIgnoreCase("DRVP")){
			ccRootObject.radioButton("Select", "RentalControlledByDRVP");
		}
		else if(data_RentalAssignment.get("RentalControlledBy").equalsIgnoreCase("Rental Desk")){
			ccRootObject.radioButton("Select", "RentalControlledByRentalDesk");
		}
		//Existing Discount Rental
		if( data_RentalAssignment.get("ExistingDiscountRental").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButton("Select", "ExistingDiscountRentalYes");
		}
		else if( data_RentalAssignment.get("ExistingDiscountRental").equalsIgnoreCase("No")) {
			ccRootObject.radioButton("Select", "ExistingDiscountRentalNo");
		}
		//Discount Contract
		ccRootObject.editBox("Input", "RentalReservation", data_RentalAssignment.get("DiscountContract"));
		
		//Insured's Information 
		//Policyholder Name
		//Policyholder Phone
		ccRootObject.comboBox("Select", "PolicyholderPhone", data_RentalAssignment.get("PolicyholderPhone"));
		//Additional Information  
		//Additional Driver
		ccRootObject.comboBox("Select", "AdditionalDriver", data_RentalAssignment.get("AdditionalDriver"));
		//Alternate Phone
		ccRootObject.comboBox("Select", "AlternatePhone", data_RentalAssignment.get("AlternatePhone"));
		//Different/Third Party Driver
		if( data_RentalAssignment.get("DifferentDriverOrTPDriver").equalsIgnoreCase("Different Driver")){
			ccRootObject.radioButton("Select", "DifferentDriverfromNamedInsured");
			ccRootObject.comboBox("Select", "DifferentDriverName", data_RentalAssignment.get("DifferentorTPDriver"));
			ccRootObject.comboBox("Select", "DifferentDriverPhone", data_RentalAssignment.get("DifferentorTPDriverPhone"));
		}
		else if( data_RentalAssignment.get("DifferentDriverOrTPDriver").equalsIgnoreCase("Third Party")) {
			ccRootObject.radioButton("Select", "ThirdPartyDriver");
			ccRootObject.comboBox("Select", "ThirdPartyDriverName", data_RentalAssignment.get("DifferentorTPDriver"));
			ccRootObject.comboBox("Select", "ThirdPartyDriverPhone", data_RentalAssignment.get("DifferentorTPDriverPhone"));
		}
		//Repair Location
		//Name
		if ( ccRootObject.comboBox("Verify", "ReapirLocationName", data_RentalAssignment.get("RepairLocationName"))
				.startsWith("PASS") ) {
			ccRootObject.comboBox("Select", "ReapirLocationName", data_RentalAssignment.get("RepairLocationName"));
		}
		else {
			String[] tokens = data_RentalAssignment.get("RepairLocationName").split(" ");
			String[] vendorSearchMenuItems = new String[] { tokens[0] };
			ccRootObject.button("Click", "ReapirLocationNameMenuIcon");
			ccRootObject.menu("Click", "ReapirLocationNameMenuList", vendorSearchMenuItems);
			AddressBook ab = new AddressBook(ccRootObject, logger);
			ab.searchSelectContact(tokens[1]);
		}
		
		//Address
		//Phone
		ccRootObject.comboBox("Select", "RepairLocationPhone", data_RentalAssignment.get("RepairLocationPhone"));

		//Policy & Claim Information 
		//Tax Paid By
		if( data_RentalAssignment.get("TaxPaidBy").equalsIgnoreCase("Insurance")) {
			ccRootObject.radioButton("Select", "TaxPaidByInsurance");
		}
		else if( data_RentalAssignment.get("TaxPaidBy").equalsIgnoreCase("Policyholder")) {
			ccRootObject.radioButton("Select", "TaxPaidByPolicyHolder");
		}
		//Total Loss
		if( data_RentalAssignment.get("TotalLoss").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButton("Select", "PolicyandClaimInformationTotalLossYes");
		}
		else if( data_RentalAssignment.get("TotalLoss").equalsIgnoreCase("No")) {
			ccRootObject.radioButton("Select", "PolicyandClaimInformationTotalLossNo");
		}
		//Claim Type
		if(data_RentalAssignment.get("ClaimType").equalsIgnoreCase("Collision")){
			ccRootObject.radioButton("Select", "ClaimTypeCollision");
		}
		else if(data_RentalAssignment.get("ClaimType").equalsIgnoreCase("Comprehensive")){
			ccRootObject.radioButton("Select", "ClaimTypeComprehensive");
		}
		else if(data_RentalAssignment.get("ClaimType").equalsIgnoreCase("Theft")){
			ccRootObject.radioButton("Select", "ClaimTypeTheft");
		}
		
		//Policy Max
		ccRootObject.editBox("Input", "PolicyMax", data_RentalAssignment.get("PolicyMax"));
		//Transferable Coverage 
		if( data_RentalAssignment.get("PolicyMax").equalsIgnoreCase("Yes") ){
			ccRootObject.radioButton("Select", "TransferrableCoverageYes");
		}
		else if( data_RentalAssignment.get("PolicyMax").equalsIgnoreCase("No") ){
			ccRootObject.radioButton("Select", "TransferrableCoverageNo");
			//Transferable Coverage Paid By
			if ( data_RentalAssignment.get("TransferrableCoveragePaidBy").equalsIgnoreCase("Insurance")) {
				ccRootObject.radioButton("Select", "TransferrableCoveragePaidByInsurance");
			}
			else if ( data_RentalAssignment.get("TransferrableCoveragePaidBy").equalsIgnoreCase("Customer")) {
				ccRootObject.radioButton("Select", "TransferrableCoveragePaidByCustomer");
			}
		}
		//Authorization Information 
		//Upgrade to Diamond
		if ( data_RentalAssignment.get("UpgradetoDiamond").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButton("Select", "UpgradetoDiamondYes");
		}
		else if ( data_RentalAssignment.get("UpgradetoDiamond").equalsIgnoreCase("No")) {
			ccRootObject.radioButton("Select", "UpgradetoDiamondNo");
		}
		//Total Repair Estimate
		ccRootObject.editBox("Input", "TotalRepairEstimateHours", data_RentalAssignment.get("TotalRepairEstimateHours"));
		//Estimated Rental Period
		//Use estimated rental period
		if ( !data_RentalAssignment.get("TotalRepairEstimateHours").trim().isEmpty() ) {
			if ( data_RentalAssignment.get("UseEstimatedRentalPeriod").equalsIgnoreCase("Yes")) {
				ccRootObject.radioButton("Select", "UseEstimatedRentalPeriodYes");
			}
			else if ( data_RentalAssignment.get("UseEstimatedRentalPeriod").equalsIgnoreCase("No")) {
				ccRootObject.radioButton("Select", "UseEstimatedRentalPeriodNo");
			}
		}
		if ( data_RentalAssignment.get("UseEstimatedRentalPeriod").equalsIgnoreCase("No") ||
			 data_RentalAssignment.get("UseEstimatedRentalPeriod").trim().isEmpty() ) {
			
			//Pick Up Date
			ccRootObject.editBox("Input", "PickUpDate", data_RentalAssignment.get("PickUpDate"));
			//Authorized To Date
			ccRootObject.editBox("Input", "AuthorizedToDate", data_RentalAssignment.get("AuthorizedToDate"));
		}
		//Days Authorized
		ccRootObject.editBox("Input", "DaysAuthorized", data_RentalAssignment.get("DaysAuthorized"));
		//Note
		ccRootObject.editBox("Input", "AuthorizationInformationNote", data_RentalAssignment.get("Note"));
	}

	/**
	 * @param data_RentalAssignment
	 * @throws Exception
	 */
	private void fillDataNonDiscountRentalAssignment( Hashtable<String, String> data_RentalAssignment) throws Exception {

		//Non Discount
		ccRootObject.radioButton("Select", "RentalAssignmentNonDiscount");
		//Vehicle 
		//Loss of Use Limit
		ccRootObject.editBox("Input", "LossofUseLimit", data_RentalAssignment.get("LossofUseLimit"));
		//Rental Controlled By 
		if(data_RentalAssignment.get("RentalControlledBy").equalsIgnoreCase("Adjuster")){
			ccRootObject.radioButton("Select", "RentalControlledByAdjuster");
		}
		else if(data_RentalAssignment.get("RentalControlledBy").equalsIgnoreCase("DRVP")){
			ccRootObject.radioButton("Select", "RentalControlledByDRVP");
		}
		else if(data_RentalAssignment.get("RentalControlledBy").equalsIgnoreCase("Rental Desk")){
			ccRootObject.radioButton("Select", "RentalControlledByRentalDesk");
		}
		//Rental Begin Date
		ccRootObject.editBox("Input", "RentalBeginDate", data_RentalAssignment.get("RentalBeginDate"));
		//Rental End Date
		ccRootObject.editBox("Input", "RentalEndDate", data_RentalAssignment.get("RentalEndDate"));
		//Reported Daily Rate
		ccRootObject.editBox("Input", "ReportedDailyRate", data_RentalAssignment.get("ReportedDailyRate"));
		//Rental Agency
		if ( ccRootObject.comboBox("Verify", "RentalAgency", data_RentalAssignment.get("RentalAgency"))
				.startsWith("PASS") ) {
			ccRootObject.comboBox("Select", "RentalAgency", data_RentalAssignment.get("RentalAgency"));
		}
		else {
			String[] tokens = data_RentalAssignment.get("RentalAgency").split(" ");
			String[] vendorSearchMenuItems = new String[] { tokens[0] };
			ccRootObject.button("Click", "VendorButton");
			ccRootObject.menu("Click", "VendorMenuList", vendorSearchMenuItems);
			AddressBook ab = new AddressBook(ccRootObject, logger);
			ab.searchSelectContact(tokens[1]);
		}
		//Reservation
		ccRootObject.editBox("Input", "RentalReservation", data_RentalAssignment.get("Reservation"));
	}

	/**
	 * Submit new Rental Assignment
	 * @param externalAssignmentIndex
	 * @param fromExposure
	 * @throws Exception
	 */
	public void submitNewRentalAssignment(String externalAssignmentIndex, Boolean fromExposure) throws Exception {
		Hashtable<String, String> data_RentalAssignment = ClaimCenterData.getData("RentalAssignments", externalAssignmentIndex);
		openNewAssignment(fromExposure, "Rental", data_RentalAssignment);

		//Rental Assignment
		if( data_RentalAssignment.get("Discount").equalsIgnoreCase("Yes")) {
			fillDataNonDiscountRentalAssignment(data_RentalAssignment);
		}
		else {
			fillDataDiscountRentalAssignment(data_RentalAssignment);
		}
		
		ccRootObject.button("Click", "SubmitButton");
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating Assignment '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		//Click if Warnings are displayed, if yes close them and update again
		if ( clearWarnings()) {
			logger.log_Message("Warnings displayed while submitting the Assignment and Closed", Logger.MessageType.WARNING);
			ccRootObject.button("Click", "SubmitButton");
		}
	}
	
	/**
	 * Create New Audatex Assignment
	 * @param externalAssignmentIndex
	 * @param fromExposure
	 * @throws Exception
	 */
	public void createAudatexAssignment(String externalAssignmentIndex, Boolean fromExposure) throws Exception {
		Hashtable<String, String> data_AudatexAssignment = ClaimCenterData.getData("AudatexAssignments", externalAssignmentIndex);
		openNewAssignment(fromExposure, "Audatex", data_AudatexAssignment);

		fillDataAudatexAssignment(data_AudatexAssignment);
			
		ccRootObject.button("Click", "UpdateButton");
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating Assignment '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		//Click if Warnings are displayed, if yes close them and update again
		if ( clearWarnings()) {
			logger.log_Message("Warnings displayed while saving Assignment and Closed", Logger.MessageType.WARNING);
			ccRootObject.button("Click", "UpdateButton");
		}
	}

	/**
	 * @param data_AudatexAssignment
	 * @throws Exception 
	 */
	private void fillDataAudatexAssignment( Hashtable<String, String> data_AudatexAssignment) throws Exception {
		
		//Deductible
		//Deductible Amount
		ccRootObject.editBox("Input", "AssignmentDeductibleAmount", data_AudatexAssignment.get("DeductibleAmount"));
		//User Information
		//Adjuster
		ccRootObject.comboBox("Select", "Adjuster", data_AudatexAssignment.get("Adjuster"));
		//Appraisal Resource
		//Vendor
		if ( ccRootObject.comboBox("Verify", "AppraisalVendor", data_AudatexAssignment.get("AppraisalVendor")).startsWith("PASS") ) {
			ccRootObject.comboBox("Select", "AppraisalVendor", data_AudatexAssignment.get("AppraisalVendor"));
		}
		else {
			String[] tokens = data_AudatexAssignment.get("AppraisalVendor").split(" ");
			String[] vendorSearchMenuItems = new String[] { tokens[0] };
			ccRootObject.button("Click", "VendorButton");
			ccRootObject.menu("Click", "VendorMenuList", vendorSearchMenuItems);
			AddressBook ab = new AddressBook(ccRootObject, logger);
			ab.searchSelectContact(tokens[1]);
		}
		
		//Dispatch Information
		//Message
		ccRootObject.editBox("Input", "DispatchMessage", data_AudatexAssignment.get("DispatchMessage"));
		//Dispatch Notes
		ccRootObject.editBox("Input", "DispatchNotes", data_AudatexAssignment.get("DispatchNotes"));
		//Proposed Inspection Date
		ccRootObject.editBox("Input", "ProposedInspectionDate", data_AudatexAssignment.get("ProposedInspectionDate"));
	}

	/**
	 * Submit a new Audatex Assignment
	 * @param externalAssignmentIndex
	 * @param fromExposure
	 * @throws Exception
	 */
	public void submitNewAudatexAssignment(String externalAssignmentIndex, Boolean fromExposure) throws Exception {

		Hashtable<String, String> data_AudatexAssignment = ClaimCenterData.getData("AudatexAssignments", externalAssignmentIndex);
		openNewAssignment(fromExposure, "Audatex", data_AudatexAssignment);
		fillDataAudatexAssignment(data_AudatexAssignment);
		//Submit the Assignment
		ccRootObject.button("Click", "SubmitButton");
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating Assignment '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		//Click if Warnings are displayed, if yes close them and update again
		if ( clearWarnings()) {
			logger.log_Message("Warnings displayed while submitting the Assignment and Closed", Logger.MessageType.WARNING);
			ccRootObject.button("Click", "SubmitButton");
		}
	}

	/**
	 * Create new XactAnalysis Assignment
	 * @param externalAssignmentIndex
	 * @param fromExposure
	 * @throws Exception
	 */
	public void createXactAnalysisAssignment(String externalAssignmentIndex, Boolean fromExposure ) throws Exception {
		
		Hashtable<String, String> data_XactAnalysisAssignment = ClaimCenterData.getData("RentalAssignments", externalAssignmentIndex);
		openNewAssignment(fromExposure, "Rental", data_XactAnalysisAssignment);

		fillDataXactAnalysisAssignment(data_XactAnalysisAssignment);
			
		ccRootObject.button("Click", "UpdateButton");
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating Assignment '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		//Click if Warnings are displayed, if yes close them and update again
		if ( clearWarnings()) {
			logger.log_Message("Warnings displayed while saving Assignment and Closed", Logger.MessageType.WARNING);
			ccRootObject.button("Click", "UpdateButton");
		}
	}
	
	/**
	 * @param data_XactAnalysisAssignment
	 * @throws Exception 
	 */
	private void fillDataXactAnalysisAssignment( Hashtable<String, String> data_XactAnalysisAssignment) throws Exception {

		//XactAnalysis Assignment 
		//Data Set
		ccRootObject.comboBox("Select", "DataSet", data_XactAnalysisAssignment.get("DataSet"));
		//Risk Location
		Hashtable<String, String> data_Location = ClaimCenterData.getData("Locations",
				data_XactAnalysisAssignment.get("RiskLocation"));
		//Address 1
		ccRootObject.editBox("Input", "RiskLocationAddress1", data_Location.get("Address1"));
		//Address 2   
		ccRootObject.editBox("Input", "RiskLocationAddress2", data_Location.get("Address2"));
		//City
		ccRootObject.editBox("Input", "RiskLocationCity", data_Location.get("City"));
		//Province/State
		ccRootObject.comboBox("Select", "RiskLocationProvince", data_Location.get("Province"));
		//Country
		ccRootObject.comboBox("Select", "RiskLocationCountry", data_Location.get("Country"));
		//Postal Code
		ccRootObject.editBox("Input", "RiskLocationPostalCode", data_Location.get("PostalCode"));
		//Person to Contact to Access Property
		//Name
		ccRootObject.editBox("Input", "AccessPropertyContactName", 
				data_XactAnalysisAssignment.get("AccessPropertyContactName"));
		//Phone 
		ccRootObject.editBox("Input", "AccessPropertyContactPhone",
				data_XactAnalysisAssignment.get("AccessPropertyContactPhone"));
		//Third Party Claimant 
		//Name
		ccRootObject.editBox("Input", "ThirdPartyClaimantName",
				data_XactAnalysisAssignment.get("ThirdPartyClaimantName"));
		//Phone
		ccRootObject.editBox("Input", "ThirdPartyClaimantPhone",
				data_XactAnalysisAssignment.get("ThirdPartyClaimantPhone"));
		//Assignment  
		//Type of Loss 
		ccRootObject.comboBox("Select", "XactAnalysisTypeofLoss",
				data_XactAnalysisAssignment.get("TypeofLoss"));
		//Job Type
		ccRootObject.comboBox("Select", "JobType", data_XactAnalysisAssignment.get("JobType"));
		//Job Size
		ccRootObject.comboBox("Select", "JobSize", data_XactAnalysisAssignment.get("JobSize"));
		//CAT Code
		//Desk Adjuster
		ccRootObject.comboBox("Select", "Adjuster", data_XactAnalysisAssignment.get("DeskAdjuster"));
		//Set Claim Rep to Desk Adjuster?  
		if( data_XactAnalysisAssignment.get("SetClaimReptoDeskAdjuster").equalsIgnoreCase("Yes") ){
			ccRootObject.radioButton("Select", "SetClaimRepYes");
		}
		else if( data_XactAnalysisAssignment.get("SetClaimReptoDeskAdjuster").equalsIgnoreCase("No") ){
			ccRootObject.radioButton("Select", "SetClaimRepNo");
			//Claim Rep
			ccRootObject.editBox("Input", "ClaimRepName", data_XactAnalysisAssignment.get("ClaimRepName"));
		}
		//Phone
		ccRootObject.editBox("Input", "ClaimRepPhone", data_XactAnalysisAssignment.get("ClaimRepPhone"));
		//Contractor
		if ( ccRootObject.comboBox("Verify", "Contractor", data_XactAnalysisAssignment.get("Contractor")).startsWith("PASS") ) {
			ccRootObject.comboBox("Select", "Contractor", data_XactAnalysisAssignment.get("Contractor"));
		}
		else {
			String[] tokens = data_XactAnalysisAssignment.get("Contractor").split(" ");
			String[] vendorSearchMenuItems = new String[] { tokens[0] };
			ccRootObject.button("Click", "VendorButton");
			ccRootObject.menu("Click", "VendorMenuList", vendorSearchMenuItems);
			AddressBook ab = new AddressBook(ccRootObject, logger);
			ab.searchSelectContact(tokens[1]);
		}

	    //Auto Rotation Dispatch Queue
		if( data_XactAnalysisAssignment.get("AutoRotationOrDispatchQueue").equalsIgnoreCase("Auto Rotation") ){
			ccRootObject.radioButton("Select", "AutoRotation");
		}
		else if( data_XactAnalysisAssignment.get("AutoRotationOrDispatchQueue").equalsIgnoreCase("Dispatch Queue") ){
			ccRootObject.radioButton("Select", "DispatchQueue");
		}
		//Policy Information
		//Apply Deductible
		if( data_XactAnalysisAssignment.get("ApplyDeductible").equalsIgnoreCase("Across All Coverage") ){
			ccRootObject.radioButton("Select", "ApplyDeductibleAcrossAllCoverage");
		}
		else if( data_XactAnalysisAssignment.get("ApplyDeductible").equalsIgnoreCase("Across All Coverage") ){
			ccRootObject.radioButton("Select", "ApplyDeductibleCoverageSpecific");
		}
		
		//Deductible Amount
		ccRootObject.editBox("Input", "DeductibleAmount", data_XactAnalysisAssignment.get("DeductibleAmount"));
		//Apply Limits
		if( data_XactAnalysisAssignment.get("ApplyLimits").equalsIgnoreCase("Yes") ){
			ccRootObject.radioButton("Select", "ApplyLimitsYes");
		}
		else if( data_XactAnalysisAssignment.get("ApplyLimits").equalsIgnoreCase("No") ){
			ccRootObject.radioButton("Select", "ApplyLimitsNo");
		}
		//Mortgage Holder
		ccRootObject.editBox("Input", "MortgageHolder", data_XactAnalysisAssignment.get("MortgageHolder"));
		//Description of Loss and/or Instruction
		ccRootObject.editBox("Input", "DescriptionofLossandorInstruction", data_XactAnalysisAssignment.get("DescriptionofLossandorInstruction"));
		//Add Coverage
		String[] coverages = data_XactAnalysisAssignment.get("Coverages").split(",");
		int coverageCnt = 0;
		for( String coverage : coverages) {
			if( !coverage.trim().isEmpty() ) {
				Hashtable<String, String> data_Coevrages = ClaimCenterData.getData("Coverages", coverage);
				ccRootObject.button("Click", "AddButton");
				//Find Coverage Table
				TextGuiTestObject coveragesTable = ccRootObject.findExcelMappedTestObject(
						ccRootObject.getBrowserDocObject(), "CoveragesTable");
				//Description
				ccRootObject.editBoxInTable("Input", coveragesTable, 
						coverageCnt +":", "CoverageDescription", data_Coevrages.get("CoverageDesc"));
				//Type
				ccRootObject.comboBoxinTable("Select", coveragesTable,
						coverageCnt +":", "CoverageType", data_Coevrages.get("CoverageType"));
				//Policy Limits
				ccRootObject.editBoxInTable("Input", coveragesTable, 
						coverageCnt +":", "PolicyLimits", data_Coevrages.get("CoverageIncidentLimit"));
				coverageCnt ++;
			}
		}
	}

	/**
	 * TODO: Verify the details of External Assignment
	 * @param externalAssignmentIndex
	 */
	public void verifyExternalAssignment(String externalAssignmentIndex) {

	}

	/**
	 * Closes the Warning message in Workspace area if they are shown
	 * @return - true if the warning messages found
	 * @throws Exception 
	 * @since 2012/03/08
	 */
	private boolean clearWarnings() throws Exception {
		
		boolean closed = false;
		String clearWarningBtnExists = ccRootObject.button("Verify", "ClearBtn");
		if (clearWarningBtnExists.contains("PASS")) {
			closed = true;
			ccRootObject.button("Click", "ClearBtn");
		}
		return closed;
	}	
}
