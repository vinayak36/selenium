package CCAppl;
import java.util.Hashtable;

import resources.CCAppl.AdministrationHelper;
import TestData.ClaimCenterData;
import Utilities.Logger;
import Utilities.Logger.MessageType;

import com.rational.test.ft.*;
import com.rational.test.ft.object.interfaces.*;
import com.rational.test.ft.object.interfaces.SAP.*;
import com.rational.test.ft.object.interfaces.WPF.*;
import com.rational.test.ft.object.interfaces.dojo.*;
import com.rational.test.ft.object.interfaces.siebel.*;
import com.rational.test.ft.object.interfaces.flex.*;
import com.rational.test.ft.object.interfaces.generichtmlsubdomain.*;
import com.rational.test.ft.script.*;
import com.rational.test.ft.value.*;
import com.rational.test.ft.vp.*;
import com.ibm.rational.test.ft.object.interfaces.sapwebportal.*;

/**
 * Implements the automation of Administration Tab which includes Create Users, Create Groups
 * View User Details, View Group Details, Edit User Details, Edit Groups
 * @author Vinayak Hegde
 */
public class Administration extends AdministrationHelper
{
	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Administration and initiates root Test object and Logger
	 * 
	 * @param rootObject
	 *            - Root test object
	 * @param log
	 *            - Logger instance
	 */
	public Administration(UIObjectsAPI rootObject,Logger log){
		ccRootObject = rootObject;
		logger =log;
	}
	
	/**
	 * Creates new user using the data provided
	 * @param usersIndex
	 * @throws Exception 
	 */
	public void createUser(String usersIndex) throws Exception {

		//Click Administration Tab
		ccRootObject.link("Click", "AdministrationTab");
		//Actions->New User
		String[] newUserMenuItem = new String[] {"New User"};
		
		ccRootObject.button("Click", "ActionsButton");
		ccRootObject.menu("Click", "MenuActionsList", newUserMenuItem);
		
		//Get User Data
		Hashtable<String, String> data_User = ClaimCenterData.getData("Users", usersIndex);
		logger.log_Message("Creating New User " + data_User.get("UserName"), MessageType.INFO);
		
		//Basic Information
		fillBasicInformation(data_User);
		
		//Profile Information
		fillProfileInformation(data_User);

		//Authority Limit
		if ( !data_User.get("AuthorityLimitProfile").trim().isEmpty()) {
			if ( ccRootObject.link("Verify", "AuthorityLimitsTab").startsWith("PASS")) {
				ccRootObject.link("Click", "AuthorityLimitsTab");
				checkErrorMessage();
			}
			//Authority Limit Profile
			ccRootObject.comboBox("Select", "AuthorityLimitProfile", data_User.get("AuthorityLimitProfile"));
		}
		//Attributes
		if ( !data_User.get("Attributes").trim().isEmpty()) {
			if (ccRootObject.link("Verify", "AttributesTab").startsWith("PASS") ) {
				ccRootObject.link("Click", "AttributesTab");
				checkErrorMessage();
			}
		}
		//Regions
		if ( !data_User.get("Attributes").trim().isEmpty()) {
			if (ccRootObject.link("Verify", "RegionsTab").startsWith("PASS")) {
				ccRootObject.link("Click", "RegionsTab");
				checkErrorMessage();
			}
		}
		
		ccRootObject.button("Click", "UpdateButton");
		checkErrorMessage();
		logger.log_Message("New User " + data_User.get("UserName") + " created successfully", MessageType.INFO);
	}

	private void checkErrorMessage() throws Exception {
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating new User '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
	}

	private void fillProfileInformation(Hashtable<String, String> data_User)
			throws Exception {
		
		if ( ccRootObject.link("Verify", "ProfileTab").startsWith("PASS")) {
			ccRootObject.link("Click", "ProfileTab");
			checkErrorMessage();
		}

		//Job Title  
		ccRootObject.editBox("Input", "JobTitle", data_User.get("JobTitle"));
		//Department
		ccRootObject.editBox("Input", "Department", data_User.get("Department"));
		//Employee ID   
		ccRootObject.editBox("Input", "EmployeeID", data_User.get("EmployeeId"));
		//Add Address Details
		AddressBook.addLocation(ccRootObject, data_User.get("Address"));
		//Proximity Search Status 
		//Primary Phone
		ccRootObject.comboBox("Select", "PrimaryPhone", data_User.get("PrimaryPhone"));

		//Home Phone
		ccRootObject.editBox("Input", "HomePhone", data_User.get("HomePhone").replace(" ", "").replace("-", ""));
		//Work Phone   
		ccRootObject.editBox("Input", "WorkPhone", data_User.get("WorkPhone").replace(" ", "").replace("-", ""));
		//Mobile Phone   
		ccRootObject.editBox("Input", "MobilePhone", data_User.get("MobilePhone").replace(" ", "").replace("-", ""));
		//Fax   
		ccRootObject.editBox("Input", "Fax", data_User.get("Fax").replace(" ", "").replace("-", ""));
		//Email
		ccRootObject.editBox("Input", "Email", data_User.get("Email"));
		//Experience Level 
		ccRootObject.comboBox("Select", "ExperienceLevel", data_User.get("ExperienceLevel"));
		//Language 
		ccRootObject.comboBox("Select", "Language", data_User.get("Language"));
		
		//External User
		if ( data_User.get("ExternalUser").equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "ExternalUserYes");
		}
		else if ( data_User.get("ExternalUser").equalsIgnoreCase("No") ){
			ccRootObject.radioButton("Select", "ExternalUserNo");
		}
		//Defaults
		//Policy Type 
		ccRootObject.comboBox("Select", "PolicyType", data_User.get("PolicyType"));
		//Loss Type 
		ccRootObject.comboBox("Select", "LossType", data_User.get("LossType"));
	}

	/**
	 * Fills Basis Information for the test user
	 * @param data_User
	 * 			- User Data
	 * @throws Exception
	 */
	private void fillBasicInformation(Hashtable<String, String> data_User) throws Exception {
		
		if ( ccRootObject.link("Verify", "BasicsTab").startsWith("PASS")) {
			ccRootObject.link("Click", "BasicsTab");
			checkErrorMessage();
		}
		//First Name
		ccRootObject.editBox("Input", "FirstName", data_User.get("FirstName"));
		//Last Name
		ccRootObject.editBox("Input", "LastName", data_User.get("LastName"));
		//User name
		ccRootObject.editBox("Input", "UserName", data_User.get("UserName"));
		//Password
		ccRootObject.editBox("Input", "SetPassword", data_User.get("Password"));
		//Confirm Password
		ccRootObject.editBox("Input", "ConfirmPassword", data_User.get("Password"));
		//Active
		if ( data_User.get("Active").equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "ActiveYes");
		}
		else if ( data_User.get("Active").equalsIgnoreCase("No") ){
			ccRootObject.radioButton("Select", "ActiveNo");
		}
			
		//Locked
		if ( data_User.get("Locked").equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "LockedYes");
		}
		else if ( data_User.get("Locked").equalsIgnoreCase("No") ){
			ccRootObject.radioButton("Select", "LockedNo");
		}
		//Vacation Status
		ccRootObject.comboBox("Select", "VacationStatus", data_User.get("VacationStatus"));
		//Backup User
		ccRootObject.comboBox("Select", "BackupUser", data_User.get("BackupUser"));
		addRoles(data_User.get("Roles"));
		addGroups(data_User.get("Groups"));
	}

	/**
	 * Verifies the fields on User Details Page
	 * @param usersIndex
	 * 			- Expected User Details Data
	 * @throws Exception
	 */
	public void viewUserDetails(String usersIndex) throws Exception {
		
		//Click Administration Tab
		ccRootObject.link("Click", "AdministrationTab");
		
		ccRootObject.link("Click", "SearchForUsers");
		//Get the Users Data
		Hashtable<String, String> data_User = ClaimCenterData.getData("Users", usersIndex);

		//search and select first user
		searchAndSelectUser(data_User.get("UserName"), data_User.get("FirstName"),
				data_User.get("LastName"), data_User.get("Address"));

		logger.log_Message("Verifying the User Details - " + usersIndex, MessageType.INFO);

		int basicInformationFailedCount = verifyBasicInformation(data_User);

		int profileInformationFailedCount = verifyProfileInformation(data_User);

		//AuthorityLimitsProfile
		int authorityLimitProfileFailedCount = 0;
		if ( !data_User.get("AuthorityLimitProfile").trim().isEmpty()) {
			ccRootObject.link("Click", "AuthorityLimitsTab");
			//Get the MainContent Window
			TestObject mainContent = ccRootObject.findExcelMappedTestObject(
					ccRootObject.getBrowserDocObject(), "MainContent");
			//Authority Limit Profile
			if ( !ccRootObject.VerifyTextField(mainContent,
					"AuthorityLimitsProfile", data_User.get("AuthorityLimitProfile"))){
				authorityLimitProfileFailedCount++;
			}
		}

		if ((	basicInformationFailedCount 
				+ profileInformationFailedCount
				+ authorityLimitProfileFailedCount)
				!= 0 ) {
			throw new Exception("Verification of User Details Failed");
		}		
	}

	private int verifyProfileInformation(Hashtable<String, String> data_User) throws Exception {

		ccRootObject.button("Click", "ProfileTab");
		
		//Get the MainContent Window
		TestObject mainContent = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "MainContent");
		
		int failedFieldsCount = 0;
		//JobTitle
		if ( !ccRootObject.VerifyTextField(mainContent,
				"JobTitleText", data_User.get("JobTitle"))){
			failedFieldsCount++;
		}
		//DepartmentText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"DepartmentText", data_User.get("Department"))){
			failedFieldsCount++;
		}
		
		Hashtable<String, String> data_LossLocation = ClaimCenterData.getData("Locations",
				data_User.get("Address"));
		
		String lossLocation = data_LossLocation.get("Address1");
		lossLocation = (data_LossLocation.get("Address2").isEmpty())?
				lossLocation : lossLocation + ", " + data_LossLocation.get("Address2");
		lossLocation = data_LossLocation.get("City").isEmpty()?lossLocation:
			lossLocation + ", " + data_LossLocation.get("City");
		String provicneCode = ClaimCenterData.getData(
				"ProvinceCodes", "Province", data_LossLocation.get("Province")).get("Code");
		if( provicneCode != null ) {
			lossLocation = provicneCode.isEmpty()? lossLocation :
				lossLocation + ", " + provicneCode;
		}
		lossLocation = data_LossLocation.get("PostalCode").isEmpty()?lossLocation:
			lossLocation + " " + data_LossLocation.get("PostalCode");

		//LocationText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"LocationText", lossLocation)){
			failedFieldsCount++;
		}

		//PrimaryPhoneText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"PrimaryPhoneText", data_User.get("PrimaryPhone"))){
			failedFieldsCount++;
		}
		//HomePhoneText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"HomePhoneText", data_User.get("HomePhone"))){
			failedFieldsCount++;
		}
		//WorkPhoneText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"WorkPhoneText", data_User.get("WorkPhone"))){
			failedFieldsCount++;
		}
		//MobilePhoneText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"MobilePhoneText", data_User.get("MobilePhone"))){
			failedFieldsCount++;
		}
		//FaxText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"FaxText", data_User.get("Fax"))){
			failedFieldsCount++;
		}
		//EmailText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"EmailText", data_User.get("Email"))){
			failedFieldsCount++;
		}
		//ExperienceText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"ExperienceText", data_User.get("ExperienceLevel"))){
			failedFieldsCount++;
		}
		//LanguageText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"LanguageText", data_User.get("Language"))){
			failedFieldsCount++;
		}
		//ExternalUserText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"ExternalUserText", data_User.get("ExternalUser"))){
			failedFieldsCount++;
		}
		//PolicyTypeText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"PolicyTypeText", data_User.get("PolicyType"))){
			failedFieldsCount++;
		}
		//LossTypeText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"LossTypeText", data_User.get("LossType"))){
			failedFieldsCount++;
		}
		return failedFieldsCount;
	}

	private int verifyBasicInformation(Hashtable<String, String> data_User) throws Exception {
		
		if ( ccRootObject.link("Verify", "BasicsTab").equalsIgnoreCase("PASS") ) {
			ccRootObject.link("Click", "BasicsTab");
		}
		
		int failedFieldsCount = 0;
		//Get the MainContent Window.
		TestObject mainContent = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "MainContent");
	
		//FirstName
		if ( !ccRootObject.VerifyTextField(mainContent,
				"FirstNameText", data_User.get("FirstName"))){
			failedFieldsCount++;
		}
		//LastName
		if ( !ccRootObject.VerifyTextField(mainContent,
				"LastNameText", data_User.get("LastName"))){
			failedFieldsCount++;
		}
		//UserName
		if ( !ccRootObject.VerifyTextField(mainContent,
				"UserNameText", data_User.get("UserName"))){
			failedFieldsCount++;
		}
		//UserActive
		if ( !ccRootObject.VerifyTextField(mainContent,
				"UserActiveText", data_User.get("Active"))){
			failedFieldsCount++;
		}
		//UserAccountLocked
		if ( !ccRootObject.VerifyTextField(mainContent,
				"UserAccountLockedText", data_User.get("Locked"))){
			failedFieldsCount++;
		}
		//UserVacationStatus
		if ( !ccRootObject.VerifyTextField(mainContent,
				"UserVacationStatusText", data_User.get("VacationStatus"))){
			failedFieldsCount++;
		}
		//BackUserText
		if ( !ccRootObject.VerifyTextField(mainContent,
				"BackUserText", data_User.get("BackupUser"))){
			failedFieldsCount++;
		}

		TestObject userRolesTable = ccRootObject.findExcelMappedTestObject(
				 ccRootObject.getBrowserDocObject(), "UserRolesTable");
		
		//For each Role
		for ( String role : data_User.get("Roles").split(",") ) {
			//Get Roles details
			Hashtable<String, String> data_UserRoles = ClaimCenterData.getData("UserRoles", role.trim());
			
			Anchor rowAnchor = atDescendant(".class", "Html.TR");
			TestObject[] allroles = userRolesTable.find(rowAnchor, false);
			boolean roleFound = false;
			for ( TestObject roleRow : allroles ) {
				if ( ccRootObject.findDescendantTestObject(roleRow, ".class", "Html.TextNode",
						".text", data_UserRoles.get("Role"))
						!= null ) {
					roleFound = true;
					//UserRoleName
					if ( !ccRootObject.VerifyTextField(roleRow,
							"UserRoleName", data_UserRoles.get("Role"))){
						failedFieldsCount++;
					}
					//RoleDescriptionText
					if ( !ccRootObject.VerifyTextField(roleRow,
							"RoleDescriptionText", data_UserRoles.get("Description"))){
						failedFieldsCount++;
					}
					break;
				}
			}
			if ( ! roleFound ) {
				logger.log_Message("Role# " + role + " Not found.", MessageType.ERROR);
				failedFieldsCount++;
			}
		}
		
		//UserGroupsTable
		TestObject userGroupsTable = ccRootObject.findExcelMappedTestObject(
				 ccRootObject.getBrowserDocObject(), "UserGroupsTable");

		for ( String group : data_User.get("Groups").split(",") ) {
			
			Hashtable<String, String> data_UserGroup = ClaimCenterData.getData("Groups", group );
			
			String allGroups[] = data_UserGroup.get("GroupName").split(",");
			String groupName = allGroups[allGroups.length-1]; 
			
			Anchor rowAnchor = atDescendant(".class", "Html.TR");
			TestObject[] allRows = userGroupsTable.find(rowAnchor, false);
			boolean groupFound = false;
			for ( TestObject groupRow : allRows ) {
				
				if ( ccRootObject.findDescendantTestObject(groupRow, ".class", "Html.TextNode",
						".text", groupName) != null ) {
					groupFound = true;
					//UserGroup
					if ( !ccRootObject.VerifyTextField(groupRow,
							"UserGroupText", groupName)){
						failedFieldsCount++;
					}
					//GroupMember
					if ( !ccRootObject.VerifyTextField(groupRow,
							"GroupMemberText", data_UserGroup.get("Member"))){
						failedFieldsCount++;
					}
					//GroupManager
					if ( !ccRootObject.VerifyTextField(groupRow,
							"GroupManagerText", data_UserGroup.get("Manager"))){
						failedFieldsCount++;
					}
					//GroupLoadFactorType
					if ( !ccRootObject.VerifyTextField(groupRow,
							"GroupLoadFactorTypeText", data_UserGroup.get("LoadFactorPermissions"))){
						failedFieldsCount++;
					}
					//GroupLoadFactor
					if ( !ccRootObject.VerifyTextField(groupRow,
							"GroupLoadFactorText", data_UserGroup.get("LoadFactor"))){
						failedFieldsCount++;
					}
					break;
				}
			}
			
			if ( ! groupFound ) {
				logger.log_Message("Group# " + groupName + " Not found.", MessageType.ERROR);
				failedFieldsCount++;
			}
		
		}
		return failedFieldsCount;
	}
	
	/**
	 * Search the User with details provided and select the First User from Rsult
	 * @param userName
	 * @param firstName
	 * @param lastName
	 * @param address
	 * @throws Exception
	 */
	public void searchAndSelectUser(String userName, String firstName, String lastName, String address) throws Exception {
		
		//If username is specfied, we don;t need other values to search
		if ( !userName.trim().isEmpty()) {
			//User name
			ccRootObject.editBox("Input", "UserName", userName);
		}
		else {
			//First Name
			ccRootObject.editBox("Input", "FirstName", firstName);
			//Last Name
			ccRootObject.editBox("Input", "LastName", lastName);

			Hashtable<String, String> data_Address = ClaimCenterData.getData("Locations", address);
			//City
			ccRootObject.editBox("Input", "LocationAddressCity", data_Address.get("City"));
			//Province
			ccRootObject.comboBox("Select", "LocationProvince", data_Address.get("Province"));
			//Country
			ccRootObject.comboBox("Select", "LocationCountry", data_Address.get("Country"));
			//Postal Code
			ccRootObject.editBox("Input", "LocationPostalCode", data_Address.get("PostalCode"));
		}
		
		//Click Search
		ccRootObject.button("Click", "SearchBtn");
		//check if search returned results
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "User Search Failed " + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		
		//Select the first user
		TextGuiTestObject searchResult = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "UserSearchResultTable");
		ccRootObject.linkinTable("Click", searchResult, "UserDisplayName");
	}

	/**
	 * Edit the user details with data in specified user index
	 * @param usersIndex
	 * 			User details
	 * @throws Exception
	 */
	public void editUser(String usersIndex) throws Exception {
		//Click Administration Tab
		ccRootObject.link("Click", "AdministrationTab");
		
		ccRootObject.link("Click", "SearchForUsers");
		//Get the Loss Details Data
		Hashtable<String, String> data_User = ClaimCenterData.getData("Users", usersIndex);

		if ( data_User.get("UserName").trim().isEmpty()) {
			throw new Exception("User name is requird to Edit");
		}
		//search and select first user
		searchAndSelectUser(data_User.get("UserName"), "", "", "");
		logger.log_Message("Modifying the User Details - " + usersIndex, MessageType.INFO);

		ccRootObject.button("Click", "EditButton");
		
		//Edit basic information
		editBasicInformation(data_User);
		
		//Edit Profile information
		editProfileInformation(data_User);
		
		//Authority Limit
		if ( !data_User.get("AuthorityLimitProfile").trim().isEmpty()) {
			if ( ccRootObject.link("Verify", "AuthorityLimitsTab").startsWith("PASS")) {
				ccRootObject.link("Click", "AuthorityLimitsTab");
				checkErrorMessage();
			}
			//Authority Limit Profile
			ccRootObject.comboBox("Select", "AuthorityLimitProfile", data_User.get("AuthorityLimitProfile"));
		}
		//Attributes
		if ( !data_User.get("Attributes").trim().isEmpty()) {
			if (ccRootObject.link("Verify", "AttributesTab").startsWith("PASS") ) {
				ccRootObject.link("Click", "AttributesTab");
				checkErrorMessage();
			}
		}
		//Regions
		if ( !data_User.get("Attributes").trim().isEmpty()) {
			if (ccRootObject.link("Verify", "RegionsTab").startsWith("PASS")) {
				ccRootObject.link("Click", "RegionsTab");
				checkErrorMessage();
			}
		}
		
		ccRootObject.button("Click", "UpdateButton");
		checkErrorMessage();
		logger.log_Message("New User " + data_User.get("UserName") + " created successfully", MessageType.INFO);
	}	

	/**
	 * Edits the basic information
	 * @param data_User
	 * 			- User Data
	 * @throws Exception
	 */
	private void editBasicInformation(Hashtable<String, String> data_User) throws Exception {
		
		if ( ccRootObject.link("Verify", "BasicsTab").startsWith("PASS")) {
			ccRootObject.link("Click", "BasicsTab");
			checkErrorMessage();
		}
		//First Name
		ccRootObject.editBox("Input", "FirstName", data_User.get("FirstName"));
		//Last Name
		ccRootObject.editBox("Input", "LastName", data_User.get("LastName"));
		//User name
		ccRootObject.editBox("Input", "UserName", data_User.get("UserName"));
		//Password
		ccRootObject.editBox("Input", "SetPassword", data_User.get("Password"));
		//Confirm Password
		ccRootObject.editBox("Input", "ConfirmPassword", data_User.get("Password"));
		//Active
		if ( data_User.get("Active").equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "ActiveYes");
		}
		else if ( data_User.get("Active").equalsIgnoreCase("No") ){
			ccRootObject.radioButton("Select", "ActiveNo");
		}
			
		//Locked
		if ( data_User.get("Locked").equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "LockedYes");
		}
		else if ( data_User.get("Locked").equalsIgnoreCase("No") ){
			ccRootObject.radioButton("Select", "LockedNo");
		}
		//Vacation Status
		ccRootObject.comboBox("Select", "VacationStatus", data_User.get("VacationStatus"));
		//Backup User
		ccRootObject.comboBox("Select", "BackupUser", data_User.get("BackupUser"));
		//Add New Roles
		//To add new roles first delete all existing roles and the add new roles
		deleteAllRoles();
		addRoles(data_User.get("Roles"));
		//Add Groups
		deleteAllGroups();
		addGroups(data_User.get("Groups"));
	}

	private void editProfileInformation(Hashtable<String, String> data_User) throws Exception {

		if ( ccRootObject.link("Verify", "ProfileTab").startsWith("PASS")) {
			ccRootObject.link("Click", "ProfileTab");
			checkErrorMessage();
		}

		//Job Title  
		ccRootObject.editBox("Input", "JobTitle", data_User.get("JobTitle"));
		//Department
		ccRootObject.editBox("Input", "Department", data_User.get("Department"));
		//Employee ID   
		ccRootObject.editBox("Input", "EmployeeID", data_User.get("EmployeeId"));
		//Add Address Details
		AddressBook.addLocation(ccRootObject, data_User.get("Address"));
		//Proximity Search Status 
		//Primary Phone
		ccRootObject.comboBox("Select", "PrimaryPhone", data_User.get("PrimaryPhone"));
		//Home Phone
		ccRootObject.editBox("Input", "HomePhone", data_User.get("HomePhone").replace(" ", "").replace("-", ""));
		//Work Phone   
		ccRootObject.editBox("Input", "WorkPhone", data_User.get("WorkPhone").replace(" ", "").replace("-", ""));
		//Mobile Phone   
		ccRootObject.editBox("Input", "MobilePhone", data_User.get("MobilePhone").replace(" ", "").replace("-", ""));
		//Fax   
		ccRootObject.editBox("Input", "Fax", data_User.get("Fax").replace(" ", "").replace("-", ""));
		//Email
		ccRootObject.editBox("Input", "Email", data_User.get("Email"));
		//Experience Level 
		ccRootObject.comboBox("Select", "ExperienceLevel", data_User.get("ExperienceLevel"));
		//Language 
		ccRootObject.comboBox("Select", "Language", data_User.get("Language"));

		//External User
		if ( data_User.get("ExternalUser").equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "ExternalUserYes");
		}
		else if ( data_User.get("ExternalUser").equalsIgnoreCase("No") ){
			ccRootObject.radioButton("Select", "ExternalUserNo");
		}
		//Defaults
		//Policy Type 
		ccRootObject.comboBox("Select", "PolicyType", data_User.get("PolicyType"));
		//Loss Type 
		ccRootObject.comboBox("Select", "LossType", data_User.get("LossType"));
	}
	
	private void addGroups(String groups)
			throws Exception {
		if (!groups.trim().isEmpty() ) {
			int groupIndex = 0;
			for( String groupIdx : groups.split(",")) {
				if ( !groupIdx.trim().isEmpty()) {
					Hashtable<String, String> data_USerGroups =
						ClaimCenterData.getData("Groups", groupIdx.trim());

					//Add
					TestObject groupsDIV = ccRootObject.findExcelMappedTestObject(
							 ccRootObject.getBrowserDocObject(), "UserGroupsDIV");
					ccRootObject.buttonInTable("Click", groupsDIV, "AddButton");
					
					TestObject orgGroupTree = ccRootObject.findExcelMappedTestObject(
							ccRootObject.getBrowserDocObject(), "BrowseGroupTree");
					//Split the Group hierarchy by commas
					String[] groupHierarchy = data_USerGroups.get("GroupName").split(",");
					for ( String group : groupHierarchy ) {
						//Check if the group is visible
						TextGuiTestObject groupObject = ccRootObject.findDescendantTestObject(
								orgGroupTree, ".class", "Html.A", ".text", group);
						if ( groupObject == null ) {
							throw new Exception("Group: " + group + " not found");
						}
						//If the group name to be selected, click it and exit 
						if ( group.equals(groupHierarchy[groupHierarchy.length-1])) {
							groupObject.click();
							ccRootObject.waitForBrowserReady();
						}
						else {
							//Check if the group is tree folder and needs to be expanded
							if ( groupObject.getProperty(".className").toString().equalsIgnoreCase("treeViewPanelFolder")) {
								//Find the + button and click it
								String groupItemId = groupObject.getProperty(".id").toString();
								TextGuiTestObject treeExpandButton = ccRootObject.findDescendantTestObject(
										orgGroupTree, ".class", "Html.A", ".id", "tv_folder_" + groupItemId);
								if ( treeExpandButton == null ) {
									throw new Exception("Group: " + group + " can not be expanded further");
								}
								
								TestObject closedImage = ccRootObject.findDescendantTestObjectRE(treeExpandButton,
										".src",
										new RegularExpression("folder-closed.png$", false),
										null, null);
								
								if ( closedImage != null ) {
									treeExpandButton.click();
									ccRootObject.waitForBrowserReady();
								}
							}
						}
					}
					
					//Groups Table
					TestObject groupsTable = ccRootObject.findExcelMappedTestObject(
							 ccRootObject.getBrowserDocObject(), "UserGroupsTable");
				
					//Member
					if (data_USerGroups.get("Member").equalsIgnoreCase("Yes")) {
						ccRootObject.radioButtoninTable("Select", groupsTable,
								groupIndex + ":" , "GroupMemberYes");
					} else if (data_USerGroups.get("Member").equalsIgnoreCase("No")) {
						ccRootObject.radioButtoninTable("Select", groupsTable, 
								groupIndex + ":", "GroupMemberNo");
					}
					//Manager
					if (data_USerGroups.get("Manager").equalsIgnoreCase("Yes")) {
						ccRootObject.radioButtoninTable("Select", groupsTable,
								groupIndex + ":", "GroupManagerYes");
					} else if (data_USerGroups.get("Manager").equalsIgnoreCase("No")) {
						ccRootObject.radioButtoninTable("Select", groupsTable,
								groupIndex + ":", "GroupManagerNo");
					}
					//Load Factor Permissions
					ccRootObject.comboBoxinTable("Select", groupsTable, groupIndex + ":",
							"LoadFactorPermissions",data_USerGroups.get("LoadFactorPermissions"));
					//Load Factor
					if ( !data_USerGroups.get("LoadFactor").toString().isEmpty()) {
							int loadFactor = (int)Double.parseDouble(
									data_USerGroups.get("LoadFactor").toString());
							ccRootObject.editBoxInTable("Input", groupsTable, groupIndex + ":",
									"LoadFactor", Integer.toString(loadFactor));
					}
					groupIndex ++;
				}
			}
		}
	}

	private void addRoles(String roles) throws Exception {
		if (!roles.trim().isEmpty() ) {
			int roleIndex = 0;
			for( String roleIdx : roles.split(",")) {
				if ( !roleIdx.trim().isEmpty()) {
					Hashtable<String, String> data_USerRoles =
						ClaimCenterData.getData("UserRoles", roleIdx.trim());
					//Add
					TestObject rolesDIV = ccRootObject.findExcelMappedTestObject(
							 ccRootObject.getBrowserDocObject(), "UserRolesDIV");

					ccRootObject.buttonInTable("Click", rolesDIV, "AddButton");

					//Role Name
					TestObject rolesTable = ccRootObject.findExcelMappedTestObject(
							 ccRootObject.getBrowserDocObject(), "UserRolesTable");
					
					ccRootObject.comboBoxinTable("Select", rolesTable,
							roleIndex + ":", "RoleName", data_USerRoles.get("Role") );
					roleIndex ++;
				}
			}
		}
	}	

	private void deleteAllRoles() throws Exception {
		
		TestObject rolesTable = ccRootObject.findExcelMappedTestObject(
				 ccRootObject.getBrowserDocObject(), "UserRolesTable");
		//CheckBox
		ccRootObject.checkBoxinTable("Check", rolesTable, null, "CheckBox");
		//Delete
		TestObject rolesDIV = ccRootObject.findExcelMappedTestObject(
				 ccRootObject.getBrowserDocObject(), "UserRolesDIV");

		ccRootObject.buttonInTable("Click", rolesDIV, "RemoveButton");
	}

	private void deleteAllGroups() throws Exception {
		
		TestObject groupsTable = ccRootObject.findExcelMappedTestObject(
				 ccRootObject.getBrowserDocObject(), "UserGroupsTable");

		//CheckBox
		ccRootObject.checkBoxinTable("Check", groupsTable, null, "CheckBox");
		//Delete
		TestObject groupsDIV = ccRootObject.findExcelMappedTestObject(
				 ccRootObject.getBrowserDocObject(), "UserGroupsDIV");
		
		ccRootObject.buttonInTable("Click", groupsDIV, "RemoveButton");

	}
	
	/**
	 * Create new Group with the specified details
	 * @param groupIndex
	 * @throws Exception
	 */
	public void createNewGroup(String groupIndex) throws Exception {
		
		//Click Administration Tab
		ccRootObject.link("Click", "AdministrationTab");
		//Actions->New User
		String[] newGroupMenuItem = new String[] {"New Group"};
		
		ccRootObject.button("Click", "ActionsButton");
		ccRootObject.menu("Click", "MenuActionsList", newGroupMenuItem);
		fillGroupDetails(groupIndex);
	}

	private void fillGroupDetails(String groupIndex) throws Exception {
		//Get User Data
		Hashtable<String, String> data_Group = ClaimCenterData.getData("Groups", groupIndex);
		
		String allGroups[] = data_Group.get("GroupName").split(",");
		
		//Name
		ccRootObject.editBox("Input", "NewGroupName", allGroups[allGroups.length-1]);
		//Type
		ccRootObject.comboBox("Select", "NewGroupType", data_Group.get("Type"));
		//Search Parent
		ccRootObject.button("Click", "ParentMenuIconBtn");

		String[] selectParentMenuItem = new String[] {"Search for Group..."};
		ccRootObject.menu("Click", "ParentMenuList", selectParentMenuItem);
		
		//Search group and select first allGroups[allGroups.length-2]
		ccRootObject.editBox("Input", "GroupName", allGroups[allGroups.length-2]);
		ccRootObject.button("Click", "SearchBtn");
		ccRootObject.button("Click", "SearchResultAssignButton");
		
		//Search and select Supervisor User
		ccRootObject.button("Click", "SupervisorMenuIconBtn");
		
		String[] selectSupervisorMenuItem = new String[] {"Search for a User..."};
		ccRootObject.menu("Click", "SupervisorMenuList", selectSupervisorMenuItem);

		Hashtable<String, String> data_Users = ClaimCenterData.getData("Users",
				data_Group.get("Supervisor"));
		ccRootObject.editBox("Input", "UserName", data_Users.get("UserName"));
		ccRootObject.button("Click", "SearchBtn");
		ccRootObject.button("Click", "SearchResultAssignButton");

		//Security Zone
		ccRootObject.comboBox("Select", "SecurityZone", data_Group.get("SecurityZone"));
		//Load Factor
		if ( !data_Group.get("LoadFactor").toString().isEmpty()) {
			int loadFactor = (int)Double.parseDouble( data_Group.get("LoadFactor").toString());
			ccRootObject.editBox("Input", "LoadFactor", Integer.toString(loadFactor));
		}
		//Update 
		ccRootObject.button("Click", "UpdateButton");
		checkErrorMessage();
	}

	/**
	 * Verifies the details of specified Group
	 * @param groupIndex
	 * 			- Group Name
	 * @throws Exception
	 */
	public void verifyGroupDetails(String groupIndex) throws Exception {

		//Click Administration Tab
		ccRootObject.link("Click", "AdministrationTab");
		ccRootObject.link("Click", "SearchForGroups");

		//Get the Groups Data
		Hashtable<String, String> data_Group = ClaimCenterData.getData("Groups", groupIndex);
		String allGroups[] = data_Group.get("GroupName").split(",");

		ccRootObject.editBox("Input", "GroupName", allGroups[allGroups.length-1]);
		ccRootObject.button("Click", "SearchBtn");
		ccRootObject.link("Click", "GroupNameSearchResult");
		
		//Verify Group Fields
		int failedFieldsCount = 0;
		//Get the MainContent Window.
		TestObject mainContent = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(), "MainContent");
	
		//Name
		if ( !ccRootObject.VerifyTextField(mainContent,
				"GroupNameText", allGroups[allGroups.length-1])){
			failedFieldsCount++;
		}
		//Type
		if ( !ccRootObject.VerifyTextField(mainContent,
				"GroupTypeText", data_Group.get("Type"))){
			failedFieldsCount++;
		}
		//Parent
		if ( !ccRootObject.VerifyTextField(mainContent,
				"GroupParentLink", allGroups[allGroups.length-2])){
			failedFieldsCount++;
		}
		//Supervisor
		Hashtable<String, String> data_Users = ClaimCenterData.getData("Users",
				data_Group.get("Supervisor"));
	
		if ( !ccRootObject.VerifyTextField(mainContent,
				"GroupSupervisorLink", data_Users.get("FirstName") + " " + data_Users.get("LastName"))){
			failedFieldsCount++;
		}
		//Security Zone
		if ( !ccRootObject.VerifyTextField(mainContent,
				"GroupSecurityZoneLink", data_Group.get("SecurityZone"))){
			failedFieldsCount++;
		}
		//Load Factor
		int loadFactor = (int)Double.parseDouble( data_Group.get("LoadFactor").toString());
		if ( !ccRootObject.VerifyTextField(mainContent,
				"GroupLoadFactorText", Integer.toString(loadFactor))){
			failedFieldsCount++;
		}
		
		if ( failedFieldsCount > 0 ) {
			throw new Exception("Verfication of Group Details failed");
		}
	}

	/**
	 * Edits the Group Specified
	 * @param groupIndex
	 * 			- Name of the group to edit
	 * @throws Exception
	 */
	public void editGroup(String groupIndex) throws Exception {
		//Click Administration Tab
		ccRootObject.link("Click", "AdministrationTab");
		ccRootObject.link("Click", "SearchForGroups");

		//Get the Groups Data
		Hashtable<String, String> data_Group = ClaimCenterData.getData("Groups", groupIndex);
		String allGroups[] = data_Group.get("GroupName").split(",");

		ccRootObject.editBox("Input", "GroupName", allGroups[allGroups.length-1]);
		ccRootObject.button("Click", "SearchBtn");
		ccRootObject.link("Click", "GroupNameSearchResult");
		
		//Edit
		ccRootObject.button("Click", "EditButton");
		fillGroupDetails(groupIndex);
	}
}

