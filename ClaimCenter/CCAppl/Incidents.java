package CCAppl;
import java.util.Hashtable;

import resources.CCAppl.IncidentsHelper;
import TestData.ClaimCenterData;
import Utilities.Logger;
import Utilities.Logger.MessageType;

/**
 * Implements the ClaimCenter Incidents. Provides methods, to create, view and modify the Incidents including
 * Vehicle Incidents, Injury Incidents and Property Incidents
 * 
 * @since 2012/04/18
 * @author Vinayak Hegde
 */
public class Incidents extends IncidentsHelper
{
	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Class and assigns root object and logger
	 * 
	 * @param rootObject
	 *            - root Test object
	 * @param log
	 *            - Logger instance to log the messages
	 * @since 2012/03/08
	 */
	public Incidents(UIObjectsAPI rootObject, Logger log) {
		ccRootObject = rootObject;
		logger = log;
	}

	/**
	 * Adds a property Incidents
	 * @param property
	 * 				- Data to use for the property incident
	 * @throws Exception 
	 * @author Vinayak Hegde
	 */
	public void addPropertyIncident(String incidentType, String property) throws Exception {

		logger.log_Message("New property Incidents", MessageType.INFO);

		//Go to Loss details screen
		ccRootObject.link("Click", "LossDetails");

		//Click on edit button
		ccRootObject.button("Click", "EditButton");

		//Click on Add button
		ccRootObject.button("Click", "IncidentAddButton");

		String[] incident = new String[]{incidentType};
		ccRootObject.menu("Click", "IncidentPanelMenu", incident);
		
		if( incidentType.trim().equalsIgnoreCase("Building"))
		{
			buildingIncident(property);
		}
		
		if( incidentType.trim().equalsIgnoreCase("Contents/Non Building")) {
			nonBuildingIncidents(property);
		}
		
		if ( incidentType.trim().equalsIgnoreCase("ALE/Loss of Rents") ) {
			lossOfRentIncident(property);
		}
		
		if ( incidentType.trim().equalsIgnoreCase("Property Liability")) {
			propertyLiability(property);
		}
		
		if( incidentType.trim().equalsIgnoreCase("Watercraft") ) {
			watercraftIncident(property);
		}

		//Click on add button for property Incidents.
		ccRootObject.link("Click", "UpdateButton");

		//check if note is created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "'Errors while creating property Incident '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("New property incident added successully", MessageType.INFO);
	}

	/*Fields common to Building incident and property Liabilties*/
	private void propertyIncidentInfo(Hashtable<String, String> data_propertyIncident) throws Exception {
		//Enter property description
		ccRootObject.editBox("Input","propertyDescription", data_propertyIncident.get("PropertyDescription"));

		//Enter the Damage Description
		ccRootObject.editBox("Input","damageDescription",data_propertyIncident.get("DamageDescription"));

		//Add Damage to Info
		if(data_propertyIncident.get("DamagetoBuilding").trim().equalsIgnoreCase("Yes")){
			ccRootObject.checkBox("Check", "DamagetoBuilding");
		}
		if((!data_propertyIncident.get("DamagetoNonBuilding").trim().isEmpty())){
			ccRootObject.checkBox("Check", "DamagetoNonBuilding");
		}
		
		/*Property*/
		//Property Name
		if ( data_propertyIncident.get("PropertyName").startsWith("New") ) {
			ccRootObject.comboBox("Select", "propertyName", "New...");
		}
		else {
			ccRootObject.comboBox("Select", "propertyName", data_propertyIncident.get("PropertyName"));
		}
		
		//Add the property location
		AddressBook.addLocation(ccRootObject,data_propertyIncident.get("PropertyLocation"));

		/*Notes*/

		if ( !data_propertyIncident.get("Note").isEmpty() )
		{
			addNote(data_propertyIncident.get("Note")); 
		}

		//Inspection
		// Inspection Required? 
		if ( data_propertyIncident.get("InspectionRequired").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "InspectionRequired");

			//Without Prejudice?
			if ( data_propertyIncident.get("WithoutPrejudice").trim().equalsIgnoreCase("Yes") ) {
				ccRootObject.radioButton("Select", "WithoutPrejudice");
			}
			else if ( data_propertyIncident.get("WithoutPrejudice").trim().equalsIgnoreCase("No") ) {
				ccRootObject.radioButton("Select", "WithPrejudice");
			}

		}
		else if ( data_propertyIncident.get("InspectionRequired").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "InspectionNotRequired");
		}

		//Inspector
		//Repairs	

		//Add initial estimates
		ccRootObject.editBox("Input", "intialEstimates", data_propertyIncident.get("InitialEstimate"));
		//Other Environmental Damage
		ccRootObject.comboBox("Select","EnvironmentalContaminates", data_propertyIncident.get("OtherEnvironmentalContamination"));
		//Environmental Damage Details
		ccRootObject.editBox("Input","OtherEnvironDetail", data_propertyIncident.get("OtherEnvironmentalDamageDetails"));

		//Repairer
		//Salvage
		if ( data_propertyIncident.get("IsThereSalvage").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "Salvageable");
			ccRootObject.editBox("Input", "SalvagibiltyDesc", data_propertyIncident.get("SalvageDesc"));
		}
		else if ( data_propertyIncident.get("IsThereSalvage").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "NotSalvageable");
		}
	}	

	/**
	 * Contents/Non Building Incidents
	 * */
	private void nonBuildingIncidents(String ALELossofRent)throws Exception{
		logger.log_Message("Contents/Non Building Incidents", MessageType.INFO);

		//Read the Data for Property  Incident sheet
		Hashtable<String, String> data_propertyIncident = ClaimCenterData.getData("ContentsAndNonBuildingIncidents",ALELossofRent);


		/*Damage*/
		//Enter property description
		ccRootObject.editBox("Input","propertyDescription", data_propertyIncident.get("PropertyDescription"));

		//Enter the Damage Description
		ccRootObject.editBox("Input","damageDescription",data_propertyIncident.get("DamageDescription"));

		/*Scheduled Items*/
		ccRootObject.comboBox("Select", "SchleduledItemsRetrievedFrom", data_propertyIncident.get("SchleduledItemsRetrievedFrom"));

		/*line items*/
		if (!data_propertyIncident.get("LineItems").trim().isEmpty() ) {
			for( String lineItems : data_propertyIncident.get("LineItems").split(",")) {
				if ( !lineItems.trim().isEmpty()) {
					Hashtable<String, String> data_LineItem = ClaimCenterData.getData("ContentsLineItems", lineItems);
					//Add
					ccRootObject.button("Click", "LineItemsAddButton");
					//Insurer
					ccRootObject.editBox("Input", "ItemQty", data_LineItem.get("ItemQty"));
					ccRootObject.editBox("Input", "ItemName", data_LineItem.get("Item"));
					ccRootObject.comboBox("Select", "ItemCategory", data_LineItem.get("ItemCategory"));
					ccRootObject.editBox("Input", "ItemTotalCost", data_LineItem.get("ItemTotalCost"));
					ccRootObject.editBox("Input", "ItemPurchaseDate", data_LineItem.get("ItemPurchaseDate"));
					ccRootObject.editBox("Input", "ItemDepreciation", data_LineItem.get("ItemDepreciation"));
					ccRootObject.editBox("Input", "ItemReplValue", data_LineItem.get("ItemReplValue"));
					ccRootObject.editBox("Input", "ItemLimitAmount", data_LineItem.get("ItemLimitAmount"));

				}
			}
		}

		/*Notes*/		
		addNote(data_propertyIncident.get("Note")); 

		/*Adjusting Details*/

		//Inspection
		//  Inspection 
		// Inspection Required? 
		if ( data_propertyIncident.get("InspectionRequired").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "InspectionRequired");

			//Without Prejudice?
			if ( data_propertyIncident.get("WithoutPrejudice").trim().equalsIgnoreCase("Yes") ) {
				ccRootObject.radioButton("Select", "WithoutPrejudice");
			}
			else if ( data_propertyIncident.get("WithoutPrejudice").trim().equalsIgnoreCase("No") ) {
				ccRootObject.radioButton("Select", "WithPrejudice");
			}

		}
		else if ( data_propertyIncident.get("InspectionRequired").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "InspectionNotRequired");
		}


		//Inspector
		//Repairs	
		//Repairer

		//Salvage
		if ( data_propertyIncident.get("Salvageable").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "Salvageable");
			ccRootObject.editBox("Input", "SalvagibiltyDesc", data_propertyIncident.get("SalvagibiltyDesc"));
		}
		else if ( data_propertyIncident.get("Salvageable").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "NotSalvageable");
		}

		//Click on finish
		ccRootObject.button("Click", "UpdateButton");


		//check  created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating Non Building Incident'" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("New Non Building Incident created successully", MessageType.INFO);
	}

	/**
	 * Loss of rent incidnet*/
	private void lossOfRentIncident(String lossOfRent)throws Exception{
		logger.log_Message("Loss of Rent Incident", MessageType.INFO);

		//Read the Data for Property  Incident sheet
		Hashtable<String, String> data_propertyIncident = ClaimCenterData.getData("ALELossofRentsIncidents",lossOfRent);

		//Enter the Damage Description
		ccRootObject.editBox("Input","damageDescription",data_propertyIncident.get("DamageDescription"));

		/*Meals*/
		//# of days
		ccRootObject.editBox("Input", "MealDays", data_propertyIncident.get("Meals_NoOfDays"));

		//# of People
		ccRootObject.editBox("Input", "MealPeople", data_propertyIncident.get("Meals_NoOfPeople"));

		//Rate/Day
		ccRootObject.editBox("Input", "RateperDay", data_propertyIncident.get("Meals_RatePerDay"));

		//PreLossFoodCost
		ccRootObject.editBox("Input", "PreLossFoodCost", data_propertyIncident.get("PreLossAverageFoodCostPerDay"));

		//Temporary Residence/Location
		if (!data_propertyIncident.get("TemporaryResidence").trim().isEmpty() ) {
			for( String temporaryResidency : data_propertyIncident.get("TemporaryResidence").split(",")) {
				if ( !temporaryResidency.trim().isEmpty()) {
					Hashtable<String, String> data_LineItem = ClaimCenterData.getData("TemporaryResidence", temporaryResidency);
					//Add
					ccRootObject.button("Click", "TemporaryResidencyAddButton");
					//Insurer
					ccRootObject.editBox("Input", "LodgingStartDate", data_LineItem.get("StartDate"));
					ccRootObject.editBox("Input", "LodgingEndDate", data_LineItem.get("EndDate"));
					ccRootObject.comboBox("Select", "LodgingLocation", data_LineItem.get("Location"));
					ccRootObject.editBox("Input", "LodgingRate", data_LineItem.get("Rate"));
					ccRootObject.comboBox("Select", "LodgingPer", data_LineItem.get("Per"));
					ccRootObject.editBox("Input", "LodgingDeposit", data_LineItem.get("Deposit"));


				}
			}
		}
		//Loss of Rents
		if (!data_propertyIncident.get("LossofRent").trim().isEmpty() ) {
			for( String lossofRent : data_propertyIncident.get("LossofRent").split(",")) {
				if ( !lossofRent.trim().isEmpty()) {
					Hashtable<String, String> data_LossOfRent= ClaimCenterData.getData("LossofRents", lossofRent);
					//Add
					ccRootObject.button("Click", "LossOfRentAddButton");
					//Insurer
					ccRootObject.editBox("Input", "LORStartDate", data_LossOfRent.get("StartDate"));
					ccRootObject.editBox("Input", "LOREndDate", data_LossOfRent.get("EndDate"));
					ccRootObject.editBox("Input", "LORRate", data_LossOfRent.get("Rate"));
					ccRootObject.comboBox("Select", "LORPer", data_LossOfRent.get("Per"));
					ccRootObject.editBox("Input", "LORTenentInfo", data_LossOfRent.get("TenentInfo"));


				}
			}
		}
		//Add Note
		addNote(data_propertyIncident.get("Note"));

		//Adjustment Details
		if (!data_propertyIncident.get("AdvancePayment").trim().isEmpty() ) {
			for( String advancePayment : data_propertyIncident.get("AdvancePayment").split(",")) {
				if ( !advancePayment.trim().isEmpty()) {
					Hashtable<String, String> data_advancePayment= ClaimCenterData.getData("AdvancePayments", advancePayment);
					//Add
					ccRootObject.button("Click", "advancePaymentAddButton");
					//Insurer
					ccRootObject.editBox("Input", "advancePaymentDate", data_advancePayment.get("Date"));
					ccRootObject.editBox("Input", "advancePaymentEndDate", data_advancePayment.get("Amount"));
					ccRootObject.editBox("Input", "advancePaymentDeposit", data_advancePayment.get("Comments"));


				}
			}
		}

		//Operations
		/*	if (!data_propertyIncident.get("Operations").trim().isEmpty() ) {
			for( String operations : data_propertyIncident.get("Operations").split(",")) {
				if ( !operations.trim().isEmpty()) {
					Hashtable<String, String> data_operations= ClaimCenterData.getData("Operations", operations);
					//Add
					ccRootObject.button("Click", "operationsAddButton");
					//Insurer
					ccRootObject.comboBox("Select", "opType", data_operations.get("Type"));
					ccRootObject.editBox("Input", "opDate", data_operations.get("Date"));



				}
			}
		}
		 */

		//Return to residence
		//RevisedTargetDate
		ccRootObject.editBox("Input", "RevisedTargetDate", data_propertyIncident.get("Return_RevisedTargetDate"));

		//InitialTargetDate
		ccRootObject.editBox("Input", "InitialTargetDate", data_propertyIncident.get("Return_InitialTargetDate"));

		//ActualReturnDate
		ccRootObject.editBox("Input", "ActualReturnDate", data_propertyIncident.get("Return_ActualReturnDate"));

		//Click on finish
		ccRootObject.button("Click", "UpdateButton");


		//check  created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating Loss of Rent Incident'" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("New Loss of rent Incident created successully", MessageType.INFO);

	}	

	/**
	 * Water craft
	 * */
	private void watercraftIncident(String waterCraft )throws Exception{
		//Read the Data for Property  Incident sheet
		Hashtable<String, String> data_watercraftIncident = ClaimCenterData.getData("WaterCraftIncidents",waterCraft);


		/*Damage*/
		ccRootObject.comboBox("String", "lossParty", data_watercraftIncident.get("LossParty"));

		//Enter property description
		ccRootObject.editBox("Input","propertyDescription", data_watercraftIncident.get("PropertyDescription"));

		//Enter the Damage Description
		ccRootObject.editBox("Input","damageDescription",data_watercraftIncident.get("DamageDescription"));

		/*Water craft*/
		//Select watercraft name
		ccRootObject.comboBox("Select","watercraftType",data_watercraftIncident.get("SelectWaterCraft"));

		//Year
		ccRootObject.editBox("Input","watercraftYear", data_watercraftIncident.get("Year"));

		//Make
		ccRootObject.editBox("Input","watercraftMake", data_watercraftIncident.get("Make"));

		//Model
		ccRootObject.editBox("Input","watercraftmodel", data_watercraftIncident.get("Model"));

		//Boat type
		ccRootObject.comboBox("Select", "watercraftboatType", data_watercraftIncident.get("BoatType"));

		//Boat Serial
		ccRootObject.editBox("Input","watercraftBoatSerial", data_watercraftIncident.get("SerialNo"));

		//Horse power
		ccRootObject.editBox("Input","watercrafthorsePower", data_watercraftIncident.get("HorsePower"));

		//Lenght
		ccRootObject.editBox("Input","watercraftLenght", data_watercraftIncident.get("Length"));

		/*Driver Info*/
		//Driver name
		ccRootObject.comboBox("Select", "watercraftdriverName", data_watercraftIncident.get("DriverName"));

		//Relation to policy holder
		ccRootObject.comboBox("Select", "watercraftpolicyHolderRelation", data_watercraftIncident.get("PolicyHolderRelation"));

		//Number of people
		ccRootObject.editBox("Input","watercraftnumberOfOccupant", data_watercraftIncident.get("NumberOfOccupant"));

		//note
		addNote(data_watercraftIncident.get("Note"));

		/*Adjusting Details*/

		//Inspection
		//  Inspection 
		// Inspection Required? 
		if ( data_watercraftIncident.get("InspectionRequired").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "InspectionRequired");

			//Without Prejudice?
			if ( data_watercraftIncident.get("InspectionWithoutPrejudice").trim().equalsIgnoreCase("Yes") ) {
				ccRootObject.radioButton("Select", "WithoutPrejudice");
			}
			else if ( data_watercraftIncident.get("InspectionWithoutPrejudice").trim().equalsIgnoreCase("No") ) {
				ccRootObject.radioButton("Select", "WithPrejudice");
			}

		}
		else if ( data_watercraftIncident.get("InspectionRequired").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "InspectionNotRequired");
		}


		//Inspector

		//Repairs	
		//Repairer

		//Owner performing repair?
		if(data_watercraftIncident.get("IsOwnerPerformingRepairs").trim().equalsIgnoreCase("Yes")){
			ccRootObject.radioButton("Check", "OwnerPerformingRepairsYes");
		}
		//Add initial estimates
		ccRootObject.editBox("Input", "intialEstimates", data_watercraftIncident.get("InitialEstimate"));

		//Mould Involved?
		ccRootObject.comboBox("Select","mouldStatus", data_watercraftIncident.get("MouldInvolved"));

		////Asbestos Involved?
		ccRootObject.comboBox("Select","asbestosStatus", data_watercraftIncident.get("AsbestosInvolved"));


		//Other Environmental Damage
		ccRootObject.editBox("Input","otherEnvironmentalDamageCB", data_watercraftIncident.get("OtherEnvironmentalContamination"));

		//Environmental Damage Details
		ccRootObject.editBox("Input","otherEnvironmentalDamageDetails", data_watercraftIncident.get("RepairsDetails"));

		//Repair details

		//Deductible Details
		ccRootObject.editBox("Input","Deductible", data_watercraftIncident.get("DeductibleAmount"));

		//Total loss
		if(data_watercraftIncident.get("TotalLoss").trim().equalsIgnoreCase("Yes")){
			ccRootObject.radioButton("Check", "WaterCraftTotalLossYes");

			///Cash value
			ccRootObject.editBox("Input", "cashValue", data_watercraftIncident.get("ActualCashValue"));

			//Date Settled
			ccRootObject.editBox("Input", "dateSettled", data_watercraftIncident.get("DateSettled"));
		}

		//Salvage
		if ( data_watercraftIncident.get("IsThereSalvage").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "Salvageable");

			//Salvage description
			ccRootObject.editBox("Input", "SalvagibiltyDesc", data_watercraftIncident.get("SalvageDescription"));

			//salvage company
			ccRootObject.comboBox("Select", "salvageCompany",  data_watercraftIncident.get("SalvageCompany"));

			//Date Contacted
			ccRootObject.editBox("Input", "dateContact", data_watercraftIncident.get("DateContactedForPickup"));

			//Stock Number
			ccRootObject.editBox("Input", "stockNumber", data_watercraftIncident.get("StockNumber"));
		}
		else if ( data_watercraftIncident.get("Salvageable").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "NotSalvageable");
		}

		//Click on finish
		ccRootObject.button("Click", "UpdateButton");


		//check  created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating WaterCraft incident'" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("New water craft incident created successully", MessageType.INFO);

	}	

	/**
	 * */
	private void buildingIncident(String buildingIncident)throws Exception{
		logger.log_Message("Building Incidents", MessageType.INFO);

		Hashtable<String, String> data_propertyIncident = ClaimCenterData.getData("BuildingIncidents",buildingIncident);

		/*Damage*/
		propertyIncidentInfo(data_propertyIncident);

		//Damage to
		if(!(data_propertyIncident.get("DamagetoBuilding").trim().isEmpty()&&data_propertyIncident.get("DamageToOtherStructure").trim().isEmpty()&&data_propertyIncident.get("DamageToOutbuilding").trim().isEmpty())){
			if((!data_propertyIncident.get("DamagetoBuilding").trim().isEmpty()&&data_propertyIncident.get("DamagetoBuilding").trim().equalsIgnoreCase("Yes"))&&(!data_propertyIncident.get("DamagetoBuilding").trim().isEmpty())){

				ccRootObject.checkBox("Select", "damageToBuilding");
			}
			if((!data_propertyIncident.get("DamageToOtherStructure").trim().isEmpty()&&data_propertyIncident.get("OtherStructure").trim().equalsIgnoreCase("Yes"))&&(!data_propertyIncident.get("DamagetoBuilding").trim().isEmpty())){
				ccRootObject.checkBox("Select", "otherStructure");
			}
			if((!data_propertyIncident.get("DamageToOutbuilding").trim().isEmpty()&&data_propertyIncident.get("OutBuilding").trim().equalsIgnoreCase("Yes"))&&(!data_propertyIncident.get("DamagetoBuilding").trim().isEmpty())){
				ccRootObject.checkBox("Select", "outBuilding");
			}			
		}else{
			throw new Exception("Check at least one Damage to option");
		}
		//Other financial Info
		if (!data_propertyIncident.get("OtherFinancialInterests ").trim().isEmpty() ) {
			for( String financialInfo : data_propertyIncident.get("OtherFinancialInterests ").split(",")) {
				if ( !financialInfo.trim().isEmpty()) {
					Hashtable<String, String> data_OtherCarrier = ClaimCenterData.getData("FinancialInformation", financialInfo);
					//Add
					ccRootObject.button("Click", "OtherfinancialInfoAddButton");
					//Insurer
					ccRootObject.editBox("Input", "OtherfinancialInfoMortgage", data_OtherCarrier.get("Mortgagee"));
					ccRootObject.editBox("Input", "OtherfinancialInfoOwnerType", data_OtherCarrier.get("OwnerType"));

				}
			}
		}

		//Home owner repairning?
		if(data_propertyIncident.get("HomeownerPerformingRepairs").trim().equalsIgnoreCase("Yes")){
			ccRootObject.radioButton("Select", "HomeownerPerformRepairYes");

		}
		else if(data_propertyIncident.get("HomeownerPerformingRepairs").trim().equalsIgnoreCase("No")){
			ccRootObject.radioButton("Select", "HomeownerPerformRepairNo");
		}

		//Mould Involved?
		ccRootObject.comboBox("Select","mouldStatus", data_propertyIncident.get("MouldInvolved"));

		////Asbestos Involved?
		ccRootObject.comboBox("Select","asbestosStatus", data_propertyIncident.get("AsbestosInvolved"));

		//Click on finish
		ccRootObject.button("Click", "UpdateButton");

		//check  created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating building incident  '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("New building incident  created successully", MessageType.INFO);

	}	

	/**
	 * Adds information on the property liability screen
	 *  @param string
	 *  		-- Data to use for the property incident
	 *  */
	public void propertyLiability( String property) throws Exception{

		logger.log_Message("property Liabilities", MessageType.INFO);

		//	get the data
		Hashtable<String, String> data_propertyIncident = ClaimCenterData.getData("PropertyLiabilityIncidents", property);
		/*Damage*/
		propertyIncidentInfo(data_propertyIncident);

		//VoluntryPDpaidOrNot
		if ( data_propertyIncident.get("VoluntryPDpaid").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "VoluntryPDpaid");
		}
		else if ( data_propertyIncident.get("VoluntryPDpaid").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "VoluntryPDNotpaid");
		}

		/*Owner Info*/
		//Select owner name
		ccRootObject.comboBox("Select", "ownerName",data_propertyIncident.get("OwnerName"));
		//Third party insurer repairing?
		if ( data_propertyIncident.get("ThirdPartyInsurerRepair").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "ThirdPartyInsurerRepairYes");
		}
		else if ( data_propertyIncident.get("ThirdPartyInsurerRepair").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "ThirdPartyInsurerRepairNo");
		}

		//Click on finish
		ccRootObject.button("Click", "UpdateButton");

		//check  created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating property  laibility'" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("New property Liabilty created successully", MessageType.INFO);
	}

	/*Method to add Notes */
	private void addNote(String noteIndex) throws Exception {
		//Get the data
		Hashtable<String, String> datapropertyIncidents_Notes = ClaimCenterData.getData( "Notes", noteIndex);

		//Topic
		ccRootObject.comboBox("Select","NoteTopic", datapropertyIncidents_Notes.get("Topic"));
		//Subject
		ccRootObject.editBox("Input","NoteSubject", datapropertyIncidents_Notes.get("Subject"));
		//Related To
		ccRootObject.comboBox("Select","RelatedTo", datapropertyIncidents_Notes.get("RelatedTo"));
		//Confidential
		ccRootObject.comboBox("Select" ,"NoteConfidential", datapropertyIncidents_Notes.get("Confidential"));
		//Text
		ccRootObject.editBox("Input","NotesText", datapropertyIncidents_Notes.get("Text"));
	}

	/**
	 * Adds Incident Details to the specified vehicle for Full AUTO wizard 
	 * @param vehicle
	 * 				- Vehicle Index VehicleIncidents
	 * @throws Exception
	 */
	public void addVehicleIncidentAUTO(String vehicle) throws Exception {

		//Read the Data for Vehicle Incident
		Hashtable<String, String> data_Incident = ClaimCenterData.getData("VehicleIncidents", vehicle);

		//Check Loss Party is drop down or radio button
		if ( ccRootObject.radioButton("Verify", "LossPartyInsured").startsWith("PASS") ) {
			if ( data_Incident.get("LossParty").trim().equalsIgnoreCase("Insured") ) {
				ccRootObject.radioButton("Select", "LossPartyInsured");
			}
			else if ( data_Incident.get("LossParty").trim().equalsIgnoreCase("Third Party") ) {
				ccRootObject.radioButton("Select", "LossPartyThirdParty");
			}
		}
		else {
			ccRootObject.comboBox("Select", "LossParty", data_Incident.get("LossParty"));
		}

		//Select Vehicle
		if ( data_Incident.get("SelectVehicle").startsWith("New") ||
				data_Incident.get("SelectVehicle").trim().equalsIgnoreCase("")) {

			ccRootObject.comboBox("Select", "SelectVehicle", "New...");
			//Year
			ccRootObject.editBox("Input", "VehicleYear", data_Incident.get("Year").replace(".0", ""));
			//Make
			ccRootObject.editBox("Input", "VehicleMake", data_Incident.get("Make"));
			//Model
			ccRootObject.editBox("Input", "VehicleModel", data_Incident.get("Model"));
			//BodyType
			ccRootObject.comboBox("Select", "VehicleStyle", data_Incident.get("BodyType"));
			//Color
			ccRootObject.editBox("Input", "VehicleColor", data_Incident.get("Colour"));
			//License Plate
			ccRootObject.editBox("Input", "VehicleLicensePlate", data_Incident.get("LicensePlate"));
			//Province
			ccRootObject.comboBox("Select", "VehicleProvince", data_Incident.get("Province"));
			//VIN
			ccRootObject.editBox("Input", "VehicleVIN", data_Incident.get("VIN"));
			//Type of Non Owned Vehicle
			ccRootObject.editBox("Input", "VehicleType", data_Incident.get("TypeOfNonOwnedVehicle"));
		}
		else {
			ccRootObject.comboBox("Select", "SelectVehicle", data_Incident.get("SelectVehicle"));
		}

		//Add Driver
		if ( !data_Incident.get("Driver").trim().isEmpty()) {
			addDriverDetails(
					data_Incident.get("Driver"),data_Incident.get("DriverNumber"),
					data_Incident.get("RelationtoPolicyholder"), data_Incident.get("RelationtoOwner"),
					data_Incident.get("OwnersPermission"),
					data_Incident.get("DriversLicenceValid"), data_Incident.get("ReasonforUse"),
					data_Incident.get("NumberOfPassengers"));
		}

		//Add Passengers
		//Get the passengers, split by comma
		addPassengers(data_Incident.get("Passengers"));

		//Damage Description
		ccRootObject.editBox("Input", "Description", data_Incident.get("DamageDescription"));

		//Airbags Deployed?
		if( data_Incident.get("AirbagsDeployed").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "AirbagsDeployedYes");
		}
		else if( data_Incident.get("AirbagsDeployed").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "AirbagsDeployedNo");
		}

		//Driveable
		if( data_Incident.get("Driveable").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "DriveableYes");
		}
		else if( data_Incident.get("Driveable").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "DriveableNo");
		}

		//Possible Total Loss?
		if( data_Incident.get("PossibleTotalLoss").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "PossibleTotalLossYes");
		}
		else if( data_Incident.get("PossibleTotalLoss").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "PossibleTotalLossNo");
		}

		//Limited Waiver of Depreciation
		if( data_Incident.get("LimitedWaiverOfDepreciation").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "LimitedWaiverDepreciationYes");
		}
		else if( data_Incident.get("LimitedWaiverOfDepreciation").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "LimitedWaiverDepreciationNo");
		}

		//Repairs
		addRepairs(
				data_Incident.get("Repairs_DeductibleAmount"),
				(data_Incident.get("Repairs_TotalLoss").equalsIgnoreCase("Yes"))?true:false,
				data_Incident.get("Repairs_ActualCashValue"),
				data_Incident.get("Repairs_DateSettled"),
				(data_Incident.get("Repairs_IsThereSalvage").equalsIgnoreCase("Yes"))?true:false,
				data_Incident.get("SalvageCompany"),
				data_Incident.get("DateContactedForPickUp"),
				data_Incident.get("StockNumber"),
				data_Incident.get("RepairShop"),
				data_Incident.get("LossEstimate"));

		//Towing
		addTowing(
				data_Incident.get("WasVehicleTowed").equalsIgnoreCase("Yes")?true:false,
				data_Incident.get("TowCompany"),
				data_Incident.get("TowCompanyContactedBy"),
				data_Incident.get("InsuredRequestedBodyShop").equalsIgnoreCase("Yes")?true:false,
				data_Incident.get("TowOperatorSuggestedBodyShop").equalsIgnoreCase("Yes")?true:false,
				data_Incident.get("DolliesUsed").equalsIgnoreCase("Yes")?true:false,
				data_Incident.get("ChargeBreakdownAndTotalCost").equalsIgnoreCase("Yes")?true:false,
				data_Incident.get("InsuredInformedOfCityLimitsCharge").equalsIgnoreCase("Yes")?true:false,
				data_Incident.get("InsuredSignAnything").equalsIgnoreCase("Yes")?true:false);


		//Inspection
		addInspection(
				data_Incident.get("InspectionRequired").equalsIgnoreCase("Yes")?true:false,
				data_Incident.get("Appraiser"),
				data_Incident.get("WithoutPrejudice").equalsIgnoreCase("Yes")?true:false);

		//Vehicle Location
		ccRootObject.comboBox("Select", "VehicleLocationContact", data_Incident.get("LocationContact"));

		//Theft and Theft Recovery
		addTheftandRecovery(
				data_Incident.get("Wasthevehiclestolen").equalsIgnoreCase("Yes")?true:false,
				data_Incident.get("VehicleLocked").equalsIgnoreCase("Yes")?true:false,
				data_Incident.get("AntiTheftDevice").equalsIgnoreCase("Yes")?true:false,
				data_Incident.get("OdometerReading"),
				data_Incident.get("WasVehicleRecovered"),
				data_Incident.get("WhenRecovered"),
				data_Incident.get("WhereRecovered"),
				data_Incident.get("RecoveryProvince"),
				data_Incident.get("RecoveredBy"),
				data_Incident.get("RecoveryCondition"),
				data_Incident.get("PolicyHold").equalsIgnoreCase("Yes")?true:false);

		ccRootObject.button("Click", "UpdateButton");
		//Check the vehicle is added correctly
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while adding Vehicle Incident '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("Vehicle Incident " + vehicle + " added correctly", MessageType.INFO);
	}

	/**
	 * @param data_Incident
	 * @throws Exception
	 */
	private void addPassengers(String passengers) throws Exception {

		String[] passengersTokens = passengers.split(",");
		for( String passenger : passengersTokens ) {
			if ( !passenger.trim().isEmpty() ) {
				//Get Passenger Contact Details
				Hashtable<String, String> data_Contact = ClaimCenterData.getData("Contacts", passenger );
				//If Loss Details screen 
				if ( ccRootObject.findExcelMappedTestObject( ccRootObject.getBrowserDocObject(), "LossDetailsVehicleDetailsDiv") != null ){
					ccRootObject.button("Click", "AddButton");
					ccRootObject.comboBox("Select", "PassengerName", data_Contact.get("ContactName"));
					ccRootObject.comboBox("Select", "Role", data_Contact.get("FirstRole"));
					ccRootObject.editBox("Input", "Comments", data_Contact.get("Notes"));
					continue;
				}
				//If FNOL screen
				if ( ccRootObject.findExcelMappedTestObject( ccRootObject.getBrowserDocObject(), "FNOLVehicleIncidentDiv") != null ){
					//AddPassengerButton
					ccRootObject.button("Click", "AddPassengerButton");
					addIncidentContactDetails(
							data_Contact.get("FirstName") + " " + data_Contact.get("LastName"),
							data_Contact);
					ccRootObject.button("Click", "UpdateButton");
				}
			}
		}
	}

	/**
	 * Adds Driver Details in the Vehicle Incident
	 * @param driverDetails
	 * 			- Index of Drivers Details Sheet
	 * @throws Exception
	 */
	private void addDriverDetails( String driverContactDetails, String driverNumber, String relationtoPolicyholder, String relationtoOwner, String ownersPermission,
			String driversLicenceValid, String reasonforUse, String numberOfPassengers ) throws Exception {

		Hashtable<String, String> data_DriverContactDetails = ClaimCenterData.getData("Contacts", driverContactDetails);

		//If the Driver details through Loss Details
		if ( ccRootObject.findExcelMappedTestObject( ccRootObject.getBrowserDocObject(), "LossDetailsVehicleDetailsDiv") != null ){
			//Driver Name
			ccRootObject.comboBox("Select", "DriverName", data_DriverContactDetails.get("ContactName"));
			//Relationship to Policy holder
			ccRootObject.comboBox("Select", "RelationshiptoPolicyholder", relationtoPolicyholder);
			//Owner's Permission?
			if ( ownersPermission.equalsIgnoreCase("Yes")) {
				ccRootObject.radioButton("Select", "OwnersPermisionYes");
			}
			else {
				ccRootObject.radioButton("Select", "OwnersPermisionNo");
			}
			//Driver's License Valid?
			if ( driversLicenceValid.equalsIgnoreCase("Yes")) {
				ccRootObject.radioButton("Select", "ValidLicenseYes");
			}
			else {
				ccRootObject.radioButton("Select", "ValidLicenseNo");
			}
			//Reason for Use
			ccRootObject.comboBox("Select", "ReasonofUse", reasonforUse);
			//Number of Passengers
			ccRootObject.editBox("Input", "NumberofPassengers", numberOfPassengers);
		}

		//If the Driver details through FNOL
		if ( ccRootObject.findExcelMappedTestObject( ccRootObject.getBrowserDocObject(), "FNOLVehicleIncidentDiv") != null ){

			ccRootObject.button("click", "AddDriverButton");

			String driverName = (data_DriverContactDetails.get("FirstName") + " " +
					data_DriverContactDetails.get("LastName") + " " + driverNumber).trim();

			addIncidentContactDetails( driverName, data_DriverContactDetails );

			ccRootObject.button("Click", "UpdateButton");
			//Check the vehicle is added correctly
			String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
			if ( errorMessagesContents != null) {
				if (!errorMessagesContents.trim().isEmpty()) {
					String error = "Errors while adding driver Deails - '" + errorMessagesContents + "'";
					throw new Exception(error);
				}
			}
		}
		logger.log_Message("Driver details are added", MessageType.INFO);			
	}

	/**
	 * Adds Contact details for the Incidents
	 * @param data_ContactDetails
	 * @throws Exception
	 */
	private void addIncidentContactDetails( String selectContact, Hashtable<String, String> data_ContactDetails) throws Exception {

		if ( !selectContact.trim().isEmpty() &&
				ccRootObject.comboBox("Verify", "ContactPerson", selectContact).startsWith("PASS")) {
			ccRootObject.comboBox("Select", "ContactPerson", selectContact);
		}
		else {
			//First Name
			ccRootObject.editBox("Input", "FirstName", data_ContactDetails.get("FirstName"));
			//Last Name
			ccRootObject.editBox("Input", "LastName", data_ContactDetails.get("LastName"));
		}
		//Add Address
		AddressBook.addLocation(ccRootObject, data_ContactDetails.get("PrimaryAddress"));

		//Business Phone
		ccRootObject.editBox("Input", "BusinessPhone", data_ContactDetails.get("BusinessPhone"));
		//Home Phone   
		ccRootObject.editBox("Input", "HomePhoneNumber", data_ContactDetails.get("HomePhone"));
		//Mobile   
		ccRootObject.editBox("Input", "Mobile", data_ContactDetails.get("Mobile"));
		//Primary Phone 
		ccRootObject.comboBox("Select", "PrimaryPhoneType", data_ContactDetails.get("PrimaryPhone"));
		//Email   
		ccRootObject.editBox("Input", "EmailAddress", data_ContactDetails.get("MainEmail"));
		//Alternate Email   
		ccRootObject.editBox("Input", "AlternateEmail", data_ContactDetails.get("AlternateEmail"));

		//License Status
		ccRootObject.comboBox("Select", "LicenceStatus", data_ContactDetails.get("LicenceStatus"));
		//Driver's License Number
		ccRootObject.editBox("Input", "LicenceNumber", data_ContactDetails.get("LicenceNumber"));
		//License Class
		ccRootObject.editBox("Input", "LicenceClass", data_ContactDetails.get("LicenceClass"));
		//Is Jurisdiction within Canada or US?
		if ( data_ContactDetails.get("IsJurisdictionwithinCanadaorUS").equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "LicenceUSCanadaYes");
		}
		else if ( data_ContactDetails.get("IsJurisdictionwithinCanadaorUS").equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "LicenceUSCanadaNo");
		}
		//Driver's License Province
		ccRootObject.comboBox("Select", "LicenceProvince", data_ContactDetails.get("LicenceProvince"));
		//Date Licensed
		ccRootObject.comboBox("Select", "LicenceDate", data_ContactDetails.get("DateLicenced"));

		//Notes
		ccRootObject.editBox("Input", "Notes", data_ContactDetails.get("Notes"));

		//Injured
		if ( data_ContactDetails.get("Injured").equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "InjuredYes");
		}
		else if ( data_ContactDetails.get("Injured").equalsIgnoreCase("No") ){
			ccRootObject.radioButton("Select", "InjuredNo");
		}
	}

	/**
	 * Adds a property incident to the existing claim
	 * @param property
	 * 				- Data to use for the property incident
	 * @throws Exception 
	 */
	public void addInjuryIncident(String injuryIndex) throws Exception {

		logger.log_Message("New Injury Incidents", MessageType.INFO);

		//Navigate to Loss details screen and click on edit
		navigateInjuryLiabilityLossDetails();

		//Injury screen
		injuryLiability(injuryIndex);

		ccRootObject.link("Click", "UpdateButton");

		//check if note is created successfully
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "'Errors while creating Injury Incident '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("New property incident added successully", MessageType.INFO);

	}

	/*Method to navigate from Summary screen to Properties Liabilities screen*/
	private void navigateInjuryLiabilityLossDetails() throws Exception {

		//Go to Loss details screen
		ccRootObject.link("Click", "LossDetails");

		//Click on edit button
		ccRootObject.button("Click", "EditButton");

		//Click on Property Liabilities add button
		if(ccRootObject.button("Click ", "AddInjuryButton")!=null){
			;
		}
		else{
			ccRootObject.link("Click", "IncidentPanel");

			String[] incident=new String[]{"Injury"};

			ccRootObject.menu("Click", "IncidentPanelMenu", incident);
		}
	}

	/**
	 * Adds a Injury incident
	 * @param injuryIndex
	 *			- Injury Incident data to use 		
	 * @throws Exception 
	 */
	public void injuryLiability(String injuryIndex) throws Exception {

		Hashtable<String, String> data_InjuryIncident = ClaimCenterData.getData("InjuryIncidents", injuryIndex);
		logger.log_Message("Creating new Injury Incident ", MessageType.INFO);

		//Injured Person
		ccRootObject.comboBox("Select", "InjuredPerson", data_InjuryIncident.get("InjuredPerson"));

		//ABInformation
		if(!data_InjuryIncident.get("AB").isEmpty()){
			aBinformation(data_InjuryIncident.get("AB"));			
		}
		//AB Disability
		if(!data_InjuryIncident.get("ABDisability").isEmpty()){
			aBDisability(data_InjuryIncident.get("ABDisability"));			
		}	

		//Click OK
		ccRootObject.button("Click", "UpdateButton");
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating Injury Incident '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("Injury Incident created successfully", MessageType.INFO);
	}

	/*Method to Add AB Information*/
	private void aBinformation(String injuryIndex) throws Exception {

		Hashtable<String, String> data_InjuryABInformation = ClaimCenterData.getData("ABInformation", injuryIndex);
		logger.log_Message("Entering AB Information ", MessageType.INFO);

		//Injury Details
		//AB Tab
		ccRootObject.link("Click", "ABCardTabact");

		//Benefit Jurisdiction
		ccRootObject.comboBox("Select", "QABBenefitJurisdictionTL", data_InjuryABInformation.get("BenefitJurisdiction"));
		//QAB_ApplicantType
		ccRootObject.comboBox("Select", "ApplicantType", data_InjuryABInformation.get("TypeofApplicant"));
		//Severity
		ccRootObject.comboBox("Select", "Severity", data_InjuryABInformation.get("Severity"));

		//Describe Injuries
		ccRootObject.editBox("Input", "InjuryDescription", data_InjuryABInformation.get("DescribeInjuries"));
		//InjuryCause
		ccRootObject.editBox("Input", "InjuryCause", data_InjuryABInformation.get("DidYouStriketheInterioroftheCar"));

		//Ambulance Used?
		if ( data_InjuryABInformation.get("AmbulanceUsed").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButton("Select", "AmbulanceUsedYes");
		}
		else if ( data_InjuryABInformation.get("AmbulanceUsed").equalsIgnoreCase("No")) {
			ccRootObject.radioButton("Select", "AmbulanceUsedNo");
		}
		//Hospital
		ccRootObject.editBox("Input", "Hospital", data_InjuryABInformation.get("WhatHospital"));
		//Received Treatment
		ccRootObject.editBox("Input", "ReceivedTreatment", data_InjuryABInformation.get("WhatTreatmentsHaveYouReceivedSincetheMVA"));
		//Prior Injury
		ccRootObject.editBox("Input", "PriorInjury", data_InjuryABInformation.get("DidYouHaveAnyPriorInjuries"));
		//Pre-Existing Condition
		ccRootObject.editBox("Input", "PreexistingCondition", data_InjuryABInformation.get("PreexistingCondition"));
		//Regular Doctor
		ccRootObject.editBox("Input", "RegularDr", data_InjuryABInformation.get("FamilyDoctor"));
		//RepresentedByCounsel
		if ( data_InjuryABInformation.get("RepresentedByCounsel").equalsIgnoreCase("Yes")) {
			ccRootObject.radioButton("Select", "LawyerYes");
		}
		else if ( data_InjuryABInformation.get("RepresentedByCounsel").equalsIgnoreCase("No")) {
			ccRootObject.radioButton("Select", "LawyerNo");
		}

		//Employment Details
		//Employee Codes
		ccRootObject.comboBox("Select", "EmpCodes", data_InjuryABInformation.get("EmploymentStatusAt90Days"));
		//Gross Income
		ccRootObject.editBox("Input", "GrossIncome", data_InjuryABInformation.get("GrossIncomeLastCalendarYear"));

		//Collateral Medical Coverage
		if( data_InjuryABInformation.get("CollateralMedicalCoverage").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "ClltrlMedCoverYes");
		}
		else if( data_InjuryABInformation.get("CollateralMedicalCoverage").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "ClltrlMedCoverNo");
		}

		//Medical Coverage Detail
		ccRootObject.editBox("Input", "MedicalCoverageDetail", data_InjuryABInformation.get("CollateralCoveragNameAndPolicyNumber"));
		//Other Disability Benefit
		ccRootObject.editBox("Input", "OtherDisabilityBenifit", data_InjuryABInformation.get("OtherDisabilityBenefit"));

		//Any STD/LTD
		if( data_InjuryABInformation.get("STDLTD").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "STDLTDYes");
		}
		else if( data_InjuryABInformation.get("STDLTD").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "STDLTDNo");
		}
		//STDLTD Detail
		ccRootObject.editBox("Input", "STDLTDDetail", data_InjuryABInformation.get("STDLTDNameAndPolicyNumber"));


		//Priority of applicant(Ontario Only)
		//Type of Priority
		if (!data_InjuryABInformation.get("TypeofPriority").isEmpty()){
			if((data_InjuryABInformation.get("TypeofPriority").equalsIgnoreCase("Named Insured"))){
				ccRootObject.checkBox("Check", "ApplicantPrioritynamedinsured");
			}
			if(data_InjuryABInformation.get("TypeofPriority").equalsIgnoreCase("Spourse")){
				ccRootObject.checkBox("Check", "ApplicantPriorityspouse");
			}
			if(data_InjuryABInformation.get("TypeofPriority").equalsIgnoreCase("Dependent")){
				ccRootObject.checkBox("Check", "ApplicantPrioritydependent");
			}
			if(data_InjuryABInformation.get("TypeofPriority").equalsIgnoreCase("Listed Driver")){
				ccRootObject.checkBox("Check", "ApplicantPrioritylisteddriver");
			}
			if(data_InjuryABInformation.get("TypeofPriority").equalsIgnoreCase("Regular use of a rental vehicle over 30 days")){
				ccRootObject.checkBox("Check", "ApplicantPriorityregularuseover30");
			}
			if(data_InjuryABInformation.get("TypeofPriority").equalsIgnoreCase("Regular use of a business vehicle")){
				ccRootObject.checkBox("Check", "ApplicantPriorityregularusebus");
			}
			if(data_InjuryABInformation.get("TypeofPriority").equalsIgnoreCase("Other")){
				ccRootObject.checkBox("Check", "ApplicantPriorityother");
			}
		}
		//Any STD/LTD
		if( data_InjuryABInformation.get("EligibilittoOptionalsElsewhere").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "OptnBenftEligibileYes");
		}
		else if( data_InjuryABInformation.get("EligibilittoOptionalsElsewhere").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "OptnBenftEligibileNo");
		}
		//Insurer Name
		ccRootObject.comboBox("Select", "InsurerNamTL", data_InjuryABInformation.get("InsuranceCompany"));
		//Policy Number
		ccRootObject.editBox("Input", "PolicyNum", data_InjuryABInformation.get("PolicyNumber"));

		//Conditionally Required Fields
		//CatInjury
		ccRootObject.comboBox("Select", "CatInjury", data_InjuryABInformation.get("CatastrophicInjury"));
		//Add Injury Codes
		if ( !data_InjuryABInformation.get("InjuryCode").trim().isEmpty() ) {

			String[] injuryCodes = data_InjuryABInformation.get("InjuryCodes").split(",");
			int injuryCount = 0;
			for( String injuryCode : injuryCodes) {
				//Add Body Parts
				ccRootObject.button("Click", "AddButton");
				//Injury code
				ccRootObject.comboBoxinTable("Select", ccRootObject.getBrowserDocObject(),
						injuryCount + ":", "InjuryCode", injuryCode.trim());
				injuryCount++;
			}
		}		

		//Add Note
		//addNote(data_InjuryABInformation.get("MedicalNotes"));	
	}

	/**
	 Method to ADD AB Disability 
	 */
	public void aBDisability(String injuryIndex) throws Exception {

		Hashtable<String, String> data_ABDisability = ClaimCenterData.getData("ABInformation", injuryIndex);
		logger.log_Message("Creating new Injury Incident ", MessageType.INFO);

		//AB Disability Tab
		ccRootObject.link("Click", "ABDisabilityTab");
		//Disability Income Employed
		//Employment At Loss
		if( data_ABDisability.get("WereYouEmployedattheTimeoftheLoss").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "EmplmntAtLossYes");
		}
		else if( data_ABDisability.get("WereYouEmployedattheTimeoftheLoss").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "EmplmntAtLossNo");
		}
		//Work Status
		ccRootObject.comboBox("Select", "WorkStatus", data_ABDisability.get("WorkStatus"));
		//Other Status
		ccRootObject.editBox("Input", "OtherStatus", data_ABDisability.get("WorkStatusDetails"));
		//Prior Job Duties
		ccRootObject.editBox("Input", "PriorJobDuties", data_ABDisability.get("JobDuties"));
		//DateOfHire
		ccRootObject.editBox("Input", "DateOfHire", data_ABDisability.get("DateofHire"));
		//Missed Work or Not
		if( data_ABDisability.get("AreYouoffWorkasaResultoftheMVA").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "MissedWorkYes");
		}
		else if( data_ABDisability.get("AreYouoffWorkasaResultoftheMVA").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "MissedWorkNo");
		}
		//Last Employment Date
		ccRootObject.editBox("Input", "LastEmpDate", data_ABDisability.get("DatelastWorked"));
		//Additional Employers
		if( data_ABDisability.get("IsThereMoreThanOneEmployerinTheLast52Weeks").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "AddtnlEmployersYes");
		}
		else if( data_ABDisability.get("IsThereMoreThanOneEmployerinTheLast52Weeks").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "AddtnlEmployersNo");
		}
		//Addtnl EmployersDet
		ccRootObject.editBox("Input", "AddtnlEmployersDet", data_ABDisability.get("IfYesProvideAdditonalDetails"));
		//EI Benefits
		if( data_ABDisability.get("AreYouCurrentlyReceivingEIBenefits").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "EIBenIndYes");
		}
		else if( data_ABDisability.get("AreYouCurrentlyReceivingEIBenefits").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "EIBenIndNo");
		}
		//EI Benefits Amount
		ccRootObject.editBox("Input", "EIBenAmount", data_ABDisability.get("WeeklyAmount"));
		//HouseKeeper Disabled
		if( data_ABDisability.get("HKDisInd").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "HKDisIndYes");
		}
		else if( data_ABDisability.get("HKDisInd").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "HKDisIndNo");
		}
		//Disability Income Employed
		if( data_ABDisability.get("DisabilityIncomeEmployedApplies").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "DisIncomeIndYes");
		}
		else if( data_ABDisability.get("DisabilityIncomeEmployedApplies").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "DisIncomeIndNo");
		}

		//CareGiver
		//Caregiver Prior MVA
		if( data_ABDisability.get("PrimaryCaregiverHomemakerPriortoMVA").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "CaregvrPriorMVAYes");
		}
		else if( data_ABDisability.get("PrimaryCaregiverHomemakerPriortoMVA").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "CaregvrPriorMVANo");
		}
		//Total Dependants
		ccRootObject.editBox("Input", "TotalDependants", data_ABDisability.get("NumberofDependants"));
		//Add Dependents
		if ( !data_ABDisability.get("Dependants").trim().isEmpty() ) {

			String[] dependents = data_ABDisability.get("Dependants").split(",");
			int dependentCount = 0;
			for( String dependent : dependents) {
				//Add Dependents
				ccRootObject.button("Click", "AddButton");
				String[] dependentFields = dependent.split("/");
				//DependantName
				ccRootObject.editBoxInTable("Input", ccRootObject.getBrowserDocObject(),
						dependentCount + ":", "DependantName", dependentFields[0].trim());
				//DateOfBirth
				ccRootObject.editBoxInTable("Input", ccRootObject.getBrowserDocObject(),
						dependentCount + ":", "DateOfBirth", dependentFields[1].trim());				
				//Disabled Individual or Not
				if( dependentFields[2].trim().equalsIgnoreCase("Yes") ) {
					ccRootObject.radioButtoninTable("Select", ccRootObject.getBrowserDocObject(),
							dependentCount + ":", "DisabledIndYes");
				}
				else if( dependentFields[2].trim().equalsIgnoreCase("No") ) {
					ccRootObject.radioButtoninTable("Select", ccRootObject.getBrowserDocObject(),
							dependentCount + ":", "DisabledIndNo");
				}
				//Disabled Individual or Not
				if( dependentFields[3].trim().equalsIgnoreCase("Yes") ) {
					ccRootObject.radioButtoninTable("Select", ccRootObject.getBrowserDocObject(),
							dependentCount + ":", "PrimeCaregiverYes");
				}
				else if( dependentFields[3].trim().equalsIgnoreCase("No") ) {
					ccRootObject.radioButtoninTable("Select", ccRootObject.getBrowserDocObject(),
							dependentCount + ":", "PrimeCaregiverNo");
				}
				dependentCount++;
			}
		}
		//Dependent Care Able

		if( data_ABDisability.get("AreYouAbletoCareforthem").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "DependantCareAbleYes");
		}
		else if( data_ABDisability.get("AreYouAbletoCareforthem").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "DependantCareAbleNo");
		}
		//CrgvrTasksUnable
		ccRootObject.editBox("Input", "CrgvrTasksUnable", data_ABDisability.get("TasksYouAreUnabletoComplete"));
		//CaregiverType
		ccRootObject.comboBox("Select", "CaregiverType", data_ABDisability.get("WhoisCaringforThemNow"));
		//OtherCaregvrDtl
		ccRootObject.editBox("Input", "OtherCaregvrDtl", data_ABDisability.get("OtherCaregiverDetails"));
		//DaycarePrMVA
		ccRootObject.editBox("Input", "DaycarePrMVA", data_ABDisability.get("DaycareNamePriortoMVA"));
		//DaysDaycareUsed
		ccRootObject.editBox("Input", "DaysDaycareUsed", data_ABDisability.get("NumberofDaysDaycareWasUsed"));
		//AddlCaregvrCost
		ccRootObject.editBox("Input", "AddlCaregvrCost", data_ABDisability.get("AdditionalCaregivingCostsSincetheAccident"));
		//CaregiverInd
		if( data_ABDisability.get("CaregiverApplies").trim().equalsIgnoreCase("Yes") ) {
			ccRootObject.radioButton("Select", "CaregiverIndYes");
		}
		else if( data_ABDisability.get("CaregiverApplies").trim().equalsIgnoreCase("No") ) {
			ccRootObject.radioButton("Select", "CaregiverIndNo");
		}


		if (!data_ABDisability.get("BodyParts").isEmpty()){

			bodyParts(data_ABDisability.get("BodyParts"));
		}
		if (!data_ABDisability.get("Treatments").isEmpty()){

			treatments(data_ABDisability.get("Treatments"));
		}
		if (!data_ABDisability.get("Assessments").isEmpty()){

			assessments(data_ABDisability.get("Assessments"));
		}

		logger.log_Message("AB Disability information entered successfully", MessageType.INFO);
	}

	/**
	 Method to ADD Body Parts 
	 */
	public void bodyParts(String injuryIndex) throws Exception {

		Hashtable<String, String> data_BodyParts = ClaimCenterData.getData("BodyParts", injuryIndex);
		logger.log_Message("Adding Body Parts ", MessageType.INFO);

		//ABDetailsTab
		ccRootObject.link("Click", "ABDetailsTab");

		//Add Body Parts
		if ( !data_BodyParts.get("BodyParts").trim().isEmpty() ) {

			//Link to add Body Parts
			String[] bodyParts = data_BodyParts.get("BodyParts").split(",");
			int bodyPartCount = 0;
			for( String bodyPart : bodyParts) {
				//Add Body Parts
				ccRootObject.button("Click", "AddBodyPartBtn");
				String[] bodyPartFields = bodyPart.split("/");
				//Area of Body
				ccRootObject.comboBoxinTable("Select", ccRootObject.getBrowserDocObject(),
						bodyPartCount + ":", "AreaofBody", bodyPartFields[0].trim());
				//Detailed Body Part
				ccRootObject.comboBoxinTable("Select", ccRootObject.getBrowserDocObject(),
						bodyPartCount + ":", "DetailedBodyPart", bodyPartFields[1].trim());
				//Detailed Injury Type
				ccRootObject.comboBoxinTable("Select", ccRootObject.getBrowserDocObject(),
						bodyPartCount + ":", "DetailedInjuryType", bodyPartFields[2].trim());
				bodyPartCount++;
			}
		}
		logger.log_Message("Body parts added successfully", MessageType.INFO);	

	}	

	/**
	 Method to ADD Treatments 
	 */
	public void treatments(String injuryIndex) throws Exception {

		Hashtable<String, String> data_ABTreatment = ClaimCenterData.getData("ABTreatment", injuryIndex);
		logger.log_Message("Adding Treatments ", MessageType.INFO);

		//ABDetailsTab
		ccRootObject.link("Click", "ABDetailsTab");
		//TreatmentDetails
		if ( !data_ABTreatment.get("Tretments").trim().isEmpty() ) {

			//Link to add Body Parts
			String[] tretments = data_ABTreatment.get("Tretments").split("(");
			int treatmentCount = 0;
			for( String tretment : tretments) {
				//Add Body Parts
				ccRootObject.button("Click", "AddButtonTreatmentDetails");
				String[] tretmentFields = tretment.split(")");
				//PlanNum
				ccRootObject.editBoxInTable("Input", ccRootObject.getBrowserDocObject(),
						treatmentCount + ":", "PlanNum", tretmentFields[0].trim());
				//PlanDate
				ccRootObject.editBoxInTable("Input", ccRootObject.getBrowserDocObject(),
						treatmentCount + ":", "PlanDate", tretmentFields[1].trim());
				//ProposedAmt
				ccRootObject.editBoxInTable("Input", ccRootObject.getBrowserDocObject(),
						treatmentCount + ":", "ProposedAmt", tretmentFields[2].trim());
				//ApprovedAmt
				ccRootObject.editBoxInTable("Input", ccRootObject.getBrowserDocObject(),
						treatmentCount + ":", "ApprovedAmt", tretmentFields[3].trim());
				//TreatmentFacility
				ccRootObject.comboBoxinTable("Select", ccRootObject.getBrowserDocObject(),
						treatmentCount + ":", "TreatmentFacility", tretmentFields[4].trim());
				//Description
				ccRootObject.editBoxInTable("Input", ccRootObject.getBrowserDocObject(),
						treatmentCount + ":", "Description", tretmentFields[5].trim());				
				treatmentCount++;
			}
		}
		logger.log_Message("Treatments added successfully", MessageType.INFO);	

	}

	/**
	 Method to ADD Assessments
	 */
	public void assessments(String injuryIndex) throws Exception {

		Hashtable<String, String> data_ABAssessment = ClaimCenterData.getData("ABAssessments", injuryIndex);
		logger.log_Message("Adding Assessments ", MessageType.INFO);

		//ABDetailsTab
		ccRootObject.link("Click", "ABDetailsTab");

		//AddButtonAssessmentDetails
		if ( !data_ABAssessment.get("Tretments").trim().isEmpty() ) {

			//Link to add Body Parts
			String[] tretments = data_ABAssessment.get("Tretments").split("(");
			int treatmentCount = 0;
			for( String tretment : tretments) {
				//Add Body Parts
				ccRootObject.button("Click", "AddButtonTreatmentDetails");
				String[] tretmentFields = tretment.split(")");
				//PlanDate
				ccRootObject.editBoxInTable("Input", ccRootObject.getBrowserDocObject(),
						treatmentCount + ":", "PlanDate", tretmentFields[0].trim());
				//DescriptionLink
				ccRootObject.linkinTable2("Input", ccRootObject.getBrowserDocObject(),
						treatmentCount + ":", "DescriptionLink");
				//Assessment Details
				if(ccRootObject.editBox("Verify", "Date", "").equalsIgnoreCase("PASS")){

					Hashtable<String, String> data_IncidentAssessments = ClaimCenterData.getData("IncidentAssessments", tretmentFields[1].trim());

					//Date
					ccRootObject.editBox("Input", "Date", data_IncidentAssessments.get("EventDate"));
					//Description
					ccRootObject.editBox("Input", "Description", data_IncidentAssessments.get("Description"));
					//ProviderPicker
					ccRootObject.comboBox("Select", "ProviderPicker", data_IncidentAssessments.get("ProviderPicker"));
					//BenefitAssessedmedical
					if((!data_IncidentAssessments.get("BenefitAssessedmedical").trim().isEmpty())){
						ccRootObject.checkBox("Check", "BenefitAssessedmedical");
					}
					//BenefitAssessedrehabilitation
					if((!data_IncidentAssessments.get("BenefitAssessedrehabilitation").trim().isEmpty())){
						ccRootObject.checkBox("Check", "BenefitAssessedrehabilitation");
					}
					//BenefitAssessedhousekeeping
					if((!data_IncidentAssessments.get("BenefitAssessedhousekeeping").trim().isEmpty())){
						ccRootObject.checkBox("Check", "BenefitAssessedhousekeeping");
					}
					//BenefitAssessedincomereplacement
					if((!data_IncidentAssessments.get("BenefitAssessedincomereplacement").trim().isEmpty())){
						ccRootObject.checkBox("Check", "BenefitAssessedincomereplacement");
					}
					//BenefitAssessednonearner
					if((!data_IncidentAssessments.get("BenefitAssessednonearner").trim().isEmpty())){
						ccRootObject.checkBox("Check", "BenefitAssessednonearner");
					}
					//BenefitAssessedcaregiver
					if((!data_IncidentAssessments.get("BenefitAssessedcaregiver").trim().isEmpty())){
						ccRootObject.checkBox("Check", "BenefitAssessedcaregiver");
					}
					//BenefitAssessedattendantcare
					if((!data_IncidentAssessments.get("BenefitAssessedattendantcare").trim().isEmpty())){
						ccRootObject.checkBox("Check", "BenefitAssessedattendantcare");
					}
					//Outcome
					ccRootObject.editBox("Verify", "Outcome", data_IncidentAssessments.get("Outcome"));
					//Update Duplicate
					ccRootObject.link("Click", "Updateduplicate");		

				}
				//ProviderName
				if (ccRootObject.comboBoxinTable("Verify", ccRootObject.getBrowserDocObject(),
						treatmentCount + ":", "ProviderName", tretmentFields[2].trim()).equalsIgnoreCase("PASS")){
					ccRootObject.comboBoxinTable("Select", ccRootObject.getBrowserDocObject(),
							treatmentCount + ":", "ProviderName", tretmentFields[2].trim());
				}
				//Outcome
				ccRootObject.editBoxInTable("Input", ccRootObject.getBrowserDocObject(),
						treatmentCount + ":", "Outcome", tretmentFields[3].trim());				
				treatmentCount++;
			}
		}
	}

	/**
	 * Enters the Towing information for Vehicle Incident
	 * @param wasVehicletowed
	 * @param towCompany
	 * @param towCompanyContactedby
	 * @param insuredRequestedBodyShop
	 * @param towOperatorSuggestedBodyShop
	 * @param dolliesUsed
	 * @param chargeBreakdownandTotalCost
	 * @param insuredInformedofCityLimitsCharge
	 * @param insuredSignAnything
	 * @throws Exception
	 */
	private void addTowing( Boolean wasVehicletowed, String towCompany, String towCompanyContactedby, Boolean insuredRequestedBodyShop,
			Boolean towOperatorSuggestedBodyShop, Boolean dolliesUsed, Boolean chargeBreakdownandTotalCost,
			Boolean insuredInformedofCityLimitsCharge, Boolean insuredSignAnything ) throws Exception {
		//Was Vehicle towed?
		if (wasVehicletowed) {
			ccRootObject.radioButton("Select", "WasVehicletowedYes");
			//Tow Company
			//Repair Shop
			AddressBook ab = new AddressBook(ccRootObject, logger);
			//Repair Shop
			String tokens[] = towCompany.split(" ");
			if ( tokens[0].trim().equalsIgnoreCase("Search") ) {
				ccRootObject.button("Click", "TowCompanyMenuIcon");
				String[] searchMenuItem = new String[] { "Search" };
				ccRootObject.menu("Click", "TowCompanyMenuList", searchMenuItem);
				ab.searchSelectContact(tokens[1].trim());
			}
			else if ( tokens[0].trim().equalsIgnoreCase("New") ) {
				Hashtable<String, String> data_Contact = ClaimCenterData.getData("Contacts", tokens[1].trim());
				ab.clickNewContactMenu(data_Contact, "TowCompanyMenuIcon", "TowCompanyMenuList");
				ab.fillContactFormAndUpdate(data_Contact);
			}

			//Tow Company Contacted by
			ccRootObject.editBox("Select", "TowCompanyContactedby", towCompanyContactedby);
			//Insured Requested Body Shop
			if (insuredRequestedBodyShop) {
				ccRootObject.radioButton("Select", "InsuredRequestedBodyShopYes");
			}
			else { 
				ccRootObject.radioButton("Select", "InsuredRequestedBodyShopNo");
			}
			//Tow Operator Suggested Body Shop
			if (towOperatorSuggestedBodyShop) {
				ccRootObject.radioButton("Select", "TowOperatorSuggestedBodyShopYes");
			}
			else { 
				ccRootObject.radioButton("Select", "TowOperatorSuggestedBodyShopNo");
			}
			//Dollies used
			if (dolliesUsed) {
				ccRootObject.radioButton("Select", "DolliesusedYes");
			}
			else { 
				ccRootObject.radioButton("Select", "DolliesusedNo");
			}
			//Charge Breakdown and Total Cost 
			if (chargeBreakdownandTotalCost) {
				ccRootObject.radioButton("Select", "ChargeBreakdownandTotalCostYes");
			}
			else { 
				ccRootObject.radioButton("Select", "ChargeBreakdownandTotalCostNo");
			}
			//Insured informed of City Limits Charge
			if (insuredInformedofCityLimitsCharge) {
				ccRootObject.radioButton("Select", "InsuredinformedofCityLimitsChargeYes");
			}
			else { 
				ccRootObject.radioButton("Select", "InsuredinformedofCityLimitsChargeNo");
			}
			//Insured sign anything 
			if (insuredSignAnything) {
				ccRootObject.radioButton("Select", "InsuredsignanythingYes");
			}
			else { 
				ccRootObject.radioButton("Select", "InsuredsignanythingNo");
			}
		}
	}

	/**
	 * Adds Inspection details in the incident
	 * @param inspectionRequired
	 *   		- Flag to select the inspection
	 * @param appraiser
	 *   		- Appraiser
	 * @param withoutPrejudice
	 *   		- Without Prejudice 
	 * @throws Exception 
	 */
	private void addInspection( Boolean inspectionRequired, String appraiser, Boolean withoutPrejudice ) throws Exception {

		//Inspection Required?
		if ( inspectionRequired ) {
			ccRootObject.radioButton("Select", "IsInspectionRequiredYes");
			//Appraiser
			//Repair Shop
			AddressBook ab = new AddressBook(ccRootObject, logger);
			//Repair Shop
			String tokens[] = appraiser.split(" ");
			if ( tokens[0].trim().equalsIgnoreCase("Search") ) {
				ccRootObject.button("Click", "AppraiserMenuBtn");
				String[] searchMenuItem = new String[] { tokens[0]};
				ccRootObject.menu("Click", "AppraiserMenuList", searchMenuItem);
				ab.searchSelectContact(tokens[1].trim());
			}
			else if ( tokens[0].trim().equalsIgnoreCase("New") ) {
				Hashtable<String, String> data_Contact = ClaimCenterData.getData("Contacts", tokens[1].trim());
				ab.clickNewContactMenu(data_Contact, "AppraiserMenuBtn", "AppraiserMenuList");
				ab.fillContactFormAndUpdate(data_Contact);
			}

			//Without Prejudice?
			if ( withoutPrejudice ) {
				ccRootObject.radioButton("Select", "PrejudiceIndYes");
			}
			else {
				ccRootObject.radioButton("Select", "PrejudiceIndNo");
			}
		}
	}

	/**
	 * Add Repairs details in the incident
	 * @param deductibleAmount
	 * @param totalLoss
	 * @param actualCashValue
	 * @param dateSettled
	 * @param isSalvage
	 * @param salvageCompany
	 * @param pickupContactDate
	 * @param stockNumber
	 * @param repairShop
	 * @throws Exception
	 */
	private void addRepairs( String deductibleAmount, Boolean totalLoss, String actualCashValue,
			String dateSettled, Boolean isSalvage, String salvageCompany, String pickupContactDate,
			String stockNumber, String repairShop, String lossEstimates) throws Exception {

		//Deductible Amount
		ccRootObject.editBox("Input", "DeductibleAmount", deductibleAmount);
		//Total Loss
		if ( totalLoss ) {
			ccRootObject.radioButton("Select", "TotalLossYes");
		}
		else
		{
			ccRootObject.radioButton("Select", "TotalLossNo");
		}
		//Actual Cash Value
		ccRootObject.editBox("Input", "ActualCashValue", actualCashValue);
		//Date Settled
		ccRootObject.editBox("Input", "DateSettled", dateSettled);
		//Is there salvage?
		if ( isSalvage ) {
			ccRootObject.radioButton("Select", "IsThereSalvageYes");
			//Salvage Company
			ccRootObject.comboBox("Select", "SalvageCompany", salvageCompany);
			//SalvageCompanyMenuIcon
			//Date contacted for pick up
			ccRootObject.editBox("Input", "DateContactedforPickup", pickupContactDate);
			//Stock Number
			ccRootObject.editBox("Input", "StockNumber", stockNumber);			
		}
		else
		{
			ccRootObject.radioButton("Select", "IsThereSalvageNo");
		}

		//Repair Shop
		AddressBook ab = new AddressBook(ccRootObject, logger);
		//Repair Shop
		String tokens[] = repairShop.split(" ");
		if ( tokens[0].trim().equalsIgnoreCase("Search") ) {
			ccRootObject.button("Click", "RepairShopMenuBtn");
			String[] searchMenuItem = new String[] { tokens[0]};
			ccRootObject.menu("Click", "RepairShopMenuList", searchMenuItem);
			ab.searchSelectContact(tokens[1].trim());
		}
		else if ( tokens[0].trim().equalsIgnoreCase("New") ) {
			Hashtable<String, String> data_Contact = ClaimCenterData.getData("Contacts", tokens[1].trim());
			ab.clickNewContactMenu(data_Contact, "RepairShopMenuBtn", "RepaiShopMenuList");
			ab.fillContactFormAndUpdate(data_Contact);
		}
		ccRootObject.editBox("Input", "LossEstimate", lossEstimates); 
	}

	/**
	 * Add the theft and recovery details
	 * @param wasthevehiclestolen
	 * @param vehicleLocked
	 * @param antitheftDevice
	 * @param odometerReading
	 * @param wasvehiclerecovered
	 * @param whenRecovered
	 * @param whereRecovered
	 * @param recoveryProvince
	 * @param recoveredByWhom
	 * @param recoveryCondition
	 * @param policyHold
	 * @throws Exception
	 */
	private void addTheftandRecovery(Boolean wasthevehiclestolen, Boolean vehicleLocked, Boolean antitheftDevice,
			String odometerReading, String wasvehiclerecovered, String whenRecovered, String whereRecovered,
			String recoveryProvince, String recoveredByWhom, String recoveryCondition, Boolean policyHold ) throws Exception {
		//Was the vehicle stolen?
		if (wasthevehiclestolen) {
			ccRootObject.radioButton("Select", "WasthevehiclestolenYes");
			//Vehicle Locked
			if ( vehicleLocked ) {
				ccRootObject.radioButton("Select", "VehicleLockedYes");
			}
			else {
				ccRootObject.radioButton("Select", "VehicleLockedNo");
			}
			//Anti-theft Device  Yes No 
			if ( antitheftDevice ) {
				ccRootObject.radioButton("Select", "AntitheftDeviceYes");
			}
			else {
				ccRootObject.radioButton("Select", "AntitheftDeviceNo");
			}
			//Odometer Reading
			ccRootObject.editBox("Input", "OdometerReading", odometerReading);
			//Theft Recovery 

			//Was vehicle recovered?
			ccRootObject.comboBox("Select", "Wasvehiclerecovered", wasvehiclerecovered);
			if ( wasvehiclerecovered.equalsIgnoreCase("Yes") ) {
				//When?
				ccRootObject.editBox("Input", "WhenRecovered", whenRecovered);
				//Where?
				ccRootObject.editBox("Input", "WhereRecovered", whereRecovered);
				//Province? 
				ccRootObject.comboBox("Select", "RecoveryProvince", recoveryProvince);
				//By Whom?   
				ccRootObject.editBox("Input", "RecoveredByWhom", recoveredByWhom);
				//Recovery Condition 
				ccRootObject.comboBox("Select", "RecoveryCondition", recoveryCondition);
				//Policy Hold? 
				if ( policyHold ) {
					ccRootObject.radioButton("Select", "PolicyHoldYes");
				}
				else {
					ccRootObject.radioButton("Select", "PolicyHoldNo");
				}
			}
		}
	}
}
