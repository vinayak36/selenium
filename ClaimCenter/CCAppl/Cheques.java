package CCAppl;


import java.util.Hashtable;

import resources.CCAppl.ChequesHelper;
import TestData.ClaimCenterData;
import Utilities.Logger;
import Utilities.Logger.MessageType;

import com.rational.test.ft.object.interfaces.TestObject;
import com.rational.test.ft.object.interfaces.TextGuiTestObject;
import com.rational.test.ft.script.Anchor;

/**
 * Automated the Cheques Moddule features
 * @author narasiv,krishnk
 */
public class Cheques extends ChequesHelper
{	
	private UIObjectsAPI ccRootObject;
	private Logger logger;

	/**
	 * Instantiates the Contacts and initiates root Test object and Logger
	 * 
	 * @param rootObject
	 *            - Root test object
	 * @param log
	 *            - Logger instance
	 * @throws Exception 
	 */
	public Cheques(UIObjectsAPI rootObject,Logger log){
		ccRootObject=rootObject;
		logger =log;
	}

	/**
	 * Method to  create  a manual draft
	 * @param chIndex
	 * 				-Data of the check to  be used.
	 * @throws Exception */
	public void createManualDraft(String chIndex) throws Exception{
		logger.log_Message("Creating manual draft", MessageType.INFO);

		//get data
		Hashtable<String, String> data_Cheques = ClaimCenterData.getData(
				"ChecksAndDrafts",chIndex);

		//Actions-> Manual Draft

		String[] manualDraftMenuItemsList = new String[] {"Manual Draft"};

		ccRootObject.button("Click", "ActionsButton");
		ccRootObject.menu("Click", "MenuActionsList", manualDraftMenuItemsList);	

		//Add Payee details
		payeeDetails(data_Cheques.get("Payees"));

		//Click on next
		ccRootObject.button("Click", "NextBtn");

		//Enter payment details
		paymentDetails(data_Cheques.get("Payments"));

		//Click on next
		ccRootObject.button("Click", "NextBtn");

		//Click on finish
		ccRootObject.button("Click", "FinishBtn");

		//check if there  are no error messages
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if(errorMessagesContents.trim().equalsIgnoreCase("TVRAu007 - Reminder: There is a lienholder on the vehicle. Are they named on the payment?")){
				ccRootObject.button("Click","ClearBtn");
			}
			else if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while manual draft '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("Manual Draft created successully", MessageType.INFO);

	}

	/**
	 * Method to verify check Summary
	 * @param chIndex
	 * 				-Data for the check to be used
	 * @throws -Exception
	 * */
	public void verfiyCheckSummary(String chIndex) throws Exception{

		TextGuiTestObject chequeNumberObject=null;

		logger.log_Message("Verifying Check Details", MessageType.INFO);

		//get data
		Hashtable<String, String> data_Cheques = ClaimCenterData.getData(
				"ChecksAndDrafts",chIndex);

		Hashtable<String, String> dataCheques_Payments = ClaimCenterData.getData(
				"Payments", data_Cheques.get("Payments"));

		Hashtable<String, String> dataCheques_Payee = ClaimCenterData.getData(
				"Payees", data_Cheques.get("Payees"));

		//Click on  the financials in claim
		ccRootObject.link("Click","Financials");

		//Click on the cheque link
		ccRootObject.link("Click", "FinancialsCheques");

		//Find Cheque table
		TextGuiTestObject chequesTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				"ClaimChequesTable");

		//Find Cheque  based on cheque number
		if(!dataCheques_Payments.get("CheckNumber").trim().isEmpty()){
			chequeNumberObject =  ccRootObject.findDescendantTestObject(
					chequesTable, ".class", "Html.A", ".text", dataCheques_Payments.get("CheckNumber"));
			if ( chequeNumberObject == null) {
				throw new Exception ("Cheque with cheque number " +  dataCheques_Payments.get("CheckNumber") + " Not found"); 
			}
		}else{
			chequeNumberObject=ccRootObject.findDescendantTestObject(
					chequesTable, ".class", "Html.A", ".text", dataCheques_Payee.get("PayeeName"));
			if ( chequeNumberObject == null) {
				throw new Exception ("Cheque for" +  dataCheques_Payee.get("PayeeName") + " Not found"); 
			}
		}

		//Get the row object with activity
		String rowID = chequeNumberObject.getProperty(".id").toString();
		TextGuiTestObject chequeRow =  ccRootObject.findDescendantTestObject(
				chequesTable, ".class", "Html.A", ".id", rowID.replace("Cheque Number", "4"));

		ccRootObject.editBoxInTable("Verify", chequeRow,null, "chequeAmountlink", dataCheques_Payments.get("Amount"));
	}

	/**
	 * Method to create Checks
	 * @param chIndex
	 * 				-Data to be used to create
	 * @throws -Exception
	 * 
	 * */
	public void createCheques(String chIndex) throws Exception{
		logger.log_Message("Creating Cheque", MessageType.INFO);

		//get data
		Hashtable<String, String> data_Cheques = ClaimCenterData.getData(
				"ChecksAndDrafts",chIndex);

		//Actions-> Cheques

		String[] createChequesMenuItemsList = new String[] {"Cheque"};

		ccRootObject.button("Click", "ActionsButton");
		ccRootObject.menu("Click", "MenuActionsList",createChequesMenuItemsList);

		//Add Payee details
		payeeDetails(data_Cheques.get("Payees"));
		//Click on next
		ccRootObject.button("Click", "NextBtn");

		//Enter payment details
		paymentDetails(data_Cheques.get("Payments"));

		//Click on next
		ccRootObject.button("Click", "NextBtn");


		//Click on finish
		ccRootObject.button("Click", "FinishBtn");

		//check if there  are no error messages
		String errorMessagesContents = ccRootObject.htmlText("Get", "ContentErrorMessage");
		if ( errorMessagesContents != null) {
			if(errorMessagesContents.trim().equalsIgnoreCase("TVRAu007 - Reminder: There is a lienholder on the vehicle. Are they named on the payment?")){
				ccRootObject.button("Click","ClearBtn");
			}
			else if (!errorMessagesContents.trim().isEmpty()) {
				String error = "Errors while creating Cheque '" + errorMessagesContents + "'";
				throw new Exception(error);
			}
		}
		logger.log_Message("Cheque created successully", MessageType.INFO);

	}

	/**
	 * Method to view check details
	 * @param chIndex
	 * 				
	 * @throws Exception */
	public void viewCheckDetails(String chIndex) throws Exception{

		TextGuiTestObject chequeObject=null;

		logger.log_Message("Verifying Check Details", MessageType.INFO);

		//get data
		Hashtable<String, String> data_Cheques = ClaimCenterData.getData(
				"ChecksAndDrafts",chIndex);

		Hashtable<String, String> dataCheques_Payments = ClaimCenterData.getData(
				"Payments", data_Cheques.get("Payments"));

		Hashtable<String, String> dataCheques_Payee = ClaimCenterData.getData(
				"Payees", data_Cheques.get("Payees"));

		//Click on  the financials in claim
		ccRootObject.link("Click","Financials");

		//Click on the cheque link
		ccRootObject.link("Click", "FinancialsCheques");

		//Find Cheque table
		TextGuiTestObject chequesTable = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				"ClaimChequesTable");

		//Find Cheque  based on cheque number or name of Payee
		if(!dataCheques_Payments.get("CheckNumber").trim().isEmpty()){
			chequeObject =  ccRootObject.findDescendantTestObject(
					chequesTable, ".class", "Html.A", ".text", dataCheques_Payments.get("CheckNumber"));
			if ( chequeObject == null) {
				throw new Exception ("Cheque with cheque number " +  dataCheques_Payments.get("CheckNumber") + " Not found"); 
			}
		}else{
			chequeObject=ccRootObject.findDescendantTestObject(
					chequesTable, ".class", "Html.SPAN", ".text", dataCheques_Payee.get("PayeeName"));
			if ( chequeObject == null) {
				throw new Exception ("Cheque for" +  dataCheques_Payee.get("PayeeName") + " Not found"); 
			}
		}

		//Get the row object with activity
		String rowID = chequeObject.getProperty(".id").toString();
		TextGuiTestObject chequeRow =  ccRootObject.findDescendantTestObject(
				chequesTable, ".class", "Html.A", ".id", rowID.replace("Cheque Number", "4"));

		//Click on the check amount
		ccRootObject.linkinTable("Click", chequeRow, "chequeAmountlink");
	}

	/**
	 * Method to create quick Cheques
	 * @throws Exception */
	public void createQuickCheque(String reIndex) throws Exception{

		logger.log_Message("Creating Quick Check", MessageType.INFO);
		//get data from Reserves
		Hashtable<String, String> data_Reserve = ClaimCenterData.getData(
				"Reserves", reIndex);
		//get data from Cheques
		Hashtable<String, String> data_Cheques = ClaimCenterData.getData(
				"ChequesAndDrafts",data_Reserve.get("Cheque"));

		//Click on  the financials in claim
		ccRootObject.link("Click","Financials");

		//Find Summary table
		TextGuiTestObject financialsSummaryTable  = ccRootObject.findExcelMappedTestObject(
				ccRootObject.getBrowserDocObject(),
				"ClaimFinancialsSummryTable");

		Anchor rowAnchor = atDescendant(".class", "Html.TR");
		TestObject[] allExposures = financialsSummaryTable.find(rowAnchor, false);
		boolean exposureFound = false;
		int count = 0;
		for ( TestObject exposureRow : allExposures ) {
			count++;

			if ( ccRootObject.findDescendantTestObject(exposureRow, ".class", "Html.TextNode",
					".text",  data_Reserve.get("Exposure"))
					!= null ) {

				exposureFound = true;

				//get id of current exposure
				System.out.print(exposureRow.getProperty(".id"));

				//Split the exposure and increment the index by 2 and get the Incremented exposure id
				String[]exposureRowSplit= exposureRow.getProperty(".id").toString().split(":");
				int index=Integer.parseInt(exposureRowSplit[4])+2;

				//String conTextMenuid=exposureRowSplit[3]+":"+index+":FinancialsSummaryMenu_helper";
				String conTextMenuid=exposureRowSplit[0]+":"+exposureRowSplit[1]+":"+exposureRowSplit[2]+":"+exposureRowSplit[3]+":"+index+":"+exposureRowSplit[5];

				//Find the Row based on id
				TextGuiTestObject helperRowOne = ccRootObject.findDescendantTestObject(
						financialsSummaryTable, ".class", "Html.TR", ".id", conTextMenuid );
				if ( helperRowOne == null){
					throw new Exception ("ID with .id " + conTextMenuid + " Not found");
				}
				ccRootObject.buttonInTable("Click", helperRowOne, "FinancialsSummaryMenuMenuIcon");

				String[] ClaimFinancialSummaryMenuList = new String[] {
				"Quick Cheque"	};
				ccRootObject.menu("Click", "ClaimFinancialSummaryList",ClaimFinancialSummaryMenuList);		
				break;
			}		
		}
		if(!exposureFound){
			throw new Exception("Exposure " +data_Reserve.get("Exposure")+ " Not found");
		}
		//payee details
		payeeDetails(data_Cheques.get("Payees"));

		//payment details
		paymentDetails(data_Cheques.get("Payments"));

		//Click on next
		if(ccRootObject.button("Verify", "NextBtn").startsWith("PASS")){ 	
			ccRootObject.button("Click", "NextBtn");
		}

		//Cheque Instructions
		ccRootObject.comboBox("Select", "CheckInstructions", data_Cheques.get("CheckInstructions"));

		//Mailing Instructions
		ccRootObject.comboBox("Select", "MailingInstructionsTL", data_Cheques.get("MailingInstruction"));

		//Finish button
		ccRootObject.button("Click", "FinishBtn");

	}

	/**
	 * Method to enter payment details
	 * 
	 * @throws Exception */
	private void paymentDetails(String payment) throws Exception {

		//get data
		Hashtable<String, String> dataCheques_payment = ClaimCenterData.getData(
				"Payments", payment);

		//Select the reserve line
		ccRootObject.comboBox("Select", "ReserveLineExposures", dataCheques_payment.get("ReserveLineExposures"));

		//Secondary kind of loss
		ccRootObject.comboBox("Select", "SecondaryLoss", dataCheques_payment.get("SecondaryLoss"));

		//Swlect the cost type
		ccRootObject.comboBox("Select", "CostType", dataCheques_payment.get("CostType"));

		//Select the cost category
		ccRootObject.comboBox("Select", "CostCategory", dataCheques_payment.get("CostCategory"));

		//Select the PaymentType
		if(dataCheques_payment.get("PaymentType").trim().equalsIgnoreCase("Final")||dataCheques_payment.get("PaymentType").trim().equalsIgnoreCase("Partial")){
			ccRootObject.comboBox("Select", "PaymentType", dataCheques_payment.get("PaymentType"));
		}
		else{
			throw new Exception("Invalid input for PaymentType");
		}
		//Select the Category
		ccRootObject.comboBox("Select", "LineCategory", dataCheques_payment.get("Category"));

		//Enter the amount
		ccRootObject.editBox("Input", "Amount", dataCheques_payment.get("Amount"));



		//Click on nxt button
		//ccRootObject.button("Click", "NextBtn");
	}

	/**
	 * Method to enter payee details.
	 * @throws Exception */
	private void payeeDetails(String payee) throws Exception {
		String regex="(pp|ss|ee|uu|vv|jj|cc|SS|PP|EE|UU|VV|JJ|CC){1}[0-9]{6}";

		//get data
		Hashtable<String, String> dataCheuqes_Payee = ClaimCenterData.getData(
				"Payees", payee);

		//Select the name
		ccRootObject.comboBox("Select", "PayeeName", dataCheuqes_Payee.get("PayeeName"));

		//Select the payee type
		ccRootObject.comboBox("Select", "PayeeType", dataCheuqes_Payee.get("PayeeType"));

		//enter  thePayToTheOrderOf
		ccRootObject.editBox("Input", "PayToTheOrderOf", dataCheuqes_Payee.get("PayToTheOrderOf"));

		//Select the Recipient
		ccRootObject.comboBox("Select", "Recipient", dataCheuqes_Payee.get("Recipient"));

		//enter  Mailing address
		ccRootObject.editBox("Input", "MailingAddress", dataCheuqes_Payee.get("MailingAddress"));

		//enter  City
		ccRootObject.editBox("Input", "City", dataCheuqes_Payee.get("City"));

		//enter  Province
		ccRootObject.editBox("Input", "Province", dataCheuqes_Payee.get("Province"));

		//enter  Postal Code
		ccRootObject.editBox("Input", "PostalCode", dataCheuqes_Payee.get("PostalCode"));

		//Select the issuing party
		//ccRootObject.comboBox("Select", "ChequeIssuer", dataCheuqes_Payee.get("ChequeIssuer"));

		//Select the issuing party
		if (ccRootObject.comboBox("Verify", "ChequeIssuer", dataCheuqes_Payee.get("ChequeIssuer")).startsWith("PASS")){
			ccRootObject.comboBox("Select", "ChequeIssuer", dataCheuqes_Payee.get("ChequeIssuer"));
		}
		//enter  CheckIssueDate
		if (ccRootObject.editBox("Verify", "CheckIssueDate", dataCheuqes_Payee.get("CheckIssueDate")).startsWith("PASS")){
			ccRootObject.editBox("Input", "CheckIssueDate", dataCheuqes_Payee.get("CheckIssueDate"));
		}
		//enter  CheckNumber
		if(!(dataCheuqes_Payee.get("CheckNumber").trim().isEmpty())){
			if(dataCheuqes_Payee.get("CheckNumber").matches(regex)){
				if (ccRootObject.editBox("Verify", "CheckNumber", dataCheuqes_Payee.get("CheckNumber")).startsWith("PASS")){
					ccRootObject.editBox("Input", "CheckNumber", dataCheuqes_Payee.get("CheckNumber"));
				}
			}
			else{
				throw new Exception("Invalid Check number");
			}
		}
	}
}
