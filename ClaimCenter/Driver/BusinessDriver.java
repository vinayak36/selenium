package Driver;

import java.io.FileWriter;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;

import com.ibm.xml.resolver.Catalog;

import resources.Driver.BusinessDriverHelper;
import CCAppl.CCAppl;
import TestData.ClaimCenterData;
import TestData.ClaimCenterTestStep;
import TestData.ClaimCenterTestCase;
import Utilities.CCUtility;
import Utilities.Logger;
import Utilities.Logger.ScreenShotEvent;

/**
 * ClaimCenter test automation main driver script
 * 
 * @since 2012/03/08
 * @author Vinayak Hegde
 */
public class BusinessDriver extends BusinessDriverHelper {
	
	Logger logger;

	/**
	 * Entry point for the test cases to execute. It reads the test cases from
	 * MainTestSuite sheet and executes them sequentially. In case of RQM test
	 * script, the drivers reads the test case name from args to executes and
	 * ignores the business flow
	 * 
	 * @since 2012/03/08
	 */
	public void testMain(Object[] args) {
		try {
			//Get Environment Details from GlobalSettings sheet
			String machineName = CCUtility.getMachineName();
			Hashtable<String, String> globalSettings = ClaimCenterData.getData( "GlobalSettings", "MachineName", machineName);

			if (globalSettings.size() <= 0) {
				throw new Exception(
						"No Environment Details found (in GlobalSettings sheet) for Test Machine "
						+ machineName);
			}
			
			ArrayList<ClaimCenterTestCase> testSuite = null;
			if (args.length > 0) {
				//Get the test case name from arguments (from RQM or Batch Scripts)
				testSuite = new ArrayList<ClaimCenterTestCase>();
				testSuite.add(ClaimCenterData.getTestCasesByID("MainTestSuite", args[0].toString(), globalSettings.get("Index") ));
			} else {
				//Each scenario with ExecuteYN = Y for execution
				testSuite = ClaimCenterData.getTestCasesToExecute("MainTestSuite", globalSettings.get("Index"));
			}

			int tcPassed = 0;
			int tcFailed = 0;
			int tcExecuted = 0;
			if (!testSuite.isEmpty()) {
				// Disable RFT Logging initially
				setCurrentLogFilter(DISABLE_LOGGING);

				// Create Logger Instance
				logger = new Logger( globalSettings.get("LogFileLocation"), globalSettings.get("URL"),
						globalSettings.get("MessageTypesToLog"), globalSettings.get("CaptureScreenShot"));
				// Create Log File
				logger.createLogFile();
				// Log Summary File Header
				logger.summaryHeader();

				// Open Browser
				CCAppl cc = CCAppl.openClaimCenter(globalSettings.get("URL"), logger);

				if (cc != null) {
					logger.log_Message("ClaimCenter Opened: ", Logger.MessageType.INFO);
					// Execute the Test Case
					for (ClaimCenterTestCase testCase : testSuite) {
						if (tcExecuted == 0) {
							logger.log_Message(
									"-------------------------------------------------------------------------",
									Logger.MessageType.NONE);
						}

						logger.setTCId(testCase.testCaseID());
						cc.setTestCase(testCase);
						
						logger.log_Message(
								"Starting Test Case: " + testCase.testCaseID(),
								Logger.MessageType.INFO);
						logger.captureScreen(ScreenShotEvent.ScreenChange);

						Boolean result = executeTestCase(cc, testCase);
						if (result == true) {
							tcPassed++;
							logger.log_Message("Finished Test Case " + testCase.testCaseID()
									+ " Result: PASSED", Logger.MessageType.PASS);
						} else {
							tcFailed++;
							logger.log_Message(
									"Finished Test Case " + testCase.testCaseID()
									+ " Result: FAILED", Logger.MessageType.FAIL);
						}
						tcExecuted++;
						logger.log_Message(
								"-------------------------------------------------------------------------",
								Logger.MessageType.NONE);
					}
					// Close Browser
					CCAppl.closeClaimCenter(cc);
				} else {
					logger.log_Message("Unable to Open ClaimCenter: ",
							Logger.MessageType.ERROR);
				}
				// Log Summary Header
				logger.summary_Footer(tcPassed, tcFailed, tcExecuted);
			}
		} catch (Exception e) {
			// Print the exception message for analysis and debugging
			e.printStackTrace();
		}
	}

	/**
	 * Executes all the test steps specified for the test case
	 * 
	 * @param cc
	 *            - CCAppl instance which has all the Test steps implemented
	 * @param testCase
	 *            - Test Case instance which has all steps to execute
	 * @since 2012/03/08
	 */
	private Boolean executeTestCase(CCAppl cc, ClaimCenterTestCase testCase) {

		Boolean tcResult = false;
		for (ClaimCenterTestStep steps : testCase.getTestSteps()) {
			//Execute the scenario
			tcResult = executeTestStep(cc, testCase.testCaseID(), steps);
			//Exit if the test step fails
			if ( tcResult == false ) {
				break;
			}
		}
		return tcResult;
	}

	/**
	 * Executes the a single test step. It dynamically executes the
	 * method, based on Test Step.
	 * 
	 * @param cc
	 *            - CCAppl instance which has all the Test steps implemented
	 * @param tcID
	 *            - Test Case ID
	 * @param testStep
	 *            - Test Step instance to execute
	 * @since 2012/03/08
	 */
	private Boolean executeTestStep(CCAppl cc, String tcID, ClaimCenterTestStep testStep) {

		Boolean result = false;
		try {
			Method keywordMethod = getTestStepMethod(CCAppl.class, testStep.testStep());
			
			if( keywordMethod == null ) {
				throw new NullPointerException("Could not find the implemented Keyword for TestStep " + testStep.testStep());
			}
			
			ArrayList<String> arguments = new ArrayList<String>();
			arguments.add(testStep.testStepType());
			arguments.add(testStep.dataIndex());
			arguments.add(testStep.expectedMessage());
			arguments.trimToSize();
			keywordMethod.setAccessible(true);
			String stepName = (testStep.testStep() != null) ? testStep.testStep() : "";
			String testStepTypeName = (testStep.testStepType() != null) ? testStep.testStepType() : "";
			String dataIndex = (testStep.dataIndex() != null) ? testStep.dataIndex() : "";
			logger.log_Message("Starting Test Step " + stepName + " " + testStepTypeName + " " + dataIndex, Logger.MessageType.INFO);
			if ( testStep.description() != null ) {
				logger.log_Message("Step Description : " + testStep.description(), Logger.MessageType.INFO);
			}
			//Execute the test step (Keyword) by invoking dynamically
			result = (Boolean) keywordMethod.invoke(cc, arguments.toArray());
			if (result) {
				logger.log_Message(stepName + " " + testStepTypeName + " " + dataIndex, Logger.MessageType.PASS);
			} else {
				logger.log_Message(stepName + " " + testStepTypeName + " " + dataIndex, Logger.MessageType.FAIL);
			}
		} catch (java.lang.IllegalAccessException e) {
			logExceptionMessage(tcID, cc.getClass().getName(),
					testStep.testStep(), testStep.dataIndex(), e);
		} catch (IllegalArgumentException e) {
			logExceptionMessage(tcID, cc.getClass().getName(),
					testStep.testStep(), testStep.dataIndex(), e);
		} catch (InvocationTargetException e) {
			logExceptionMessage(tcID, cc.getClass().getName(),
					testStep.testStep(), testStep.dataIndex(),
					e.getTargetException());
		} catch (NullPointerException e) {
			logExceptionMessage(tcID, cc.getClass().getName(),
					testStep.testStep(), testStep.dataIndex(), e);
		}

		return result;
	}

	/**
	 * Logs the exception message in summary report for analysis and debugging
	 * 
	 * @param tcID
	 *            - Test Case ID
	 * @param keyWordClass
	 *            - Keyword name
	 * @param testStep
	 *            - Test Step Type name
	 * @param dataIndex
	 *            - Test data index used for execution of the step
	 * @param exception
	 *            - Exception caught
	 * @since 2012/03/08
	 */
	private void logExceptionMessage(String tcID, String keyWordClass,
			String testStep, String dataIndex, Throwable exception) {
		
		StackTraceElement[] st = exception.getStackTrace();
		String stack = "";
		for (StackTraceElement ste : st) {
			stack += " at Method " + ste.getMethodName() + " on line# "
					+ ste.getLineNumber() + " in file#" + ste.getFileName();
			if (ste.getClassName().equalsIgnoreCase(keyWordClass)
				|| ste.getMethodName().equalsIgnoreCase("testMain")) {
				// Do not get the stack beyond keyword class
				break;
			}
		}

		logger.log_Message(exception.getMessage(), Logger.MessageType.ERROR);
		logger.log_Message(stack, Logger.MessageType.ERROR);
		String logMessage = tcID + "\t\t" + testStep + "\t\t" + dataIndex;
		logger.log_Message(logMessage, Logger.MessageType.FAIL);
	}

	/**
	 * Gets the Test Step method from specified call where it is implemented.
	 * The Test Step is keyword method which has an annotation "Keyword"
	 * 
	 * @param testSuiteClass
	 *            - The class where keyword method is defined
	 * @param stepName
	 *            - Name of the method to return
	 * @since 2012/03/08
	 */
	private Method getTestStepMethod(Class<?> testSuiteClass,
			String stepName) {
		Method keyWordMethod = null;
		for (Method ccMethod : testSuiteClass.getDeclaredMethods()) {
			if (ccMethod.isAnnotationPresent(CCAppl.TCKeyword.class)) {
				CCAppl.TCKeyword annotation = ccMethod.getAnnotation(CCAppl.TCKeyword.class);
				if (stepName.equalsIgnoreCase(annotation.Name())) {
					//Keyword Method Found
					keyWordMethod = ccMethod;
					break;
				}
			}
		}
		return keyWordMethod;
	}
}
