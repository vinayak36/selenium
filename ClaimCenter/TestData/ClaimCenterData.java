package TestData;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.io.*;

import Utilities.CCUtility;

import resources.TestData.ClaimCenterDataHelper;

/**
 * Implements the methods to read the data from excel files
 * 
 * @since 2012/03/08
 * @author Prashant Deogade
 */
public class ClaimCenterData extends ClaimCenterDataHelper {
	static private Connection conn = null;
	static private Statement stmnt = null;
	static private String dataFileName = "C:\\CC7.0 scripts\\Functional_Flow.xls";
	static private String saveExcelVBSScript = "R:\\Test Automation Framework\\CC\\CC7.0\\TestCases\\SaveExcelFile.vbs";
	//static private String dataFileName = "C:\\Documents and Settings\\deogadp\\Desktop\\Functional_Flow.xls";
	//static private String saveExcelVBSScript = "R:\\Test Automation Framework\\CC\\CC7.0\\TestCases\\SaveExcelFile.vbs";
	static private String connString = "Driver={Microsoft Excel Driver (*.xls)};DBQ=" + dataFileName + ";ReadOnly=0";

	/**
	 * Read the row from the specified spreadsheet(table) and specified row
	 * index
	 * 
	 * @param tableName
	 *            - Sheet name
	 * @param indexVal
	 *            - Index value from the Index Column
	 * @return Collection of the Values in the row
	 * @since 2012/03/08
	 */
	static public Hashtable<String, String> getData(String tableName, String indexVal) {
		return getData(tableName, "Index", indexVal);
	}

	/**
	 * Read the row from the specified spreadsheet(table) and specified row
	 * index
	 * 
	 * @param tableName
	 *            - Sheet name
	 * @param indexCol
	 *            - Column name
	 * @param indexVal
	 *            - value of the indexcol
	 * @return Collection of the Values in the row
	 * @since 2012/03/08
	 */
	static public Hashtable<String, String> getData(String tableName,
			String indexCol, String indexVal) {
		Hashtable<String, String> fnolData = new Hashtable<String, String>();
		try {
			// Connection
			conn = DriverManager.getConnection("jdbc:odbc:" + connString, "",
					"");
			stmnt = conn.createStatement();

			// Query
			String query = "select * from [" + tableName + "$] where " + indexCol + "='" + indexVal + "'";
			ResultSet rs = stmnt.executeQuery(query);
			ResultSetMetaData metadata = rs.getMetaData();

			while (rs.next()) {
				for (int colIdx = 1; colIdx <= metadata.getColumnCount(); colIdx++) {
					// store the the values in HashTable
					String columnName = metadata.getColumnLabel(colIdx);
					String value = rs.getString(columnName);
					//If the data is number and ends with ".0" get the integer part only
					if (!columnName.trim().isEmpty()) {
						if (value != null) {
							if ( metadata.getColumnTypeName(colIdx).equalsIgnoreCase("NUMBER") && value.endsWith(".0")) {
								value = value.substring(0, value.length()-2);
							}
							fnolData.put(columnName, value);
						} else {
							fnolData.put(columnName, "");
						}
					}
				}
			}
			rs.close();
			stmnt.close();
			conn.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				stmnt.close();
				conn.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return fnolData;
	}

	/**
	 * Saves the data into excel file at the specified field of the specified
	 * sheet name and specified row
	 * 
	 * @param tableName
	 *            - Sheet name
	 * @param indexVal
	 *            - Index value from the Index Column
	 * @param fieldCol
	 *            - field Column name
	 * @param fieldValue
	 *            - field Value to save
	 * @since 2012/03/08
	 */
	static public void saveData(String tableName, String indexVal, String fieldCol, String fieldValue) {
		saveData(tableName, "Index", indexVal,
				new String[] { fieldCol }, new String[] { fieldValue });
	}
	
	/**
	 * Modifies the Test case by entering specified data in specified fieldName 
	 * @param tcID
	 *            - Test Case ID
	 * @param filterColumn
	 *            - Additional Column name to filter the records
	 * @param filderValue
	 *            - Value of the additional Column to filter the records 
	 * @param fieldName
	 *             - Field Name to Update
	 * @param fieldValue
	 *             - Value to save
	 */
	static public void ModifyTestCase(String tcID, String filterColumn, String filderValue, String fieldName, String fieldValue) {
		try {
			String tableName = "MainTestSuite";
			String indexCol = "TestCaseID";
			// Connection
			conn = DriverManager.getConnection("jdbc:odbc:" + connString, "",
					"");
			stmnt = conn.createStatement();

			// Query
			String query = "UPDATE [" + tableName + "$] SET ";
			// Field and Values
			query += fieldName + "='" + fieldValue + "'";
			// Where testCase
			query += " WHERE " + indexCol + "='" + tcID + "'";
			
			if ( !filterColumn.isEmpty() ) {
				query += " AND " + filterColumn + "='" + filderValue + "'";
			}
			stmnt.executeUpdate(query);

			stmnt.close();
			conn.close();
			
			//Open and Close the excel to shrink the Excel file Size
			String commandToSaveExcel = "cscript \"" + saveExcelVBSScript + "\" \"" + dataFileName + "\"";
			Runtime.getRuntime().exec(commandToSaveExcel).waitFor();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				stmnt.close();
				conn.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Saves the data into excel file at the specified field of the specified
	 * sheet name and specified row
	 * 
	 * @param tableName
	 *            - Sheet name
	 * @param indexCol
	 *            - Index column name
	 * @param indexVal
	 *            - Index value from the indexCol
	 * @param fieldCol
	 *            - field Column name
	 * @param fieldValue
	 *            - field Value to save
	 * @since 2012/03/08
	 */
	static public void saveData(String tableName,
			String indexCol, String indexVal, String[] fieldCol,
			String[] fieldValue) {

		try {

			if (fieldCol.length != fieldValue.length) {
				throw new Exception("Mismatch in Fields and Values");
			}

			// Connection
			conn = DriverManager.getConnection("jdbc:odbc:" + connString, "",
					"");
			stmnt = conn.createStatement();

			// Query
			// where " +indexCol +"='" + indexVal+"'";
			String query = "UPDATE [" + tableName + "$] SET ";
			// Column Values
			for (int colIdx = 0; colIdx < fieldCol.length; colIdx++) {
				query += fieldCol[colIdx] + "='" + fieldValue[colIdx] + "'";
				if (colIdx + 1 < fieldCol.length) {
					query += ",";
				}
			}
			query += " WHERE " + indexCol + "='" + indexVal + "'";
			stmnt.executeUpdate(query);

			stmnt.close();
			conn.close();
			
			//Open and Close the excel to shrink the Excel file Size
			String commandToSaveExcel = "cscript " + saveExcelVBSScript + " \"" + dataFileName + "\"";
			Runtime.getRuntime().exec(commandToSaveExcel);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				stmnt.close();
				conn.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Collects all the test cases from the MainTestSuite sheet which are marked
	 * as "Y" to execute
	 * 
	 * @param tableName
	 *            - Sheet name like "MainTestSuite"
	 * @param globalSetting
	 *            - Environment record from GlobalSettings Sheet
	 * @return Collection of the test cases to execute
	 * @since 2012/03/08
	 */
	static public ArrayList<ClaimCenterTestCase> getTestCasesToExecute(
			String tableName, String globalSetting) {
		ArrayList<ClaimCenterTestCase> testCases = new ArrayList<ClaimCenterTestCase>();
		try {
			// Connection
			conn = DriverManager.getConnection("jdbc:odbc:" + connString, "",
					"");
			stmnt = conn.createStatement();

			// Query
			String query = "select * from [" + tableName + "$] where " + globalSetting + "_ExecuteYN LIKE 'Y%'";
			//String query = "select * from [" + tableName + "$]";
			ResultSet rs = stmnt.executeQuery(query);
			String currentTC = "";
			while (rs.next()) {
				String tcID = rs.getString("TestCaseID");
				String description = rs.getString("Description");
				String step = rs.getString("TestStep");
				String testStepType = rs.getString("TestStepType");
				String expectedMessage = rs.getString("ExpectedMessage");
				String dataIdx = rs.getString(globalSetting + "_Data");

				ClaimCenterTestStep ccStep = null;
				ccStep = new ClaimCenterTestStep(
						step, testStepType, description, true, dataIdx,
						expectedMessage, globalSetting);

				if (!testCases.isEmpty()) {
					if (currentTC.equalsIgnoreCase(tcID)
							|| tcID.trim().isEmpty()) {
						ClaimCenterTestCase testCase = getTestCaseFromList(
								testCases, tcID);
						testCase.addTestStep(ccStep);
					}
				}
				if (!currentTC.equalsIgnoreCase(tcID)) {
					currentTC = tcID;
					ClaimCenterTestCase newTC = new ClaimCenterTestCase(
							(tcID != null) ? tcID : "");
					newTC.addTestStep(ccStep);
					testCases.add(newTC);
				}
			}
			testCases.trimToSize();
			rs.close();
			stmnt.close();
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				stmnt.close();
				conn.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return testCases;
	}

	/**
	 * Gets the test case with specified ID from the specified list
	 * 
	 * @param testCases
	 *            - List of test cases
	 * @param tcID
	 *            - test case ID to find
	 * @return Test case found else null
	 * @since 2012/03/08
	 */
	private static ClaimCenterTestCase getTestCaseFromList(
			ArrayList<ClaimCenterTestCase> testCases, String tcID) {

		ClaimCenterTestCase tcFound = null;
		for (ClaimCenterTestCase testCase : testCases) {
			if (testCase.testCaseID().equalsIgnoreCase(tcID)) {
				tcFound = testCase;
				break;
			}
		}
		return tcFound;
	}

	/**
	 * Read the test case with specified Test case ID
	 * 
	 * @param tableName
	 *            - Sheet name like "MainTestSuite"
	 * @param tcID
	 *            - Test Case ID
	 * @param globalSetting
	 *            - Entity from GlobalSettings sheet
	 * @return The test case
	 * @since 2012/03/08
	 */
	static public ClaimCenterTestCase getTestCasesByID(String tableName, String tcID, String globalSetting) {
		ClaimCenterTestCase testCase = null;
		try {
			// Connection
			conn = DriverManager.getConnection("jdbc:odbc:" + connString, "", "");
			stmnt = conn.createStatement();

			//Query
			String query = "select * from [" + tableName + "$] where TestCaseID = '" + tcID + "'";
			ResultSet rs = stmnt.executeQuery(query);

			testCase = new ClaimCenterTestCase(tcID);
			while (rs.next()) {
				String description = rs.getString("Description");
				String step = rs.getString("TestStep");
				String testStepType = rs.getString("TestStepType");
				String expectedMessage = rs.getString("ExpectedMessage");
				String dataIdx = rs.getString(globalSetting + "_Data");

				ClaimCenterTestStep ccStep = null;
				ccStep = new ClaimCenterTestStep(
						step, testStepType, description, true, dataIdx,
						expectedMessage, globalSetting);
				
				testCase.addTestStep(ccStep);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				stmnt.close();
				conn.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return testCase;
	}
}
