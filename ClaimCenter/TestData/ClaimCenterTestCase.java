package TestData;

import java.util.ArrayList;

/**
 * ClaimCenter Test Case. Test Case contains, testCaseID and collection of Test
 * Steps
 * 
 * @since 2012/03/08
 * @author Prashant Deogade
 */
public class ClaimCenterTestCase {
	private String tcID;
	private ArrayList<ClaimCenterTestStep> steps;

	/**
	 * Instantiates the Test Case with specified test case ID
	 * 
	 * @param testcaseID
	 *            - Test Case ID
	 * @since 2012/03/08
	 */
	public ClaimCenterTestCase(String testcaseID) {
		tcID = testcaseID;
		steps = new ArrayList<ClaimCenterTestStep>();
	}

	/**
	 * @return Test Case ID
	 * @since 2012/03/08
	 */
	public String testCaseID() {
		return tcID;
	}

	/**
	 * Adds the Test Step to the Test Case
	 * 
	 * @param testStep
	 *            - Test Action
	 * @since 2012/03/08
	 */
	public void addTestStep(ClaimCenterTestStep testStep) {
		steps.add(testStep);
	}

	/**
	 * @return Collection of the test Steps
	 * @since 2012/03/08
	 */
	public ArrayList<ClaimCenterTestStep> getTestSteps() {
		return steps;
	}
	
	/**
	 * Saves the ClaimNo in all the rows of the test case
	 * @param newClaimNo
	 * 			- Claim No to save
	 */
	public void SaveNewClaimNo(String newClaimNo) {
		
		//Save the new Claim in test case to use
		for ( ClaimCenterTestStep step : steps) {
			if ( step.testStep().equalsIgnoreCase("OpenClaim")) {
				//Set Claim Number
				step.setClaimNumber(newClaimNo);
				//Write the claim number is spreadsheet
				ClaimCenterData.ModifyTestCase(
						tcID,
						"TestStep",
						"OpenClaim",
						steps.get(0).globalSetting().trim() + "_Data",
						newClaimNo);
				break;
			}
		}
	}
}
