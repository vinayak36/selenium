package TestData;

import resources.TestData.ClaimCenterTestStepHelper;

/**
 * ClaimCenter Test Step. Test Step contains, Name,
 * test case Type,description, claimNumber, ExecutionFlag, dataIndex to use
 * for input data, expectedMsg at the end of execution
 * 
 * @since 2012/03/08
 * @author Prashant Deogade
 */
public class ClaimCenterTestStep extends ClaimCenterTestStepHelper {
	private String stepName;
	private String desc;
	private String testStepTypeName;
	private String data;
	private String expectedMsg;
	private Boolean exeYN;
	private String gSetting;

	/**
	 * Instantiates the object with specified parameters
	 * 
	 * @param step
	 *            - Step Name
	 * @param testStepType
	 *            - Step Type
	 * @param description
	 *            - Description
	 * @param executeYN
	 *            - Execution Flag
	 * @param dataIdx
	 *            - Data row index for input data to use
	 * @param expectedMessage
	 *            - Expected message to verify after execution
	 * @since 2012/03/08
	 */
	public ClaimCenterTestStep(String step, String testStepType,
			String description, Boolean executeYN, String dataIdx,
			String expectedMessage, String globalSetting) {
		desc = description;
		stepName = step;
		exeYN = executeYN;
		testStepTypeName = testStepType;
		data = dataIdx;
		expectedMsg = expectedMessage;
		gSetting = globalSetting;
		
	}

	/**
	 * @return Name of the test Step
	 * @since 2012/03/08
	 */
	public String testStep() {
		return stepName;
	}

	/**
	 * @return Sub Step type name
	 * @since 2012/03/08
	 */
	public String testStepType() {
		return testStepTypeName;
	}

	/**
	 * @return Description of the test step
	 * @since 2012/03/08
	 */
	public String description() {
		return desc;
	}

	/**
	 * Sets the claim no for the test case
	 * @since 2012/03/08
	 */
	public void setClaimNumber(String claimNo) {
		data = claimNo;
	}

	/**
	 * @return executeYN, to execute yes else no
	 * @since 2012/03/08
	 */
	public Boolean executeYN() {
		return exeYN;
	}

	/**
	 * @return Row Index used for input data
	 * @since 2012/03/08
	 */
	public String dataIndex() {
		return data;
	}

	/**
	 * @return Expected Message to verify
	 * @since 2012/03/08
	 */
	public String expectedMessage() {
		return expectedMsg;
	}
	
	/**
	 * @return GlobalSetting for the execution
	 * @since 2012/03/08
	 */
	public String globalSetting() {
		return gSetting;
	}
}
